//
//  SubTotalTableCell.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 22/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class SubTotalTableCell: UITableViewCell {

    @IBOutlet var totalMnyLbl: UILabel!
    @IBOutlet var planNameLbl: UILabel!
    @IBOutlet var amountLbl: UILabel!
    @IBOutlet var discountLbl: UILabel!    
    @IBOutlet var totalView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        totalView.layer.cornerRadius = 5
        totalView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        totalView.layer.shadowOpacity = 1.0
        totalView.layer.shadowRadius = 3
        totalView.layer.masksToBounds = false
        totalView.layer.shadowColor = UIColor.gray.cgColor

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
