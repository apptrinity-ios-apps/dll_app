//
//  PlanTypeTableCell.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 22/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class PlanTypeTableCell: UITableViewCell {
    
    @IBOutlet var paymentView: UIView!
    @IBOutlet var cashView: UIView!
    @IBOutlet var cashBtn: UIButton!
    @IBOutlet var cashImg: UIImageView!
    @IBOutlet var onlineView: UIView!
    @IBOutlet var onlineBtn: UIButton!    
    @IBOutlet var onlineImg: UIImageView!
    
    @IBOutlet var cardDetailsView: UIView!    
    @IBOutlet var giftCardLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        paymentView.layer.cornerRadius = 5
        paymentView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        paymentView.layer.shadowOpacity = 1.0
        paymentView.layer.shadowRadius = 3
        paymentView.layer.masksToBounds = false
        paymentView.layer.shadowColor = UIColor.gray.cgColor
        paymentView.layer.borderWidth = 1
        paymentView.layer.borderColor = UIColor.lightGray.cgColor
               
        //cashBtn.layer.cornerRadius = 5
        onlineBtn.layer.cornerRadius = 5
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
