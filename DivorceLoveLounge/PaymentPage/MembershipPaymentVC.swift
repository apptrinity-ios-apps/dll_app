//
//  MembershipPaymentVC.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 22/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
import StoreKit
//import Stripe
class MembershipPaymentVC: UIViewController,UITableViewDelegate,UITableViewDataSource,PaymentData,UITextFieldDelegate,skpaymentStatusDelegate{
    func skpaymentStatus(status: String, tranactionId: String) {
        SkPayment_TransactionID = tranactionId
        if(status == "purchased"){
            makePaymentAPI()
        }else if (status == "failed"){
          //  self.themes.showAlert(title: "Alert", message: "Your transactions is failed", sender: self)
        }
        else if (status == "restored"){
            makePaymentAPI()
        }else if (status == "deferred"){
            self.themes.showAlert(title: "Alert", message: "Your transactions is deferred", sender: self)
        }
    }
// Delegates Methods
//UITableViewDelegate,UITableViewDataSource,STPPaymentCardTextFieldDelegate,PaymentData,UITextFieldDelegate {
    var SkPayment_TransactionID = ""
    var window: UIWindow?
    var cashOrOnlineIndex = Int()
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    var collectionSelectedRow = Int()
    var discount = ""
    var planeamount = ""
    var monthplan = ""
    var dataDict = [String:AnyObject]()
    var planId = ""
    var plan_duration = ""
    var selectedPlan = String()
   //  var stripeToken:STPToken!
     var monthsArray = [AnyObject]()
    // var paymentTextField = STPPaymentCardTextField()
    var promocodeStr = ""
    var SelectedInt = Int()
    var offerdata = [String:AnyObject]()
    var prmoSelected = ""
    var paymentSelection = ""
    var finlaamount = Double()
    var promocodeOfferText = ""
    var iapService = IAPService()
    var selectedROW = Int()
    var adsListArr = [AnyObject]()
    
    @IBOutlet var paymentTableView: UITableView!
    @IBAction func backAction(_ sender: Any) {

        self.navigationController?.popViewController(animated: true)
    }

    @IBOutlet var makePaymentBtn: UIButton!

    override func viewDidLoad() {

        super.viewDidLoad()

        iapService.getProducts()
       //IAPService.shared.getProducts()
        
        cashOrOnlineIndex = 1

       paymentSelection = "Online"

        paymentTableView.delegate = self
        paymentTableView.dataSource = self
        paymentTableView.separatorColor = .clear
        makePaymentBtn.layer.cornerRadius = makePaymentBtn.frame.size.height/2

        self.paymentTableView.register(UINib(nibName: "SubTotalTableCell", bundle: nil), forCellReuseIdentifier: "SubTotalTableCell")
        self.paymentTableView.register(UINib(nibName: "PlanDurationTableCell", bundle: nil), forCellReuseIdentifier: "PlanDurationTableCell")
        self.paymentTableView.register(UINib(nibName: "PlanTypeTableCell", bundle: nil), forCellReuseIdentifier: "PlanTypeTableCell")


        NotificationCenter.default.addObserver(self, selector: #selector(MembershipPaymentVC.keyboardUp), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MembershipPaymentVC.keyboardDown), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

//        let frame1 = CGRect(x: 0, y: 0, width: self.view.frame.width - 40 , height:60 )
//        paymentTextField = STPPaymentCardTextField(frame: frame1)
//        paymentTextField.layer.cornerRadius = 5
//        paymentTextField.layer.borderColor = ligtGray.cgColor
//        paymentTextField.backgroundColor = UIColor.white
//        paymentTextField.delegate = self

        if dataDict.isEmpty {

        }else{

            if self.themes.getplanStatus() == ""{
              
                
                
                
                
                let packageType = dataDict["package_type"] as! String
                
                if(packageType != "test"){
                     let dict = ["month" : "Free", "amount" : "0", "discount" : "0", "duration_id" : "one", "membership_id" : "1"]
                                  let planAmnt = dataDict["plan_amount"] as! String
                                  let discAmnt = dataDict["discount_amount"] as! String

                                  
                                  
                                   let dict2 = ["month" : "1", "amount" : planAmnt, "discount" : discAmnt, "duration_id" : "one", "membership_id" : "1"]
                                  monthsArray.append(dict as AnyObject)
                                   monthsArray.append(dict2 as AnyObject)
                                  selectedPlan = "Free"
                    
                }else if(packageType == "test"){
                    selectedPlan = "Test"
                    
                }
              
                
            } else {
                
                let packageType = dataDict["package_type"] as! String
                               
                               if(packageType != "test"){
                                let planAmnt = dataDict["plan_amount"] as! String
                                               let discAmnt = dataDict["discount_amount"] as! String
                                               let dict2 = ["month" : "1", "amount" : planAmnt, "discount" : discAmnt, "duration_id" : "one", "membership_id" : "1"]
                                               selectedPlan = "1"
                                               monthsArray.append(dict2 as AnyObject)
                }else if(packageType == "test"){
                    selectedPlan = "Test"
                    
                }
                
            }
            let duration = dataDict["durations"] as! AnyObject
            for i in 0..<duration.count {
                let dict = duration.object(at: i)
                monthsArray.append(dict as AnyObject)
            }
        }

        // Do any additional setup after loading the view.
        
        
        adsListServiceApi(ScreenType: "Membership")
        
    }

    func adsListServiceApi(ScreenType:String){
        self.urlService.adslistServiceCall(ScreenType: ScreenType) { response in
            let success = response["status"] as! String
            if(success == "1"){
                let data = response["data"] as! [AnyObject]
                self.adsListArr = data
              
                self.paymentTableView.reloadData()
                
            }else{
                
            }
        }
    }
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {


        self.resignFirstResponder()
       // paymentTextField.resignFirstResponder()

    }


    @objc func keyboardUp(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            //
            self.view.frame.origin.y = 100
            if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardRectangle = keyboardFrame.cgRectValue
                let keyboardHeight = keyboardRectangle.height
                self.view.frame.origin.y -= keyboardHeight
            }
        }
    }
    @objc func keyboardDown(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            self.view.frame.origin.y = 0
        }
    }



    @IBAction func makePaymentAction(_ sender: Any) {

        if paymentSelection == ""{
            self.themes.showAlert(title: "Alert", message: "Please Select PaymentType", sender: self)
        }else if plan_duration == ""{
            self.themes.showAlert(title: "Alert", message: "Please Select Plan Duration", sender: self)
        }else{

            if paymentSelection == "Cash"{
                makePaymentAPI()
            }else  if paymentSelection == "Online"{
                if(selectedPlan == "Free" || selectedPlan == "1"){
                   // makePaymentAPI()
                    iapService.delagate = self
                    iapService.purchase(product: .nonRenewingOneMonthSubcription)
                }else if selectedPlan == "3"{
                    iapService.delagate = self
                    iapService.purchase(product: .nonRenewing3MonthSubcription)
                }else if selectedPlan == "12"{
                    iapService.delagate = self
                    iapService.purchase(product: .nonRenewing12MonthSubcription)
                }else if selectedPlan == "Test"{
                    iapService.delagate = self
                    iapService.purchase(product: .nonRenewingTestSubcription)
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubTotalTableCell", for: indexPath) as! SubTotalTableCell
            cell.selectionStyle = .none
            let obj = monthsArray[collectionSelectedRow]
            let planamount = obj["amount"] as! String
            let discoun = obj["discount"] as! String
           // let amount = String(format: "%.2f", amount1)
            let amount = Double(planamount)
            let disamount = Double(discoun)
            let discou = disamount!/100
            let n = discou*amount!
            let totalamount = amount!-n
            let finlaamount = Double(totalamount)

            var amount2 = String()


            if(selectedPlan == "1"){
                amount2 = String(format: "%.2f", finlaamount)
            }else{
                amount2 = "\(finlaamount)"
            }

            cell.amountLbl.text = "$ " + amount2
            cell.planNameLbl.text = (dataDict["package_type"] as! String)
            cell.discountLbl.text = obj["discount"] as? String
            cell.totalMnyLbl.text = " $ " + amount2
             planId = dataDict["id"] as! String
            return cell

        } else if indexPath.row == 1 {

            let cell = tableView.dequeueReusableCell(withIdentifier: "PlanDurationTableCell", for: indexPath) as! PlanDurationTableCell
                cell.selectionStyle = .none
            cell.promoCodeField.delegate = self
            if prmoSelected == "1"{
                cell.codeLabel.text = self.promocodeOfferText
            }else{
                cell.codeLabel.text = ""
            }
            if(selectedPlan == "Free"){
                cell.promoCodeField.isHidden = true
                cell.promocodeView.isHidden = true
                cell.codeLabel.isHidden = true
                cell.promoCodeTitleLabel.isHidden = true
                cell.applyBtn.isHidden = true
            }else{
                 cell.applyBtn.isHidden = false
                cell.promoCodeField.isHidden = false
                cell.promocodeView.isHidden = false
                cell.codeLabel.isHidden = false
                cell.promoCodeTitleLabel.isHidden = false
            }
            cell.applyBtn.addTarget(self, action: #selector(applyBtnClicked(button:)), for:.touchUpInside)
            cell.promoCodeField.text = promocodeStr
            cell.monthsArr = monthsArray
            cell.delegate = self
            
            
            cell.adsListArr = self.adsListArr
            cell.adsCollectionView.reloadData()
            if self.adsListArr.count > 0 {
                cell.adsHeightConstant.constant = 120
                if self.adsListArr.count > 1 {
                        cell.setTimer()
                                      }
            }else{
                cell.adsHeightConstant.constant = 0
            }
            cell.durationCollectionView.reloadData()
                return cell
        } else {

            if(selectedPlan != "Free"){
                let cell = tableView.dequeueReusableCell(withIdentifier: "PlanTypeTableCell", for: indexPath) as! PlanTypeTableCell
                cell.selectionStyle = .none

                if cashOrOnlineIndex == 0{
                    cell.giftCardLbl.isHidden = true
                    cell.cardDetailsView.isHidden = true
                    cell.cashView.layer.cornerRadius = 5
                    cell.cashView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
                    cell.cashView.layer.shadowOpacity = 1.0
                    cell.cashView.layer.shadowRadius = 3
                    cell.cashView.layer.masksToBounds = false
                    cell.cashView.layer.shadowColor = UIColor.gray.cgColor
                    cell.cashView.backgroundColor = MonthYellow
                    cell.onlineView.layer.cornerRadius = 5
                    cell.onlineView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
                    cell.onlineView.layer.shadowOpacity = 1.0
                    cell.onlineView.layer.shadowRadius = 3
                    cell.onlineView.layer.masksToBounds = false
                    cell.onlineView.layer.shadowColor = UIColor.gray.cgColor
                    cell.onlineView.backgroundColor = UIColor.white
                }else{
                    cell.giftCardLbl.isHidden = false
                    cell.cardDetailsView.isHidden = false
                    cell.cashView.layer.cornerRadius = 5
                    cell.cashView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
                    cell.cashView.layer.shadowOpacity = 1.0
                    cell.cashView.layer.shadowRadius = 3
                    cell.cashView.layer.masksToBounds = false
                    cell.cashView.layer.shadowColor = UIColor.gray.cgColor
                    cell.cashView.backgroundColor = UIColor.white
                    cell.onlineView.layer.cornerRadius = 5
                    cell.onlineView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
                    cell.onlineView.layer.shadowOpacity = 1.0
                    cell.onlineView.layer.shadowRadius = 3
                    cell.onlineView.layer.masksToBounds = false
                    cell.onlineView.layer.shadowColor = UIColor.gray.cgColor
                    cell.onlineView.backgroundColor = MonthYellow
                   // cell.cardDetailsView.addSubview(paymentTextField)
                    cell.cardDetailsView.layer.shadowOffset = CGSize(width: 0.8, height: 0.8)
                    cell.cardDetailsView.layer.shadowColor = UIColor.black.cgColor
                    cell.cardDetailsView.layer.cornerRadius = 5
                    cell.cardDetailsView.layer.shadowOpacity = 0.5
                    cell.cardDetailsView.layer.shadowRadius = 3
                    cell.cardDetailsView.layer.masksToBounds = false
                }


                cell.cashBtn.addTarget(self, action: #selector(cashBtnAction(button:)), for:.touchUpInside)
                cell.onlineBtn.addTarget(self, action: #selector(onlineBtnAction(button:)), for:.touchUpInside)

                return cell
            }else{
                return UITableViewCell()
            }



       }


    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return 201
        } else if indexPath.row == 1 {
            if(selectedPlan == "Free"){
                if self.adsListArr.count > 0 {
                                return 310
                    }else{
                        return 150
                    }
            }else{
               if self.adsListArr.count > 0 {
                        return 310
                }else{
                    return 150
                }
                
            }
        } else {
            if(selectedPlan != "Free"){
                if cashOrOnlineIndex == 1 {
                     if self.adsListArr.count > 0 {
                        return 310
                    }else{
                     return 150
                }
                } else {
                    return 150
                }
            }else{
                return 0
            }
        }
    }



    @objc func applyBtnClicked(button: UIButton) {

        promoCode_Api()
    }



  @objc func cashBtnAction(button: UIButton){
     paymentSelection = "Cash"
     let cell = paymentTableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! PlanTypeTableCell

     cell.giftCardLbl.isHidden = true
     cell.cardDetailsView.isHidden = true
     cashOrOnlineIndex = 0
     paymentTableView.reloadData()

   }


  @objc func onlineBtnAction(button: UIButton){

    let cell = paymentTableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! PlanTypeTableCell

     paymentSelection = "Online"
     cell.giftCardLbl.isHidden = false
     cell.cardDetailsView.isHidden = false

       cashOrOnlineIndex = 1
       paymentTableView.reloadData()


   }

    func PaymentData(planamount: String, discount: String, Plan_ID: String, selectRow: Int,planType:String) {

        if(selectedPlan == "Test"){
            
            self.planeamount = planamount
                   self.discount = discount
                   self.plan_duration = Plan_ID
                   collectionSelectedRow = selectRow
                   selectedPlan = "Test"
                   paymentTableView.reloadData()
            
        }else{
            
            self.planeamount = planamount
                   self.discount = discount
                   self.plan_duration = Plan_ID
                   collectionSelectedRow = selectRow
                   selectedPlan = planType
                   paymentTableView.reloadData()
        }
       
    }

    func promoCode_Api(){

        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let urserId = themes.getUserId()

            var dict = [String : AnyObject]()
            dict.updateValue(urserId as AnyObject, forKey: "user_id")
            dict.updateValue(promocodeStr as AnyObject, forKey: "promocode")

            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:checkPromocode, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){

                    self.prmoSelected = "1"
                    self.offerdata = response["data"] as! [String:AnyObject]

                    let type = self.offerdata["type"] as! String


                    if type == "percent"{

                        let discount = self.offerdata["value"] as! String
                        let amount = Double(self.planeamount)
                        let disamount = Double(discount)
                        let discou = disamount!/100
                        let n = discou * amount!
                        let totalamount = amount! - n
                        self.planeamount = "\( totalamount.rounded())"
                self.promocodeOfferText =  "Promo code \(self.promocodeStr) has been applied for   \(discount)% off "


                    }else {
                        let discount = self.offerdata["value"] as! String
                        let amount = Double(self.planeamount)
                        let disamount = Double(discount)
                        let discou = disamount!
                        let n = amount! - discou
                        let totalamount = amount!-n
                         self.planeamount = "\( totalamount.rounded())"

            self.promocodeOfferText =  "Promo code \(self.promocodeStr) has been applied for   $ \(discount) off "


                    }

                    self.paymentTableView.reloadData()
                    self.themes.showToast(message: "Promo code Applied", sender: self)


                } else {
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }




    func makePaymentAPI(){

        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let urserId = themes.getUserId()

            var dict = [String : AnyObject]()

            if paymentSelection == "Online"{
                if(selectedPlan != "Free" || selectedPlan != "1"){
                    
                    dict.updateValue("IOS_paymentGateway" as AnyObject, forKey: "payment_type")
                    dict.updateValue(SkPayment_TransactionID as AnyObject, forKey: "transaction_id")
                    dict.updateValue(plan_duration as AnyObject, forKey: "duration_id")
                    dict.updateValue(planId as AnyObject, forKey: "plan_id")
                }else{
                    dict.updateValue("Cash" as AnyObject, forKey: "payment_type")
                    dict.updateValue("Free" as AnyObject, forKey: "duration_id")
                    dict.updateValue(planId as AnyObject, forKey: "plan_id")


                }
                dict.updateValue(urserId as AnyObject, forKey: "user_id")


            }else{

                dict.updateValue(urserId as AnyObject, forKey: "user_id")
                dict.updateValue(paymentSelection as AnyObject, forKey: "payment_type")
                dict.updateValue(plan_duration as AnyObject, forKey: "duration_id")
                dict.updateValue(planId as AnyObject, forKey: "plan_id")

            }
            if self.prmoSelected == "1"{

                dict.updateValue(self.offerdata["offercode"] as AnyObject, forKey: "offercode")
                dict.updateValue(self.offerdata["type"] as AnyObject, forKey: "type")
                dict.updateValue(self.offerdata["value"] as AnyObject, forKey: "value")

            }

            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:makePayment, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){

                    let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopViewController") as! PopViewController

                    popOverVC.fromScreen = "paymentDone"
//                    popOverVC.ImageStr = ""
                    popOverVC.headingMesg = "Congratulations"
                    popOverVC.subMesg = "Your Payment is successfull"

                    self.addChildViewController(popOverVC)
                    popOverVC.view.center = self.view.frame.center
                    self.view.addSubview(popOverVC.view)
                    popOverVC.didMove(toParentViewController: self)

//                    self.themes.showAlert(title: "Success", message: "Your membership plan activaated", sender: self)
//                    let userDetailsVc = self.storyboard!.instantiateViewController(withIdentifier: "myPlanVC") as! myPlanVC
//                    userDetailsVc.fromScreen = "payment"
//                    self.navigationController?.pushViewController(userDetailsVc, animated: true)


                } else {
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }

    }


//    func getStripeToken(card:STPCardParams) {
//        // get stripe token for current card
//        STPAPIClient.shared().createToken(withCard: card) { token, error in
//            if let token = token {
//                print(token)
//                // token.tokenId
//                self.stripeToken = token
//                self.makePaymentAPI()
//
//            } else {
//                print(error)
//                self.themes.showAlert(title: "Alert", message: error!.localizedDescription, sender: self)
//
//            }
//        }
//    }

//    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
//
//        if textField.isValid{
//            if textField.cvc?.count == 3{
//               // paymentTextField.resignFirstResponder()
//            }
//        }
//    }



// UITextField Delegates
func textFieldDidBeginEditing(_ textField: UITextField) {
    print("TextField did begin editing method called")
}
func textFieldDidEndEditing(_ textField: UITextField) {

    promocodeStr = textField.text!
    print("TextField did end editing method called\(textField.text!)")


}

func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    print("TextField should begin editing method called")
    return true;
}
func textFieldShouldClear(_ textField: UITextField) -> Bool {
    print("TextField should clear method called")
    return true;
}
func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    print("TextField should end editing method called")
    return true;
}
func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
{
    return true
}
func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    print("TextField should return method called")
    textField.resignFirstResponder();
    return true;
  }

}



