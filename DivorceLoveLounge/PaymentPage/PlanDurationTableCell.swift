//
//  PlanDurationTableCell.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 22/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit


protocol PaymentData {
    
    func PaymentData(planamount:String,discount:String,Plan_ID:String,selectRow:Int,planType:String)
    
}

class PlanDurationTableCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate {
   
    
    
    @IBOutlet var adsBannerView: UIView!
    
    @IBOutlet var adsCollectionView: UICollectionView!
    @IBOutlet var adsHeightConstant: NSLayoutConstraint!
    
    var adsListArr = [AnyObject]()
    
    var monthsArr = [AnyObject]()
    
    @IBOutlet var durationView: UIView!
    @IBOutlet var promocodeView: UIView!
    @IBOutlet var promoCodeField: UITextField!
    @IBOutlet var applyBtn: UIButton!
    @IBOutlet var durationCollectionView: UICollectionView!
    @IBOutlet var codeLabel: UILabel!
    
    @IBOutlet weak var promoCodeTitleLabel: UILabel!
    var delegate:PaymentData?
    
    var selectedIndex = 0

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
     
        durationCollectionView.delegate = self
        durationCollectionView.dataSource = self
        
        self.adsCollectionView.delegate = self
        self.adsCollectionView.dataSource = self
        
        
        promoCodeField.delegate = self
        
        self.durationCollectionView.register(UINib(nibName: "MonthsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "MonthsCollectionCell")
        self.adsCollectionView.register(UINib(nibName: "AdsBannerCollecctionCell", bundle: nil), forCellWithReuseIdentifier: "AdsBannerCollecctionCell")
        
        durationView.layer.cornerRadius = 5
        durationView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        durationView.layer.shadowOpacity = 1.0
        durationView.layer.shadowRadius = 3
        durationView.layer.masksToBounds = false
        durationView.layer.shadowColor = UIColor.gray.cgColor
        durationView.layer.borderWidth = 1
        durationView.layer.borderColor = UIColor.lightGray.cgColor
        
        promocodeView.layer.cornerRadius = promocodeView.frame.size.height/2
        promocodeView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        promocodeView.layer.shadowOpacity = 1.0
        promocodeView.layer.shadowRadius = 3
        promocodeView.layer.masksToBounds = false
        promocodeView.layer.shadowColor = UIColor.gray.cgColor
        promocodeView.layer.borderWidth = 1
        promocodeView.layer.borderColor = UIColor.lightGray.cgColor
        
        promoCodeField.layer.cornerRadius = promoCodeField.frame.size.height/2
        promoCodeField.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        promoCodeField.layer.shadowOpacity = 1.0
        promoCodeField.layer.shadowRadius = 3
        promoCodeField.layer.masksToBounds = false
        promoCodeField.layer.shadowColor = UIColor.gray.cgColor
        
        applyBtn.layer.cornerRadius = applyBtn.frame.size.height/2
        applyBtn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        applyBtn.layer.shadowOpacity = 1.0
        applyBtn.layer.shadowRadius = 3
        applyBtn.layer.masksToBounds = false
        applyBtn.layer.shadowColor = UIColor.gray.cgColor
        
        
        // Initialization code
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == adsCollectionView {
            
           return adsListArr.count
        }else{
        
        return monthsArr.count
         }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if collectionView == adsCollectionView {
            let cell = adsCollectionView.dequeueReusableCell(withReuseIdentifier: "AdsBannerCollecctionCell", for: indexPath) as! AdsBannerCollecctionCell
            let data = self.adsListArr[indexPath.row] as! [String:AnyObject]
            let type = data["add_type"] as! String
            if type == "Custom" {
                cell.adsWebView.isHidden = true
                cell.adsImageView.isHidden = false
                let url = "\(adsUrl)\(data["add_image"] as! String)"
                cell.adsImageView.af_setImage(withURL: URL(string:url)!)
            }else{
                cell.adsWebView.isHidden = false
                cell.adsImageView.isHidden = true
            }
            
            return cell
        }else{
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MonthsCollectionCell", for: indexPath) as! MonthsCollectionCell

        let obj = monthsArr[indexPath.item]
        cell.amountLabel.text = (obj["amount"] as! String)
        
        let plan = obj["month"]as! String
        if(plan == "Free"){
            cell.monthLabel.text = obj["month"] as? String
        }else if(plan == "1"){
             cell.monthLabel.text = (obj["month"] as! String + " Month")
        }else{
            cell.monthLabel.text = (obj["month"] as! String + " Months")
        }
        
        
        DispatchQueue.main.async {
            
          
            
            if self.selectedIndex == indexPath.item{
                cell.backView.backgroundColor = MonthYellow
                
                cell.backView.layer.cornerRadius = 5
                cell.backView.layer.shadowOffset = CGSize(width: 1.0, height: 2.0)
                cell.backView.layer.shadowOpacity = 1.5
                cell.backView.layer.shadowRadius = 3
                cell.backView.layer.masksToBounds = false
                cell.backView.layer.shadowColor = UIColor.gray.cgColor
                
            }else{
                cell.backView.backgroundColor = UIColor.white
                
                cell.backView.layer.cornerRadius = 5
                cell.backView.layer.shadowOffset = CGSize(width: 1.0, height: 2.0)
                cell.backView.layer.shadowOpacity = 1.5
                cell.backView.layer.shadowRadius = 3
                cell.backView.layer.masksToBounds = false
                cell.backView.layer.shadowColor = UIColor.gray.cgColor
            }
            
            
        }
        return cell
        
        }
        
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        if collectionView == adsCollectionView {
            let data = self.adsListArr[indexPath.row] as! [String:AnyObject]
            let url = data["add_link"]
            if let url = URL(string: url as! String) {
                UIApplication.shared.open(url)
            }
        }else{
            
        let array = self.monthsArr[indexPath.row]
        let planamount = array["amount"] as! String
        let discoun = array["discount"] as! String
        let plan_duration  = array["duration_id"] as! String
        let month  = array["month"] as! String
        let amount = Double(planamount)
        let disamount = Double(discoun)
        let discou = disamount!/100
        let n = discou*amount!
        let totalamount = amount!-n
        let discount =  discoun + "%"
        let planeamount = "$ " + planamount
        let finlaamount = Double(totalamount)
        let amount2 = "\(finlaamount)"
        selectedIndex = indexPath.item
        durationCollectionView.reloadData()
            
            
                 delegate?.PaymentData(planamount: amount2, discount: discount, Plan_ID: plan_duration, selectRow: indexPath.row, planType: "Test")
                
           
       
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == adsCollectionView {
           return CGSize(width:self.adsBannerView.frame.size.width , height: self.adsBannerView.frame.size.height )
        }else{
        return CGSize(width:107 , height: 65)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        
        if collectionView == adsCollectionView {
            return 10
        }else{
            return 15
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        if collectionView == adsCollectionView {
            return 0
        }else{
            return 3
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    func setTimer() {
        let _ = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(PlanDurationTableCell.autoScroll), userInfo: nil, repeats: true)
    }
    var x = 0
    @objc func autoScroll() {
        if self.x < self.adsListArr.count {
            let indexPath = IndexPath(item: x, section: 0)
            self.adsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.x = self.x + 1
        }else{
            self.x = 0
            self.adsCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
}
