//
//  ChatViewController.swift
//  DivorceLoveLounge
//
//  Created by nagaraj  kumar on 14/06/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
import WebKit

class ChatVC_TalkJS: UIViewController,WKNavigationDelegate,UIScrollViewDelegate {
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    var userID = ""
    var userName = ""
    var userEmail = ""
    var photoUrl = ""
    var myphoto = ""
    var userphoto = ""
    @IBOutlet var topHeadingLbl: UILabel!
    @IBOutlet var chatWebView: WKWebView!
    @IBAction func backBtnAction(_ sender: Any) {
self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
     //  getChatListApi()
        userphoto = "https://divorcelovelounge.com/uploads/userprofiles/" + photoUrl
        myphoto = "https://divorcelovelounge.com/uploads/userprofiles/" + self.themes.getphoto1()!
          self.chatLogic(id: userID, userName: userName, emailID: userEmail)
       
    }
    

    func loadHTMLContent(_ htmlContent: String) {
        
        let htmlStart = "<HTML><HEAD><meta name=\"viewport\" content=\"width=device-width, maximum-scale=1.0,user-scalable=no\"></HEAD><BODY>"
        let htmlEnd = "</BODY></HTML>"
        let htmlString = "\(htmlStart)\(htmlContent)\(htmlEnd)"
       // chatWebView.loadHTMLString(htmlString, baseURL: Bundle.main.bundleURL)
          chatWebView.loadHTMLString(htmlString, baseURL: Bundle.main.bundleURL)
   
    }
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        chatWebView.frame.size = webView.scrollView.contentSize
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
 
    func chatLogic(id:String,userName:String,emailID:String){
        
        let myid  = self.themes.getUserId()
        let myname = self.themes.getFirstName() as! String
        let myemail = self.themes.getuserMail() as! String
        
        
        let js = "<script>\n" +

            "(function(t,a,l,k,j,s){\n" +
            "s=a.createElement('script');s.async=1;s.src=\"https://cdn.talkjs.com/talk.js\";a.head.appendChild(s)\n" +
            ";k=t.Promise;t.Talk={v:1,ready:{then:function(f){if(k)return new k(function(r,e){l.push([f,r,e])});l\n" +
            ".push([f])},catch:function(){return k&&new k()},c:l}};})(window,document,[]);\n" +
            "</script>\n" +
            "\n" +
            "<!-- container element in which TalkJS will display a chat UI -->\n" +
            "<div id=\"talkjs-container\" style=\"width: 100%; margin: auto; height: 100%\"><i>Loading please Wait...</i></div>\n" +
            "\n" +
            "<!-- TalkJS initialization code, which we'll customize in the next steps -->\n" +
            "<script>\n" +
            "Talk.ready.then(function() {\n" +
            "    var me = new Talk.User({\n" +
            "        id: \"\(myid)\",\n" +
            "        name: \"\(myname)\",\n" +
            "        role: \"user\",\n" +
            "        email: \"\(myemail)\",\n" +
            "        photoUrl: \"\(myphoto)\",\n" +
            "        welcomeMessage: \"Hey, how can I help?\"\n" +
            "    });\n" +
            "    window.talkSession = new Talk.Session({\n" +
            "        appId: \"M5iybBK9\",\n" +
            "        me: me\n" +
            "    });\n" +
            "    var other = new Talk.User({\n" +
            "        id: \"\(id)\",\n" +
            "        name: \"\(userName)\",\n" +
            "        role: \"user\",\n" +
            "        email: \"\(emailID)\",\n" +
            "        photoUrl: \"\(userphoto)\",\n" +
            "        welcomeMessage: \"Hey there! How are you? :-)\"\n" +
            "    });\n" +
            "\n" +
            "    var conversation = talkSession.getOrCreateConversation(Talk.oneOnOneId(me, other))\n" +
            "    conversation.setParticipant(me);\n" +
            "    conversation.setParticipant(other);\n" +
            "    var chatbox = talkSession.createChatbox(conversation);\n" +
            "     chatbox.mount(document.getElementById(\"talkjs-container\"));\n" +
            "});\n" +
        "</script>\n"


        
//        let js = "<link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"http://35.154.208.171/sampleproject/js/mobile.min.css\"><link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"http://35.154.208.171/sampleproject/js/converse.min.css\"><script src=\"http://35.154.208.171/sampleproject/js/converse.min.js\" charset=\"utf-8\"></script><script src=\"http://35.154.208.171/sampleproject/js/converse-mobile.min.js\" charset=\"utf-8\"></script><script>converse.initialize({allow_logout: false,authentication: 'login',auto_login: true,bosh_service_url: 'http://35.154.208.171:5280/http-bind/',jid: 'S3122@35.154.208.171',password:'abc123', allow_muc_invitations: false,allow_contact_requests:false,allow_contact_removal:false,keepalive: true,allow_muc: false,view_mode:'mobile',autocomplete_add_contact: false,muc_nickname_from_jid:true,message_archiving:'roster',locked_muc_nickname:true,play_sounds: true,allow_registration:false,show_controlbox_by_default: true,strict_plugin_dependencies:false,});</script>"

        
        loadHTMLContent(js)
    }
    

    func getChatListApi(){
        
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
           // let urserId = themes.getUserId()
            var dict = [String : String]()
            dict.updateValue(userID , forKey: "user_id")
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:getChatList, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    let data = response["data"]as! [AnyObject]
                    
                    let userInfo = data[0] as! [String:AnyObject]
                    
                    let id = self.themes.checkNullValue(userInfo["id"]) as! String
                    let firstName = self.themes.checkNullValue(userInfo["first_name"]) as! String
                    let email = self.themes.checkNullValue(userInfo["email"]) as! String
                    
                    
                    self.chatLogic(id: id, userName: firstName, emailID: email)
                    
                    
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
}
