//
//  SupportVC.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 26/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
import WebKit

class SupportVC: UIViewController,UITextViewDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.supportData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "countryTableCell", for: indexPath)as! countryTableCell
        tableView.separatorColor = UIColor.clear
        cell.selectionStyle = .none
        cell.backView.layer.cornerRadius = cell.backView.frame.size.height/2
        cell.backView.layer.borderWidth = 1
        cell.backView.layer.borderColor = UIColor.lightGray.cgColor
        let obj = self.supportData[indexPath.row]
        cell.countryLbl.text = obj["name"] as? String
        if(selectedRow == indexPath.row){
            cell.backView.backgroundColor = darkBrownColor
            cell.countryLbl.textColor = UIColor.white
        }else{
            cell.backView.backgroundColor = UIColor.clear
            cell.countryLbl.textColor = UIColor.black
        }
        return cell
    }
    @IBAction func typeAction(_ sender: Any) {
        hideView.isHidden = false
        popUpView.isHidden = false
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRow = indexPath.row
        let obj = supportData[indexPath.row]
        selectedID = obj["id"] as! String
        typeTextField.text = obj["name"] as? String
        supportTableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    var selectedID = String()
    
    
    @IBOutlet var adsBackView: UIView!
    
    @IBOutlet var adsBackViewHieght: NSLayoutConstraint!
    
    @IBOutlet var adsCollectionView: UICollectionView!
    
   
    var adsListArr = [AnyObject]()
    
    var supportData = Array<AnyObject>()
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    @IBOutlet var hideView: UIView!
    var selectedRow = Int()
    @IBOutlet var popUpView: UIView!
    @IBOutlet var popUpDoneBtn: UIButton!
    @IBOutlet var supportTableView: UITableView!
    @IBOutlet var typeTextField: UITextField!
    @IBOutlet var msgTextView: UITextView!
    @IBOutlet var sendBtn: UIButton!
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendAction(_ sender: Any) {
        if(typeTextField.text == ""){
            self.themes.showAlert(title: "Alert", message: "Please choose support type", sender: self)
        } else {
            if(msgTextView.text == "Message..."){
                self.themes.showAlert(title: "Alert", message: "Please enter message", sender: self)
            } else {
                if(msgTextView.text == ""){
                    self.themes.showAlert(title: "Alert", message: "Please enter message", sender: self)
                } else {
                   sendSupportApi()
           }
        }
      }
    }
    @IBAction func supportDoneAction(_ sender: Any) {
        hideView.isHidden = true
        popUpView.isHidden = true
    }
    
    @IBAction func supportCloseAction(_ sender: Any) {
        typeTextField.text = ""
        hideView.isHidden = true
        popUpView.isHidden = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
         msgTextView.contentInset = UIEdgeInsets(top: 2, left: 20, bottom: 2, right: 10)
        msgTextView.text = "Message..."
        msgTextView.textColor = UIColor.lightGray
        
        
        self.supportTableView.register(UINib(nibName: "countryTableCell", bundle: nil), forCellReuseIdentifier: "countryTableCell")
        
        
        self.adsCollectionView.register(UINib(nibName: "AdsBannerCollecctionCell", bundle: nil), forCellWithReuseIdentifier: "AdsBannerCollecctionCell")
        
        
        
        self.popUpView.layer.cornerRadius = 5
        popUpDoneBtn.layer.cornerRadius = 5
        typeTextField.delegate = self
        typeTextField.layer.cornerRadius = typeTextField.frame.size.height/2
        typeTextField.layer.borderWidth = 1
        typeTextField.layer.borderColor = UIColor.lightGray.cgColor
        // For Left side Padding
        typeTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: typeTextField.frame.height))
        typeTextField.leftViewMode = .always
        // For Right side Padding
        typeTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 33, height: typeTextField.frame.height))
        typeTextField.rightViewMode = .always
        typeTextField.inputView = UIView()

        msgTextView.delegate = self
        msgTextView.layer.cornerRadius = 15
        msgTextView.layer.borderWidth = 1
        msgTextView.layer.borderColor = UIColor.lightGray.cgColor
        sendBtn.layer.cornerRadius = typeTextField.frame.size.height/2
        sendBtn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        sendBtn.layer.shadowOpacity = 1.0
        sendBtn.layer.shadowRadius = 3
        sendBtn.layer.masksToBounds = false
        sendBtn.layer.shadowColor = UIColor.gray.cgColor
        
        // Keyboard Action
        NotificationCenter.default.addObserver(self, selector: #selector(SupportVC.keyboardUp), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SupportVC.keyboardDown), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
     
        
        
        getSupportTypeListApi()
        
        adsListServiceApi(ScreenType: "Support")
        
        
        
 
        // Do any additional setup after loading the view.
    }
    
   
    
    @objc func keyboardUp(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            //
            self.view.frame.origin.y = 150
            if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardRectangle = keyboardFrame.cgRectValue
                let keyboardHeight = keyboardRectangle.height
                self.view.frame.origin.y -= keyboardHeight
            }
        }
    }
    @objc func keyboardDown(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            self.view.frame.origin.y = 0
        }
    }
    
    
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
        hideView.isHidden = false
        popUpView.isHidden = false
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    /// Text View Delegates
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
//            textView.text = "Message"
//            textView.textColor = UIColor.lightGray
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func getSupportTypeListApi(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            urlService.serviceCallGetMethod(url: supportTypes, completionHandler: { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    self.supportData = (response["data"] as AnyObject) as! [AnyObject]
                    self.supportTableView.reloadData()
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            })
        }else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    
    func sendSupportApi(){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid,
                        "support_id":selectedID,
                        "message":msgTextView.text] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:supportInsert, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopViewController") as! PopViewController
                    
                    popOverVC.fromScreen = "SupportScreen"
                    //                    popOverVC.ImageStr = ""
                    popOverVC.headingMesg = "Thank You!"
                    popOverVC.subMesg = "We will reach you shortly."
                    
                    self.addChildViewController(popOverVC)
                    popOverVC.view.center = self.view.frame.center
                    self.view.addSubview(popOverVC.view)
                    popOverVC.didMove(toParentViewController: self)
                    
                } else {
                    
                    // let result = response["result"] as! String
                    //  self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
                
            }
            
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
  
    func adsListServiceApi(ScreenType:String){
        self.urlService.adslistServiceCall(ScreenType: ScreenType) { response in
            let success = response["status"] as! String
            if(success == "1"){
                
                let data = response["data"] as! [AnyObject]
                self.adsListArr = data
                self.adsCollectionView.delegate = self
                self.adsCollectionView.dataSource = self
                
                if self.adsListArr.count > 1 {
                    self.setTimer()
                }
                self.adsCollectionView.reloadData()
               // self.setTimer()
                
            }else{
                
            }
        }
        
    }
    
    func setTimer() {
        let _ = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(ViewController.autoScroll), userInfo: nil, repeats: true)
    }
    var x = 0
    @objc func autoScroll() {
        if self.x < self.adsListArr.count {
            let indexPath = IndexPath(item: x, section: 0)
            self.adsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.x = self.x + 1
        }else{
            self.x = 0
            self.adsCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.adsListArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: adsCollectionView.frame.size.width, height: adsCollectionView.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = adsCollectionView.dequeueReusableCell(withReuseIdentifier: "AdsBannerCollecctionCell", for: indexPath) as! AdsBannerCollecctionCell
        let data = self.adsListArr[indexPath.row] as! [String:AnyObject]
        let type = data["add_type"] as! String
        if type == "Custom" {
            cell.adsWebView.isHidden = true
            cell.adsImageView.isHidden = false
            let url = "\(adsUrl)\(data["add_image"] as! String)"
            cell.adsImageView.af_setImage(withURL: URL(string:url)!)
        }else{
            cell.adsWebView.isHidden = false
            cell.adsImageView.isHidden = true
            
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let data = self.adsListArr[indexPath.row] as! [String:AnyObject]
        let url = data["add_link"]
        if let url = URL(string: url as! String) {
            UIApplication.shared.open(url)
        }
    }
    
    
    
    
    
    
    
    

}
