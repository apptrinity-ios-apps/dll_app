//
//  ChatListTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 3/20/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class ChatListTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var activeBtn: UIImageView!
    @IBOutlet weak var namelabel: UILabel!
    @IBOutlet weak var mesglabel: UILabel!
    @IBOutlet var backview: UIView!
    
    @IBOutlet var chatBlink_Lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
