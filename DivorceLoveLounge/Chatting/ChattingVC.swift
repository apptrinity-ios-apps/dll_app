//
//  ChattingVC.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 31/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//
import UIKit
protocol backBtnClickDelegate1 {
    
    func backAction(from:String)
    
}

class ChattingVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIScrollViewDelegate {
    
    @IBOutlet var profileimage: UIImageView!
    
    
    var match_id = String()
    var date_id = String()
    
    
    
    
    
    @IBAction func profileviewBtnAction(_ sender: Any) {
        
        let userDetailsVc = self.storyboard?.instantiateViewController(withIdentifier: "UserDetailsVC") as! UserDetailsVC
       // let obj = matchesData[indexPath.item]
        userDetailsVc.fromScreen = "myfavourite"
      //  userDetailsVc.userDataDict = obj as! [String : AnyObject]
        
        let userid = self.themes.checkNullValue(self.themes.getUserId()) as! String
        var match_id = String()
        if (msg_from == userid){
            match_id = msg_to
        }else{
            match_id = msg_from
        }
        userDetailsVc.match_id = match_id
        userDetailsVc.imageApprove = true
        self.navigationController?.pushViewController(userDetailsVc, animated: true)
        
        
    }
    
    
    @IBAction func moreAction(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Delete ", style: .destructive , handler:{ (UIAlertAction)in
            print("User click Approve button")
            
            let id = self.themes.getUserId()
            
            if self.msgId == id {
               // msg_type = "sender"
                self.trash_API(trash_status: "Yes", msg_id: self.msgId, msg_type: "sender")
            }else{
              //  msg_type = "reciever"
                self.trash_API(trash_status: "Yes", msg_id: self.msgId, msg_type: "reciever")
                
            }
           
           
            

            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Clear History", style: .destructive , handler:{ (UIAlertAction)in
            print("User click Approve button")
           

        }))
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
        
        
        
        
        
    }
    
    
    
    func trash_API(trash_status:String ,msg_id:String,msg_type:String){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid,
                        "trash_status":trash_status,
                        "msg_id":msg_id,
                        "msg_type":msg_type] as [String:Any]
            
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:makeTrash, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    self.navigationController?.popViewController(animated: false)
                    
                } else {
                    
                }
                
            }
            
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messagesArray.count
    }
    
     var delegate : backBtnClickDelegate1?
    var msgType = String()
    
    var refreshControl = UIRefreshControl()
    
    
    let fromColor = UIColor(red: 230/255.0, green: 229/255.0, blue: 235/255.0, alpha: 1.0)
    let toColor = UIColor(red: 255/255.0, green: 184/255.0, blue: 14/255.0, alpha: 1.0)
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChattingCell", for: indexPath) as! ChattingCell
       
        cell.selectionStyle = .none
        let obj = self.messagesArray[indexPath.row]
        cell.messageLabel.text =  obj["msg_content"] as? String
        let fromId = obj["msg_from"] as? String
        let userId = themes.getUserId()
        let date_Time_value = self.themes.checkNullValue(obj["msg_date"] as? String)
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let showDate = inputFormatter.date(from: date_Time_value as! String)
        inputFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
        let resultString = inputFormatter.string(from: showDate!)
        
        cell.dateLabel.text = resultString

        //if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == true){
          //  cell.messageLabel.layer.cornerRadius = cell.messageLabel.frame.height / 4
            cell.messageLabel.sizeToFit()
            cell.messageLabel.lineBreakMode = .byWordWrapping
            cell.messageLabel.numberOfLines = 0
      
            if(userId == fromId){
          //  cell.Myimageview.isHidden = true
            cell.messageLabel.backgroundColor = toColor
            cell.messageLabel.textAlignment = .center
            cell.dateLabel.textAlignment = .center
            cell.msg_LeftConstaint.isActive = false
            cell.msg_RightConstaint.isActive = true
           }else{
           // cell.Myimageview.isHidden = false
            cell.messageLabel.backgroundColor = fromColor
            cell.messageLabel.textAlignment = .center
            cell.dateLabel.textAlignment = .center
            cell.msg_RightConstaint.isActive = false
            cell.msg_LeftConstaint.isActive = true
            }
  
        return cell
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 63
    }
    
    //To calculate height for label based on text size and width
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGRect {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame
    }
    
   
    
    @IBOutlet weak var replyView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var listTableView: UITableView!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var messageTypeLabel: UILabel!
    @IBOutlet weak var replyTextField: UITextField!
    var photo = String()
    var photoApproved = String()
    var msg_from = String()
    var msg_to = String()
    var messagesArray = Array<AnyObject>()
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    var msgId = String()

    var rowsWhichAreChecked = [NSIndexPath]()
    
    var userID = String()
    var first_name = String()
    @IBOutlet var topview: UIView!
    
    
    
    // Scroll to refresh
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        messageList()
        refreshControl.endRefreshing()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshControl.endRefreshing()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        // Scroll to refresh Add target
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(ChattingVC.refresh(sender:)), for: .allEvents)
        listTableView.addSubview(refreshControl)
       // refreshControl.endRefreshing()
 
        if(msgType == "inbox"){
           messageTypeLabel.text = "Inbox"
        }else if(msgType == "sent"){
             messageTypeLabel.text = "Sent"
        }else if(msgType == "important"){
            messageTypeLabel.text = "Important"
        }else if(msgType == "trash"){
            messageTypeLabel.text = "Trash"
        }
         self.listTableView.register(UINib(nibName: "ChattingCell", bundle: nil), forCellReuseIdentifier: "ChattingCell")
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)
        replyView.layer.cornerRadius = 5
        replyView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        replyView.layer.shadowOpacity = 1.0
        replyView.layer.shadowRadius = 3
        replyView.layer.masksToBounds = false
        replyView.layer.shadowColor = UIColor.gray.cgColor
        replyTextField.layer.borderWidth = 1
        replyTextField.layer.borderColor = UIColor.lightGray.cgColor
        // For Right side Padding
        replyTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: replyTextField.frame.height))
        replyTextField.layer.cornerRadius = 10
        replyTextField.leftViewMode = .always
        // For left side Padding
        replyTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: replyTextField.frame.height))
        replyTextField.rightViewMode = .always
        listTableView.separatorColor = UIColor.clear
        
        messageList()
        
            let fullimageUrl = ImagebaseUrl + photo
                self.profileimage.sd_setImage(with: URL(string: fullimageUrl), placeholderImage:UIImage(named: "defaultPic"), completed: { (image, error, cacheType, url) -> Void in
                    if ((error) != nil) {
                         self.profileimage.image = UIImage(named: "defaultPic")
                    } else {
                         self.profileimage.image = image
                    }
                })
        
        self.profileimage.layer.cornerRadius = self.profileimage.frame.height/2
        self.profileimage.layer.borderColor = UIColor.white.cgColor
        self.profileimage.layer.borderWidth = 2
        self.profileimage.clipsToBounds = true
        
        self.nameLabel.text = first_name
        
    }
//    override func viewDidAppear(_ animated: Bool) {
//        let scrollPoint = CGPoint(x: 0, y: self.listTableView.contentSize.height - self.listTableView.frame.size.height)
//        self.listTableView.setContentOffset(scrollPoint, animated: false)
//     //   refreshControl.endRefreshing()
//
//    }
    func scrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.messagesArray.count-1, section: 0)
            self.listTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame!.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.bottomConstraint?.constant = 0.0
            } else {
                self.bottomConstraint?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    func messageList(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            
            let userid = self.themes.checkNullValue(self.themes.getUserId()) as! String
            
           var match_id = String()
            
            if (msg_from == userid){
                
                match_id = msg_to
            }else{
                
                  match_id = msg_from
                
            }
            
            
            let dict = ["date_id":date_id,"user_id":userid as Any ,"member_id":match_id] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:memberMessages, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    let msg_data = response["userdata"] as! [String:AnyObject]
                    self.messagesArray = msg_data["msg_data"] as! [AnyObject]
                    if(self.messagesArray.count == 0){
                        
                    }else{
                        for i in 0...self.messagesArray.count - 1 {
                            let obj = self.messagesArray[i]
                            let message = obj["msg_content"] as! String
                            let label = UILabel()
                            label.text = message
                            //let cell:ChattingCell = (self.listTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ChattingCell)!
                            label.frame = CGRect(x:0,y:0,width: 250,height:20)
                            let labelCount = label.maxNumberOfLines
                            if(labelCount == 1){
                            }else{
                                let index = NSIndexPath(row: i, section: 0)
                                self.rowsWhichAreChecked.append(index)
                            }
                        }
                        
                        DispatchQueue.main.async {
                            self.listTableView.reloadData()
                            //    self.scrollToBottom()
                        }
                        self.scrollToBottom()
                    }
                   
                } else {
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
//    func scrollToBottom(){
//        DispatchQueue.main.async {
//            let indexPath = IndexPath(row: self.messagesArray.count-1, section: 0)
//            self.listTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
//        }
//    }

    func replyMessageApi(){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            
            var dict = [String : AnyObject]()
            
            
//            if(msgType == "inbox"){
//                dict = ["msg_from":msg_to,"msg_to":msg_from,"msg_content":replyTextField.text!,"msg_status":msgType,"msg_id":msgId] as [String:AnyObject]
//            }else if(msgType == "sent"){
//                 dict = ["msg_from":msg_from,"msg_to":msg_to,"msg_content":replyTextField.text!,"msg_status":msgType,"msg_id":msgId] as [String:AnyObject]
//            }else {
////                let id = themes.getUserId()
////
////                if(id == msg_from){
////                    msg_from = id
////
////                }else{
////                    msg_from = msg_to
////                    msg_to = id
////                }
//
//            }
            
            var user__id = String()
            let userid = self.themes.checkNullValue(self.themes.getUserId()) as! String
            
            if(userid == msg_to){
               user__id = msg_to
               dict = ["msg_from":user__id,"msg_to":msg_from,"msg_content":replyTextField.text!,"msg_status":msgType,"msg_id":msgId] as [String:AnyObject]
            }else{
                user__id = msg_from
                dict = ["msg_from":user__id,"msg_to":msg_to,"msg_content":replyTextField.text!,"msg_status":msgType,"msg_id":msgId] as [String:AnyObject]
            }
            
            
            
            
            // dict = ["msg_from":user__id,"msg_to":msg_to,"msg_content":replyTextField.text!,"msg_status":msgType,"msg_id":msgId] as [String:AnyObject]
            
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:replyMessage, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    self.replyTextField.text = ""
                    self.messageList()
                } else {
                    
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        if(msgType == "inbox"){
            delegate?.backAction(from: "inbox")
        }else if(msgType == "sent"){
             delegate?.backAction(from: "sent")
        }else if(msgType == "trash"){
            delegate?.backAction(from: "trash")
        }else if(msgType == "important"){
            delegate?.backAction(from: "important")
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func messageSendAction(_ sender: Any) {
        if(replyTextField.text == ""){
            themes.showAlert(title: "Message", message: "Please enter message", sender: self)
        }else{
            replyTextField.resignFirstResponder()
            
            replyMessageApi()
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
   
    

}
extension UIView {
    func roundCornersWithLayerMask(cornerRadii: CGFloat, corners: UIRectCorner) {
        let path = UIBezierPath(roundedRect: bounds,
                                byRoundingCorners: corners,
                                cornerRadii: CGSize(width: cornerRadii, height: cornerRadii))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        layer.mask = maskLayer
    }
}
extension UILabel {
    var maxNumberOfLines: Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(MAXFLOAT))
        let text = (self.text ?? "") as NSString
        let textHeight = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil).height
        let lineHeight = font.lineHeight
        return Int(ceil(textHeight / lineHeight))
    }
}
