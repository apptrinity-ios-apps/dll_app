//
//  CreateProfileVC.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 3/12/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class CreateProfileVC: UIViewController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
    var countryArr = ["India","Indonesia","Bangladesh","Japan","Philippines","VietNam","Turkey","Iran","SouthKorea","Iraq","Afghanistan","SaudiArabia","Uzbekistan","Malaysia","Nepal","Yemen","China"]
    var CityArr = ["Tokyo","Delhi","Shanghai","Beijing","Mumbai","Osaka","Dhaka","Kolkata","Guangzhou","Manila","Tianjin","Shenzhen","Bangalore","Jakarta","Chennai","Seoul","Bangkok","Hyderabad"]
    
    let countryPicker = UIPickerView()
    let cityPicker = UIPickerView()
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var dobField: UITextField!
    @IBOutlet weak var countryField: UITextField!
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var countryBtn: UIButton!
    @IBOutlet weak var cityBtn: UIButton!
    @IBAction func addPhotoAction(_ sender: Any) {
        ImagePickerManager().pickImage(self){ image in
            //here is the image
//            let pickImgVc = self.storyboard?.instantiateViewController(withIdentifier: "pickImgVc")as! ImagePickerManager
//            pickImgVc.pickImageCallback = profImg
        }
    }
        
    @IBAction func countryBtnAction(_ sender: Any) {
        
    }
    
    @IBAction func cityBtnAction(_ sender: Any) {
        
    }
    @IBAction func nextBtnAction(_ sender: Any) {
        
        let locVc = self.storyboard?.instantiateViewController(withIdentifier: "LocationVC") as! LocationVC
        self.navigationController?.pushViewController(locVc, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        countryPicker.delegate = self
        cityPicker.delegate = self
        countryPicker.backgroundColor = UIColor.white
        cityPicker.backgroundColor = UIColor.white
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CreateProfileVC.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(CreateProfileVC.cancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        countryField.inputView = countryPicker
        countryField.inputAccessoryView = toolBar
        cityField.inputView = cityPicker
        cityField.inputAccessoryView = toolBar
        
        self.nameField.delegate = self
        self.dobField.delegate = self
        self.countryField.delegate = self
        self.cityField.delegate = self
        
        self.profileImgView.layer.cornerRadius = (profileImgView.frame.size.height)/2
        self.nextButton.layer.cornerRadius = 22
        
        self.nameField.layer.cornerRadius = 22
        self.nameField.layer.borderWidth = 1
        self.nameField.layer.borderColor = UIColor(red: 205/255, green: 205/255, blue: 205/255, alpha: 1.0).cgColor
        nameField.attributedPlaceholder = NSAttributedString(string: "First Name",
       attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: CGFloat(28/255.0), green: CGFloat(37/255.0), blue: CGFloat(42/255.0), alpha: CGFloat(1.0) )])
        // For Right side Padding
        nameField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: nameField.frame.height))
        nameField.leftViewMode = .always
        // For left side Padding
        nameField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: nameField.frame.height))
        nameField.rightViewMode = .always
        
        self.dobField.layer.cornerRadius = 22
        self.dobField.layer.borderWidth = 1
        self.dobField.layer.borderColor = UIColor(red: 205/255, green: 205/255, blue: 205/255, alpha: 1.0).cgColor
        dobField.attributedPlaceholder = NSAttributedString(string: "Date of Birth",
        attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: CGFloat(28/255.0), green: CGFloat(37/255.0), blue: CGFloat(42/255.0), alpha: CGFloat(1.0) )])
        // For Right side Padding
        dobField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: dobField.frame.height))
        dobField.leftViewMode = .always
        // For left side Padding
        dobField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: dobField.frame.height))
        dobField.rightViewMode = .always

        self.countryField.layer.cornerRadius = 22
        self.countryField.layer.borderWidth = 1
        self.countryField.layer.borderColor = UIColor(red: 205/255, green: 205/255, blue: 205/255, alpha: 1.0).cgColor
        countryField.attributedPlaceholder = NSAttributedString(string: "Country",
        attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: CGFloat(28/255.0), green: CGFloat(37/255.0), blue: CGFloat(42/255.0), alpha: CGFloat(1.0) )])
        // For Right side Padding
        countryField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: countryField.frame.height))
        countryField.leftViewMode = .always
        // For left side Padding
        countryField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: countryField.frame.height))
        countryField.rightViewMode = .always
        
        self.cityField.layer.cornerRadius = 22
        self.cityField.layer.borderWidth = 1
        self.cityField.layer.borderColor = UIColor(red: 205/255, green: 205/255, blue: 205/255, alpha: 1.0).cgColor
        cityField.attributedPlaceholder = NSAttributedString(string: "City",
        attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: CGFloat(28/255.0), green: CGFloat(37/255.0), blue: CGFloat(42/255.0), alpha: CGFloat(1.0) )])
        // For Right side Padding
        cityField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: cityField.frame.height))
        cityField.leftViewMode = .always
        // For left side Padding
        cityField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: cityField.frame.height))
        cityField.rightViewMode = .always
        
    countryBtn.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "countryBtnAction"))
//Date Picker Action
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = UIDatePickerMode.date
        datePicker.addTarget(self, action: #selector(CreateProfileVC.datePickerValueChanged(sender:)), for: UIControlEvents.valueChanged)
        dobField.inputView = datePicker
        
// Keyboard Action
        NotificationCenter.default.addObserver(self, selector: #selector(CreateProfileVC.keyboardUp), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CreateProfileVC.keyboardDown), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        nameField.resignFirstResponder()
        dobField.resignFirstResponder()
        countryField.resignFirstResponder()
        cityField.resignFirstResponder()
    }
    @objc func keyboardUp(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            //
            self.view.frame.origin.y = 130
            if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardRectangle = keyboardFrame.cgRectValue
                let keyboardHeight = keyboardRectangle.height
                self.view.frame.origin.y -= keyboardHeight
            }
        }
    }
    @objc func keyboardDown(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            self.view.frame.origin.y = 0
        }
    }
    @objc func datePickerValueChanged(sender: UIDatePicker){
    let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.medium
        dobField.text = formatter.string(from: sender.date)
    }
    @objc func donePicker() {
        countryField.resignFirstResponder()
        cityField.resignFirstResponder()
    }
    @objc func cancelPicker() {
        countryField.text = ""
        cityField.text = ""
        countryField.resignFirstResponder()
        cityField.resignFirstResponder()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == countryPicker {
    return countryArr.count
        } else {
        pickerView == cityPicker
        return CityArr.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == countryPicker {
        return countryArr[row]
        } else {
        pickerView == cityPicker
        return CityArr[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == countryPicker {
        countryField.text = countryArr[row]
        } else {
        pickerView == cityPicker
        cityField.text = CityArr[row]
            }
        }
    
// UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
