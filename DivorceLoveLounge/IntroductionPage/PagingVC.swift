//
//  PagingVC.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 17/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

var urlService = URLservices.sharedInstance

class PagingVC: UIViewController,UIScrollViewDelegate {
    
    
    var pageNumbr = Int()

    @IBOutlet var nextBtn: UIButton!
    
    @IBOutlet var scroollView: UIScrollView!{
        didSet{
            scroollView.delegate = self
        }
    }
    
    @IBAction func pageControlAction(_ sender: Any) {
        
//        if (pageNumbr == 5) {
//            nextBtn.isEnabled = true
//            nextBtn.setTitleColor(BlackColor, for: .selected)
//        } else {
//            nextBtn.isEnabled = false
//            nextBtn.setTitleColor(ligtGray, for: .normal)
//        }
    }
    

    @IBOutlet var pageControl: UIPageControl!
    
    @IBAction func nextAction(_ sender: Any) {
         UserDefaults.standard.set(true, forKey: "checkTutorial")
        
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(Vc, animated: true)
    }
    
    @IBAction func skipAction(_ sender: Any) {
        
        UserDefaults.standard.set(true, forKey: "checkTutorial")
        
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(Vc, animated: true)

    }

    var slides:[Slide] = [];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        scroollView.delegate = self
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        view.bringSubview(toFront: pageControl)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func createSlides() -> [Slide] {
        
        let slide1:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide1.imgView.image = UIImage(named: "register_img")
        slide1.titleLbl.text = "Register"
       // slide1.descLabel.text = "Lorem ipusm is a dummy text tool platform"
        
        let slide2:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide2.imgView.image = UIImage(named: "discover_img")
        slide2.titleLbl.text = "Discover"
       // slide2.descLabel.text = "Lorem ipusm is a dummy text tool platform"
        
        let slide3:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide3.imgView.image = UIImage(named: "favorites_img")
        slide3.titleLbl.text = "Favourites"
       // slide3.descLabel.text = "Lorem ipusm is a dummy text tool platform"
        
        let slide4:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide4.imgView.image = UIImage(named: "match_preferences_img")
        slide4.titleLbl.text = "Match Preferences"
        //slide4.descLabel.text = "Lorem ipusm is a dummy text tool platform"
        
        
        let slide5:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide5.imgView.image = UIImage(named: "message_img")
        slide5.titleLbl.text = "Message"
        //slide5.descLabel.text = "Lorem ipusm is a dummy text tool platform"
        
        let slide6:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide6.imgView.image = UIImage(named: "chat_img")
        slide6.titleLbl.text = "Chat"
        //slide6.descLabel.text = "Lorem ipusm is a dummy text tool platform"
        
        return [slide1, slide2, slide3, slide4, slide5, slide6]
    }
    
    
    func setupSlideScrollView(slides : [Slide]) {
        scroollView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        scroollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
        scroollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            scroollView.addSubview(slides[i])
        }
    }
    
    /*
     * default function called when view is scolled. In order to enable callback
     * when scrollview is scrolled, the below code needs to be called:
     * slideScrollView.delegate = self or
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        pageNumbr = pageControl.currentPage
  
        if (pageNumbr == 5) {
            nextBtn.isHidden = false
        } else {
            nextBtn.isHidden = true
        }
        
        if scrollView.contentOffset.y > 0 || scrollView.contentOffset.y < 0 {
                    scrollView.contentOffset.y = 0
            }
    }
 
}
