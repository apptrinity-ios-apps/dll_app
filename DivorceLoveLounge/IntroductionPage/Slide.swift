//
//  Slide.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 17/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import Foundation
import UIKit

class Slide : UIView {
    
    @IBOutlet var imgView: UIImageView!
    
    @IBOutlet var titleLbl: UILabel!
    
    @IBOutlet var descLabel: UITextView!
    
}
