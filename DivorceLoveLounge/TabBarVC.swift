//  TabBarVC.swift
//  DivorceLoveLounge
//  Created by S s Vali on 3/15/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
import UIKit
class TabBarVC: UITabBarController,UITabBarControllerDelegate {
    @IBOutlet var profileTabBar : UITabBar!
    @IBInspectable var defaultIndex: Int = 0
    var profileVC: ProfileVC!
    var chatVC: ChatVC!
    var favoriteVC: FavoriteVC!
    var notificationVC: NotificationVC!
    override func viewDidLoad() {
        super.viewDidLoad()
        

        
        selectedIndex = defaultIndex
        self.delegate = self
//        Do any additional setup after loading the view.
//        profileTabBar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha:1).cgColor
//        profileTabBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
//        profileTabBar.layer.shadowOpacity = 1.0
//        profileTabBar.layer.shadowRadius = 1.0
//        profileTabBar.layer.masksToBounds = true
//        profileTabBar.layer.shadowOffset = CGSize(width:0, height:0)
//        profileTabBar.layer.shadowRadius = 2
//        profileTabBar.layer.shadowColor = UIColor.black.cgColor;
//        profileTabBar.layer.shadowOpacity = 0.3
        self.tabBarController?.tabBar.layer.shadowColor = UIColor.black.cgColor
        self.tabBarController?.tabBar.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.tabBarController?.tabBar.layer.shadowRadius = 5
        self.tabBarController?.tabBar.layer.shadowOpacity = 1
        self.tabBarController?.tabBar.layer.masksToBounds = false
        
        
        
        profileTabBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        profileTabBar.layer.shadowOpacity = 1.0
        profileTabBar.layer.shadowRadius = 3
        profileTabBar.layer.masksToBounds = false
        profileTabBar.layer.shadowColor = UIColor.lightGray.cgColor
        
        
        
    addTopShadow()
    }
    func addTopShadow() {
        let frame = CGRect(x: 0, y: 50, width: self.view.frame.width, height: 0)
        let backgroundView = UIView(frame: frame)
        self.view.addSubview(backgroundView)
        
        //A linear Gradient Consists of two colours: top colour and bottom colour
        let topColor = UIColor.black
        let bottomColor = UIColor.clear
        
        //Add the top and bottom colours to an array and setup the location of those two.
        let gradientColors: [CGColor] = [topColor.cgColor, bottomColor.cgColor]
        let gradientLocations: [CGFloat] = [0.0, 1.0]
        
        //Create a Gradient CA layer
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        gradientLayer.locations = gradientLocations as [NSNumber]?
        gradientLayer.frame = frame
        gradientLayer.opacity = 0.2
        backgroundView.layer.insertSublayer(gradientLayer, at: 0)
        backgroundView.layer.zPosition = 100
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
       
    }
    
    override func viewDidLayoutSubviews()
    {
//        profileTabBar.frame = CGRect( x: 0, y: 20, width: self.view.frame.size.width, height: 50)
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        print("screen width is \(screenWidth)")
        
//        profileTabBar.frame = CGRect( x: 0, y: 20, width: self.view.frame.size.width, height: 60)
//        if(screenWidth == 320){
//         profileTabBar.frame = CGRect( x: 0, y: 20, width: self.view.frame.size.width, height: 60)
//        }else if(screenWidth == 414){
//            profileTabBar.frame = CGRect( x: 0, y: 20, width: self.view.frame.size.width, height: 60)
//        }else if(screenWidth == 375) {
//            profileTabBar.frame = CGRect( x: 0, y: 35, width: self.view.frame.size.width, height: 60)
//        }else if(screenWidth == 768) {
//            profileTabBar.frame = CGRect( x: 0, y: 20, width: self.view.frame.size.width, height: 60)
//        }else if(screenWidth == 1024) {
//            profileTabBar.frame = CGRect( x: 0, y: 20, width: self.view.frame.size.width, height: 60)
//        } else {
//            profileTabBar.frame = CGRect( x: 0, y: 20, width: self.view.frame.size.width, height: 60)
//        }
        
//        if (screenHeight == 812){
//            profileTabBar.frame = CGRect( x: 0, y: 60, width: self.view.frame.size.width, height: 60)
//        } else {
//
        
        
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            //sizeThatFits.height = UITabBar.height + window.safeAreaInsets.top

            profileTabBar.frame = CGRect( x: 0, y: window?.safeAreaInsets.top ?? 0 + 20, width: self.view.frame.size.width, height: 60)
        } else {
            profileTabBar.frame = CGRect( x: 0, y: 20, width: self.view.frame.size.width, height: 60)
        }
//        self.tabBarController?.view.frame
        
//        }

    }
}
extension UITabBar {
    static let height: CGFloat = 49.0
    
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        guard let window = UIApplication.shared.keyWindow else {
            return super.sizeThatFits(size)
        }
        var sizeThatFits = super.sizeThatFits(size)
        if #available(iOS 11.0, *) {
            sizeThatFits.height = UITabBar.height + window.safeAreaInsets.top
        } else {
            sizeThatFits.height = UITabBar.height
        }
        return sizeThatFits
    }
}
