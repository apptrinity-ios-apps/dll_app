//
//  ContactUsViewController.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/30/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate {
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    @IBOutlet weak var contactUsTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        contactUsTableView.delegate = self
        contactUsTableView.dataSource = self
        contactUsTableView.separatorColor = UIColor.clear
        
        
        self.contactUsTableView.register(UINib(nibName: "ContactTableViewCell", bundle: nil), forCellReuseIdentifier: "ContactTableViewCell")
        self.contactUsTableView.register(UINib(nibName: "AddressTableViewCell", bundle: nil), forCellReuseIdentifier: "AddressTableViewCell")

        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell", for: indexPath)as! ContactTableViewCell
            cell.nameField.delegate = self
            cell.mailField.delegate = self
            cell.phoneNumberField.delegate = self
            cell.messageTextView.delegate = self
            cell.messageTextView.text = "Message"
            cell.messageTextView.textColor = UIColor.lightGray
            cell.selectionStyle = .none
            cell.sendBtn.addTarget(self, action: #selector(sendBtnClick(button:)), for:.touchUpInside)            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddressTableViewCell", for: indexPath)as! AddressTableViewCell
            cell.selectionStyle = .none
            cell.backView.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
            cell.backView.layer.shadowColor = UIColor.black.cgColor
            cell.backView.layer.cornerRadius = 5
            cell.backView.layer.shadowOpacity = 1
            cell.backView.layer.shadowRadius = 2
            cell.backView.layer.masksToBounds = false
           
            
            
            return cell
        }
   }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 464
        } else {
            return 179
        }
    }
    
     @objc func sendBtnClick(button:UIButton) {
        if (ValidatedTextField()){
          contact()
        }
        else{
        }
    }
    
    func contact(){
        
        let cell = contactUsTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ContactTableViewCell
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            
            let parameters = ["name":cell?.nameField.text, "email":cell?.mailField.text,"phone":cell?.phoneNumberField.text, "msg":cell?.messageTextView.text] as! [String : String]
            
            print("contact parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:contactUs, params: parameters as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    let data = response["data"] as! String
                    self.themes.showAlert(title: "Alert", message: data, sender: self)
                    
             } else {
               let result = response["result"] as! String
               self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    func ValidatedTextField()-> Bool {
        let cell = contactUsTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ContactTableViewCell
        
        if (cell?.nameField.text == ""){
            self.themes.showAlert(title: "Alert", message: "Name is Mandatory", sender: self)
            // self.userNameField.becomeFirstResponder()
            return false
        } else if (cell?.mailField.text == "") {
            self.themes.showAlert(title: "Alert", message: "Mail is Mandatory", sender: self)
            // self.passwordField.becomeFirstResponder()
            return false
        } else if (cell?.messageTextView.text == "") {
            self.themes.showAlert(title: "Alert", message: "Message is Mandatory", sender: self)
            // self.passwordField.becomeFirstResponder()
            return false
        }
        return true
    }
    
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        self.view.endEditing(true)
        textField.resignFirstResponder();
        return true;
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Message"
            textView.textColor = UIColor.lightGray
        }
    }
}
