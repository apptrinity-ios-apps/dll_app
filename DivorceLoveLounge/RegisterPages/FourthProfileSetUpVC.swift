//
//  FourthProfileSetUpVC.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/20/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class FourthProfileSetUpVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var religionArr = ["CHRISTIAN","JEWISH","MUSLIM","HINDU","BUDDHIST","SIKH","SHINTO","OTHER","SPIRITUAL,BUT NOT RELIGIOUS","NEITHER RELIGIOUS NOR SPIRITUAL","BAHA'I","CAO DAI","CONFUCIANISM","JAINISM","CHRISTIAN SCIENCE","RASTAFARIANISM","TAOISM","TENRIKYO","UNITARIAN-UNIVERSALISM","SCIENTOLOGY","METAPHYSICAL","PAGAN","WICCAN","NEW AGE","PREFEER NOT TO SPECIFY"]
    var religionSelectedRow = Int()
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var religionTableView: UITableView!
    @IBOutlet weak var leftBtn: UIButton!
     let themes = Themes()
    @IBOutlet weak var rightBtn: UIButton!
    override func viewDidLoad() {
        
         self.nextBtn.layer.cornerRadius = 5
        super.viewDidLoad()
        religionSelectedRow = -1
//        nextBtn.backgroundColor = buttonHideColor
        rightBtn.setImage(#imageLiteral(resourceName: "right-arrow-hide"), for: .normal)
        rightBtn.isEnabled = false
        nextBtn.isEnabled = false

        religionTableView.delegate = self
        religionTableView.dataSource = self
        
        self.religionTableView.register(UINib(nibName: "myDataTableViewCell", bundle: nil), forCellReuseIdentifier: "myDataTableViewCell")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func skipBtnAction(_ sender: Any) {
        
        
        
        let usermail =  self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginUserName") as? String)
        let userpass =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginPassword") as? String)
        
        
        let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard1.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        vc.logIn(fromScreen: "signup", userEmail: usermail! as! String, userPassword: userpass! as! String)
        
        
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to skip this section?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { [] (_) in
            let dashBord = self.storyboard?.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
            self.navigationController?.pushViewController(dashBord, animated: true)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func nextBtnAction(_ sender: Any) {
        
//        leftBtn.setImage(#imageLiteral(resourceName: "left-arrow (16)"), for: .normal)
         leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        leftBtn.isEnabled = false
        let secondProfileVc = self.storyboard?.instantiateViewController(withIdentifier: "FifthProfileSetUpVC") as! FifthProfileSetUpVC
        self.navigationController?.pushViewController(secondProfileVc, animated: true)
    }
    @IBAction func nextBarBtn(_ sender: Any) {
//        leftBtn.setImage(#imageLiteral(resourceName: "left-arrow (16)"), for: .normal)
        leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        leftBtn.isEnabled = false
        let secondProfileVc = self.storyboard?.instantiateViewController(withIdentifier: "FifthProfileSetUpVC") as! FifthProfileSetUpVC
        self.navigationController?.pushViewController(secondProfileVc, animated: true)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        religionSelectedRow = indexPath.row
         profileDict["religion"] = religionArr[indexPath.row] as AnyObject
//        nextBtn.backgroundColor = darkBrownColor
        rightBtn.setImage(#imageLiteral(resourceName: "right-arrow-black"), for: .normal)
        rightBtn.isEnabled = true
        nextBtn.isEnabled = true
        
        religionTableView.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return religionArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myDataTableViewCell", for: indexPath) as! myDataTableViewCell
       
        
     
        tableView.separatorStyle = .none
        cell.dataLabel.text = religionArr[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52
    }
    
}
