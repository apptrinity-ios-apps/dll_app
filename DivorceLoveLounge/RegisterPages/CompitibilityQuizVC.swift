//
//  CompitibilityQuizVC.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/22/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//
import UIKit
class CompitibilityQuizVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate {
    var window: UIWindow?
    @IBOutlet weak var percentLabel: UILabel!
    @IBOutlet weak var percentSlider: CustomSlider1!
    let width = UIScreen.main.bounds.width
    let height = UIScreen.main.bounds.height
    var rowsWhichAreChecked = [NSIndexPath]()
    var optionType = String()
    var radioHint = Int()
    var question = String()
    var answerArray = Array<AnyObject>()
    var questionIncrement = Int()
    var data = [AnyObject]()
    var finalDict = [String:AnyObject]()
    var lastFinalArray = [AnyObject]()
     var percentHint = 0.0
    var dividedVal = 0.0
    @IBOutlet var noteLabelHeight: NSLayoutConstraint!
    
    @IBOutlet var noteLabel: UILabel!
    var selectedChecked = Bool()
    @IBOutlet weak var scrollBtn: UIButton!
 //  var lastFinalDict = Array<AnyObject>()
    var generalArr = ["WARM","CLEVER","DOMINANT","OUTGOING","QUARRELSOMW","STABLE","ENERGETIC","PREDICTABLE","AFFECTIONATE","INTELLIGENT","ATTRACTIVE","COMPASSIONATE","LOYAL","WITTY","SENSITIVE","GENEROUS","SENSUAL"]
    var physicalArr = ["STYLISH","ATHLETIC","OVERWEIGHT","PLAIN","HEALTHY","SEXY"]
    var happyArr = ["HAPPY"]
    var checkBoxSelection = [AnyObject]()
    var radioSelection = [AnyObject]()
    var selectedHint = Int()
    var generalSelectedRow = Int()
    var physicalSelectedRow = Int()
    var happySelectedRow = Int()
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet var scrollView: UIView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet var firstView: UIView!
    @IBOutlet weak var generalTableView: UITableView!
    var ratingArray = [AnyObject]()
    override func viewDidLoad(){
        super.viewDidLoad()
        
        scrollView.layer.cornerRadius = scrollView.frame.height/2
        nextBtn.layer.cornerRadius = 5
        percentSlider.value = 0
        percentLabel.text = "0%"
        percentSlider.setThumbImage(UIImage(), for: .normal)
        radioHint = -1
        questionLabel.sizeToFit()
        generalTableView.delegate = self
        generalTableView.dataSource = self
        selectedHint = 0
        generalTableView.allowsSelection = true
        generalTableView.allowsMultipleSelection = true
        FirstView(hidden: false)
//        rightBtn.isEnabled = false
//        nextBtn.isEnabled = false
//        nextBtn.backgroundColor = buttonHideColor
//        leftBtn.setImage(#imageLiteral(resourceName: "left-arrow (16)"), for: .normal)
         leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        self.generalTableView.register(UINib(nibName: "myDataTableViewCell", bundle: nil), forCellReuseIdentifier: "myDataTableViewCell")
        self.generalTableView.register(UINib(nibName: "radioQuizCell", bundle: nil), forCellReuseIdentifier: "radioQuizCell")
        self.generalTableView.register(UINib(nibName: "sliderQuizCell", bundle: nil), forCellReuseIdentifier: "sliderQuizCell")
                quizAPI()
        // Do any additional setup after loading the view.
        let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard1.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let mail = UserDefaults.standard.object(forKey: "getMail")
        let password = themes.checkNullValue(UserDefaults.standard.object(forKey: "getPassword")) as! String
        if(password == ""){
            
            let hint = self.themes.checkNullValue(UserDefaults.standard.object(forKey: "getSignUpHint") as AnyObject) as! String
            
            if(hint == "GMAIL"){
                vc.gmailLoginApi(email: themes.getuserMail()!, name: themes.getFirstName()!, type: "google")
            }else{
                vc.gmailLoginApi(email: themes.getuserMail()!, name: themes.getFirstName()!, type: "facebook")
            }
        }else{
        
            vc.logIn(fromScreen: "signup", userEmail: mail as! String, userPassword: password )
        }

        buttonEnableOrDisableforleftBtn(val: 0)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func FirstView(hidden: Bool){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136, 1334, 1920, 2208:
                print("iPhone 5 or 5S or 5C")
                firstView.frame = CGRect(x: 0, y: 120, width: Int(width), height: Int(self.view.frame.size.height - 170))
                firstView.alpha = 1
                firstView.isHidden = hidden
                self.view.addSubview(firstView)
            case 2436, 2688, 1792:
                print("iPhone X, XS")
                firstView.frame = CGRect(x: 0, y: 140, width: Int(width), height: Int(self.view.frame.size.height - 230))
                firstView.alpha = 1
                firstView.isHidden = hidden
                self.view.addSubview(firstView)
            default:
                print("Unknown")
            }
        }
    }
    func buttonEnableOrDisableforleftBtn(val : Int){
        if(val == 0){
//            leftBtn.setImage(#imageLiteral(resourceName: "left-arrow (16)"), for: .normal)
             leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        }else{
//            leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-black"), for: .normal)
             leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        }
    }
    func buttonEnableOrDisableforRightButton(val : Int){
        if(val == 0){
            rightBtn.setImage(#imageLiteral(resourceName: "right-arrow-hide"), for: .normal)
        }else{
            rightBtn.setImage(#imageLiteral(resourceName: "right-arrow-black"), for: .normal)
        }
    }
    func buttonEnableOrDisableforNextButton(val : Int){
        if(val == 0){
//          nextBtn.backgroundColor = buttonHideColor
        }else{
//            nextBtn.backgroundColor = darkBrownColor
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.answerArray.count < Int(scrollView.contentOffset.y)) {
            // did move up
            self.scrollView.isHidden = true
        } else if (self.answerArray.count > Int(scrollView.contentOffset.y)) {
            self.scrollView.isHidden = false
        } else {
            // didn't move
        }
    }
    @IBAction func skipBtnAction(_ sender: Any) {
        let usermail =  self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginUserName") as? String)
        let userpass =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginPassword") as? String)
        
        
        let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard1.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        vc.logIn(fromScreen: "signup", userEmail: usermail! as! String, userPassword: userpass! as! String)
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to skip this section?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { [] (_) in
            let imageUpload = UserDefaults.standard.object(forKey: "imageUpload")
            if(imageUpload is NSNull || imageUpload == nil){
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "EditProfilePicVC") as! EditProfilePicVC
                vc.fromScreen = "AppDelegate"
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func nextBtnAction(_ sender: Any) {
        self.generalTableView.scroll(to: .top, animated: true)
        percentHint = percentHint + dividedVal
        percentSlider.value = Float(percentHint)
        let val = Int(percentHint)
        percentLabel.text = String(val) + "%"
          var finalArray = [AnyObject]()
       rowsWhichAreChecked.removeAll()
          var subDict = [String:AnyObject]()
         questionIncrement = questionIncrement+1
        if(questionIncrement >= data.count){
            print("exceeded")
            
            if(selectedChecked){
                self.saveQuizAPI()
            }else{
                self.leftBtn.isEnabled = false
                UserDefaults.standard.set(true, forKey: "checkProfile4")
                //                    self.performSegue(withIdentifier: "next", sender: nil)
                let dashBord = self.storyboard?.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
                self.navigationController?.pushViewController(dashBord, animated: true)
            }
      
            
            
        }else{
            let obj = self.data[questionIncrement]
            self.optionType = obj["option_type"] as! String
            self.question = obj["question"] as! String
            self.answerArray = obj["answer"] as! Array
            self.questionLabel.text = self.question
            let val = questionIncrement - 1
             let obj1 = self.data[val]
            var ansArray1 = Array<AnyObject>()
             ansArray1 = obj1["answer"] as! Array
            let optionType = obj1["option_type"] as! String
            let questionID = obj1["question_id"] as! String
            if(optionType == "checkbox"){
                noteLabelHeight.constant = 17
                if(checkBoxSelection.count == 0){
                   //   self.themes.showAlert(title: "Message", message: "Please select atleast one.", sender: self)
                }else{
                    for i in 0...checkBoxSelection.count-1{
                        let obj4 = checkBoxSelection[i]
                        let id = obj4["answer_id"]
                        let answer = obj4["answer"]
                        subDict.updateValue(answer as AnyObject, forKey: id as! String)
                    }
                    selectedChecked = true
                    finalArray.append(subDict as AnyObject)
                    finalDict.updateValue(finalArray as AnyObject, forKey: questionID)
                    

                    
                 //  lastFinalDict.append(finalDict as AnyObject)
                     checkBoxSelection.removeAll()
                }
            }else if(optionType == "rating"){
                noteLabelHeight.constant = 17
                if(ansArray1.count == 0){
                    
                  //  self.themes.showAlert(title: "Message", message: "Please rate atleast one.", sender: self)
                }else{
                    for i in 0...ansArray1.count-1{
                        print(i)
                        
                        if(ratingArray[i] as! Int == 0){
                        }else{
                             selectedChecked = true
                            let obj3 = ansArray1[i]
                            let id = obj3["answer_id"]
                            subDict.updateValue(ratingArray[i] as AnyObject, forKey: id as! String)
                        }
                    }
                     finalArray.append(subDict as AnyObject)
                    finalDict.updateValue(finalArray as AnyObject, forKey: questionID)
                  //  lastFinalDict.append(finalDict as AnyObject)
                }

            }else if(optionType == "radio"){
                noteLabelHeight.constant = 0
                if(radioSelection.count == 0){
                    // self.themes.showAlert(title: "Message", message: "Please selct atleast one.", sender: self)
                }else{
                    for i in 0...radioSelection.count-1{
                        let obj5 = radioSelection[i]
                        let id = obj5["answer_id"]
                        let answer = obj5["answer"]
                        subDict.updateValue(answer as AnyObject, forKey: id as! String)
                        
                    }
                    selectedChecked = true
                    finalArray.append(subDict as AnyObject)
                    finalDict.updateValue(finalArray as AnyObject, forKey: questionID)
                   // lastFinalDict.append(finalDict as AnyObject)
                    radioSelection.removeAll()
                }
            }
            
            ratingArray.removeAll()
            
            if(self.optionType == "rating"){
                for j in 0...self.answerArray.count - 1{
                    self.ratingArray.append(0 as AnyObject)
                }
                print( self.ratingArray)
            }
            
            buttonEnableOrDisableforleftBtn(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
            //nextBtn.isEnabled = false
        }
        
        
        if(answerArray.count > 10){
            // scrollBtn.isHidden = false
            scrollView.isHidden = false
        }else{
            scrollView.isHidden = true
        }
        self.generalTableView.reloadData()
    }
    
    @IBAction func nextBarBtn(_ sender: Any) {
        leftBtn.isEnabled = true
        if(selectedHint == 0){
            FirstView(hidden: true)
            buttonEnableOrDisableforleftBtn(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
            selectedHint = 1
        }else if(selectedHint == 1){
            rightBtn.isEnabled = false
            nextBtn.isEnabled = false
            FirstView(hidden: true)
            buttonEnableOrDisableforleftBtn(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
            selectedHint = 2
        }else if(selectedHint == 2){
            rightBtn.isEnabled = false
            nextBtn.isEnabled = false
            FirstView(hidden: true)
            buttonEnableOrDisableforleftBtn(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
//            let secondProfileVc = self.storyboard?.instantiateViewController(withIdentifier: "ThirdProfileSetupVC") as! ThirdProfileSetupVC
//            self.navigationController?.pushViewController(secondProfileVc, animated: true)
        }
    }
    @IBAction func leftAction(_ sender: Any) {
        percentHint = percentHint - dividedVal
        percentSlider.value = Float(percentHint)
        let val = Int(percentHint)
        percentLabel.text = String(val) + "%"
        if(questionIncrement == 0){
            self.navigationController?.popViewController(animated: true)
        }else{
            questionIncrement = questionIncrement - 1
            let obj = self.data[questionIncrement]
            self.optionType = obj["option_type"] as! String
            self.question = obj["question"] as! String
            self.answerArray = obj["answer"] as! Array
            self.questionLabel.text = self.question
        }
        if(self.optionType == "rating"){
            for j in 0...self.answerArray.count - 1{
                self.ratingArray.append(0 as AnyObject)
            }
            print( self.ratingArray)
        }
        
        if(answerArray.count > 10){
            // scrollBtn.isHidden = false
            scrollView.isHidden = false
        }else{
            scrollView.isHidden = true
        }
        self.generalTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return answerArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.separatorColor = UIColor.clear
        if(optionType == "rating"){
           // generalTableView.allowsMultipleSelection = false
             let cell = tableView.dequeueReusableCell(withIdentifier: "sliderQuizCell", for: indexPath) as! sliderQuizCell
            cell.slider.tag = indexPath.row
            cell.slider.value = ratingArray[indexPath.row] as! Float
            cell.maxLabel.text = String(ratingArray[indexPath.row] as! Float)
            cell.slider.addTarget(self, action: #selector(CompitibilityQuizVC.updateMinMaxValue(sender:)), for: .allEvents)
           // cell.slider.value = 0.0
            cell.selectionStyle = .none
            let obj = answerArray[indexPath.row]
            
            let text = (obj["answer"] as! String)
            cell.nameLabel.text = text.capitalizingFirstLetter()
            return cell
        }else if(optionType == "checkbox"){
            let cell = tableView.dequeueReusableCell(withIdentifier: "myDataTableViewCell", for: indexPath) as! myDataTableViewCell
           
            // generalTableView.allowsMultipleSelection = true
            tableView.separatorStyle = .none
             let obj = answerArray[indexPath.row]
            let text = (obj["answer"] as! String)
            cell.dataLabel.text = text.capitalizingFirstLetter()
            cell.selectionStyle = .none
            
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                
                cell.backView.backgroundColor = UIColor.white
                cell.dataLabel.textColor = UIColor.black
                
           
            }else{
                
                cell.backView.backgroundColor = darkBrownColor
                cell.dataLabel.textColor = UIColor.white
               
            }
//            if(generalSelectedRow == indexPath.row){
//                cell.dataLabel.textColor = UIColor.white
//                cell.dataLabel.backgroundColor = darkBrownColor
//            }else{
//                cell.dataLabel.textColor = UIColor.black
//                cell.dataLabel.backgroundColor = UIColor.white
//            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "myDataTableViewCell", for: indexPath) as! myDataTableViewCell
            tableView.separatorStyle = .none
            // generalTableView.allowsMultipleSelection = false
          
            let obj = answerArray[indexPath.row]
            if(radioHint == indexPath.row){
                radioSelection.append(obj as AnyObject)
                cell.backView.backgroundColor = darkBrownColor
                cell.dataLabel.textColor = UIColor.white
            }else{
                radioSelection.removeAll()
                cell.backView.backgroundColor = UIColor.white
                cell.dataLabel.textColor = UIColor.black
            }
            let text = (obj["answer"] as! String)
            cell.dataLabel.text = text.capitalizingFirstLetter()
            cell.selectionStyle = .none
            return cell
        }
    }
    @objc func updateMinMaxValue(sender: UISlider!) {
        
        nextBtn.isEnabled = true
        buttonEnableOrDisableforleftBtn(val: 1)
        buttonEnableOrDisableforNextButton(val: 1)
        buttonEnableOrDisableforRightButton(val: 1)
       let value = Int(sender.value)
         ratingArray.remove(at: sender.tag)
        ratingArray.insert(Int(sender!.value) as AnyObject, at: sender.tag)
        let cell:sliderQuizCell = generalTableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! sliderQuizCell
        print(ratingArray)
        cell.maxLabel.text = String(value)
        print(value)
//        DispatchQueue.main.async {
//            self.kmsLabel.text = "\(value)"
//            print("Slider value = \(value)")
//        }
        
       // generalTableView.reloadData()
    }
    @objc func radioBtnClicked(button: UIButton) {
        
        buttonEnableOrDisableforNextButton(val: 1)
        radioHint = button.tag
        generalTableView.reloadData()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        nextBtn.isEnabled = true
        buttonEnableOrDisableforleftBtn(val: 1)
        buttonEnableOrDisableforNextButton(val: 1)
        buttonEnableOrDisableforRightButton(val: 1)
        radioHint = indexPath.row
        if(optionType == "checkbox"){
            let cell:myDataTableViewCell = tableView.cellForRow(at: indexPath) as! myDataTableViewCell
            // cross checking for checked rows
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
               
                rowsWhichAreChecked.append(indexPath as NSIndexPath)
                let obj = answerArray[indexPath.row]
                checkBoxSelection.append(obj)
                
                cell.backView.backgroundColor = darkBrownColor
                cell.dataLabel.textColor = UIColor.white
                print(checkBoxSelection)
            }else{
                
                cell.backView.backgroundColor = UIColor.clear
                cell.dataLabel.textColor = UIColor.black
                // cell.backgroundColor = UIColor.black
                // remove the indexPath from rowsWhichAreCheckedArray
                if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                  
                    rowsWhichAreChecked.remove(at: checkedItemIndex)
                    checkBoxSelection.remove(at: checkedItemIndex)
                     print(checkBoxSelection)
                }
            }
        }
        generalTableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath){
        if(optionType == "checkbox"){
            //            let cell = tableView.cellForRow(at: indexPath) as! myDataTableViewCell
            //            cell.accessoryType = UITableViewCellAccessoryType.none
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
        }
        generalTableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(optionType == "rating"){
            return 83
        }else{
            return 57
        }
    }
    
    func saveQuizAPI(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
             let urserId = themes.getUserId()
            lastFinalArray.append(finalDict as AnyObject)
            var dict = [String : AnyObject]()
            dict.updateValue(finalDict
                as AnyObject, forKey: "quiz")
            dict.updateValue(urserId as AnyObject, forKey: "user_id")
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:saveQuiz, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
//                    let SignVc = self.storyboard?.instantiateViewController(withIdentifier: "DiscoverVC") as! DiscoverVC
//                    self.navigationController?.pushViewController(SignVc, animated: true)
                    self.buttonEnableOrDisableforleftBtn(val: 0)
                    self.leftBtn.isEnabled = false
                    UserDefaults.standard.set(true, forKey: "checkProfile4")
//                    self.performSegue(withIdentifier: "next", sender: nil)
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "EditProfilePicVC") as! EditProfilePicVC
                    vc.fromScreen = "AppDelegate"
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                } else {
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    @IBAction func scrollAction(_ sender: Any) {
        if(self.answerArray.count == 0){
        }else{
            let indexPath = NSIndexPath(row: self.answerArray.count-1, section: 0)
            self.generalTableView.scrollToRow(at: indexPath as IndexPath, at: .bottom, animated: true)
        }
    }
    func quizAPI(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            // let urserId = themes.getUserId()
            let dict = [String : AnyObject]()
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:quiz, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    self.data = response["data"] as! [AnyObject]
                    self.dividedVal = Double(Float(100/self.data.count))
                    let obj = self.data[0]
                    self.optionType = obj["option_type"] as! String
                    self.question = obj["question"] as! String
                    self.answerArray = obj["answer"] as! Array
                    self.questionLabel.text = self.question
                    self.buttonEnableOrDisableforleftBtn(val: 1)
                    self.buttonEnableOrDisableforNextButton(val: 1)
                    self.buttonEnableOrDisableforRightButton(val: 1)
                    if(self.optionType == "rating"){
                        for j in 0...self.answerArray.count - 1{
                             self.ratingArray.append(0 as AnyObject)
                        }
                        print(self.ratingArray)
                    }
                    self.generalTableView.reloadData()
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
}
open class CustomSlider : UISlider {
    @IBInspectable open var trackWidth:CGFloat = 2 {
        didSet {setNeedsDisplay()}
    }
    
    override open func trackRect(forBounds bounds: CGRect) -> CGRect {
        let defaultBounds = super.trackRect(forBounds: bounds)
        return CGRect(
            x: defaultBounds.origin.x,
            y: defaultBounds.origin.y + defaultBounds.size.height/2 - trackWidth/2,
            width: defaultBounds.size.width,
            height: trackWidth
        )
    }
}
extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
