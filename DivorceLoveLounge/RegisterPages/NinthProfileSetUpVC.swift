//
//  NinthProfileSetUpVC.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 21/05/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//
import UIKit
class NinthProfileSetUpVC: UIViewController,UITextViewDelegate {

    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var thirdTextView: UITextView!
    @IBOutlet weak var secondTextView: UITextView!
    @IBOutlet weak var firsttextView: UITextView!
    @IBOutlet var percentLabel: UILabel!
    @IBOutlet var percentSlider: CustomSlider1!
    @IBOutlet weak var rightBtn: UIButton!
    
    @IBOutlet weak var continueBtn: UIButton!
     var percentHint = Float()
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()
        percentSlider.value = percentHint
        let val = Int(percentHint)
        percentLabel.text = String(val) + "%"
        percentSlider.setThumbImage(UIImage(), for: .normal)
        firsttextView.layer.cornerRadius = 10
        firsttextView.layer.borderWidth = 1
        firsttextView.layer.borderColor = UIColor.lightGray.cgColor
        
        secondTextView.layer.cornerRadius = 10
        secondTextView.layer.borderWidth = 1
        secondTextView.layer.borderColor = UIColor.lightGray.cgColor
        
        thirdTextView.layer.cornerRadius = 10
        thirdTextView.layer.borderWidth = 1
        thirdTextView.layer.borderColor = UIColor.lightGray.cgColor
        buttonEnableOrDisableforLeftButton(val: 1)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardUp), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardDown), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
       
        
        
        thirdTextView.textContainerInset =
            UIEdgeInsetsMake(10,10,2,10)
        secondTextView.textContainerInset =
            UIEdgeInsetsMake(10,10,2,10)
        firsttextView.textContainerInset =
            UIEdgeInsetsMake(10,10,2,10)
        
        firsttextView.text = "I am thankful for..."
        firsttextView.textColor = UIColor.lightGray
        
        secondTextView.text = "I am thankful for..."
        secondTextView.textColor = UIColor.lightGray
        
        thirdTextView.text = "I am thankful for..."
        thirdTextView.textColor = UIColor.lightGray

         continueBtn.layer.cornerRadius = 5
        leftButton.isEnabled = true
        // Do any additional setup after loading the view.
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    @objc func keyboardUp(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            //
            self.view.frame.origin.y = 150
            if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardRectangle = keyboardFrame.cgRectValue
                let keyboardHeight = keyboardRectangle.height
                self.view.frame.origin.y -= keyboardHeight
            }
        }
    }
    @objc func keyboardDown(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            self.view.frame.origin.y = 0
        }
    }
    func buttonEnableOrDisableforLeftButton(val : Int){
        if(val == 0){
//            leftButton.setImage(UIImage(named: "left-arrow (16)"), for: .normal)
            leftButton.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        }else{
//            leftButton.setImage(UIImage(named: "left-arrow-black"), for: .normal)
            leftButton.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        }
    }
    @IBAction func skipAction(_ sender: Any) {
        
        
        
      
        let usermail =  self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginUserName") as? String)
        let userpass =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginPassword") as? String)
        
        
        let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard1.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        vc.logIn(fromScreen: "signup", userEmail: usermail! as! String, userPassword: userpass! as! String)
        
        
        
        
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to skip this section?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { [] (_) in
            let checkTips = UserDefaults.standard.object(forKey: "checkTips")
            
            if checkTips is NSNull || checkTips == nil {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "TipsVC") as! TipsVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let imageUpload = UserDefaults.standard.object(forKey: "imageUpload")
                if(imageUpload is NSNull || imageUpload == nil){
                    
                    
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "EditProfilePicVC") as! EditProfilePicVC
                    vc.fromScreen = "AppDelegate"
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func leftAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func continueAction(_ sender: Any) {
        
        
        if(firsttextView.textColor == .lightGray){
             profileDict["thank1"] = "" as AnyObject?
        }else{
             profileDict["thank1"] = firsttextView.text as AnyObject?
        }
        
        if(secondTextView.textColor == .lightGray){
            profileDict["thank2"] = "" as AnyObject
        }else{
            profileDict["thank2"] = secondTextView.text as AnyObject?
        }
        
        if(thirdTextView.textColor == .lightGray){
            profileDict["thank3"] = "" as AnyObject
        }else{
            profileDict["thank3"] = thirdTextView.text as AnyObject?
        }
        
        
       
        print("final profile dict is \(profileDict)")
//          UserDefaults.standard.set(true, forKey: "checkProfile2")
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let profileVc = storyboard.instantiateViewController(withIdentifier: "MatchPreferenceVC") as! MatchPreferenceVC
//        self.navigationController?.pushViewController(profileVc , animated: true)
  
        profileSetUp()
    }
    
    func profileSetUp(){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let urserId = themes.getUserId()
           // let date =  profileDict["date"] as! String
            
//            let sep = date.split(separator: "-")
//            let year: String = String(sep[0])
//            let month: String = String(sep[1])
//            let day: String = String(sep[2])
            
     
            var dict = [String : AnyObject]()
            dict.updateValue(urserId as AnyObject, forKey: "user_id")
            dict.updateValue(profileDict["chilDrens"] as AnyObject, forKey: "children")
            dict.updateValue(profileDict["live"] as AnyObject, forKey: "city")
           
            dict.updateValue(profileDict["country"] as AnyObject, forKey: "ethnicity")
            dict.updateValue(profileDict["religion"] as AnyObject, forKey: "religion")
            dict.updateValue(profileDict["denomination"] as AnyObject, forKey: "mtongue_name")
            dict.updateValue(profileDict["education"] as AnyObject, forKey: "education")
            dict.updateValue(profileDict["occupation"] as AnyObject, forKey: "occupation")
            dict.updateValue(profileDict["income"] as AnyObject, forKey: "income")
            dict.updateValue(profileDict["smoke"] as AnyObject, forKey: "smoke")
            dict.updateValue(profileDict["drink"] as AnyObject, forKey: "drink")
             dict.updateValue(profileDict["cmHeight"] as AnyObject, forKey: "height_cms")
            dict.updateValue(profileDict["ftHeight"] as AnyObject, forKey: "height_ft")
            dict.updateValue(profileDict["inHeight"] as AnyObject, forKey: "height_inc")
             dict.updateValue(profileDict["passionate"] as AnyObject, forKey: "passionate")
             dict.updateValue(profileDict["leisue"] as AnyObject, forKey: "leisure_time")
             dict.updateValue(profileDict["thank1"] as AnyObject, forKey: "thankful_1")
             dict.updateValue(profileDict["thank2"] as AnyObject, forKey: "thankful_2")
            dict.updateValue("thank3" as AnyObject, forKey: "thankful_3")
  
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:profileSetup, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    self.buttonEnableOrDisableforLeftButton(val: 1)
                    self.leftButton.isEnabled = true
                   UserDefaults.standard.set(true, forKey: "checkProfile2")
                    
                   
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let profileVc = storyboard.instantiateViewController(withIdentifier: "MatchPreferenceVC") as! MatchPreferenceVC
                    self.navigationController?.pushViewController(profileVc , animated: true)
                    
                    
                   
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    @IBAction func rightAction(_ sender: Any) {
    }
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        secondTextView.resignFirstResponder()
        thirdTextView.resignFirstResponder()
        firsttextView.resignFirstResponder()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView)
    {
            if(textView.text == ""){
                textView.text = "I am thankful for..."
                textView.textColor = UIColor.lightGray
            }else{
                
            }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
