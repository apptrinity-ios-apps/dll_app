//
//  EighthProfileSetupVC.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 21/05/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//
import UIKit
class EighthProfileSetupVC: UIViewController,UITextViewDelegate {
    @IBOutlet weak var passionateTextview: UITextView!
    @IBOutlet weak var leisureTV: UITextView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet var secondView: UIView!
    @IBOutlet var firstView: UIView!
    @IBOutlet var percentSlider: CustomSlider1!
    @IBOutlet var percentLabel: UILabel!
    var percentHint = Float()
    var selectedHint = Int()
    
     let themes = Themes()
    let width = UIScreen.main.bounds.width
    override func viewDidLoad(){
        super.viewDidLoad()
        percentSlider.value = percentHint
        let val = Int(percentHint)
        percentLabel.text = String(val) + "%"
        percentSlider.setThumbImage(UIImage(), for: .normal)
        nextBtn.layer.cornerRadius = 5
        // Do any additional setup after loading the view.
        passionateTextview.delegate = self
        leisureTV.delegate = self
        passionateTextview.layer.borderWidth = 1
        passionateTextview.layer.borderColor = UIColor.lightGray.cgColor
        passionateTextview.layer.cornerRadius = 20
         leisureTV.layer.cornerRadius = 20
        leisureTV.layer.borderWidth = 1
        leisureTV.layer.borderColor = UIColor.lightGray.cgColor
        leisureTV.contentInset = UIEdgeInsets(top: 2, left: 20, bottom: 2, right: 10)
         passionateTextview.contentInset = UIEdgeInsets(top: 2, left: 20, bottom: 2, right: 10)
        
        leisureTV.textContainerInset =
            UIEdgeInsetsMake(10,10,2,10)
        passionateTextview.textContainerInset =
            UIEdgeInsetsMake(10,10,2,10)
        
        
        
        
        passionateTextview.text = "Be yourself and have fun..."
        passionateTextview.textColor = UIColor.lightGray
        
        leisureTV.text = "Be yourself and have fun..."
        leisureTV.textColor = UIColor.lightGray
        
        
        

        FirstView(hidden: false)
        buttonEnableOrDisableforLeftButton(val: 1)
        buttonEnableOrDisableforRightButton(val: 0)
        buttonEnableOrDisableforNextButton(val: 0)
        passionateTextview.becomeFirstResponder()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    func buttonEnableOrDisableforLeftButton(val : Int){
        if(val == 0){
//            leftBtn.setImage(UIImage(named: "left-arrow (16)"), for: .normal)
            leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        }else{
//            leftBtn.setImage(UIImage(named: "left-arrow-black"), for: .normal)
            leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        }
    }
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        passionateTextview.resignFirstResponder()
        leisureTV.resignFirstResponder()
    }
    func buttonEnableOrDisableforRightButton(val : Int){
        if(val == 0){
            rightBtn.setImage(#imageLiteral(resourceName: "right-arrow-hide"), for: .normal)
        }else{
            rightBtn.setImage(#imageLiteral(resourceName: "right-arrow-black"), for: .normal)
        }
    }
    func buttonEnableOrDisableforNextButton(val : Int){
        if(val == 0){
//            nextBtn.backgroundColor = buttonHideColor
        }else{
//            nextBtn.backgroundColor = darkBrownColor
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView)
    {
       
        if(textView == passionateTextview){
            if(textView.text == ""){
                textView.text = "Be yourself and have fun..."
                textView.textColor = UIColor.lightGray
                
                
            }else{
                nextBtn.isEnabled = true
                buttonEnableOrDisableforRightButton(val: 1)
                buttonEnableOrDisableforNextButton(val: 1)
                buttonEnableOrDisableforLeftButton(val: 1)
            }
        }else if(textView == leisureTV){
            if(textView.text == ""){
                textView.text = "Be yourself and have fun..."
                textView.textColor = UIColor.lightGray
               
            }else{
                
                
                nextBtn.isEnabled = true
                buttonEnableOrDisableforRightButton(val: 1)
                buttonEnableOrDisableforNextButton(val: 1)
                buttonEnableOrDisableforLeftButton(val: 1)
            }
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    @IBAction func skipAction(_ sender: Any) {
   
        let usermail =  self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginUserName") as? String)
        let userpass =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginPassword") as? String)
        
        let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard1.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        vc.logIn(fromScreen: "signup", userEmail: usermail! as! String, userPassword: userpass! as! String)
        
        
        
        
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to skip this section?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { [] (_) in
            let checkTips = UserDefaults.standard.object(forKey: "checkTips")
            
            if checkTips is NSNull || checkTips == nil {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "TipsVC") as! TipsVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let imageUpload = UserDefaults.standard.object(forKey: "imageUpload")
                if(imageUpload is NSNull || imageUpload == nil){
                    
                    
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "EditProfilePicVC") as! EditProfilePicVC
                    vc.fromScreen = "AppDelegate"
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func nextAction(_ sender: Any){
        percentHint = percentHint + 6
        percentSlider.value = percentHint
        let val = Int(percentHint)
        percentLabel.text = String(val) + "%"
        nextBtn.isEnabled = false
        if(selectedHint == 0){
            FirstView(hidden: true)
            SecondView(hidden: false)
            leftBtn.isEnabled = true
            buttonEnableOrDisableforLeftButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 1)
            buttonEnableOrDisableforRightButton(val: 0)
            selectedHint = 1
            leisureTV.becomeFirstResponder()
        }else if(selectedHint == 1){
           
            buttonEnableOrDisableforLeftButton(val: 1)
            leftBtn.isEnabled = false
            
            if(passionateTextview.textColor == .lightGray){
                profileDict["passionate"] = "" as AnyObject
            }else{
                profileDict["passionate"] = passionateTextview.text as AnyObject?
            }
            
            
            if(leisureTV.textColor == .lightGray){
                profileDict["leisue"] = "" as AnyObject
            }else{
                profileDict["leisue"] = leisureTV.text as AnyObject?
            }
            
            
            
            let secondProfileVc = self.storyboard?.instantiateViewController(withIdentifier: "NinthProfileSetUpVC") as! NinthProfileSetUpVC
            secondProfileVc.percentHint = percentHint
            self.navigationController?.pushViewController(secondProfileVc, animated: true)
        }
    }
    @IBAction func leftAction(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
  
    }
    @IBAction func rightAction(_ sender: Any) {
        if(selectedHint == 0){
            if(passionateTextview.text == ""){
                leftBtn.isEnabled = false
                nextBtn.isEnabled = false
            }else{
                rightBtn.isEnabled = true
                nextBtn.isEnabled = true
            }
            FirstView(hidden: true)
            SecondView(hidden: false)
            buttonEnableOrDisableforLeftButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
            selectedHint = 1
            leisureTV.becomeFirstResponder()
        }else if(selectedHint == 1){
            if(leisureTV.text == ""){
                leftBtn.isEnabled = false
                nextBtn.isEnabled = false
            }else{
                leftBtn.isEnabled = true
                nextBtn.isEnabled = true
            }
            buttonEnableOrDisableforLeftButton(val: 0)
            leftBtn.isEnabled = false
            let secondProfileVc = self.storyboard?.instantiateViewController(withIdentifier: "NinthProfileSetUpVC") as! NinthProfileSetUpVC
            self.navigationController?.pushViewController(secondProfileVc, animated: true)
        }
    }
    func FirstView(hidden: Bool){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136, 1334, 1920, 2208:
                print("iPhone 5 or 5S or 5C")
                firstView.frame = CGRect(x: 0, y: 110, width: Int(width), height: Int(self.view.frame.size.height - 160))
                firstView.alpha = 1
                firstView.isHidden = hidden
                self.view.addSubview(firstView)
                
            case 2436, 2688, 1792:
                print("iPhone X, XS")
                
                firstView.frame = CGRect(x: 0, y: 140, width: Int(width), height: Int(self.view.frame.size.height - 230))
                firstView.alpha = 1
                firstView.isHidden = hidden
                self.view.addSubview(firstView)
                
            default:
                print("Unknown")
            }
        }

    }
    func SecondView(hidden: Bool){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136, 1334, 1920, 2208:
                print("iPhone 5 or 5S or 5C")
                secondView.frame = CGRect(x: 0, y: 110, width: Int(width), height: Int(self.view.frame.size.height - 160))
                secondView.alpha = 1
                secondView.isHidden = hidden
                self.view.addSubview(secondView)
                
            case 2436, 2688, 1792:
                print("iPhone X, XS")
                
                secondView.frame = CGRect(x: 0, y: 140, width: Int(width), height: Int(self.view.frame.size.height - 230))
                secondView.alpha = 1
                secondView.isHidden = hidden
                self.view.addSubview(secondView)
                
            default:
                print("Unknown")
            }
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
