//
//  SecondRegistrationVC.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/17/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
import WebKit


import XMPPFramework
import MBProgressHUD


class SecondRegistrationVC: UIViewController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    var themes = Themes()
    var adsListArr = [AnyObject]()

      weak var xmppController: XMPPController!
    
    var urlService = URLservices.sharedInstance
    let aboutPicker = UIPickerView()
    var aboutArray = ["Online video (YouTube, Hulu, etc)","Press (News interview or Article)","Podcast","Online (Search, Banner & Email)","Radio","Television","Word of Mouth","Social Media (Facebook, Twitter etc)"]
    var gender = ""
    var lookingFor = ""
    var firstName = ""
    var dob = ""
    var zipCode = ""
    var state = ""
    var country = ""
    var statee = ""
    var city = ""
    @IBOutlet weak var mailView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var numberView: UIView!
    @IBOutlet weak var aboutView: UIView!
    @IBOutlet weak var mailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var phNumberField: UITextField!
    @IBOutlet weak var aboutField: UITextField!
    @IBOutlet weak var nextBtn: UIButton!
    
    
    @IBOutlet var userView: UIView!
    
    @IBOutlet var userField: UITextField!
    
    
    @IBOutlet var adBannerview: UIView!
    
    
    @IBOutlet var adsCollectionView: UICollectionView!
    
    @IBOutlet var adBannerHieght: NSLayoutConstraint!
    
    
    
    
    
    var pickerHint = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mailView.layer.cornerRadius = mailView.frame.size.height/2
        self.mailView.layer.borderWidth = 0
        self.mailView.layer.borderColor = UIColor.lightGray.cgColor
        self.numberView.layer.cornerRadius = numberView.frame.size.height/2
        self.numberView.layer.borderWidth = 0
        self.numberView.layer.borderColor = UIColor.lightGray.cgColor
        
        self.passwordView.layer.cornerRadius = passwordView.frame.size.height/2
        self.passwordView.layer.borderWidth = 0
        self.passwordView.layer.borderColor = UIColor.lightGray.cgColor
        
        self.userView.layer.cornerRadius = userView.frame.size.height/2
        self.userView.layer.borderWidth = 0
        self.userView.layer.borderColor = UIColor.lightGray.cgColor
        
        
        self.aboutView.layer.cornerRadius = aboutView.frame.size.height/2
        self.aboutView.layer.borderWidth = 0
        self.aboutView.layer.borderColor = UIColor.lightGray.cgColor
        self.nextBtn.layer.cornerRadius = nextBtn.frame.size.height/2
        
        aboutPicker.backgroundColor = UIColor.lightGray
        aboutPicker.dataSource = self
        aboutPicker.delegate = self
        self.mailField.delegate = self
        self.passwordField.delegate = self
        self.aboutField.delegate = self
        self.phNumberField.delegate = self
        self.userField.delegate = self
        
        
        // For Right side Padding
        mailField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: mailField.frame.height))
        mailField.leftViewMode = .always
        // For left side Padding
        mailField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: mailField.frame.height))
        mailField.rightViewMode = .always
        
        // For Right side Padding
        userField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: userField.frame.height))
        userField.leftViewMode = .always
        // For left side Padding
        userField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: userField.frame.height))
        userField.rightViewMode = .always
        
        
        
        // For Right side Padding
        passwordField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: passwordField.frame.height))
        passwordField.leftViewMode = .always
        // For left side Padding
        passwordField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: passwordField.frame.height))
        passwordField.rightViewMode = .always
        
        // For Right side Padding
        phNumberField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: phNumberField.frame.height))
        phNumberField.leftViewMode = .always
        // For left side Padding
        phNumberField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: phNumberField.frame.height))
        phNumberField.rightViewMode = .always
        
        // For Right side Padding
        aboutField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: aboutField.frame.height))
        aboutField.leftViewMode = .always
        // For left side Padding
        aboutField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: aboutField.frame.height))
        aboutField.rightViewMode = .always
        aboutField.attributedPlaceholder = NSAttributedString(string: "Where'd you hear about us?",
                                                              attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: CGFloat(28/255.0), green: CGFloat(37/255.0), blue: CGFloat(42/255.0), alpha: CGFloat(1.0) )])
        
        // Picker view Done Cancel Actions
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(SecondRegistrationVC.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(SecondRegistrationVC.cancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        aboutField.inputView = aboutPicker
        aboutField.inputAccessoryView = toolBar
        phNumberField.inputAccessoryView = toolBar
        
        // Keyboard Action
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.keyboardUp), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.keyboardDown), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view, typically from a nib.
        
        
        self.adsCollectionView.register(UINib(nibName: "AdsBannerCollecctionCell", bundle: nil), forCellWithReuseIdentifier: "AdsBannerCollecctionCell")
        
        
        adsListServiceApi(ScreenType: "Signup")
        
        mailField.text = self.themes.checkNullValue(self.themes.getuserMail()) as!String
        
        
        
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        mailField.resignFirstResponder()
        passwordField.resignFirstResponder()
        aboutField.resignFirstResponder()
        phNumberField.resignFirstResponder()
        userField.resignFirstResponder()
    }
    @objc func keyboardUp(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            //
            self.view.frame.origin.y = 150
            if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardRectangle = keyboardFrame.cgRectValue
                let keyboardHeight = keyboardRectangle.height
                self.view.frame.origin.y -= keyboardHeight
            }
        }
    }
    @objc func keyboardDown(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            self.view.frame.origin.y = 0
        }
    }
    @objc func donePicker() {
        aboutField.resignFirstResponder()
        phNumberField.resignFirstResponder()
    }
    @objc func cancelPicker() {
        aboutField.text = ""
        aboutField.resignFirstResponder()
        phNumberField.text = ""
        phNumberField.resignFirstResponder()
    }
    @IBAction func nextBtnAction(_ sender: Any) {
        self.view.endEditing(true)
        if (ValidatedTextField()){
            if  isValidEmail(testStr: mailField.text!)
            {
                register()
            } else {
                self.themes.showAlert(title: "Alert", message: "Please enter valid emailId", sender: self)
            }
        }
        else{
        }
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func ValidatedTextField()-> Bool {
        
        if (mailField.text == ""){
            self.themes.showAlert(title: "Alert", message: "Email is Mandatory", sender: self)
            // self.userNameField.becomeFirstResponder()
            return false
        }
        else if (passwordField.text == "") {
            self.themes.showAlert(title: "Alert", message: "Password is Mandatory", sender: self)
            // self.passwordField.becomeFirstResponder()
            return false
        }else if (passwordField.text!.count < 6) {
            self.themes.showAlert(title: "Alert", message: "Password length should be atleast 6 character or more", sender: self)
            // self.passwordField.becomeFirstResponder()
            return false
        }
        return true
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return aboutArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return aboutArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        aboutField.text = aboutArray[row]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func register(){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            var dict = [String : AnyObject]()
            dict.updateValue(gender as AnyObject, forKey: "gender")
            dict.updateValue(lookingFor as AnyObject, forKey: "looking_for")
            dict.updateValue(firstName as AnyObject, forKey: "first_name")
            dict.updateValue(dob as AnyObject, forKey: "dob")
            dict.updateValue(zipCode as AnyObject, forKey: "zipcode")
            dict.updateValue(state as AnyObject, forKey: "state")
            dict.updateValue(mailField.text! as AnyObject, forKey: "email")
            dict.updateValue(passwordField.text! as AnyObject, forKey: "password")
            dict.updateValue(phNumberField.text! as AnyObject, forKey: "phone_number")
            dict.updateValue("ravi" as AnyObject, forKey: "referal_from")
            dict.updateValue(country as AnyObject, forKey: "country")
            dict.updateValue("IOS" as AnyObject, forKey: "device_type")
            dict.updateValue(city as AnyObject, forKey: "city")
            dict.updateValue(userField.text! as AnyObject, forKey: "username")
            
            let devicetoken =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "kDeviceToken") )
            dict.updateValue(devicetoken as AnyObject, forKey: "device_token")
            
            UserDefaults.standard.set(mailField.text, forKey: "getMail")
            UserDefaults.standard.set(passwordField.text, forKey: "getPassword")
            
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:Register, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    UserDefaults.standard.set(nil, forKey: "checkTips")
                    UserDefaults.standard.set(true, forKey: "checkProfile1")
                    let userInfo = response["user_info"]as! [String : AnyObject]
                    let userMail = userInfo["email"]as! String
                    let userPassword = userInfo["password"]as! String
                    let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
                    
                    
                    UserDefaults.standard.set(userMail, forKey: "LoginUserName")
                    UserDefaults.standard.set(userPassword, forKey: "LoginPassword")
                    
                    UserDefaults.standard.synchronize()
                    
                    
                    let id = self.themes.checkNullValue(userInfo["id"])
                    let date_id = self.themes.checkNullValue(userInfo["date_id"])
                    
                    let gender = self.themes.checkNullValue(userInfo["gender"])
                    
                    let lookingfor = self.themes.checkNullValue(userInfo["looking_for"])
                    
                    let firstName = self.themes.checkNullValue(userInfo["first_name"])
                    
                    let email = self.themes.checkNullValue(userInfo["email"])
                    
                    let zipcode = self.themes.checkNullValue(userInfo["zipcode"])
                    
                    let state = self.themes.checkNullValue(userInfo["state"])
                    
                    let phNumber = self.themes.checkNullValue(userInfo["phone_number"])
                    
                    let email_notification = self.themes.checkNullValue(userInfo["email_notification_status"]) as! String
                    
                    let push_notification = self.themes.checkNullValue(userInfo["push_notification_status"]) as! String
                    
                    
                    
                    
                    let referal_from = self.themes.checkNullValue(userInfo["referal_from"])
                    
                    let children = self.themes.checkNullValue(userInfo["children"])
                    
                    let city = self.themes.checkNullValue(userInfo["city"])
                    let country = self.themes.checkNullValue(userInfo["country"])
                    
                    let dob1 = self.themes.checkNullValue(userInfo["dob"])
                    
                    
                    //   let dob = self.themes.dateFormateConverter(date: dob1 as! String)
                    
                    
                    
                    
                    let ethnicity = self.themes.checkNullValue(userInfo["ethnicity"])
                    
                    let religion = self.themes.checkNullValue(userInfo["religion"])
                    
                    let denomination = self.themes.checkNullValue(userInfo["denomination"])
                    
                    let education = self.themes.checkNullValue(userInfo["education"])
                    
                    let occupation = self.themes.checkNullValue(userInfo["occupation"])
                    
                    
                    
                    
                    
                    let income = self.themes.checkNullValue(userInfo["income"])
                    
                    let smoke = self.themes.checkNullValue(userInfo["smoke"])
                    
                    let drink = self.themes.checkNullValue(userInfo["drink"])
                    
                    let heightFt = self.themes.checkNullValue(userInfo["height_ft"])as? String
                    let heightIn = self.themes.checkNullValue(userInfo["height_inc"])as? String
                    let height = heightFt! + "." + heightIn!
                    
                    let passionate = self.themes.checkNullValue(userInfo["passionate"])
                    
                    let leisure_time = self.themes.checkNullValue(userInfo["leisure_time"])
                    
                    let thankful_1 = self.themes.checkNullValue(userInfo["thankful_1"])
                    
                    let thankful_2 = self.themes.checkNullValue(userInfo["thankful_2"])
                    
                    let thankful_3 = self.themes.checkNullValue(userInfo["thankful_3"])
                    
                    
                    
                    
                    
                    let partner_age_min = self.themes.checkNullValue(userInfo["partner_age_min"])
                    
                    let partner_age_max = self.themes.checkNullValue(userInfo["partner_age_max"])
                    
                    let partner_match_distance = self.themes.checkNullValue(userInfo["partner_match_distance"])
                    
                    let partner_match_search = self.themes.checkNullValue(userInfo["partner_match_search"])
                    
                    let partner_ethnicity = self.themes.checkNullValue(userInfo["partner_ethnicity"])
                    
                    let partner_religion = self.themes.checkNullValue(userInfo["partner_religion"])
                    
                    let partner_education = self.themes.checkNullValue(userInfo["partner_education"])
                    
                    let partner_occupation = self.themes.checkNullValue(userInfo["partner_occupation"])
                    
                    let partner_smoke = self.themes.checkNullValue(userInfo["partner_smoke"])
                    
                    let partner_denomination = self.themes.checkNullValue(userInfo["partner_denomination"])
                    
                    
                    
                    
                    
                    
                    
                    let partner_drink = self.themes.checkNullValue(userInfo["partner_drink"])
                    
                    let photo1 = self.themes.checkNullValue(userInfo["photo1"])
                    
                    let photo1_approve = self.themes.checkNullValue(userInfo["photo1_approve"])
                    
                    let photo2 = self.themes.checkNullValue(userInfo["photo2"])
                    
                    let photo2_approve = self.themes.checkNullValue(userInfo["photo2_approve"])
                    
                    let photo3 = self.themes.checkNullValue(userInfo["photo3"])
                    UserDefaults.standard.set(self.themes.checkNullValue(userInfo["state"]), forKey: "selectedState")
                    
                    
                    
                    
                    let photo3_approve = self.themes.checkNullValue(userInfo["photo3_approve"])
                    
                    let photo4 = self.themes.checkNullValue(userInfo["photo4"])
                    
                    let photo4_approve = self.themes.checkNullValue(userInfo["photo4_approve"])
                    
                    
                    
                    let photo5 = self.themes.checkNullValue(userInfo["photo5"])
                    
                    let photo5_approve = self.themes.checkNullValue(userInfo["photo5_approve"])
                    
                    let photo6 = self.themes.checkNullValue(userInfo["photo6"])
                    
                    let photo6_aprove = self.themes.checkNullValue(userInfo["photo6_aprove"])
                    
                    let signup_step = self.themes.checkNullValue(userInfo["signup_step"])
                    
                    let status = self.themes.checkNullValue(userInfo["status"])
                    
                    
                    
                    
                    
                    
                    
                    self.themes.saveUserID(id as? String)
                    
                    self.themes.savedate_id(date_id as? String)
                    
                    self.themes.saveGender(gender as? String)
                    
                    self.themes.savelooking_for(lookingfor as? String)
                    
                    self.themes.saveFirstName(firstName as? String)
                    
                    self.themes.saveuserEmail(email as? String)
                    
                    self.themes.saveEmailNotification(email_notification)
                    
                    self.themes.savePushNotification(push_notification)
                    
                    
                    
                    
                    self.themes.saveZipcode(zipcode as? String)
                    
                    self.themes.savestate(state as? String)
                    self.themes.saveMobileNumber(phNumber as? String)
                    
                    self.themes.savereferal_from(referal_from as? String)
                    
                    self.themes.saveChildren(children as? String)
                    
                    self.themes.savecity(city as? String)
                    
                    self.themes.savecountry(country as? String)
                    
                    
                    
                    
                    
                    
                    self.themes.savedob(dob1 as? String)
                    
                    self.themes.saveEthinicity(ethnicity as? String)
                    
                    self.themes.saveReligion(religion as? String)
                    
                    self.themes.savedenomination(denomination as? String)
                    
                    self.themes.saveEducation(education as? String)
                    
                    self.themes.saveoccupation(occupation as? String)
                    
                    self.themes.saveIncome(income as? String)
                    
                    self.themes.savesmoke(smoke as? String)
                    self.themes.savePartnerDenomination(partner_denomination as? String)
                    self.themes.savedrink(drink as? String)
                    
                    self.themes.saveHeight(height as? String)
                    
                    self.themes.savepassionate(passionate as? String)
                    
                    self.themes.saveleisure_time(leisure_time as? String)
                    
                    self.themes.savethankful_1(thankful_1 as? String)
                    
                    self.themes.savethankful_2(thankful_2 as? String)
                    
                    self.themes.savethankful_3(thankful_3 as? String)
                    
                    self.themes.savepartner_age_max(partner_age_max as? String)
                    
                    self.themes.savepartner_age_min(partner_age_min as? String)
                    
                    self.themes.savepartner_match_distance(partner_match_distance as? String)
                    
                    self.themes.savepartner_match_search(partner_match_search as? String)
                    
                    self.themes.savepartner_ethnicity(partner_ethnicity as? String)
                    
                    self.themes.savepartner_religion(partner_religion as? String)
                    
                    self.themes.savepartner_education(partner_education as? String)
                    
                    self.themes.savepartner_occupation(partner_occupation as? String)
                    
                    self.themes.savepartner_smoke(partner_smoke as? String)
                    
                    self.themes.savepartner_drink(partner_drink as? String)
                    
                    self.themes.savephoto1(photo1 as? String)
                    
                    self.themes.savephoto2(photo2 as? String)
                    
                    self.themes.savephoto3(photo3 as? String)
                    
                    self.themes.savephoto4(photo4 as? String)
                    
                    self.themes.savephoto5(photo5 as? String)
                    
                    self.themes.savephoto6(photo6 as? String)
                    
                    self.themes.savephoto1_approve(photo1_approve as? String)
                    
                    self.themes.savephoto2_approve(photo2_approve as? String)
                    
                    self.themes.savephoto3_approve(photo3_approve as? String)
                    self.themes.savephoto4_approve(photo4_approve as? String)
                    self.themes.savephoto5_approve(photo5_approve as? String)
                    self.themes.savephoto6_aprove(photo6_aprove as? String)
                    self.themes.savesignup_step(signup_step as? String)
                    
                    
                    
                    let xmppPassword = self.themes.checkNullValue(userInfo["password"])
                    let XmppUserName = self.themes.checkNullValue(userInfo["username"])
                    
                    self.themes.saveXmppUserName(XmppUserName as? String)
                    self.themes.savexmppPassword(xmppPassword as? String)
                    
                    
                    
                    // for xmpp connecting
                    var servername = UITextField()
                    servername.text = "divorcelovelounge.com"
                    var password = UITextField()
                    password.text = xmppPassword as? String
                    var username = UITextField()
                    username.text = "\(XmppUserName)@divorcelovelounge.com"
                    if let serverText = servername.text, (servername.text?.characters.count)! > 0 {
                        let auth = AuthenticationModel(jidString: username.text!, serverName: servername.text!, password: password.text!)
                        auth.save()
                    } else {
                        let auth = AuthenticationModel(jidString: username.text!, password: password.text!)
                        auth.save()
                    }
                    self.configureAndStartStream()
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    let vc = storyboard1.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    vc.logIn(fromScreen: "signup", userEmail: userMail, userPassword: userPassword)
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let profileVc = storyboard.instantiateViewController(withIdentifier: "ProfileSetUpVC") as! ProfileSetUpVC
                    
                    self.navigationController?.pushViewController(profileVc , animated: true)
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    func logIn(userMail: String, userPassword : String){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            
            let parameters = [ "email":userMail, "password":userPassword,
                               ] as [String : Any]
            
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:login, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    
                    
                    
                    let userInfo = response["user_info"]as! [String : AnyObject]
                    let id = self.themes.checkNullValue(userInfo["id"])
                    let date_id = self.themes.checkNullValue(userInfo["date_id"])
                    
                    let gender = self.themes.checkNullValue(userInfo["gender"])
                    
                    let lookingfor = self.themes.checkNullValue(userInfo["looking_for"])
                    
                    let firstName = self.themes.checkNullValue(userInfo["first_name"])
                    
                    let email = self.themes.checkNullValue(userInfo["email"])
                    
                    let zipcode = self.themes.checkNullValue(userInfo["zipcode"])
                    
                    let state = self.themes.checkNullValue(userInfo["state"])
                    
                    let phNumber = self.themes.checkNullValue(userInfo["phone_number"])
                    
                    let email_notification = self.themes.checkNullValue(userInfo["email_notification_status"]) as! String
                    
                    let push_notification = self.themes.checkNullValue(userInfo["push_notification_status"]) as! String
                    
                    
                    
                    
                    let referal_from = self.themes.checkNullValue(userInfo["referal_from"])
                    
                    let children = self.themes.checkNullValue(userInfo["children"])
                    
                    let city = self.themes.checkNullValue(userInfo["city"])
                    let country1 = self.themes.checkNullValue(userInfo["country"])
                    
                    let dob1 = self.themes.checkNullValue(userInfo["dob"])
                    
                    
                    //   let dob = self.themes.dateFormateConverter(date: dob1 as! String)
                    
                    
                    
                    
                    let ethnicity = self.themes.checkNullValue(userInfo["ethnicity"])
                    
                    let religion = self.themes.checkNullValue(userInfo["religion"])
                    
                    let denomination = self.themes.checkNullValue(userInfo["denomination"])
                    
                    let education = self.themes.checkNullValue(userInfo["education"])
                    
                    let occupation = self.themes.checkNullValue(userInfo["occupation"])
                    
                    
                    
                    
                    
                    let income = self.themes.checkNullValue(userInfo["income"])
                    
                    let smoke = self.themes.checkNullValue(userInfo["smoke"])
                    
                    let drink = self.themes.checkNullValue(userInfo["drink"])
                    
                    let heightFt = self.themes.checkNullValue(userInfo["height_ft"])as? String
                    let heightIn = self.themes.checkNullValue(userInfo["height_inc"])as? String
                    let height = heightFt! + "." + heightIn!
                    
                    let passionate = self.themes.checkNullValue(userInfo["passionate"])
                    
                    let leisure_time = self.themes.checkNullValue(userInfo["leisure_time"])
                    
                    let thankful_1 = self.themes.checkNullValue(userInfo["thankful_1"])
                    
                    let thankful_2 = self.themes.checkNullValue(userInfo["thankful_2"])
                    
                    let thankful_3 = self.themes.checkNullValue(userInfo["thankful_3"])
                    
                    
                    
                    
                    
                    let partner_age_min = self.themes.checkNullValue(userInfo["partner_age_min"])
                    
                    let partner_age_max = self.themes.checkNullValue(userInfo["partner_age_max"])
                    
                    let partner_match_distance = self.themes.checkNullValue(userInfo["partner_match_distance"])
                    
                    let partner_match_search = self.themes.checkNullValue(userInfo["partner_match_search"])
                    
                    let partner_ethnicity = self.themes.checkNullValue(userInfo["partner_ethnicity"])
                    
                    let partner_religion = self.themes.checkNullValue(userInfo["partner_religion"])
                    
                    let partner_education = self.themes.checkNullValue(userInfo["partner_education"])
                    
                    let partner_occupation = self.themes.checkNullValue(userInfo["partner_occupation"])
                    
                    let partner_smoke = self.themes.checkNullValue(userInfo["partner_smoke"])
                    
                    let partner_denomination = self.themes.checkNullValue(userInfo["partner_denomination"])
                    
                    
                    
                    
                    
                    
                    
                    let partner_drink = self.themes.checkNullValue(userInfo["partner_drink"])
                    
                    let photo1 = self.themes.checkNullValue(userInfo["photo1"])
                    
                    let photo1_approve = self.themes.checkNullValue(userInfo["photo1_approve"])
                    
                    let photo2 = self.themes.checkNullValue(userInfo["photo2"])
                    
                    let photo2_approve = self.themes.checkNullValue(userInfo["photo2_approve"])
                    
                    let photo3 = self.themes.checkNullValue(userInfo["photo3"])
                    UserDefaults.standard.set(self.themes.checkNullValue(userInfo["state"]), forKey: "selectedState")
                    
                    
                    
                    
                    let photo3_approve = self.themes.checkNullValue(userInfo["photo3_approve"])
                    
                    let photo4 = self.themes.checkNullValue(userInfo["photo4"])
                    
                    let photo4_approve = self.themes.checkNullValue(userInfo["photo4_approve"])
                    
                    
                    
                    let photo5 = self.themes.checkNullValue(userInfo["photo5"])
                    
                    let photo5_approve = self.themes.checkNullValue(userInfo["photo5_approve"])
                    
                    let photo6 = self.themes.checkNullValue(userInfo["photo6"])
                    
                    let photo6_aprove = self.themes.checkNullValue(userInfo["photo6_aprove"])
                    
                    let signup_step = self.themes.checkNullValue(userInfo["signup_step"])
                    
                    let status = self.themes.checkNullValue(userInfo["status"])
                    
                    
                    let xmppPassword = self.themes.checkNullValue(userInfo["password"])
                    let XmppUserName = self.themes.checkNullValue(userInfo["username"])
                    
                    self.themes.saveXmppUserName(XmppUserName as? String)
                    self.themes.savexmppPassword(xmppPassword as? String)
                    
                    
                    
                    // for xmpp connecting
                    var servername = UITextField()
                    servername.text = "divorcelovelounge.com"
                    var password = UITextField()
                    password.text = xmppPassword as? String
                    var username = UITextField()
                    username.text = "\(XmppUserName)@divorcelovelounge.com"
                    if let serverText = servername.text, (servername.text?.characters.count)! > 0 {
                        let auth = AuthenticationModel(jidString: username.text!, serverName: servername.text!, password: password.text!)
                        auth.save()
                    } else {
                        let auth = AuthenticationModel(jidString: username.text!, password: password.text!)
                        auth.save()
                    }
                    self.configureAndStartStream()
                    
                    
                    
                    
                    
                    self.themes.saveUserID(id as? String)
                    
                    self.themes.savedate_id(date_id as? String)
                    
                    self.themes.saveGender(gender as? String)
                    
                    self.themes.savelooking_for(lookingfor as? String)
                    
                    self.themes.saveFirstName(firstName as? String)
                    
                    self.themes.saveuserEmail(email as? String)
                    
                    self.themes.saveEmailNotification(email_notification)
                    
                    self.themes.savePushNotification(push_notification)
                    
                    
                    
                    
                    self.themes.saveZipcode(zipcode as? String)
                    
                    self.themes.savestate(state as? String)
                    self.themes.saveMobileNumber(phNumber as? String)
                    
                    self.themes.savereferal_from(referal_from as? String)
                    
                    self.themes.saveChildren(children as? String)
                    
                    self.themes.savecity(city as? String)
                    
                    self.themes.savecountry(country1 as? String)
                    
                    
                    
                    
                    
                    
                    self.themes.savedob(dob1 as? String)
                    
                    self.themes.saveEthinicity(ethnicity as? String)
                    
                    self.themes.saveReligion(religion as? String)
                    
                    self.themes.savedenomination(denomination as? String)
                    
                    self.themes.saveEducation(education as? String)
                    
                    self.themes.saveoccupation(occupation as? String)
                    
                    self.themes.saveIncome(income as? String)
                    
                    self.themes.savesmoke(smoke as? String)
                    self.themes.savePartnerDenomination(partner_denomination as? String)
                    self.themes.savedrink(drink as? String)
                    
                    self.themes.saveHeight(height as? String)
                    
                    self.themes.savepassionate(passionate as? String)
                    
                    self.themes.saveleisure_time(leisure_time as? String)
                    
                    self.themes.savethankful_1(thankful_1 as? String)
                    
                    self.themes.savethankful_2(thankful_2 as? String)
                    
                    self.themes.savethankful_3(thankful_3 as? String)
                    
                    self.themes.savepartner_age_max(partner_age_max as? String)
                    
                    self.themes.savepartner_age_min(partner_age_min as? String)
                    
                    self.themes.savepartner_match_distance(partner_match_distance as? String)
                    
                    self.themes.savepartner_match_search(partner_match_search as? String)
                    
                    self.themes.savepartner_ethnicity(partner_ethnicity as? String)
                    
                    self.themes.savepartner_religion(partner_religion as? String)
                    
                    self.themes.savepartner_education(partner_education as? String)
                    
                    self.themes.savepartner_occupation(partner_occupation as? String)
                    
                    self.themes.savepartner_smoke(partner_smoke as? String)
                    
                    self.themes.savepartner_drink(partner_drink as? String)
                    
                    self.themes.savephoto1(photo1 as? String)
                    
                    self.themes.savephoto2(photo2 as? String)
                    
                    self.themes.savephoto3(photo3 as? String)
                    
                    self.themes.savephoto4(photo4 as? String)
                    
                    self.themes.savephoto5(photo5 as? String)
                    
                    self.themes.savephoto6(photo6 as? String)
                    
                    self.themes.savephoto1_approve(photo1_approve as? String)
                    
                    self.themes.savephoto2_approve(photo2_approve as? String)
                    
                    self.themes.savephoto3_approve(photo3_approve as? String)
                    self.themes.savephoto4_approve(photo4_approve as? String)
                    self.themes.savephoto5_approve(photo5_approve as? String)
                    self.themes.savephoto6_aprove(photo6_aprove as? String)
                    self.themes.savesignup_step(signup_step as? String)
                    
                    
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    func configureAndStartStream() {
        self.xmppController = XMPPController.sharedInstance
        self.xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
        // TODO: revert to UIActivityIndicatorView.
       // let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
       // hud?.labelText = "Please wait"
        _ = self.xmppController.connect()
        
    }
    func checkMailAPI(email: String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            
            let dict = ["email":mailField.text!]
            
            print("change password parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:checkMail, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                }
                else if(success == "2"){
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    self.mailField.text = ""
                } else {
                    
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    
    func check_UserName_API(Username: String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            
            let dict = ["username":userField.text!]
            
            print("change password parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:checkUsername, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                }
                else if(success == "2"){
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    self.userField.text = ""
                } else {
                    
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    
    
    
    
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
        if textField == mailField {
            print("textFieldDidEndEditing")
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        if textField == mailField {
            print("textFieldShouldEndEditing")
            //let mail = self.themes.getuserMail()!
             textField.resignFirstResponder();
            checkMailAPI(email: mailField.text!)
        }
        else if (textField == userField){
             textField.resignFirstResponder();
            check_UserName_API(Username: userField.text!)
        }
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
    func adsListServiceApi(ScreenType:String){
        self.urlService.adslistServiceCall(ScreenType: ScreenType) { response in
            let success = response["status"] as! String
            if(success == "1"){
                
                let data = response["data"] as! [AnyObject]
                self.adsListArr = data
                self.adsCollectionView.delegate = self
                self.adsCollectionView.dataSource = self
                self.adsCollectionView.reloadData()
                
                
                
                
            }else{
                
            }
        }
        
    }
    func setTimer() {
        let _ = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(SecondRegistrationVC.autoScroll), userInfo: nil, repeats: true)
    }
    var x = 1
    @objc func autoScroll() {
        if self.x < self.adsListArr.count {
            let indexPath = IndexPath(item: x, section: 0)
            self.adsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.x = self.x + 1
        }else{
            self.x = 0
            self.adsCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.adsListArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: adBannerview.frame.size.width, height: adBannerview.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = adsCollectionView.dequeueReusableCell(withReuseIdentifier: "AdsBannerCollecctionCell", for: indexPath) as! AdsBannerCollecctionCell
        let data = self.adsListArr[indexPath.row] as! [String:AnyObject]
        let type = data["add_type"] as! String
        if type == "Custom" {
            cell.adsWebView.isHidden = true
            cell.adsImageView.isHidden = false
            let url = "\(adsUrl)\(data["add_image"] as! String)"
            cell.adsImageView.af_setImage(withURL: URL(string:url)!)
        }else{
            cell.adsWebView.isHidden = false
            cell.adsImageView.isHidden = true
            
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let data = self.adsListArr[indexPath.row] as! [String:AnyObject]
        let url = data["add_link"]
        if let url = URL(string: url as! String) {
            UIApplication.shared.open(url)
        }
    }
}
                                                                                                     
