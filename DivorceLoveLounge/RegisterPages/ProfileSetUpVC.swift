//
//  ProfileSetUpVC.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/18/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class ProfileSetUpVC: UIViewController {

    @IBOutlet var contentLabel: UILabel!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var nextPageBtn: UIButton!
    
  let themes = Themes()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileImgView.layer.cornerRadius = (profileImgView.frame.size.height)/2
        self.nextPageBtn.layer.cornerRadius = 5
        // Do any additional setup after loading the view.
        
        let name = UserDefaults.standard.object(forKey: "getUserName") as! String
        let gender = UserDefaults.standard.object(forKey: "getGender") as! String
        contentLabel.text = "Hi " + name + " Nice to meet you"
        if(gender == "Man"){
            profileImgView.image = UIImage(named: "profile_men")
            
        }else{
             profileImgView.image = UIImage(named: "profile_women")
        }
    }
    
    @IBAction func nextPageBtnAction(_ sender: Any) {
        
        
        let usermail =   UserDefaults.standard.object(forKey: "LoginUserName") as? String
        let userpass =    UserDefaults.standard.object(forKey: "LoginPassword") as? String
        
        
        let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard1.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        
        
        
        vc.logIn(fromScreen: "signup", userEmail: usermail!, userPassword: userpass!)
        
        
        
        let secondProfileVc = self.storyboard?.instantiateViewController(withIdentifier: "SecondProfileSetUpVC") as! SecondProfileSetUpVC
        self.navigationController?.pushViewController(secondProfileVc, animated: true)
    }
    
    @IBAction func skipBtnAction(_ sender: Any) {
        
        
       
        let usermail =  self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginUserName") as? String)
        let userpass =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginPassword") as? String)
        
        
        let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard1.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        vc.logIn(fromScreen: "signup", userEmail: usermail! as! String, userPassword: userpass! as! String)
        
        
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to skip this section?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { [] (_) in
            let checkTips = UserDefaults.standard.object(forKey: "checkTips")
            
            if checkTips is NSNull || checkTips == nil {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "TipsVC") as! TipsVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let imageUpload = UserDefaults.standard.object(forKey: "imageUpload")
                if(imageUpload is NSNull || imageUpload == nil){
                  
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "EditProfilePicVC") as! EditProfilePicVC
                    vc.fromScreen = "AppDelegate"
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
