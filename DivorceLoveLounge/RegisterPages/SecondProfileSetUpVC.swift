//
//  SecondProfileSetUpVC.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/18/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//
import UIKit
class SecondProfileSetUpVC: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource
{
    let datePicker = UIDatePicker()
    let width = UIScreen.main.bounds.width
    let themes = Themes()
    var citiesArray = Array<AnyObject>()
    var childArry = ["0","1","2","3","3+"]
    var childStr = String()
    var percentHint = 0.0
    var urlService = URLservices.sharedInstance
    var filterCityArray = Array<AnyObject>()
    var selectedRow = Int()
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var popView: UIView!
    @IBOutlet weak var hiddenView: UIView!
    @IBOutlet weak var popTableView: UITableView!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet var firstView: UIView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var nextBarBtn: UIButton!
    @IBOutlet weak var liveBtn: UIButton!
     let citiesPicker = UIPickerView()
    @IBOutlet weak var liveField: UITextField!
    @IBOutlet var secondView: UIView!    
    @IBOutlet weak var dobField: UITextField!
    @IBOutlet var thirdView: UIView!
    @IBOutlet weak var ftField: UITextField!
    @IBOutlet weak var inField: UITextField!
    @IBOutlet weak var cmField: UITextField!
    @IBOutlet var percentSlider: CustomSlider1!
    @IBOutlet var percentLabel: UILabel!
    @IBOutlet var fourthView: UIView!
    @IBOutlet weak var childField: UITextField!
    @IBOutlet var childTableView: UITableView!
    var selectedHint = Int() //for changing ui and buttons
    @IBOutlet weak var popDoneBn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        leftButton.isEnabled = false
        
        childTableView.layer.borderWidth = 1
        childTableView.layer.borderColor = UIColor.lightGray.cgColor
        childTableView.layer.cornerRadius = 5
        childTableView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        childTableView.layer.shadowOpacity = 1.0
        childTableView.layer.shadowRadius = 3
        childTableView.layer.masksToBounds = true
        childTableView.layer.shadowColor = UIColor.gray.cgColor
//        childTableView.clipsToBounds = true

        
        
        
        selectedHint  = 1
        //Picket View ToolBar
        selectedRow = -1
        percentSlider.value = 0
        percentLabel.text = "0%"
        percentSlider.setThumbImage(UIImage(), for: .normal)
        searchField.layer.cornerRadius = searchField.frame.height/2
        searchField.delegate = self
        searchField.layer.borderWidth = 1
        searchField.layer.borderColor = UIColor.lightGray.cgColor
        // For Right side Padding
        searchField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: searchField.frame.height))
        searchField.leftViewMode = .always
        // For left side Padding
        searchField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: searchField.frame.height))
        searchField.rightViewMode = .always
        
         popDoneBn.layer.cornerRadius = popDoneBn.frame.size.height/2
      //  leftButton.isEnabled = true
        childTableView.delegate = self
        childTableView.dataSource = self
        childTableView.layer.cornerRadius = 5
        childTableView.isHidden = true
        self.childTableView.register(UINib(nibName: "ParkPopUpCell", bundle: nil), forCellReuseIdentifier: "ParkPopUpCell")
        nextBtn.isEnabled = true
        self.popTableView.register(UINib(nibName: "countryTableCell", bundle: nil), forCellReuseIdentifier: "countryTableCell")
        searchField.autocapitalizationType = .sentences
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(SecondProfileSetUpVC.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(SecondProfileSetUpVC.cancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        dobField.inputView = datePicker
        dobField.inputAccessoryView = toolBar
        ftField.inputAccessoryView = toolBar
        inField.inputAccessoryView = toolBar
        cmField.inputAccessoryView = toolBar
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
//        self.view.addGestureRecognizer(tapGesture)
        
        // For First view        
        liveField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: liveField.frame.height))
        liveField.leftViewMode = .always
        liveField.layer.cornerRadius = liveField.frame.size.height/2
        liveField.layer.borderWidth = 1
        liveField.layer.borderColor = UIColor.lightGray.cgColor
        liveField.delegate = self
        
        // For Second View
        dobField.layer.cornerRadius = dobField.frame.size.height/2
        dobField.layer.borderWidth = 1
        dobField.layer.borderColor = UIColor.lightGray.cgColor
        dobField.delegate = self
           //Date Picker Action        
        datePicker.datePickerMode = UIDatePickerMode.date
        datePicker.addTarget(self, action: #selector(CreateProfileVC.datePickerValueChanged(sender:)), for: UIControlEvents.valueChanged)
        dobField.inputView = datePicker
        
        // For Third view
        ftField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: ftField.frame.height))
        ftField.leftViewMode = .always
        ftField.layer.cornerRadius = ftField.frame.size.height/2
        ftField.layer.borderWidth = 1
        ftField.layer.borderColor = UIColor.lightGray.cgColor
        ftField.delegate = self
        inField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: inField.frame.height))
        inField.leftViewMode = .always
        inField.layer.cornerRadius = inField.frame.size.height/2
        inField.layer.borderWidth = 1
        inField.layer.borderColor = UIColor.lightGray.cgColor
        inField.delegate = self
        cmField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: cmField.frame.height))
        cmField.leftViewMode = .always
        cmField.layer.cornerRadius = cmField.frame.size.height/2
        cmField.layer.borderWidth = 1
        cmField.layer.borderColor = UIColor.lightGray.cgColor
        cmField.delegate = self
        //For Fourth View
        childField.layer.cornerRadius = childField.frame.size.height/2
        childField.layer.borderWidth = 1
        childField.layer.borderColor = UIColor.lightGray.cgColor
        childField.delegate = self
        self.childField.inputView = UIView()
        self.childField.inputAccessoryView = UIView()
        FirstView(hidden: true)
        SecondView(hidden: true)
       ThirdView(hidden: false)
        nextBtn.layer.cornerRadius = 5
  
        //liveField.becomeFirstResponder()
        buttonEnableOrDisableforLeftButton(val: 1)
      buttonEnableOrDisableforRightButton(val: 1)
        buttonEnableOrDisableforNextButton(val: 1)
        // Do any additional setup after loading the view.
        
//        let hint = UserDefaults.standard.object(forKey: "getCountrySelectedHint")
//
//        if()
 
       
                
    }
    
    @objc func donePicker() {
        dobField.resignFirstResponder()
        let dateOfBirth = datePicker.date
        let today = NSDate()
        let gregorian = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        //let age = gregorian.components([.year], from: dateOfBirth, to: today as Date, options: [])
//        if age.year! < 18 {
//            self.themes.showAlert(title: "Message", message: "You should have 18+ for registration", sender: self)
//            dobField.text = ""
//        }
        ftField.resignFirstResponder()
        inField.resignFirstResponder()
        cmField.resignFirstResponder()
    }
    @objc func cancelPicker() {
        dobField.text = ""
        dobField.resignFirstResponder()
        ftField.resignFirstResponder()
        ftField.text = ""
        inField.resignFirstResponder()
        inField.text = ""
        cmField.resignFirstResponder()
        cmField.text = ""
    }
    
    @IBAction func childBtnAction(_ sender: Any) {
        childTableView.isHidden = false
    }
    
    
    @IBAction func skipAction(_ sender: Any) {
        
        
       
        let usermail =  self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginUserName") as? String)
        let userpass =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginPassword") as? String)
        
        
        let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard1.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        vc.logIn(fromScreen: "signup", userEmail: usermail! as! String, userPassword: userpass! as! String)
        
        
        
        
        
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to skip this section?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { [] (_) in
            
            let checkTips = UserDefaults.standard.object(forKey: "checkTips")
            
            if checkTips is NSNull || checkTips == nil {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "TipsVC") as! TipsVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                
                let imageUpload = UserDefaults.standard.object(forKey: "imageUpload")
                if(imageUpload is NSNull || imageUpload == nil){
                    
                    
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "EditProfilePicVC") as! EditProfilePicVC
                    vc.fromScreen = "AppDelegate"
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
             
            }
            
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == popTableView){
             return citiesArray.count
        }else{
             return childArry.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == popTableView){
            let cell = tableView.dequeueReusableCell(withIdentifier: "countryTableCell", for: indexPath)as! countryTableCell
            tableView.separatorColor = UIColor.clear
            cell.selectionStyle = .none
            cell.backView.layer.cornerRadius = cell.backView.frame.size.height/2
            cell.backView.layer.borderWidth = 1
            cell.backView.layer.borderColor = UIColor.lightGray.cgColor
            cell.countryLbl.text = citiesArray[indexPath.row] as! String
            if(selectedRow == indexPath.row){
                cell.backView.backgroundColor = darkBrownColor
                cell.countryLbl.textColor = UIColor.white
            }else{
                cell.backView.backgroundColor = UIColor.clear
                cell.countryLbl.textColor = UIColor.black
            }
            return cell
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "ParkPopUpCell", for: indexPath)as! ParkPopUpCell
        cell.selectionStyle = .none
        tableView.separatorColor = UIColor.clear
       cell.nameLabel.text = childArry[indexPath.row]
        return cell
            
        }
    }
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(popTableView == tableView){
            selectedRow = indexPath.row
            searchField.text = citiesArray[indexPath.row] as? String
        } else {
            childField.text = childArry[indexPath.row]
            childTableView.isHidden = true
            nextBtn.isEnabled = true
            buttonEnableOrDisableforRightButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 1)
            buttonEnableOrDisableforLeftButton(val: 1)
        }
        childTableView.isHidden = true
        childTableView.reloadData()
        popTableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(tableView == popTableView){
           return 50
        }else{
            return 44
        }
    }
 
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
         liveField.resignFirstResponder()
         dobField.resignFirstResponder()
         ftField.resignFirstResponder()
         inField.resignFirstResponder()
         cmField.resignFirstResponder()
         childField.resignFirstResponder()
    }

    func buttonEnableOrDisableforLeftButton(val : Int){
        
        if(val == 0){
//           leftButton.setImage(#imageLiteral(resourceName: "left-arrow (16)"), for: .normal)
           leftButton.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
            
        }else{
//             leftButton.setImage(#imageLiteral(resourceName: "left-arrow-black"), for: .normal)
             leftButton.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        }
 
    }
    
    func buttonEnableOrDisableforRightButton(val : Int){
        if(val == 0){
            nextBarBtn.setImage(#imageLiteral(resourceName: "right-arrow-hide"), for: .normal)
        }else{
            nextBarBtn.setImage(#imageLiteral(resourceName: "right-arrow-black"), for: .normal)
        }
    }
    func buttonEnableOrDisableforNextButton(val : Int){
        if(val == 0){
        
//           nextBtn.backgroundColor = buttonHideColor
        }else{
//            nextBtn.backgroundColor = darkBrownColor
        }
    }
    @objc func datePickerValueChanged(sender: UIDatePicker){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: 1, to: Date())
       
//        formatter.dateStyle = DateFormatter.Style.medium
        let myStringafd = formatter.string(from: sender.date)
        profileDict["date"] = myStringafd as AnyObject
        dobField.text = formatter.string(from: sender.date)
    }
   
    
    func FirstView(hidden :Bool){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136, 1334, 1920, 2208:
                print("iPhone 5 or 5S or 5C")
                firstView.frame = CGRect(x: 0, y: 110, width: Int(width), height: Int(self.view.frame.size.height - 160))
                firstView.alpha = 1
                firstView.isHidden = hidden
                self.view.addSubview(firstView)
                
            case 2436, 2688, 1792:
                print("iPhone X, XS")
                
                firstView.frame = CGRect(x: 0, y: 140, width: Int(width), height: Int(self.view.frame.size.height - 230))
                firstView.alpha = 1
                firstView.isHidden = hidden
                self.view.addSubview(firstView)
                
            default:
                print("Unknown")
            }
        }
        
    }
    func SecondView(hidden :Bool){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136, 1334, 1920, 2208:
                print("iPhone 5 or 5S or 5C")
                secondView.frame = CGRect(x: 0, y: 110, width: Int(width), height: Int(self.view.frame.size.height - 160))
                secondView.alpha = 1
                secondView.isHidden = hidden
                self.view.addSubview(secondView)
                
            case 2436, 2688, 1792:
                print("iPhone X, XS")
                
                secondView.frame = CGRect(x: 0, y: 140, width: Int(width), height: Int(self.view.frame.size.height - 230))
                secondView.alpha = 1
                secondView.isHidden = hidden
                self.view.addSubview(secondView)
                
            default:
                print("Unknown")
            }
        }
        
    }
    func ThirdView(hidden :Bool){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136, 1334, 1920, 2208:
                print("iPhone 5 or 5S or 5C")
                thirdView.frame = CGRect(x: 0, y: 110, width: Int(width), height: Int(self.view.frame.size.height - 160))
                thirdView.alpha = 1
                thirdView.isHidden = hidden
                self.view.addSubview(thirdView)
                
            case 2436, 2688, 1792:
                print("iPhone X, XS")
                
                thirdView.frame = CGRect(x: 0, y: 140, width: Int(width), height: Int(self.view.frame.size.height - 230))
                thirdView.alpha = 1
                thirdView.isHidden = hidden
                self.view.addSubview(thirdView)
            default:
                print("Unknown")
            }
        }
        
    }
    func FourthView(hidden :Bool){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136, 1334, 1920, 2208:
                print("iPhone 5 or 5S or 5C")
                fourthView.frame = CGRect(x: 0, y: 110, width: Int(width), height: Int(self.view.frame.size.height - 160))
                fourthView.alpha = 1
                fourthView.isHidden = hidden
                self.view.addSubview(fourthView)
                
            case 2436, 2688, 1792:
                print("iPhone X, XS")
                fourthView.frame = CGRect(x: 0, y: 140, width: Int(width), height: Int(self.view.frame.size.height - 230))
                fourthView.alpha = 1
                fourthView.isHidden = hidden
                self.view.addSubview(fourthView)
                
            default:
                print("Unknown")
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
//        self.view.endEditing(true)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
        if(textField == liveField){
            if(textField.text == ""){
                
            }else{
                buttonEnableOrDisableforRightButton(val: 1)
                buttonEnableOrDisableforNextButton(val: 1)
                buttonEnableOrDisableforLeftButton(val: 1)
            }
        }else if(textField == dobField){
            if(textField.text == ""){
                
            }else{
                nextBtn.isEnabled = true
                buttonEnableOrDisableforRightButton(val: 1)
                buttonEnableOrDisableforNextButton(val: 1)
                buttonEnableOrDisableforLeftButton(val: 1)
            }
        }else if(textField == ftField || textField == inField || textField == cmField){
            
            if((ftField.text == "" || inField.text == "") && cmField.text == ""){
                
            }else{
                 nextBtn.isEnabled = true
                buttonEnableOrDisableforRightButton(val: 1)
                buttonEnableOrDisableforNextButton(val: 1)
                buttonEnableOrDisableforLeftButton(val: 1)
            }
        }else if(textField == childField){
             nextBtn.isEnabled = true
            buttonEnableOrDisableforRightButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 1)
            buttonEnableOrDisableforLeftButton(val: 1)
        }
        
        
        if(textField == inField){
            
            // 1 feet = 30.48 cm
            // 1 inch = 2.54 cm
            let val = ftField.text
            let val1 = inField.text
            let feetValue = Double(val!) ?? 0.0
            
            let inchValue = Double(val1!) ?? 0.0
            let cmValue = (30.48 * feetValue) + (2.54 * inchValue)
            let cmVal = Int(cmValue)
            cmField.text = String(cmVal)
            
            
        }else if(textField == cmField){
          
//            val feet = Math.floor(cm / 2.54 / 12).toInt()
//            val inch = Math.floor(cm / 2.54 - feet * 12).toInt()
//            et_height_ft.setText(feet.toString())
//            et_height_inch.setText(inch.toString())
//
              let cm = cmField.text
            
            let cmDoubleVal = Double(cm!) ?? 0.0
            let feetValue = Int(cmDoubleVal/2.54/12)
            let feet = Double(feetValue)
            let inch1 = cmDoubleVal/2.54 - feet * 12
            let feetVal1 = Int(feetValue)
             let inchVal1 = Int(inch1)
             ftField.text = String(feetVal1)
            
             inField.text = String(inchVal1)
        }
       
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        return true;
    }
    
    
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == searchField {
            citiesArray.removeAll()
            citiesArray = filterCityArray.filter{ $0.localizedCaseInsensitiveContains(searchField.text!) }
            if citiesArray.count == 0 || searchField.text == ""{
                citiesArray = filterCityArray
            }
            else {
            }
            DispatchQueue.main.async {
                self.popTableView.reloadData()
            }
        }else if(textField == liveField){
        }else if(textField == cmField){
        }else if(textField == inField){
            
            if(textField.text == ""){
                
                
            }else{
                
                let textFieldText: NSString = (textField.text ?? "") as NSString
                let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
                
                var inch = Int()
                
                if(txtAfterUpdate == ""){
                    inch = 0
                }else{
                    inch = Int(txtAfterUpdate)!
                }
                
                
                
                
                
                
                
                if(inch >= 12){
                    let maxLength = 1
                    let currentString: NSString = textField.text! as NSString
                    let newString: NSString =
                        currentString.replacingCharacters(in: range, with: string) as NSString
                    return newString.length <= maxLength
                }else{
                    let maxLength = 2
                    let currentString: NSString = textField.text! as NSString
                    let newString: NSString =
                        currentString.replacingCharacters(in: range, with: string) as NSString
                    return newString.length <= maxLength
                    
               }
            }
       
        }
        
        
        
        else{
            
            
        let maxLength = 1
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
         self.view.endEditing(true)
        textField.resignFirstResponder();
        return true;
    }
    @IBAction func leftAction(_ sender: Any) {

        
        nextBtn.isEnabled = true
        percentHint = percentHint - 5
        percentSlider.value = Float(percentHint)
        let val = Int(percentHint)
        percentLabel.text = String(val) + "%"
        if(selectedHint == 0){
           
            self.navigationController?.popViewController(animated: true)
            buttonEnableOrDisableforLeftButton(val: 0)
            leftButton.isEnabled = false
        }else if(selectedHint == 1){
//                FirstView(hidden: false)
//                SecondView(hidden: true)
//                ThirdView(hidden: true)
//                buttonEnableOrDisableforLeftButton(val: 0)
//                leftButton.isEnabled = false
//                selectedHint = 0
            
            //            }
            self.navigationController?.popViewController(animated: true)
            buttonEnableOrDisableforLeftButton(val: 0)
            leftButton.isEnabled = false
           
        }else if(selectedHint == 2){
            //              if(ftField.text == "" || inField.text == "" || cmField.text == ""){
            //                nextBarBtn.isEnabled = false
            //                nextBtn.isEnabled = false
            //              }else{
            //                nextBarBtn.isEnabled = true
            //                nextBtn.isEnabled = true
            //            }
           
        
            
            FirstView(hidden: true)
           // SecondView(hidden: false)
            ThirdView(hidden: false)
            FourthView(hidden: true)
            buttonEnableOrDisableforLeftButton(val: 0)
            leftButton.isEnabled = false
            buttonEnableOrDisableforNextButton(val: 1)
            buttonEnableOrDisableforRightButton(val: 1)
            childField.becomeFirstResponder()
            selectedHint = 1
        }
        
        
    }
    @IBAction func rightAction(_ sender: Any) {
        
        
        if(selectedHint == 0){
            FirstView(hidden: true)
            SecondView(hidden: false)
            buttonEnableOrDisableforLeftButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
            selectedHint = 1
        }else if(selectedHint == 1){
            FirstView(hidden: true)
            SecondView(hidden: true)
            ThirdView(hidden: false)
            buttonEnableOrDisableforLeftButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
            selectedHint = 2
        }else if(selectedHint == 2){
            FirstView(hidden: true)
            SecondView(hidden: true)
            ThirdView(hidden: true)
            FourthView(hidden: false)
            buttonEnableOrDisableforLeftButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 1)
            buttonEnableOrDisableforRightButton(val: 1)
            selectedHint = 3
        }else if(selectedHint == 3){
            buttonEnableOrDisableforLeftButton(val: 0)
            profileDict["chilDrens"] = childField.text as AnyObject?
             profileDict["cmHeight"] =  cmField.text as AnyObject?
             profileDict["ftHeight"] =  ftField.text as AnyObject?
             profileDict["inHeight"] =  inField.text as AnyObject?
           // profileDict["inch"] =  inField.text as AnyObject?
            let secondProfileVc = self.storyboard?.instantiateViewController(withIdentifier: "ThirdProfileSetupVC") as! ThirdProfileSetupVC
            self.navigationController?.pushViewController(secondProfileVc, animated: true)
        }
    }
    @IBAction func nextAction(_ sender: Any) {
        nextBtn.isEnabled = false
        buttonEnableOrDisableforLeftButton(val: 1)
        leftButton.isEnabled = true
        if(selectedHint == 0){
            if(liveField.text == ""){
                self.themes.showAlert(title: "Alert", message: "Please enter city name", sender: self)
                 nextBtn.isEnabled = true
            }else{
                FirstView(hidden: true)
                SecondView(hidden: true)
                ThirdView(hidden: false)
                buttonEnableOrDisableforLeftButton(val: 1)
                buttonEnableOrDisableforNextButton(val: 0)
                buttonEnableOrDisableforRightButton(val: 0)
                ftField.becomeFirstResponder()
                selectedHint = 1
                percentHint = percentHint + 5
                percentSlider.value = Float(percentHint)
                let val = Int(percentHint)
                percentLabel.text = String(val) + "%"
            }
        }else if(selectedHint == 1){
//            if(dobField.text == ""){
//                nextBarBtn.isEnabled = false
//                nextBtn.isEnabled = false
//            }else{
//                nextBarBtn.isEnabled = true
//                nextBtn.isEnabled = true
//            }
            
            
            if(cmField.text == ""){
                           self.themes.showAlert(title: "Alert", message: "Please enter your height", sender: self)
                            nextBtn.isEnabled = true
                       }else{
            
            
            
            
            
            percentHint = percentHint + 10
            percentSlider.value = Float(percentHint)
            let val = Int(percentHint)
            percentLabel.text = String(val) + "%"
            
            
            
            
            FirstView(hidden: true)
            SecondView(hidden: true)
            ThirdView(hidden: true)
            FourthView(hidden: false)
            buttonEnableOrDisableforLeftButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 1)
            buttonEnableOrDisableforRightButton(val: 1)
            childField.becomeFirstResponder()
            selectedHint = 2
                
            }
        }else if(selectedHint == 2){
//              if(ftField.text == "" || inField.text == "" || cmField.text == ""){
//                nextBarBtn.isEnabled = false
//                nextBtn.isEnabled = false
//              }else{
//                nextBarBtn.isEnabled = true
//                nextBtn.isEnabled = true
//            }
            
            profileDict["chilDrens"] = childField.text as AnyObject?
            //profileDict["live"] = liveField.text as AnyObject?
            // profileDict["cmHeight"] =  cmField.text as AnyObject?
            //profileDict["inch"] =  inField.text as AnyObject?
            profileDict["cmHeight"] =  cmField.text as AnyObject?
            profileDict["ftHeight"] =  ftField.text as AnyObject?
            profileDict["inHeight"] =  inField.text as AnyObject?
            buttonEnableOrDisableforLeftButton(val: 0)
            leftButton.isEnabled = false
            let secondProfileVc = self.storyboard?.instantiateViewController(withIdentifier: "ThirdProfileSetupVC") as! ThirdProfileSetupVC
            secondProfileVc.percentHint = Float(20)
        
            self.navigationController?.pushViewController(secondProfileVc, animated: true)
        }

    }
    @IBAction func liveBtnClickAction(_ sender: Any) {
        FirstView(hidden: true)
        hiddenView.isHidden = false
        popView.isHidden = false
    }
    @IBAction func popUpCloseAction(_ sender: Any) {
         FirstView(hidden: false)
        hiddenView.isHidden = true
        popView.isHidden = true
        liveField.text = ""
    }
    @IBAction func popUpDoneAction(_ sender: Any) {
         FirstView(hidden: false)
        hiddenView.isHidden = true
        popView.isHidden = true
        liveField.text = searchField.text
    }
}
