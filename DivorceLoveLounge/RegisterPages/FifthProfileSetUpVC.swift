//
//  FifthProfileSetUpVC.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/20/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class FifthProfileSetUpVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
   let themes = Themes()
   var denominationArr = ["PEFEER NOT TO SPECIFY","ASSAMESE","BENGALI","GUJARATI","HINDI","KANNADA","MALAYALEE","MARATHI","MARWADI","ORIYA","PARIS","PUNJABI","SAIVISM","SHAKTISM","SINDHI","TAMIL","TELUGU","VAISHNAVISM"]
    var denominationSelectedRow = Int()
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet weak var denominationTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nextBtn.layer.cornerRadius = 5
      denominationSelectedRow = -1
        denominationTableView.delegate = self
        denominationTableView.dataSource = self
        
        self.denominationTableView.register(UINib(nibName: "myDataTableViewCell", bundle: nil), forCellReuseIdentifier: "myDataTableViewCell")
        // Do any additional setup after loading the view.
        
//        nextBtn.backgroundColor = buttonHideColor
        rightBtn.setImage(#imageLiteral(resourceName: "right-arrow-hide"), for: .normal)
        rightBtn.isEnabled = false
        nextBtn.isEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func skipBtnAction(_ sender: Any) {
        
        
       
        let usermail =  self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginUserName") as? String)
        let userpass =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginPassword") as? String)
        
        
        let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard1.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        vc.logIn(fromScreen: "signup", userEmail: usermail! as! String, userPassword: userpass! as! String)
        
        
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to skip this section?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { [] (_) in
            let dashBord = self.storyboard?.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
            self.navigationController?.pushViewController(dashBord, animated: true)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func nextBtnAction(_ sender: Any) {
//        leftBtn.setImage(#imageLiteral(resourceName: "left-arrow (16)"), for: .normal)
         leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        leftBtn.isEnabled = false
        let secondProfileVc = self.storyboard?.instantiateViewController(withIdentifier: "SixthProfileSetUpVC") as! SixthProfileSetUpVC
        self.navigationController?.pushViewController(secondProfileVc, animated: true)
    }
    
    @IBAction func nextBarBtn(_ sender: Any) {
        
//        leftBtn.setImage(#imageLiteral(resourceName: "left-arrow (16)"), for: .normal)
        leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        leftBtn.isEnabled = false
        let secondProfileVc = self.storyboard?.instantiateViewController(withIdentifier: "SixthProfileSetUpVC") as! SixthProfileSetUpVC
        self.navigationController?.pushViewController(secondProfileVc, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return denominationArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myDataTableViewCell", for: indexPath) as! myDataTableViewCell
     
        tableView.separatorStyle = .none
        cell.dataLabel.text = denominationArr[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        denominationSelectedRow = indexPath.row
         profileDict["denomination"] = denominationArr[indexPath.row] as AnyObject
        nextBtn.backgroundColor = darkBrownColor
        rightBtn.setImage(#imageLiteral(resourceName: "right-arrow-black"), for: .normal)
        rightBtn.isEnabled = true
        nextBtn.isEnabled = true
        denominationTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52
    }
}
