//
//  RegisterViewController.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/16/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate{
    var themes = Themes()
    var urlService = URLservices.sharedInstance
//    let countryPicker = UIPickerView()
    var search:String=""
    var popCountry = String()
    var selectedRow = Int()
    var filterd_customersArr = [String]()
    var stateID = String()
    let datePicker = UIDatePicker()
    var termsSelectionHint = Bool()
    var data = [AnyObject]()
    var filterCityArray = Array<AnyObject>()
    var citiesArray = Array<AnyObject>()
    
    
    
    
    
    var states1 = ["alaska",
                  "alabama",
                  "arkansas",
                  "american Samoa",
                  "arizona",
                  "california",
                  "colorado",
                  "connecticut",
                  "district of Columbia",
                  "delaware",
                  "florida",
                  "georgia",
                  "guam",
                  "hawaii",
                  "iowa",
                  "idaho",
                  "illinois",
                  "indiana",
                  "kansas",
                  "kentucky",
                  "louisiana",
                  "massachusetts",
                  "maryland",
                  "maine",
                  "michigan",
                  "minnesota",
                  "missouri",
                  "mississippi",
                  "montana",
                  "north Carolina",
                  "north Dakota",
                  "nebraska",
                  "new Hampshire",
                  "new Jersey",
                  "new Mexico",
                  "nevada",
                  "new York",
                  "ohio",
                  "oklahoma",
                  "oregon",
                  "pennsylvania",
                  "puerto Rico",
                  "rhode Island",
                  "south Carolina",
                  "south Dakota",
                  "tennessee",
                  "texas",
                  "utah",
                  "virginia",
                  "virgin Islands",
                  "vermont",
                  "washington",
                  "wisconsin",
                  "west Virginia",
                  "wyoming"]
    
    let statesIDS1 = [ "AK",
                   "AL",
                   "AR",
                   "AS",
                   "AZ",
                   "CA",
                   "CO",
                   "CT",
                   "DC",
                   "DE",
                   "FL",
                   "GA",
                   "GU",
                   "HI",
                   "IA",
                   "ID",
                   "IL",
                   "IN",
                   "KS",
                   "KY",
                   "LA",
                   "MA",
                   "MD",
                   "ME",
                   "MI",
                   "MN",
                   "MO",
                   "MS",
                   "MT",
                   "NC",
                   "ND",
                   "NE",
                   "NH",
                   "NJ",
                   "NM",
                   "NV",
                   "NY",
                   "OH",
                   "OK",
                   "OR",
                   "PA",
                   "PR",
                   "RI",
                   "SC",
                   "SD",
                   "TN",
                   "TX",
                   "UT",
                   "VA",
                   "VI",
                   "VT",
                   "WA",
                   "WI",
                   "WV",
                   "WY"]
    var selectedHint = Int()
    var countriesArray = Array<AnyObject>()
    var filterCountriesArray = Array<AnyObject>()
    var states = [String]()
    
    
    @IBOutlet weak var usernameView: UIView!
    
    @IBOutlet weak var userNmaeTFT: UITextField!
    @IBOutlet weak var usernameTFTHeight: NSLayoutConstraint!
    
    @IBOutlet weak var usernameviewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var usernameTopheight: NSLayoutConstraint!
    @IBOutlet weak var usernameTFTTopheight: NSLayoutConstraint!
    
    
    @IBOutlet var cityField: UITextField!
    @IBOutlet var cityBtn: UIButton!
    @IBOutlet var webOkBtn: UIButton!
    @IBOutlet var webTotalView: UIView!
    @IBOutlet var checkImageView: UIImageView!
    @IBOutlet weak var stateBtn: UIButton!
    @IBOutlet weak var selectTypeLabel: UILabel!
    @IBOutlet weak var fstBackView: UIView!
    @IBOutlet weak var fstGenderLbl: UILabel!
    @IBOutlet weak var fstDropDownImg: UIImageView!
    @IBOutlet weak var fstGenderBtn: UIButton!
    @IBOutlet weak var staeFild: UITextField!
    @IBOutlet weak var fstGenderView: UIView!
    @IBOutlet weak var secndBackView: UIView!
    @IBOutlet weak var secndGenderLbl: UILabel!
    @IBOutlet weak var secndDropDownImg: UIImageView!
    @IBOutlet weak var secndgenderBtn: UIButton!
    @IBOutlet weak var secondGenderView: UIView!
    @IBOutlet weak var fstNameView: UIView!
    @IBOutlet weak var zipCodeView: UIView!
    @IBOutlet weak var dobView: UIView!
    @IBOutlet weak var fstNameField: UITextField!
    @IBOutlet weak var zipCodeField: UITextField!
    @IBOutlet weak var dobField: UITextField!
    
    @IBOutlet weak var countryBackView: UIView!
    @IBOutlet weak var countryField: UITextField!  
    @IBOutlet weak var letsGoBtn: UIButton!
    
    @IBOutlet var hideView: UIView!
    @IBOutlet var countryPopView: UIView!
    @IBOutlet var countrySearchField: UITextField!
    @IBOutlet var countryTableView: UITableView!
    @IBOutlet var popDoneBtn: UIButton!
    @IBOutlet var webview: UIWebView!
    
    var registrationType = ""
    
    @IBAction func countryBtnClick(_ sender: Any) {
        selectTypeLabel.text = "Select your Country"
        selectedHint = 1
        popCountry = ""
        countryPopView.isHidden = false
        hideView.isHidden = false
        filterd_customersArr = countriesArray as! [String]
        states = filterCountriesArray as! [String]
        countryTableView.reloadData()
       // selectedRow = -1
        countrySearchField.text = ""
    }
    
    @IBAction func stateBtnClick(_ sender: Any) {
         selectTypeLabel.text = "Select your State"
        if(countryField.text == ""){
            themes.showAlert(title: "Alert", message: "Please select country before selecting state", sender: self)
        }else{
             popCountry = ""
            selectedHint = 2
            countryPopView.isHidden = false
            hideView.isHidden = false
            filterd_customersArr = states1
            states = states1
            countryTableView.reloadData()
            selectedRow = -1
            countrySearchField.text = ""
        }
        
    }
    
    @IBAction func popCancelAction(_ sender: Any) {
        countryPopView.isHidden = true
        hideView.isHidden = true
        countrySearchField.text = ""
        
    }
    
    @IBAction func popDoneAction(_ sender: Any) {
        
        if(selectedHint == 1){
            countryField.text = popCountry
        }else if(selectedHint == 2){
            staeFild.text = popCountry
        }else{
             cityField.text = popCountry
        }
       
        countryPopView.isHidden = true
        hideView.isHidden = true
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        countryField.text = "USA"
        popCountry = "USA"
        stateBtn.isHidden = false
        
        staeFild.delegate = self

        let hint = UserDefaults.standard.object(forKey: "getSignUpHint") as! String
        if(hint == "GMAIL"){
            fstNameField.isUserInteractionEnabled = false
            let name = UserDefaults.standard.object(forKey: "getFacebookGmailName")
            fstNameField.text = name as? String
        }else if(hint == "FACEBOOK"){
             let name = UserDefaults.standard.object(forKey: "getFacebookGmailName")
            fstNameField.isUserInteractionEnabled = false
            fstNameField.text = name as? String
        }else if(hint == "SIGNUP"){
            fstNameField.isUserInteractionEnabled = true
        }
 
        
        checkImageView.setImageColor(color: UIColor(red: 255/255.0, green: 222/255.0, blue: 121/255.0, alpha: 1.0))
        
        webTotalView.layer.cornerRadius = 5
        webOkBtn.layer.cornerRadius = 5
        
        countryPopView.isHidden = true
        hideView.isHidden = true
        selectedRow = -1
        filterd_customersArr = states
        countrySearchField.autocapitalizationType = .sentences
        self.countryTableView.register(UINib(nibName: "countryTableCell", bundle: nil), forCellReuseIdentifier: "countryTableCell")
        cityField.delegate = self
        countryTableView.delegate = self
        countryTableView.dataSource = self
        countryTableView.separatorColor = UIColor.clear
        fstGenderView.isHidden = true
        secondGenderView.isHidden = true
        self.fstGenderView.layer.cornerRadius = 5
        self.secondGenderView.layer.cornerRadius = 5
        self.fstBackView.layer.cornerRadius = fstNameView.frame.size.height/2
        self.fstBackView.layer.borderWidth = 0
        self.fstBackView.layer.borderColor = UIColor.lightGray.cgColor
        self.fstGenderBtn.layer.borderWidth = 0
        self.fstGenderBtn.layer.borderColor = UIColor.lightGray.cgColor
        self.fstGenderBtn.layer.cornerRadius = fstNameView.frame.size.height/2
        
        self.cityField.layer.cornerRadius = cityField.frame.size.height/2
        
        self.secndBackView.layer.cornerRadius = fstNameView.frame.size.height/2
        self.secndBackView.layer.borderWidth = 0
        self.secndBackView.layer.borderColor = UIColor.lightGray.cgColor
        self.secndgenderBtn.layer.borderWidth = 0
        self.secndgenderBtn.layer.borderColor = UIColor.lightGray.cgColor
        self.secndgenderBtn.layer.cornerRadius = fstNameView.frame.size.height/2
        
        self.fstNameView.layer.cornerRadius = fstNameView.frame.size.height/2
        self.fstNameView.layer.borderWidth = 0
        self.fstNameView.layer.borderColor = UIColor.lightGray.cgColor
        
        self.usernameView.layer.cornerRadius = usernameView.frame.size.height/2
        self.usernameView.layer.borderWidth = 0
        self.usernameView.layer.borderColor = UIColor.lightGray.cgColor
               
        
        self.dobView.layer.cornerRadius = dobView.frame.size.height/2
        self.dobView.layer.borderWidth = 0
        self.dobView.layer.borderColor = UIColor.lightGray.cgColor
        self.zipCodeView.layer.cornerRadius = zipCodeView.frame.size.height/2
        self.zipCodeView.layer.borderWidth = 0
        self.zipCodeView.layer.borderColor = UIColor.lightGray.cgColor
        self.fstNameField.layer.cornerRadius = fstNameField.frame.size.height/2
         self.staeFild.layer.cornerRadius = staeFild.frame.size.height/2
        self.dobField.layer.cornerRadius = dobField.frame.size.height/2
        self.zipCodeField.layer.cornerRadius = zipCodeField.frame.size.height/2
        self.userNmaeTFT.layer.cornerRadius = userNmaeTFT.frame.size.height/2

        self.countryBackView.layer.cornerRadius = countryBackView.frame.size.height/2
        self.countryBackView.layer.borderWidth = 0
        self.countryBackView.layer.borderColor = UIColor.lightGray.cgColor
        self.letsGoBtn.layer.cornerRadius = letsGoBtn.frame.size.height/2
//        countryPicker.backgroundColor = UIColor.lightGray
//        countryPicker.dataSource = self
//        countryPicker.delegate = self
        self.fstNameField.delegate = self
        self.zipCodeField.delegate = self
        self.countryField.delegate = self
          self.staeFild.delegate = self
        self.dobField.delegate = self
        self.userNmaeTFT.delegate = self
        
        // For Right side Padding
        fstNameField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: fstNameField.frame.height))
        fstNameField.leftViewMode = .always
        // For left side Padding
        fstNameField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: fstNameField.frame.height))
        fstNameField.rightViewMode = .always
        
        cityField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: cityField.frame.height))
        cityField.leftViewMode = .always
        // For left side Padding
        cityField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: fstNameField.frame.height))
        fstNameField.rightViewMode = .always
        
        // For Right side Padding
        dobField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: dobField.frame.height))
        dobField.leftViewMode = .always
        // For left side Padding
        dobField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: dobField.frame.height))
        dobField.rightViewMode = .always
        
        // For Right side Padding
        zipCodeField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: zipCodeField.frame.height))
        zipCodeField.leftViewMode = .always
        // For left side Padding
        zipCodeField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: zipCodeField.frame.height))
        zipCodeField.rightViewMode = .always
        
        // For Right side Padding
        countryField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: countryField.frame.height))
        countryField.leftViewMode = .always
        // For left side Padding
        countryField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: countryField.frame.height))
        countryField.rightViewMode = .always
        
        // For Right side Padding
               userNmaeTFT.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: userNmaeTFT.frame.height))
               userNmaeTFT.leftViewMode = .always
               // For left side Padding
               userNmaeTFT.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: userNmaeTFT.frame.height))
               userNmaeTFT.rightViewMode = .always
               
        
        staeFild.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: staeFild.frame.height))
        staeFild.leftViewMode = .always
        // For left side Padding
        staeFild.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: countryField.frame.height))
        staeFild.rightViewMode = .always
        
        countryPopView.layer.cornerRadius = 5
        countryTableView.layer.cornerRadius = 5
        countrySearchField.layer.cornerRadius = countrySearchField.frame.height/2
        countrySearchField.delegate = self
        countrySearchField.layer.borderWidth = 0
        countrySearchField.layer.borderColor = UIColor.lightGray.cgColor
        
        // For Right side Padding
        countrySearchField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: countrySearchField.frame.height))
        countrySearchField.leftViewMode = .always
        // For left side Padding
        countrySearchField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: countrySearchField.frame.height))
        countrySearchField.rightViewMode = .always
        popDoneBtn.layer.cornerRadius = popDoneBtn.frame.size.height/2
        // Picker view Done Cancel Actions
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        toolBar.sizeToFit()        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(RegisterViewController.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(RegisterViewController.cancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
//        countryField.inputView = countryPicker
//        countryField.inputAccessoryView = toolBar
        dobField.inputAccessoryView = toolBar
        zipCodeField.inputAccessoryView = toolBar
        cityField.inputAccessoryView = toolBar
        staeFild.inputAccessoryView = toolBar
        countrySearchField.inputAccessoryView = toolBar
        fstNameField.inputAccessoryView = toolBar
        
        //Date Picker Action
        
        datePicker.datePickerMode = UIDatePickerMode.date
        datePicker.addTarget(self, action: #selector(RegisterViewController.datePickerValueChanged(sender:)), for: UIControlEvents.valueChanged)
        dobField.inputView = datePicker
    
        
        getCountries()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        if registrationType == "social"{
                    usernameTFTHeight.constant = 40
                       usernameviewHeight.constant = 40
                       usernameTopheight.constant = 15
                       usernameTFTTopheight.constant = 15
        }else {
            usernameTFTHeight.constant = 0
            usernameviewHeight.constant = 0
            usernameTopheight.constant = 0
            usernameTFTTopheight.constant = 0
        
        }
        
        
        
        
    }
    @objc func donePicker() {

        dobField.resignFirstResponder()
        zipCodeField.resignFirstResponder()
        cityField.resignFirstResponder()
        staeFild.resignFirstResponder()
        countrySearchField.resignFirstResponder()
        fstNameField.resignFirstResponder()
        
        if (dobField.text?.isEmpty)!{
            
        } else {

            
        let dateOfBirth = datePicker.date
        let today = NSDate()
        let gregorian = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let age = gregorian.components([.year], from: dateOfBirth, to: today as Date, options: [])
        if age.year! < 18 {
            self.themes.showAlert(title: "Message", message: "You should have 18+ for registration", sender: self)
            dobField.text = ""
        }
        
    }
        
        
    }
    @objc func cancelPicker() {
        dobField.text = ""
        dobField.resignFirstResponder()
        zipCodeField.text = ""
        zipCodeField.resignFirstResponder()
        cityField.resignFirstResponder()
        cityField.text = ""
        staeFild.resignFirstResponder()
        staeFild.text = ""
        countrySearchField.resignFirstResponder()
        countrySearchField.text = ""
        fstNameField.resignFirstResponder()
      //  fstNameField.text = ""
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker){
        let formatter = DateFormatter()
//        formatter.dateStyle = DateFormatter.Style.short
        formatter.dateFormat = "MM-dd-yyyy"
        dobField.text = formatter.string(from: sender.date)
    }
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        fstNameField.resignFirstResponder()
        zipCodeField.resignFirstResponder()
        countryField.resignFirstResponder()
        staeFild.resignFirstResponder()
        dobField.resignFirstResponder()
    }
    @objc func keyboardUp(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            //
            self.view.frame.origin.y = 200
            if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardRectangle = keyboardFrame.cgRectValue
                let keyboardHeight = keyboardRectangle.height
                self.view.frame.origin.y -= keyboardHeight
            }
        }
    }
    @objc func keyboardDown(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            self.view.frame.origin.y = 0
        }
   
    }
    @objc func countrykeyboardUp(notification : NSNotification){
        
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            //
            self.view.frame.origin.y = 230
            if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardRectangle = keyboardFrame.cgRectValue
                let keyboardHeight = keyboardRectangle.height
                self.view.frame.origin.y -= keyboardHeight
            }
        }
    }
    @IBAction func fstGenderAction(_ sender: UIButton) {
        secondGenderView.isHidden = true
        secndDropDownImg.image = UIImage(named: "drop-down-arrow")
        if fstGenderView.isHidden ==  true {
            fstGenderView.isHidden = false
            fstDropDownImg.image = UIImage(named: "drop-up-arrow")
        } else{
            fstGenderView.isHidden = true
            fstDropDownImg.image = UIImage(named: "drop-down-arrow")
        }
    }
    @IBAction func manBtnAction(_ sender: UIButton) {
        fstGenderLbl.text = "Man"
        secndGenderLbl.text = "Woman"
        
//        secndGenderLbl.text = "Woman"
        fstGenderView.isHidden = true
        fstDropDownImg.image = UIImage(named: "drop-down-arrow")
    }
    
    @IBAction func womenBtnAction(_ sender: UIButton) {
        fstGenderLbl.text = "Woman"
        secndGenderLbl.text = "Man"
//        secndGenderLbl.text = "Man"
        fstGenderView.isHidden = true
        fstDropDownImg.image = UIImage(named: "drop-down-arrow")
    }
    
    @IBAction func secndGenderAction(_ sender: UIButton) {
        fstGenderView.isHidden = true
        fstDropDownImg.image = UIImage(named: "drop-down-arrow")
        if secondGenderView.isHidden ==  true {
            secondGenderView.isHidden = false
            secndDropDownImg.image = UIImage(named: "drop-up-arrow")
        } else{
            secondGenderView.isHidden = true
            secndDropDownImg.image = UIImage(named: "drop-down-arrow")
        }
    }
    @IBAction func ScndManAction(_ sender: UIButton) {
        secndGenderLbl.text = "Man"
        fstGenderLbl.text = "Woman"
//        fstGenderLbl.text = "Woman"
        secondGenderView.isHidden = true
        secndDropDownImg.image = UIImage(named: "drop-down-arrow")
    }
    
    @IBAction func scendWomenAction(_ sender: UIButton) {
        secndGenderLbl.text = "Woman"
        fstGenderLbl.text = "Man"
//         fstGenderLbl.text = "Man"
        secondGenderView.isHidden = true
        secndDropDownImg.image = UIImage(named: "drop-down-arrow")
    }
        
    @IBAction func letsGoAction(_ sender: Any) {
        if(fstGenderLbl.text == "" || secndGenderLbl.text == ""){
            self.themes.showAlert(title: "Message", message: "Please select gender", sender: self)
        }else if(fstNameField.text == ""){
             self.themes.showAlert(title: "Message", message: "Please enter your name", sender: self)
        }else if(dobField.text == ""){
            self.themes.showAlert(title: "Message", message: "Please enter your date of birth", sender: self)
        }else if(zipCodeField.text == ""){
            self.themes.showAlert(title: "Message", message: "Please enter your zipcode", sender: self)
        }else if(staeFild.text == ""){
            self.themes.showAlert(title: "Message", message: "Please select state", sender: self)
        }else if(countryField.text == ""){
            self.themes.showAlert(title: "Message", message: "Please select Country", sender: self)
        }else if(cityField.text == ""){
            self.themes.showAlert(title: "Message", message: "Please select City", sender: self)
        }else{
            
            
             UserDefaults.standard.set(fstGenderLbl.text!, forKey: "getGender")
            UserDefaults.standard.set(fstNameField.text!, forKey: "getUserName")
            
            
            
            let hint = UserDefaults.standard.object(forKey: "getSignUpHint") as! String
            if(hint == "GMAIL"){
                socialRegister(gender: fstGenderLbl.text!, lookingFor: secndGenderLbl.text!, dob: dobField.text!, zipCode: zipCodeField.text!, state: staeFild.text!, country: countryField.text!)
            }else if(hint == "FACEBOOK"){
               socialRegister(gender: fstGenderLbl.text!, lookingFor: secndGenderLbl.text!, dob: dobField.text!, zipCode: zipCodeField.text!, state: staeFild.text!, country: countryField.text!)
              
            }else if(hint == "SIGNUP"){
                
                if(termsSelectionHint){
                    let SecondRegVc = self.storyboard?.instantiateViewController(withIdentifier: "SecondRegistrationVC") as! SecondRegistrationVC
                    SecondRegVc.gender = fstGenderLbl.text!
                    SecondRegVc.lookingFor = secndGenderLbl.text!
                    SecondRegVc.firstName = fstNameField.text!
                    SecondRegVc.dob = dobField.text!
                    SecondRegVc.zipCode = zipCodeField.text!
                    SecondRegVc.city = cityField.text!
                    if(countryField.text == "USA"){
                        SecondRegVc.state = stateID
                    }else{
                        SecondRegVc.state = staeFild.text!
                    }
                    SecondRegVc.statee = staeFild.text!
                    SecondRegVc.country = countryField.text!
                    self.navigationController?.pushViewController(SecondRegVc, animated: true)
                }else{
                    self.themes.showAlert(title: "Alert", message: "Please accept terms and conditions", sender: self)
                }
               
            }
        }
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 1
//    }
//
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//            return states.count
//    }
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return states[row]
//    }
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//            countryField.text = states[row]
//
//
//         stateID = statesIDS[row]
//        UserDefaults.standard.set(statesIDS[row], forKey: "selectedState")
//
//
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterd_customersArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "countryTableCell", for: indexPath)as! countryTableCell
        cell.selectionStyle = .none
        cell.backView.layer.cornerRadius = cell.backView.frame.size.height/2
        cell.backView.layer.borderWidth = 1
        cell.backView.layer.borderColor = UIColor.lightGray.cgColor
        cell.countryLbl.text = filterd_customersArr[indexPath.row]
        if(selectedRow == indexPath.row){
            cell.backView.backgroundColor = darkBrownColor
            cell.countryLbl.textColor = UIColor.white
        }else{
            cell.backView.backgroundColor = UIColor.clear
            cell.countryLbl.textColor = UIColor.black
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let cell = tableView.dequeueReusableCell(withIdentifier: "countryTableCell", for: indexPath) as! countryTableCell
        self.view.endEditing(true)
        popCountry = filterd_customersArr[indexPath.row]
        selectedRow = indexPath.row
         countrySearchField.text = popCountry
        if(selectedHint == 1){
            if(popCountry == "USA"){
                stateBtn.isHidden = false
                cityBtn.isHidden = false
            }else{
                  stateBtn.isHidden = true
                  cityBtn.isHidden = true
            }
        }else if(selectedHint == 2){
            let index = states.index(of: popCountry)
            stateID = statesIDS1[index ?? 0]
             getCitiesAPI()
        }else{
            
        }
        countryTableView.reloadData()
    }
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        if textField == countrySearchField {
            print("some")
            NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.countrykeyboardUp), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.keyboardDown), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
            //            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
            //            self.view.addGestureRecognizer(tapGesture)
        }
        else {
            // Keyboard Action
            NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.keyboardUp), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.keyboardDown), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
            //            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
            //            self.view.addGestureRecognizer(tapGesture)
        }
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        
        
        
        
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        
        if textField == userNmaeTFT {
            textField.resignFirstResponder();
            check_UserName_API(Username: userNmaeTFT.text!)
        }
        
        
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == countrySearchField {
            filterd_customersArr.removeAll()
            
            filterd_customersArr = states.filter{ $0.localizedCaseInsensitiveContains(countrySearchField.text!) }
            if filterd_customersArr.count == 0 || countrySearchField.text == ""{
                
                filterd_customersArr = states
            }
            else {
                
            }
            DispatchQueue.main.async {
                self.countryTableView.reloadData()
            }
            countryTableView.reloadData()
        } else if(textField == zipCodeField){
            
             var maxLength = 5
            if countryField.text == "USA"{
                maxLength = 5
            }else if countryField.text == "India"{
                maxLength = 6
            }
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
    func check_UserName_API(Username: String){
           let networkRechability = urlService.connectedToNetwork()
           if(networkRechability){
               themes.showActivityIndicator(uiView: self.view)
               
               let dict = ["username":userNmaeTFT.text!]
               
               print("change password parameters is \(dict)")
               urlService.serviceCallPostMethodWithParams(url:checkUsername, params: dict as Dictionary<String, AnyObject>) { response in
                   print(response)
                   self.themes.hideActivityIndicator(uiView: self.view)
                   let success = response["status"] as! String
                   if(success == "1"){
                       
                   }
                   else if(success == "2"){
                       let result = response["result"] as! String
                       self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                       self.userNmaeTFT.text = ""
                   } else {
                       
                   }
               }
           } else {
               self.themes.hideActivityIndicator(uiView: self.view)
               themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
           }
       }
       
    
    
    
    func socialRegister(gender:String,lookingFor:String,dob:String,zipCode:String,state:String,country:String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            var dict = [String : AnyObject]()
            dict.updateValue(themes.getUserId() as AnyObject, forKey: "user_id")
            dict.updateValue(gender as AnyObject, forKey: "gender")
            dict.updateValue(lookingFor as AnyObject, forKey: "looking_for")
            dict.updateValue(dob as AnyObject, forKey: "dob")
            dict.updateValue(zipCode as AnyObject, forKey: "zipcode")
            dict.updateValue(state as AnyObject, forKey: "state")
            dict.updateValue(country as AnyObject, forKey: "country")
            dict.updateValue(cityField.text as AnyObject, forKey: "city")
            dict.updateValue(userNmaeTFT.text! as AnyObject, forKey: "username")
            dict.updateValue("IOS" as AnyObject, forKey: "device_type")
            let devicetoken =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "kDeviceToken") )
            dict.updateValue(devicetoken as AnyObject, forKey: "device_token")
            
             
            
            
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:socialregister, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                     UserDefaults.standard.set(nil, forKey: "checkTips")
                    UserDefaults.standard.set(true, forKey: "checkProfile1")
                   
                    let data = response["data"] as! [String:AnyObject]
                    let xmppPassword = self.themes.checkNullValue(data["password"])
                    let XmppUserName = self.themes.checkNullValue(data["username"])
                                      //
                                          self.themes.saveXmppUserName(XmppUserName as? String)
                                          self.themes.savexmppPassword(xmppPassword as? String)
                                      //
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let profileVc = storyboard.instantiateViewController(withIdentifier: "ProfileSetUpVC") as! ProfileSetUpVC
                    self.navigationController?.pushViewController(profileVc , animated: true)
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        }else{
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    @IBAction func termsAndConditionsAction(_ sender: Any) {
        
        let url = URL (string: "https://divorcelovelounge.com/appTermsOfUse")
        let requestObj = URLRequest(url: url!)
        self.webview.loadRequest(requestObj)
        
        self.themes.showActivityIndicator(uiView: self.view)
        webTotalView.isHidden = false
        hideView.isHidden = false
        
    }
    @IBAction func termsCheckAction(_ sender: Any) {
        
        if(termsSelectionHint){
            termsSelectionHint = false
            checkImageView.image = UIImage(named: "checkBoxUnselect")
             checkImageView.setImageColor(color: UIColor(red: 255/255.0, green: 222/255.0, blue: 121/255.0, alpha: 1.0))
        }else{
            termsSelectionHint = true
            checkImageView.image = UIImage(named: "checkBoxSelect")
             checkImageView.setImageColor(color: UIColor(red: 255/255.0, green: 222/255.0, blue: 121/255.0, alpha: 1.0))
        }
        
    }
    func getCountries(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            urlService.serviceCallGetMethod(url: getCountriess, completionHandler: { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    self.data = (response["data"] as AnyObject) as! [AnyObject]
                    
                    for i in 0...self.data.count - 1{
                        
                        
                        let obj = self.data[i]
                        
                        let name = obj["country_name"] as! String
                        
                        
                        if(name == "USA"){
                            self.selectedRow = i
                        }
                        self.countriesArray.append(name as AnyObject)
                        self.filterCountriesArray.append(name as AnyObject)
                    }
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            })
        }else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    @IBAction func cityBtnAction(_ sender: Any) {
        selectTypeLabel.text = "Select your City"
        selectedRow = -1
        selectedHint = 3
        if(staeFild.text == ""){
          self.themes.showAlert(title: "Alert", message: "Please select State before selected the City", sender: self)
        }else{
            filterd_customersArr = citiesArray as! [String]
            states = citiesArray as! [String]
            hideView.isHidden = false
            countryPopView.isHidden = false
             countrySearchField.text = ""
            countryTableView.reloadData()
        }
    }
    
    func getCitiesAPI(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            
            var dict = [String : AnyObject]()
            
            
            dict.updateValue(stateID as AnyObject, forKey: "state_code")
            
            
            //            let parameters = ["gender":gender, "looking_for":lookingFor,"first_name":firstName, "zipcode":zipCode,"state":state,"email":mailField.text!,"password":passwordField.text!] as [String : String]
            
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:getCities, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    let array = response["data"] as! [AnyObject]
                    
                     self.cityBtn.isHidden = false
                    for i in 0...array.count-1{
                        let obj = array[i]
                        self.citiesArray.append(obj["city_name"] as AnyObject)
                        //self.states.append((obj["city_name"] as AnyObject) as! String)
                    }
                    
                    self.countryTableView.reloadData()
                }else if(success == "2"){
                    
                    self.cityBtn.isHidden = true
                    
                    
                }else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    
    @IBAction func webOkAction(_ sender: Any) {
        webTotalView.isHidden = true
        hideView.isHidden = true
        
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
      //  self.themes.showActivityIndicator(uiView: self.view)
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.themes.hideActivityIndicator(uiView: self.view)
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.themes.hideActivityIndicator(uiView: self.view)
    }
}
