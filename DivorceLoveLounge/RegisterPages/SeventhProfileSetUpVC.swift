//
//  SeventhProfileSetUpVC.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/20/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
class SeventhProfileSetUpVC: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    let width = UIScreen.main.bounds.width
    var incomeArr = ["<$20,000","$20,000-$40,000","$40,000-$60,000","$60,000-$125,000","$125,000-$250,000"]
    var smokeArr = ["NEVER","SOCIALLY","ONCE A WEEK","FEW TIMES A WEEK","DAILY"]
    var drinkArr = ["NEVER","ON SPECIAL OCCASIONS","ONCE A WEEK","FEW TIMES A WEEK","DAILY"]
    let themes = Themes()
    var selectedHint = Int()
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet var firstView: UIView!
    @IBOutlet weak var occupationField: UITextField!
    @IBOutlet var secondView: UIView!
    @IBOutlet weak var incomeTableView: UITableView!
    @IBOutlet var thirdView: UIView!
    @IBOutlet weak var smokeTableView: UITableView!
    @IBOutlet var fourthView: UIView!
    @IBOutlet weak var drinkTableView: UITableView!
    
    var incomeSelectedRow = Int()
    var smokeSelectedRow = Int()
    var drinkSelectedRow = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        incomeSelectedRow = -1
        smokeSelectedRow = -1
        drinkSelectedRow = -1
        selectedHint = 0
        incomeTableView.delegate = self
        incomeTableView.dataSource = self
        smokeTableView.delegate = self
        smokeTableView.dataSource = self
        drinkTableView.delegate = self
        drinkTableView.dataSource = self
        self.incomeTableView.register(UINib(nibName: "myDataTableViewCell", bundle: nil), forCellReuseIdentifier: "myDataTableViewCell")
        self.smokeTableView.register(UINib(nibName: "myDataTableViewCell", bundle: nil), forCellReuseIdentifier: "myDataTableViewCell")
        self.drinkTableView.register(UINib(nibName: "myDataTableViewCell", bundle: nil), forCellReuseIdentifier: "myDataTableViewCell")
        // For First view
        occupationField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: occupationField.frame.height))
        occupationField.leftViewMode = .always
        occupationField.layer.cornerRadius = 5
        occupationField.layer.borderWidth = 1
        occupationField.layer.borderColor = UIColor.lightGray.cgColor
        occupationField.delegate = self
        FirstView(hidden: false)
        occupationField.becomeFirstResponder()
        buttonEnableOrDisableforleftBtn(val: 1)
        buttonEnableOrDisableforRightButton(val: 0)
        buttonEnableOrDisableforNextButton(val: 0)
         nextBtn.layer.cornerRadius = 5
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
//        self.view.addGestureRecognizer(tapGesture)
        
        
        
        // Do any additional setup after loading the view.
    }
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        occupationField.resignFirstResponder()
       
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        self.view.endEditing(true)
        textField.resignFirstResponder();
        return true;
    }
    func buttonEnableOrDisableforleftBtn(val : Int){
        
        if(val == 0){
//            leftBtn.setImage(#imageLiteral(resourceName: "left-arrow (16)"), for: .normal)
            leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        }else{
//            leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-black"), for: .normal)
            leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        }
        
    }
    
    func buttonEnableOrDisableforRightButton(val : Int){
        
        if(val == 0){
            rightBtn.setImage(#imageLiteral(resourceName: "right-arrow-hide"), for: .normal)
//            rightBtn.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
            
        }else{
            rightBtn.setImage(#imageLiteral(resourceName: "right-arrow-black"), for: .normal)
//            rightBtn.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        }
        
    }
    func buttonEnableOrDisableforNextButton(val : Int){
        if(val == 0){
 //           nextBtn.backgroundColor = buttonHideColor
        }else{
            nextBtn.backgroundColor = darkBrownColor
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
        if(textField == occupationField){
            if(textField.text == ""){
            }else{
                buttonEnableOrDisableforRightButton(val: 1)
                buttonEnableOrDisableforNextButton(val: 1)
                buttonEnableOrDisableforleftBtn(val: 1)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func skipAction(_ sender: Any) {
        
        
        
        
        
        
        
       
        let usermail =  self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginUserName") as? String)
        let userpass =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginPassword") as? String)
        
        
        let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard1.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        vc.logIn(fromScreen: "signup", userEmail: usermail! as! String, userPassword: userpass! as! String)
        
        
        
        
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to skip this section?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { [] (_) in
            let checkTips = UserDefaults.standard.object(forKey: "checkTips")
            
            if checkTips is NSNull || checkTips == nil {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "TipsVC") as! TipsVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func nextBtnAction(_ sender: Any) {
        leftBtn.isEnabled = true
        if(selectedHint == 0){
            if(occupationField.text == ""){
                rightBtn.isEnabled = false
                nextBtn.isEnabled = false
            }else{
                rightBtn.isEnabled = true
                nextBtn.isEnabled = true
            }
            FirstView(hidden: true)
            SecondView(hidden: false)
            buttonEnableOrDisableforleftBtn(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
            selectedHint = 1
           profileDict["occupation"] = occupationField.text as AnyObject?
            
        }else if(selectedHint == 1){
            
            
                rightBtn.isEnabled = false
                nextBtn.isEnabled = false
            
            
            FirstView(hidden: true)
            SecondView(hidden: true)
            ThirdView(hidden: false)
            buttonEnableOrDisableforleftBtn(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
            selectedHint = 2
        }else if(selectedHint == 2){
            rightBtn.isEnabled = false
            nextBtn.isEnabled = false
            FirstView(hidden: true)
            SecondView(hidden: true)
            ThirdView(hidden: true)
            FourthView(hidden: false)
            buttonEnableOrDisableforleftBtn(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
            selectedHint = 3
        }else if(selectedHint == 3){
            rightBtn.isEnabled = false
            nextBtn.isEnabled = false
            buttonEnableOrDisableforleftBtn(val: 0)
            leftBtn.isEnabled = false
            let secondProfileVc = self.storyboard?.instantiateViewController(withIdentifier: "EighthProfileSetupVC") as! EighthProfileSetupVC
            self.navigationController?.pushViewController(secondProfileVc, animated: true)
        }
    }
    
    @IBAction func nextBarBtn(_ sender: Any) {
        leftBtn.isEnabled = true
        if(selectedHint == 0){
            if(occupationField.text == ""){
                rightBtn.isEnabled = false
                nextBtn.isEnabled = false
            }else{
                rightBtn.isEnabled = true
                nextBtn.isEnabled = true
            }
            FirstView(hidden: true)
            SecondView(hidden: false)
            buttonEnableOrDisableforleftBtn(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
            
            profileDict["occupation"] = occupationField.text as AnyObject? 
            selectedHint = 1
            
            
        }else if(selectedHint == 1){
            
            
            rightBtn.isEnabled = false
            nextBtn.isEnabled = false
            
            
            FirstView(hidden: true)
            SecondView(hidden: true)
            ThirdView(hidden: false)
            buttonEnableOrDisableforleftBtn(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
            selectedHint = 2
        }else if(selectedHint == 2){
            rightBtn.isEnabled = false
            nextBtn.isEnabled = false
            FirstView(hidden: true)
            SecondView(hidden: true)
            ThirdView(hidden: true)
            FourthView(hidden: false)
            buttonEnableOrDisableforleftBtn(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
            selectedHint = 3
        }else if(selectedHint == 3){
            rightBtn.isEnabled = false
            nextBtn.isEnabled = false
            buttonEnableOrDisableforleftBtn(val: 0)
            leftBtn.isEnabled = false
            let secondProfileVc = self.storyboard?.instantiateViewController(withIdentifier: "EighthProfileSetupVC") as! EighthProfileSetupVC
            self.navigationController?.pushViewController(secondProfileVc, animated: true)
        }
    }
    func FirstView(hidden: Bool){
        firstView.frame = CGRect(x: 0, y: 120, width: Int(width), height: 200)
        firstView.alpha = 1
        firstView.isHidden = hidden
        self.view.addSubview(firstView)
    }
    func SecondView(hidden: Bool){
        secondView.frame = CGRect(x: 0, y: 80, width: Int(width), height: Int(UIScreen.main.bounds.height - 130))
        secondView.alpha = 1
         secondView.isHidden = hidden
        self.view.addSubview(secondView)
    }
    func ThirdView(hidden: Bool){
        thirdView.frame = CGRect(x: 0, y: 80, width: Int(width), height: Int(UIScreen.main.bounds.height - 130))
        thirdView.alpha = 1
         thirdView.isHidden = hidden
        self.view.addSubview(thirdView)
    }
    func FourthView(hidden: Bool){
        fourthView.frame = CGRect(x: 0, y: 80, width: Int(width), height: Int(UIScreen.main.bounds.height - 130))
        fourthView.alpha = 1
         fourthView.isHidden = hidden
        self.view.addSubview(fourthView)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == incomeTableView {
         return incomeArr.count
        } else if tableView == smokeTableView {
            return smokeArr.count
        } else {
            return drinkArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == incomeTableView {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myDataTableViewCell", for: indexPath) as! myDataTableViewCell
           
            
           
        tableView.separatorStyle = .none
        cell.dataLabel.text = incomeArr[indexPath.row]
        cell.selectionStyle = .none
        return cell
        } else if tableView == smokeTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "myDataTableViewCell", for: indexPath) as! myDataTableViewCell
            
        
            
           
            tableView.separatorStyle = .none
            cell.dataLabel.text = smokeArr[indexPath.row]
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "myDataTableViewCell", for: indexPath) as! myDataTableViewCell
            
        
            tableView.separatorStyle = .none
            cell.dataLabel.text = drinkArr[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(tableView == incomeTableView){
            incomeSelectedRow = indexPath.row
        }else if(tableView == smokeTableView){
            smokeSelectedRow = indexPath.row
        }else if(tableView == drinkTableView){
            drinkSelectedRow = indexPath.row
        }
    
        print("selected")
         if(selectedHint == 1){
            rightBtn.isEnabled = true
            nextBtn.isEnabled = true
            
             profileDict["income"] = incomeArr[indexPath.row] as AnyObject
            buttonEnableOrDisableforleftBtn(val: 1)
            buttonEnableOrDisableforNextButton(val: 1)
            buttonEnableOrDisableforRightButton(val: 1)
            
            incomeTableView.reloadData()
        }else if(selectedHint == 2){
            
            rightBtn.isEnabled = true
            nextBtn.isEnabled = true
             profileDict["smoke"] = smokeArr[indexPath.row] as AnyObject
            
            buttonEnableOrDisableforleftBtn(val: 1)
            buttonEnableOrDisableforNextButton(val: 1)
            buttonEnableOrDisableforRightButton(val: 1)
            smokeTableView.reloadData()
           
        }else if(selectedHint == 3){
            rightBtn.isEnabled = true
            nextBtn.isEnabled = true
             profileDict["drink"] = drinkArr[indexPath.row] as AnyObject
            buttonEnableOrDisableforleftBtn(val: 1)
            buttonEnableOrDisableforNextButton(val: 1)
            buttonEnableOrDisableforRightButton(val: 1)
            drinkTableView.reloadData()
        }
        
    }
    @IBAction func leftAction(_ sender: Any) {
        if(selectedHint == 0){
            self.navigationController?.popViewController(animated: true)
        }else if(selectedHint == 1){
            SecondView(hidden: true)
            ThirdView(hidden: true)
            FourthView(hidden: true)
            FirstView(hidden: false)
            buttonEnableOrDisableforleftBtn(val: 0)
            buttonEnableOrDisableforNextButton(val: 1)
            buttonEnableOrDisableforRightButton(val: 1)
            selectedHint = 0
            leftBtn.isEnabled = false
        }else if(selectedHint == 2){
            FirstView(hidden: true)
            
            ThirdView(hidden: true)
            FourthView(hidden: true)
            SecondView(hidden: false)
            buttonEnableOrDisableforleftBtn(val: 0)
            buttonEnableOrDisableforNextButton(val: 1)
            buttonEnableOrDisableforRightButton(val: 1)
             leftBtn.isEnabled = false
            selectedHint = 1
        }else if(selectedHint == 3){
            FirstView(hidden: true)
            SecondView(hidden: true)
           
            FourthView(hidden: true)
             ThirdView(hidden: false)
            buttonEnableOrDisableforleftBtn(val: 0)
            buttonEnableOrDisableforNextButton(val: 1)
            buttonEnableOrDisableforRightButton(val: 1)
             leftBtn.isEnabled = false
            selectedHint = 2
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == incomeTableView {
        return 52
        } else if tableView == smokeTableView{
            return 52
        } else {
            return 52
        }
    }
}
