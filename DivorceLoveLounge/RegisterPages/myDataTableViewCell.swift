//
//  myDataTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/20/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class myDataTableViewCell: UITableViewCell {

    @IBOutlet weak var dataLabel: UILabel!
    
    
    @IBOutlet weak var backView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        dataLabel.layer.cornerRadius = 5
//        dataLabel.layer.borderWidth = 1
//        dataLabel.layer.borderColor = UIColor.lightGray.cgColor
        // Initialization code
        backView.layer.cornerRadius = backView.frame.height/2
        backView.clipsToBounds = true
        backView.layer.borderWidth = 1
        backView.layer.borderColor = UIColor.black.cgColor
        //        dataLabel.layer.borderColor = UIColor.lightGray.cgColor
//        dataLabel.layer.borderWidth = 1
//        dataLabel.layer.borderColor = UIColor.lightGray.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
