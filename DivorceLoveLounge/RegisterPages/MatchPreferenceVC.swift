//
//  MatchPreferenceVC.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/21/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class MatchPreferenceVC: UIViewController {
 let themes = Themes()
    @IBOutlet weak var readyBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.readyBtn.layer.cornerRadius = 5

        // Do any additional setup after loading the view.
    }
    
    @IBAction func skipBtnAction(_ sender: Any) {
        

        let usermail =  self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginUserName") as? String)
        let userpass =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginPassword") as? String)
        
        
        let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard1.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        vc.logIn(fromScreen: "signup", userEmail: usermail! as! String, userPassword: userpass! as! String)

        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to skip this section?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { [] (_) in
            let checkTips = UserDefaults.standard.object(forKey: "checkTips")
            
            if checkTips is NSNull || checkTips == nil {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "TipsVC") as! TipsVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let imageUpload = UserDefaults.standard.object(forKey: "imageUpload")
                if(imageUpload is NSNull || imageUpload == nil){
                    
                    
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "EditProfilePicVC") as! EditProfilePicVC
                    vc.fromScreen = "AppDelegate"
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func readyBtnAction(_ sender: UIButton) {
        let MatchVc = self.storyboard?.instantiateViewController(withIdentifier: "MatchPreferenceFirstVC") as! MatchPreferenceFirstVC
        self.navigationController?.pushViewController(MatchVc, animated: true)        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
