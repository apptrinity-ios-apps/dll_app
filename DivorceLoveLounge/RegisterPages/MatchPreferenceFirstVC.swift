//  MatchPreferenceFirstVC.swift
//  DivorceLoveLounge
//  Created by S s Vali on 5/21/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
import UIKit
class MatchPreferenceFirstVC: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource,UIPopoverPresentationControllerDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate{
    
    @IBOutlet var tableViewNoteLabel: UILabel!
    
    @IBOutlet var noteLabelHeight: NSLayoutConstraint!
    @IBOutlet var noteLabel: UILabel!
    
    @IBOutlet weak var maxTableview: UITableView!
    @IBOutlet weak var minTableview: UITableView!
    @IBOutlet weak var percentSlider: CustomSlider1!
    @IBOutlet weak var profileCollectionView: UICollectionView!
    @IBOutlet weak var searchFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var percentLabel: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var ageMaxBtn: UIButton!
    @IBOutlet weak var ageMinBtn: UIButton!
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet weak var collectionViewQuestionLabel: UILabel!
    @IBOutlet weak var scrollBtn: UIButton!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet var scrollView: UIView!
    
    
    var filterd_OccupationArr = [String]()
    var search:String=""
    var rowsWhichAreChecked = [NSIndexPath]()
    
     var backRowsWhichAreChecked = [NSIndexPath]()
    var rowsWhichAreCheckedForAll = [NSIndexPath]()
    var optionType = String()
    var question = String()
    var religionId = String()
    var questionIncrement = Int()
    var religiousArray = Array<AnyObject>()
    var answerArray = Array<AnyObject>()
    var casteArray = Array<AnyObject>()
    var questionTitle = String()
    var data = [AnyObject]()
    var selectedString = String()
    var ethinicitySelectedArry = Array<AnyObject>()
    var religiousSelectedArray = Array<AnyObject>()
    var denominationSelectedArray = Array<AnyObject>()
    var smokeSelectedArray = Array<AnyObject>()
    var drinkSelectedArray = Array<AnyObject>()
    var occupationSelectedArray = Array<AnyObject>()
    var educationSelectedArray = Array<AnyObject>()
//    var farArr = ["WITHIN 30 MILES","WITHIN 60 MILES (RECOMMENDED)","WITHIN 120 MILES","WITHIN 300 MILES","SPECIFIC STATES","IN MY COUNTRY","ANYWHERE IN THE WORLD","SPECIFIC COUNTRIES"]
    var farArr = ["WITHIN 30 MILES","WITHIN 60 MILES (RECOMMENDED)","WITHIN 120 MILES","WITHIN 300 MILES","IN MY COUNTRY","ANYWHERE IN THE WORLD"]
    var distanceArr  = ["Not at all important \n Search outside my range","Search outside my range \n Search slightly outside my range","Somewhat important \n Search only within my range"]
    var questionArray = ["How far should we search for your matches?","How important is the distance of your match?","Ethnicity you are looking for?","Which describes your Partner's level of education?","What best describes your Partner's religious beliefs or spirituality?","What best describes your Partner's Language preference","What do you expect your Partner to do?","How often can your Partner smoke?","How often can your Partner drink?"]
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    var minAge = String()
    var maxAge = String()
    var smoke = String()
    var distance = String()
    var ethnicity = String()
    var education = String()
    var occupation = String()
    var drink = String()
    var far = String()
    var religious = String()
    var selectedHint = Int()
    var ageArray = Array<AnyObject>()
    let width = UIScreen.main.bounds.width
    let height = UIScreen.main.bounds.height
    let occupationPicker = UIPickerView()
    var farlSelectedRow = Int()
    var backlickfarlSelectedRow = Int()
    var distanceSelectedRow = Int()
    var ethnicitySelectedRow = Int()
    var religiousSelectedRow = Int()
    var educationSelectedRow = Int()
    var smokeSelectedRow = Int()
    var drinkSelectedRow = Int()
    var percentHint = 0.0
    var selectedReligions = Array<AnyObject>()
    var ethnicityArr = Array<AnyObject>()
    var religiousArr = Array<AnyObject>()
    var educationArr = Array<AnyObject>()
    var occupationArr = Array<AnyObject>()
    var smokeArr = Array<AnyObject>()
    var drinkArr = Array<AnyObject>()
    var denominationArr = Array<AnyObject>()
    @IBOutlet var firstView: UIView!
    @IBOutlet var secondView: UIView!
    @IBOutlet weak var farTableView: UITableView!
    @IBOutlet var thirdView: UIView!    
    @IBOutlet weak var distanceTableView: UITableView!
    @IBOutlet var fourthView: UIView!
    @IBOutlet weak var ethnicityTableView: UITableView!
    @IBOutlet var fifthView: UIView!
    @IBOutlet weak var religiousTableView: UITableView!
    @IBOutlet var sixthView: UIView!
    @IBOutlet weak var educationTableView: UITableView!
    @IBOutlet var seventhView: UIView!
    @IBOutlet weak var occupationField: UITextField!
    @IBOutlet var eightView: UIView!
    @IBOutlet weak var smokeTableView: UITableView!
    @IBOutlet var ninthView: UIView!
    @IBOutlet weak var drinkTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        minAge = "18"
        maxAge = "24"
        // Picker view Done Cancel Actions
        
        
        minTableview.layer.cornerRadius = 5
        minTableview.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        minTableview.layer.shadowOpacity = 1.0
        minTableview.layer.shadowRadius = 3
        minTableview.layer.masksToBounds = true
        
        minTableview.layer.shadowColor = UIColor.gray.cgColor
        
        maxTableview.layer.cornerRadius = 5
        maxTableview.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        maxTableview.layer.shadowOpacity = 1.0
        maxTableview.layer.shadowRadius = 3
        maxTableview.layer.masksToBounds = true
        maxTableview.layer.shadowColor = UIColor.gray.cgColor
        
        maxTableview.layer.borderWidth = 1
        maxTableview.layer.borderColor = UIColor.lightGray.cgColor
    
        
        
        minTableview.layer.borderWidth = 1
        minTableview.layer.borderColor = UIColor.lightGray.cgColor
        
        
        scrollView.layer.cornerRadius = scrollView.frame.height/2
        scrollView.isHidden = true
        searchField.autocapitalizationType = .sentences
        searchField.layer.cornerRadius = searchField.frame.height/2
        self.searchField.layer.borderWidth = 1
        self.searchField.layer.borderColor = UIColor(red: 205/255, green: 205/255, blue: 205/255, alpha: 1.0).cgColor
        searchField.attributedPlaceholder = NSAttributedString(string: "Search",
                                                                 attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: CGFloat(28/255.0), green: CGFloat(37/255.0), blue: CGFloat(42/255.0), alpha: CGFloat(1.0) )])
        // For Right side Padding
        searchField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: searchField.frame.height))
        searchField.leftViewMode = .always
        percentSlider.value = 0
        percentLabel.text = "0%"
        percentSlider.setThumbImage(UIImage(), for: .normal)
        nextBtn.layer.cornerRadius = 5
        ethnicitySelectedRow = -1
        religiousSelectedRow = -1
        smokeSelectedRow = -1
        educationSelectedRow = -1
        drinkSelectedRow = -1
        distanceSelectedRow = -1
        farlSelectedRow = -1
        //nextBtn.isEnabled = false
        selectedHint = 0
        minTableview.layer.cornerRadius = 5
        maxTableview.layer.cornerRadius = 5
        selectedString = "distance"
        profileCollectionView.isHidden = false
        noteLabel.isHidden = false
        minTableview.isHidden = true
        maxTableview.isHidden = true
        for i in 18...99{
            ageArray.append(i as AnyObject)
        }
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(MatchPreferenceFirstVC.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(MatchPreferenceFirstVC.cancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        occupationField.inputView = occupationPicker
        occupationField.inputAccessoryView = toolBar
        occupationField.layer.cornerRadius = 5
        occupationField.layer.borderWidth = 1
        occupationField.layer.borderColor = UIColor.lightGray.cgColor
        // For Right side Padding
        occupationField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: occupationField.frame.height))
        occupationField.leftViewMode = .always
        farTableView.delegate = self
        farTableView.dataSource = self
        distanceTableView.delegate = self
        distanceTableView.dataSource = self
        ethnicityTableView.delegate = self
        ethnicityTableView.dataSource = self
        religiousTableView.delegate = self
        religiousTableView.dataSource = self
        educationTableView.delegate = self
        educationTableView.dataSource = self
        smokeTableView.delegate = self
        smokeTableView.dataSource = self
        drinkTableView.delegate = self
        drinkTableView.dataSource = self
        occupationPicker.delegate = self
        occupationPicker.dataSource = self
        minTableview.delegate = self
        minTableview.dataSource = self
        maxTableview.delegate = self
        maxTableview.dataSource = self
        
        self.farTableView.register(UINib(nibName: "myDataTableViewCell", bundle: nil), forCellReuseIdentifier: "myDataTableViewCell")
        self.profileCollectionView.register(UINib(nibName:"gridCollectionCell" , bundle:nil), forCellWithReuseIdentifier: "gridCollectionCell")
         self.farTableView.register(UINib(nibName: "myDataTableViewCell", bundle: nil), forCellReuseIdentifier: "myDataTableViewCell")
        self.distanceTableView.register(UINib(nibName: "myDataTableViewCell", bundle: nil), forCellReuseIdentifier: "myDataTableViewCell")
        self.minTableview.register(UINib(nibName: "ParkPopUpCell", bundle: nil), forCellReuseIdentifier: "ParkPopUpCell")
         self.maxTableview.register(UINib(nibName: "ParkPopUpCell", bundle: nil), forCellReuseIdentifier: "ParkPopUpCell")
        self.ethnicityTableView.register(UINib(nibName: "myDataTableViewCell", bundle: nil), forCellReuseIdentifier: "myDataTableViewCell")
        self.religiousTableView.register(UINib(nibName: "myDataTableViewCell", bundle: nil), forCellReuseIdentifier: "myDataTableViewCell")
        self.educationTableView.register(UINib(nibName: "myDataTableViewCell", bundle: nil), forCellReuseIdentifier: "myDataTableViewCell")
        self.smokeTableView.register(UINib(nibName: "myDataTableViewCell", bundle: nil), forCellReuseIdentifier: "myDataTableViewCell")
        self.drinkTableView.register(UINib(nibName: "myDataTableViewCell", bundle: nil), forCellReuseIdentifier: "myDataTableViewCell")
        // Do any additional setup after loading the view.
        FirstView(hidden: false)
        buttonEnableOrDisableforLeftButton(val: 1)
        buttonEnableOrDisableforRightButton(val: 0)
        buttonEnableOrDisableforNextButton(val: 0)
        ageMaxBtn.layer.cornerRadius = 20
        ageMaxBtn.layer.borderWidth = 1
        ageMaxBtn.layer.borderColor = UIColor.lightGray.cgColor
        ageMinBtn.layer.cornerRadius = 20
        ageMinBtn.layer.borderWidth = 1
        ageMinBtn.layer.borderColor = UIColor.lightGray.cgColor
        
        dynamicProfileValues()
        getReligions()
        
        getMotherToungeApi()
        
        
        
    }
    @IBAction func skipBtnAction(_ sender: Any) {
    
        let usermail =  self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginUserName") as? String)
        let userpass =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginPassword") as? String)
        
        
        let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard1.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        vc.logIn(fromScreen: "signup", userEmail: usermail! as! String, userPassword: userpass! as! String)
        
 
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to skip this section?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { [] (_) in
            let imageUpload = UserDefaults.standard.object(forKey: "imageUpload")
            if(imageUpload is NSNull || imageUpload == nil){
                
                
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "EditProfilePicVC") as! EditProfilePicVC
                vc.fromScreen = "AppDelegate"
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func ageMinAction(_ sender: AnyObject) {
       minTableview.isHidden = false
        maxTableview.isHidden = true
        minTableview.reloadData()
    }
    @IBAction func ageMaxAction(_ sender: AnyObject) {
       maxTableview.isHidden = false
        minTableview.isHidden = true
        maxTableview.reloadData()
    }
    @IBAction func nextBtnAction(_ sender: UIButton){
          self.farTableView.scroll(to: .top, animated: true)
       // self.profileCollectionView?.scrollToItem(at: IndexPath(row: 0, section: 0),
//                                                 at: .top,
//                                                 animated: true)
        
       // self.profileCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: UICollectionViewScrollPosition.top, animated: false)
        
        leftBtn.isEnabled = true
        buttonEnableOrDisableforLeftButton(val: 1)
        backRowsWhichAreChecked = rowsWhichAreChecked
        rowsWhichAreChecked.removeAll()
        backlickfarlSelectedRow = farlSelectedRow
        farlSelectedRow = -1
        if(selectedHint == 0){
        }else{
            nextBtn.isEnabled = false
        }
       
        percentHint = percentHint + 10
        percentSlider.value = Float(percentHint)
        let val = Int(percentHint)
        percentLabel.text = String(val) + "%"
        if(selectedHint == 0){
            FirstView(hidden: true)
            SecondView(hidden: false)
            noteLabelHeight.constant = 0
            searchFieldHeight.constant = 0
            profileCollectionView.isHidden = true
            noteLabel.isHidden = true
            self.answerArray = farArr as [AnyObject]
            self.questionLabel.text = questionArray[0]
            self.collectionViewQuestionLabel.text = questionArray[0]
            self.questionTitle = "far"
            selectedHint = 1
            buttonEnableOrDisableforLeftButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
            
          
            
           
        }else if(selectedHint == 1){
            SecondView(hidden: false)
            searchFieldHeight.constant = 0
             noteLabelHeight.constant = 0
            profileCollectionView.isHidden = true
            noteLabel.isHidden = true
            self.answerArray = distanceArr as [AnyObject]
            self.questionLabel.text = questionArray[1]
            self.collectionViewQuestionLabel.text = questionArray[1]
            self.questionTitle = "distance"
            selectedHint = 2
            buttonEnableOrDisableforLeftButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
        }
        else if(selectedHint == 4){
            SecondView(hidden: false)
            searchFieldHeight.constant = 0
             noteLabelHeight.constant = 17
            profileCollectionView.isHidden = true
            noteLabel.isHidden = true
            self.answerArray = ethnicityArr as [AnyObject]
            self.questionLabel.text = questionArray[2]
            self.collectionViewQuestionLabel.text = questionArray[2]
            self.questionTitle = "ethinicity"
            selectedHint = 5
            buttonEnableOrDisableforLeftButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
   
        }else if(selectedHint == 5){
            SecondView(hidden: false)
            searchFieldHeight.constant = 0
             noteLabelHeight.constant = 17
            profileCollectionView.isHidden = true
            noteLabel.isHidden = true
            self.answerArray = educationArr as [AnyObject]
            self.questionLabel.text = questionArray[3]
            self.collectionViewQuestionLabel.text = questionArray[3]
            self.questionTitle = "education"
            selectedHint = 6
            buttonEnableOrDisableforLeftButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)

        }else if(selectedHint == 2){
            SecondView(hidden: true)
            profileCollectionView.isHidden = false
            searchFieldHeight.constant = 0
            noteLabel.isHidden = false
            self.answerArray = religiousArr as [AnyObject]
            self.questionLabel.text = questionArray[4]
            self.collectionViewQuestionLabel.text = questionArray[4]
            self.questionTitle = "religious"
            selectedHint = 3
            buttonEnableOrDisableforLeftButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
        }else if(selectedHint == 3){
            SecondView(hidden: true)
            profileCollectionView.isHidden = false
            noteLabel.isHidden = false
            
            self.answerArray = denominationArr as [AnyObject]
            self.questionLabel.text = questionArray[5]
            self.collectionViewQuestionLabel.text = questionArray[5]
            self.questionTitle = "denomination"
            selectedHint = 4
            buttonEnableOrDisableforLeftButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
        }else if(selectedHint == 6){
            SecondView(hidden: true)
            profileCollectionView.isHidden = false
            noteLabel.isHidden = false
            searchFieldHeight.constant = 40
            self.answerArray = occupationArr as [AnyObject]
            //filterd_customersArr = occupationArr as! [String]
            self.questionLabel.text = questionArray[6]
            self.collectionViewQuestionLabel.text = questionArray[6]
            self.questionTitle = "occupation"
            selectedHint = 7
            buttonEnableOrDisableforLeftButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
        }else if(selectedHint == 7){
            SecondView(hidden: false)
            noteLabel.isHidden = true
             noteLabelHeight.constant = 17
            profileCollectionView.isHidden = true
            noteLabel.isHidden = true
            searchFieldHeight.constant = 0
            self.answerArray = smokeArr as [AnyObject]
            self.questionLabel.text = questionArray[7]
            self.collectionViewQuestionLabel.text = questionArray[7]
            self.questionTitle = "smoke"
            selectedHint = 8
            buttonEnableOrDisableforLeftButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
        }else if(selectedHint == 8){
            SecondView(hidden: false)
            profileCollectionView.isHidden = true
            noteLabel.isHidden = true
             noteLabelHeight.constant = 17
            searchFieldHeight.constant = 0
            self.answerArray = drinkArr as [AnyObject]
            self.questionLabel.text = questionArray[8]
            self.collectionViewQuestionLabel.text = questionArray[8]
            self.questionTitle = "drink"
            selectedHint = 9
            buttonEnableOrDisableforLeftButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
            buttonEnableOrDisableforRightButton(val: 0)
        }else if(selectedHint == 9){
            leftBtn.isEnabled = false
            buttonEnableOrDisableforLeftButton(val: 0)
            profileSetUp()
        }
        if(answerArray.count > 10){
           // scrollBtn.isHidden = false
            scrollView.isHidden = false
        }else{
            scrollView.isHidden = true
        }
        profileCollectionView.reloadData()
        farTableView.reloadData()
    }
    func profileSetUp(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let urserId = themes.getUserId()
            var dict = [String : AnyObject]()
            
            for i in 0...ethinicitySelectedArry.count-1{
                
                
                ethnicity = ethnicity + "," + (ethinicitySelectedArry[i] as! String)
            }
            ethnicity.remove(at: ethnicity.startIndex)
            
            for i in 0...religiousSelectedArray.count-1{
                
                
                religious = religious + "," + (religiousSelectedArray[i] as! String)
            }
            religious.remove(at: religious.startIndex)
            
            for i in 0...educationSelectedArray.count-1{
                
                
                education = education + "," + (educationSelectedArray[i] as! String)
            }
            education.remove(at: education.startIndex)
            
            for i in 0...occupationSelectedArray.count-1{
                
                
                occupation = occupation + "," + (occupationSelectedArray[i] as! String)
            }
            occupation.remove(at: occupation.startIndex)
            
            
            for i in 0...smokeSelectedArray.count-1{
                
                
                smoke = smoke + "," + (smokeSelectedArray[i] as! String)
            }
            smoke.remove(at: smoke.startIndex)
            
            for i in 0...drinkSelectedArray.count-1{
                
                
                drink = drink + "," + (drinkSelectedArray[i] as! String)
            }
            drink.remove(at: drink.startIndex)
            var denomination = String()
            for i in 0...denominationSelectedArray.count-1{
                
                
                denomination = denomination + "," + (denominationSelectedArray[i] as! String)
            }
            denomination.remove(at: denomination.startIndex)
            
            dict.updateValue(urserId as AnyObject, forKey: "user_id")
            dict.updateValue(ethnicity as AnyObject, forKey: "partner_ethnicity")
            dict.updateValue(religious as AnyObject, forKey: "partner_religion")
            dict.updateValue(education as AnyObject, forKey: "partner_education")
            dict.updateValue(minAge as AnyObject, forKey: "partner_age_min")
            dict.updateValue(maxAge as AnyObject, forKey: "partner_age_max")
            dict.updateValue(distance as AnyObject, forKey: "partner_match_distance")
            dict.updateValue(far as AnyObject, forKey: "partner_match_search")
            dict.updateValue(occupation as AnyObject, forKey: "partner_occupation")
            dict.updateValue(smoke as AnyObject, forKey: "partner_smoke")
            dict.updateValue(drink as AnyObject, forKey: "partner_drink")
             dict.updateValue(denomination as AnyObject, forKey: "partner_mtongue_name")
            
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:profileMatch, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    UserDefaults.standard.set(true, forKey: "checkProfile3")
                    self.buttonEnableOrDisableforLeftButton(val: 0)
                    self.leftBtn.isEnabled = false
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let profileVc = storyboard.instantiateViewController(withIdentifier: "CompitibilityQuizProfileVC") as! CompitibilityQuizProfileVC
                    self.navigationController?.pushViewController(profileVc , animated: true)

                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    @IBAction func nextBarBtn(_ sender: UIButton) {
    }
    @IBAction func leftAction(_ sender: Any) {
        
        
       
        if(backRowsWhichAreChecked.count != 0 || self.questionTitle == "distance" ||  self.questionTitle == "religious"){
            nextBtn.isEnabled = true
        }
        
         rowsWhichAreChecked.removeAll()
        
        rowsWhichAreChecked = backRowsWhichAreChecked
        farlSelectedRow = backlickfarlSelectedRow
        //nextBtn.isEnabled = false
        percentHint = percentHint - 10
        searchFieldHeight.constant = 0
        percentSlider.value = Float(percentHint)
        let val = Int(percentHint)
        percentLabel.text = String(val) + "%"
        if(selectedHint == 0){
             self.navigationController?.popViewController(animated: true)
        }else if(selectedHint == 1){
                    FirstView(hidden: false)
                    SecondView(hidden: true)
            selectedHint = 0
        }
        else if(selectedHint == 4){
            SecondView(hidden: true)
            profileCollectionView.isHidden = false
            noteLabel.isHidden = false
            
            searchFieldHeight.constant = 0
            self.answerArray = religiousArr as [AnyObject]
            self.questionLabel.text = questionArray[4]
            self.collectionViewQuestionLabel.text = questionArray[4]
            self.questionTitle = "religious"
            selectedHint = 3
        }else if(selectedHint == 5){
            
            SecondView(hidden: true)
                       profileCollectionView.isHidden = false
                       noteLabel.isHidden = false
                       self.answerArray = denominationArr as [AnyObject]
                       self.questionLabel.text = questionArray[5]
                       self.collectionViewQuestionLabel.text = questionArray[5]
                       self.questionTitle = "denomination"
                       selectedHint = 4
            
            
            
            
            
            
            
            
            
            
           
        }else if(selectedHint == 2){
            SecondView(hidden: false)
            searchFieldHeight.constant = 0
            profileCollectionView.isHidden = true
            noteLabel.isHidden = true
            self.answerArray = farArr as [AnyObject]
            self.questionLabel.text = questionArray[0]
            self.collectionViewQuestionLabel.text = questionArray[0]
             noteLabelHeight.constant = 0
            self.questionTitle = "far"
            selectedHint = 1
        }else if(selectedHint == 3){
                      SecondView(hidden: false)
                                   searchFieldHeight.constant = 0
                                   profileCollectionView.isHidden = true
                                    noteLabel.isHidden = true
                                   self.answerArray = distanceArr as [AnyObject]
                                   self.questionLabel.text = questionArray[1]
                                   self.collectionViewQuestionLabel.text = questionArray[1]
                                    noteLabelHeight.constant = 0
                                   self.questionTitle = "distance"
                                   selectedHint = 2
        }else if(selectedHint == 6){
            SecondView(hidden: false)
            searchFieldHeight.constant = 0
            profileCollectionView.isHidden = true
            noteLabel.isHidden = true
            self.answerArray = ethnicityArr as [AnyObject]
            self.questionLabel.text = questionArray[2]
            self.collectionViewQuestionLabel.text = questionArray[2]
            self.questionTitle = "ethinicity"
            selectedHint = 5
        }else if(selectedHint == 7){

            
            
            SecondView(hidden: false)
                       searchFieldHeight.constant = 0
                       profileCollectionView.isHidden = true
                       noteLabel.isHidden = true
                       self.answerArray = educationArr as [AnyObject]
                       self.questionLabel.text = questionArray[3]
                       self.collectionViewQuestionLabel.text = questionArray[3]
                       self.questionTitle = "education"
                       selectedHint = 6
            
            
        }else if(selectedHint == 8){
            
            
            SecondView(hidden: true)
            profileCollectionView.isHidden = false
            noteLabel.isHidden = false
            searchFieldHeight.constant = 40
            self.answerArray = occupationArr as [AnyObject]
            //filterd_customersArr = occupationArr as! [String]
            self.questionLabel.text = questionArray[6]
            self.collectionViewQuestionLabel.text = questionArray[6]
            self.questionTitle = "occupation"
            
            selectedHint = 7
           
        }else if(selectedHint == 9){
            
            SecondView(hidden: false)
            profileCollectionView.isHidden = true
            noteLabel.isHidden = true
            searchFieldHeight.constant = 0
            self.answerArray = smokeArr as [AnyObject]
            self.questionLabel.text = questionArray[7]
            self.collectionViewQuestionLabel.text = questionArray[7]
            self.questionTitle = "smoke"
            
            selectedHint = 8
            
        }
        leftBtn.isEnabled = false
        buttonEnableOrDisableforLeftButton(val: 0)
        
        if(answerArray.count > 10){
            // scrollBtn.isHidden = false
            scrollView.isHidden = false
        }else{
            scrollView.isHidden = true
        }
        profileCollectionView.reloadData()
        farTableView.reloadData()
       
       
    }
    func buttonEnableOrDisableforLeftButton(val : Int){
        if(val == 0){
//            leftBtn.setImage(#imageLiteral(resourceName: "left-arrow (16)"), for: .normal)
             leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        }else{
//            leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-black"), for: .normal)
             leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        }
    }
    
    func buttonEnableOrDisableforRightButton(val : Int){
        if(val == 0){
            rightBtn.setImage(#imageLiteral(resourceName: "right-arrow-hide"), for: .normal)
            
        }else{
            rightBtn.setImage(#imageLiteral(resourceName: "right-arrow-black"), for: .normal)
        }
    }
    
    func buttonEnableOrDisableforNextButton(val : Int){
        if(val == 0){
//            nextBtn.backgroundColor = buttonHideColor
        }else{
//            nextBtn.backgroundColor = darkBrownColor
        }
        
    }
    @objc func donePicker() {
        occupationField.resignFirstResponder()
    }
    @objc func cancelPicker() {
        occupationField.text = ""
        occupationField.resignFirstResponder()
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return occupationArr.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return occupationArr[row] as! String
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        occupationField.text = occupationArr[row] as! String
         nextBtn.isEnabled = true
        occupation = occupationArr[row] as! String
        buttonEnableOrDisableforRightButton(val: 1)
        buttonEnableOrDisableforNextButton(val: 1)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.answerArray.count < Int(scrollView.contentOffset.y)) {
            // did move up
            
            
            
            self.scrollView.isHidden = true
        } else if (self.answerArray.count > Int(scrollView.contentOffset.y)) {
            
            
            
            self.scrollView.isHidden = false
        } else {
            // didn't move
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    class var hasTopNotch: Bool {
        if #available(iOS 11.0, tvOS 11.0, *) {
            // with notch: 44.0 on iPhone X, XS, XS Max, XR.
            // without notch: 24.0 on iPad Pro 12.9" 3rd generation, 20.0 on iPhone 8 on iOS 12+.
            
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 24
        }
        return false
    }

    func FirstView(hidden:Bool){
        scrollBtn.isHidden = true
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136, 1334, 1920, 2208:
                print("iPhone 5 or 5S or 5C")
                firstView.frame = CGRect(x: 0, y: 120, width: Int(width), height: Int(self.view.frame.size.height - 180))
                firstView.alpha = 1
                firstView.isHidden = hidden
                self.view.addSubview(firstView)

            case 2436, 2688, 1792:
                print("iPhone X, XS, XR")
                
                firstView.frame = CGRect(x: 0, y: 150, width: Int(width), height: Int(self.view.frame.size.height - 230))
                firstView.alpha = 1
                firstView.isHidden = hidden
                self.view.addSubview(firstView)
                
            default:
                print("Unknown")
            }
        }

    }
    
    func SecondView(hidden:Bool){

        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136, 1334, 1920, 2208:
                print("iPhone 5 or 5S or 5C")
                secondView.frame = CGRect(x: 0, y: 110, width: Int(width), height: Int(self.view.frame.size.height - 180))
                secondView.alpha = 1
                secondView.isHidden = hidden
                self.view.addSubview(secondView)
                
            case 2436, 2688, 1792:
                print("iPhone X, XS")
                
                secondView.frame = CGRect(x: 0, y: 140, width: Int(width), height: Int(self.view.frame.size.height - 230))
                secondView.alpha = 1
                secondView.isHidden = hidden
                self.view.addSubview(secondView)
                
            default:
                print("Unknown")
            }
        }
    }
//    func ThirdView(hidden:Bool){
//        thirdView.frame = CGRect(x: 0, y: 140, width: Int(width), height: Int(self.view.frame.size.height - 230))
//        thirdView.alpha = 1
//                thirdView.isHidden = hidden
//        self.view.addSubview(thirdView)
//    }
//    func FourthView(hidden:Bool){
//        fourthView.frame = CGRect(x: 0, y: 140, width: Int(width), height: Int(self.view.frame.size.height - 230))
//        fourthView.alpha = 1
//                fourthView.isHidden = hidden
//        self.view.addSubview(fourthView)
//    }
//    func FifthView(hidden:Bool){
//        fifthView.frame = CGRect(x: 0, y: 140, width: Int(width), height: Int(self.view.frame.size.height - 230))
//        fifthView.alpha = 1
//                fifthView.isHidden = hidden
//        self.view.addSubview(fifthView)
//    }
//    func SixthView(hidden:Bool){
//        sixthView.frame = CGRect(x: 0, y: 140, width: Int(width), height: Int(self.view.frame.size.height - 230))
//        sixthView.alpha = 1
//                sixthView.isHidden = hidden
//        self.view.addSubview(sixthView)
//    }
//    func SeventhView(hidden:Bool){
//        seventhView.frame = CGRect(x: 0, y: 140, width: Int(width), height: Int(self.view.frame.size.height - 230))
//        seventhView.alpha = 1
//                seventhView.isHidden = hidden
//        self.view.addSubview(seventhView)
//    }
//    func EightView(hidden:Bool){
//        eightView.frame = CGRect(x: 0, y: 140, width: Int(width), height: Int(self.view.frame.size.height - 230))
//        eightView.alpha = 1
//                eightView.isHidden = hidden
//        self.view.addSubview(eightView)
//    }
//    func NinthView(hidden:Bool){
//        ninthView.frame = CGRect(x: 0, y: 140, width: Int(width), height: Int(self.view.frame.size.height - 230))
//        ninthView.alpha = 1
//                ninthView.isHidden = hidden
//        self.view.addSubview(ninthView)
//    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == farTableView {
            return answerArray.count
        }   else if tableView == minTableview {
            return ageArray.count
        } else if tableView == maxTableview {
            return ageArray.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == farTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "myDataTableViewCell", for: indexPath) as! myDataTableViewCell
           
            cell.dataLabel.textAlignment = .center
            tableView.separatorStyle = .none
            let obj = answerArray[indexPath.row]
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                cell.backView.backgroundColor = UIColor.clear
                cell.dataLabel.textColor = UIColor.black
            }else{
                cell.backView.backgroundColor = darkBrownColor
                cell.dataLabel.textColor = UIColor.white
            }
            if(questionTitle == "far"){
              
                let text = farArr[indexPath.row]
                cell.dataLabel.text = text.capitalizingFirstLetter()
                if(farlSelectedRow == indexPath.row){
                    cell.backView.backgroundColor = darkBrownColor
                    cell.dataLabel.textColor = UIColor.white
                }else{
                    cell.backView.backgroundColor = UIColor.clear
                    cell.dataLabel.textColor = UIColor.black
                }
                
                
                
            }else if(questionTitle == "distance"){
              
                let text = distanceArr[indexPath.row]
                cell.dataLabel.text = text.capitalizingFirstLetter()
                if(farlSelectedRow == indexPath.row){
                    cell.backView.backgroundColor = darkBrownColor
                    cell.dataLabel.textColor = UIColor.white
                }else{
                    cell.backView.backgroundColor = UIColor.clear
                    cell.dataLabel.textColor = UIColor.black
                }
            }else if(questionTitle == "religious"){
                let text = (obj["religion_name"] as! String)
                cell.dataLabel.text = text.capitalizingFirstLetter()
              
            }else if(questionTitle == "occupation"){
                let text = answerArray[indexPath.row] as? String
                cell.dataLabel.text = text!.capitalizingFirstLetter()
            }else if(questionTitle == "denomination"){
                let text = (obj["mtongue_name"] as! String)
                cell.dataLabel.text = text.capitalizingFirstLetter()
             
            }else{
                let text = (obj["answer"] as! String)
                cell.dataLabel.text = text.capitalizingFirstLetter()
                
            }
            cell.selectionStyle = .none
            return cell
        }else if tableView == minTableview {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ParkPopUpCell", for: indexPath) as! ParkPopUpCell
            tableView.separatorStyle = .none
            let obj:Int = ageArray[indexPath.row] as! Int
            cell.nameLabel.text = String(obj)
            cell.selectionStyle = .none
            return cell
        }else if tableView == maxTableview {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ParkPopUpCell", for: indexPath) as! ParkPopUpCell
            tableView.separatorStyle = .none
            let obj:Int = ageArray[indexPath.row] as! Int
            cell.nameLabel.text = String(obj)
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        if(tableView == farTableView){
//            nextBtn.isEnabled = true
//            farlSelectedRow = indexPath.row
//
        
        
    
        if(tableView == minTableview){
            let obj:Int = ageArray[indexPath.row] as! Int
            minAge = String(obj)
            minTableview.isHidden = true
            ageMinBtn.setTitle(String(obj), for: .normal)
            if(minAge == "" || maxAge == ""){
                
            }else{
                nextBtn.isEnabled = true
                buttonEnableOrDisableforRightButton(val: 1)
                buttonEnableOrDisableforNextButton(val: 1)
            }
        }else if(tableView == maxTableview){
            let obj:Int = ageArray[indexPath.row] as! Int
            maxAge = String(obj)
            ageMaxBtn.setTitle(String(obj), for: .normal)
            maxTableview.isHidden = true
            if(minAge == "" || maxAge == ""){
            }else{
                nextBtn.isEnabled = true
                buttonEnableOrDisableforRightButton(val: 1)
                buttonEnableOrDisableforNextButton(val: 1)
            }
        }else if(tableView == farTableView){
//            let obj = farArr[indexPath.row]
//            far = obj
//            buttonEnableOrDisableforRightButton(val: 1)
//            buttonEnableOrDisableforNextButton(val: 1)
            if(self.questionTitle == "religious"){
              //getDenomination(id: id as! String)
                let obj = religiousArr[indexPath.row]
                let id = obj["religion_name"] as? String ?? ""
                
                
                if(id == "All"){
                   rowsWhichAreChecked.removeAll()
                    religiousSelectedArray.removeAll()
                    
                     if(rowsWhichAreCheckedForAll.contains(indexPath as NSIndexPath) == false){
                        for i in 0...religiousArr.count - 1 {
                            let obj = religiousArr[indexPath.row]
                            let id1 = obj["religion_name"] as? String ?? ""
                            let indexPath = NSIndexPath(row: i, section: 0)
                            rowsWhichAreChecked.append(indexPath)
                            religiousSelectedArray.append(id1 as AnyObject )
                        }
                     }else{
                        rowsWhichAreChecked.removeAll()
                        religiousSelectedArray.removeAll()
                    }
               
                }else{
                    if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                        rowsWhichAreChecked.append(indexPath as NSIndexPath)
                        religiousSelectedArray.append(id as AnyObject )
                    }else{
                        // cell.backgroundColor = UIColor.black
                        // remove the indexPath from rowsWhichAreCheckedArray
                        if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                            rowsWhichAreChecked.remove(at: checkedItemIndex)
                            religiousSelectedArray.remove(at: checkedItemIndex)
                        }
                    }
                }
                
              
                
//                var religionString = String()
//                for i in 0...religiousSelectedArray.count-1{
//
//
//                    religionString = religionString + "," + (religiousSelectedArray[i] as! String)
//                }
//                religionString.remove(at: religionString.startIndex)
//                getDenomination(id: religionString)
                
                
                
            }else if(self.questionTitle == "far"){
                far = farArr[indexPath.row]
                farlSelectedRow = indexPath.row
                
                
            }else if(self.questionTitle == "distance"){
                distance = distanceArr[indexPath.row]
                farlSelectedRow = indexPath.row
                
                
            }else if(self.questionTitle == "ethinicity"){
                //getDenomination(id: id as! String)
                let obj = ethnicityArr[indexPath.row]
                let id = obj["answer"] as? String ?? ""
                
                
                if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                    rowsWhichAreChecked.append(indexPath as NSIndexPath)
                    
                    
                    ethinicitySelectedArry.append(id as AnyObject )
                    
                }else{
                    // cell.backgroundColor = UIColor.black
                    // remove the indexPath from rowsWhichAreCheckedArray
                    if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                        rowsWhichAreChecked.remove(at: checkedItemIndex)
                        ethinicitySelectedArry.remove(at: checkedItemIndex)
                    }
 
                }

                
            }else if(self.questionTitle == "education"){
                //getDenomination(id: id as! String)
                let obj = educationArr[indexPath.row]
                let id = obj["answer"] as? String ?? ""
                
                
                if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                    rowsWhichAreChecked.append(indexPath as NSIndexPath)
                    
                    
                    educationSelectedArray.append(id as AnyObject )
                    
                }else{
                    // cell.backgroundColor = UIColor.black
                    // remove the indexPath from rowsWhichAreCheckedArray
                    if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                        rowsWhichAreChecked.remove(at: checkedItemIndex)
                        educationSelectedArray.remove(at: checkedItemIndex)
                    }
                    
                    
                }

            }else if(self.questionTitle == "occupation"){
                //getDenomination(id: id as! String)
                let id = occupationArr[indexPath.row]
                
                
                if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                    rowsWhichAreChecked.append(indexPath as NSIndexPath)
                    
                    
                    occupationSelectedArray.append(id as AnyObject )
                    
                }else{
                    // cell.backgroundColor = UIColor.black
                    // remove the indexPath from rowsWhichAreCheckedArray
                    if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                        rowsWhichAreChecked.remove(at: checkedItemIndex)
                        occupationSelectedArray.remove(at: checkedItemIndex)
                    }
                    
                    
                }
                
            }else if(self.questionTitle == "smoke"){
                //getDenomination(id: id as! String)
                let obj = smokeArr[indexPath.row]
                let id = obj["answer"] as? String ?? ""
                
                
                if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                    rowsWhichAreChecked.append(indexPath as NSIndexPath)
                    
                    
                    smokeSelectedArray.append(id as AnyObject )
                    
                }else{
                    // cell.backgroundColor = UIColor.black
                    // remove the indexPath from rowsWhichAreCheckedArray
                    if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                        rowsWhichAreChecked.remove(at: checkedItemIndex)
                        smokeSelectedArray.remove(at: checkedItemIndex)
                    }
                    
                    
                }
                
            }else if(self.questionTitle == "drink"){
                //getDenomination(id: id as! String)
                let obj = drinkArr[indexPath.row]
                let id = obj["answer"] as? String ?? ""
                
                
                if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                    rowsWhichAreChecked.append(indexPath as NSIndexPath)
                    
                    
                    drinkSelectedArray.append(id as AnyObject )
                    
                }else{
                    // cell.backgroundColor = UIColor.black
                    // remove the indexPath from rowsWhichAreCheckedArray
                    if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                        rowsWhichAreChecked.remove(at: checkedItemIndex)
                        drinkSelectedArray.remove(at: checkedItemIndex)
                    }
                    
                    
                }
                
            }else if(self.questionTitle == "denomination"){
                //getDenomination(id: id as! String)
                let obj = denominationArr[indexPath.row]
                let id = obj["mtongue_name"] as? String ?? ""
                
                
                if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                    rowsWhichAreChecked.append(indexPath as NSIndexPath)
                    
                    
                    denominationSelectedArray.append(id as AnyObject )
                    
                }else{
                    // cell.backgroundColor = UIColor.black
                    // remove the indexPath from rowsWhichAreCheckedArray
                    if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                        rowsWhichAreChecked.remove(at: checkedItemIndex)
                        denominationSelectedArray.remove(at: checkedItemIndex)
                    }
                    
                    
                }
                
            }
            nextBtn.isEnabled = true
            farTableView.reloadData()
        }else if(tableView == distanceTableView){
            nextBtn.isEnabled = true

            let obj = distanceArr[indexPath.row]
           // distance = (obj["answer"] as? String)!
            buttonEnableOrDisableforRightButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 1)
            
            
            
        }else if(tableView == ethnicityTableView){
            let obj = ethnicityArr[indexPath.row]
            ethnicity = (obj["answer"] as? String)!
            
            buttonEnableOrDisableforRightButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 1)
            
            nextBtn.isEnabled = true
            
        }else if(tableView == religiousTableView){
           
            
            let obj = religiousArr[indexPath.row]
            religious = (obj["answer"] as? String)!
            
             let id = obj["religion_id"]
            
           // getDenomination(id: id as! String)
            buttonEnableOrDisableforRightButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 1)
            nextBtn.isEnabled = true
        }else if(tableView == educationTableView){
            let obj = educationArr[indexPath.row]
            education = (obj["answer"] as? String)!
            buttonEnableOrDisableforRightButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 1)
            nextBtn.isEnabled = true
        }else if(tableView == smokeTableView){
            let obj = smokeArr[indexPath.row]
            smoke = (obj["answer"] as? String)!
            buttonEnableOrDisableforRightButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 1)
            nextBtn.isEnabled = true
        }else if(tableView == drinkTableView){
            let obj = drinkArr[indexPath.row]
            drink = (obj["answer"] as? String)!
            buttonEnableOrDisableforRightButton(val: 1)
            buttonEnableOrDisableforNextButton(val: 1)
            nextBtn.isEnabled = true
        }
        farTableView.reloadData()
        distanceTableView.reloadData()
        ethnicityTableView.reloadData()
        religiousTableView.reloadData()
        educationTableView.reloadData()
        smokeTableView.reloadData()
        drinkTableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == farTableView {
            return 57
        } else {
            return 57
        }
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        
        // Force popover style
        return UIModalPresentationStyle.none
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return answerArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gridCollectionCell", for: indexPath) as! gridCollectionCell
        cell.contentView.layer.cornerRadius = cell.contentView.frame.height/2
        cell.contentView.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.darkGray.cgColor
        let obj = answerArray[indexPath.row]
      
        if(questionTitle == "far"){
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                cell.contentView.backgroundColor = UIColor.white
                cell.nameLabel.textColor = UIColor.black
            }else{
                cell.contentView.backgroundColor = darkBrownColor
                cell.nameLabel.textColor = UIColor.white
            }
            cell.nameLabel.text =  farArr[indexPath.row]
        }else if(questionTitle == "distance"){
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                cell.contentView.backgroundColor = UIColor.white
                cell.nameLabel.textColor = UIColor.black
            }else{
                cell.contentView.backgroundColor = darkBrownColor
                cell.nameLabel.textColor = UIColor.white
            }
            cell.nameLabel.text =  distanceArr[indexPath.row]
        }else if(questionTitle == "religious"){
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                cell.contentView.backgroundColor = UIColor.white
                cell.nameLabel.textColor = UIColor.black
            }else{
                cell.contentView.backgroundColor = darkBrownColor
                cell.nameLabel.textColor = UIColor.white
            }
            cell.nameLabel.text =  (obj["religion_name"] as! String)
        }else if(questionTitle == "denomination"){
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                cell.contentView.backgroundColor = UIColor.white
                cell.nameLabel.textColor = UIColor.black
            }else{
                cell.contentView.backgroundColor = darkBrownColor
                cell.nameLabel.textColor = UIColor.white
            }
            cell.nameLabel.text =  (obj["mtongue_name"] as! String)
        }else if(questionTitle == "occupation"){
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                cell.contentView.backgroundColor = UIColor.white
                cell.nameLabel.textColor = UIColor.black
            }else{
                cell.contentView.backgroundColor = darkBrownColor
                cell.nameLabel.textColor = UIColor.white
            }
//            if(occupationSelectedArray.count == 0){
//                cell.contentView.backgroundColor = UIColor.white
//                cell.nameLabel.textColor = UIColor.black
//            }else{
//                for i in 0...occupationSelectedArray.count - 1{
//
//                     let id = occupationArr[indexPath.row] as! String
//                    let id1 = occupationSelectedArray[i] as! String
//                    if(id == id1){
//                        cell.contentView.backgroundColor = darkBrownColor
//                        cell.nameLabel.textColor = UIColor.white
//                    }else{
//
//                        cell.contentView.backgroundColor = UIColor.white
//                        cell.nameLabel.textColor = UIColor.black
//
//
//                    }
//
//
//
//                }
//            }
            
            cell.nameLabel.text =  answerArray[indexPath.row] as! String
        }else{
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                cell.contentView.backgroundColor = UIColor.white
                cell.nameLabel.textColor = UIColor.black
            }else{
                cell.contentView.backgroundColor = darkBrownColor
                cell.nameLabel.textColor = UIColor.white
            }
            cell.nameLabel.text =  (obj["answer"] as! String)
        }
        
        return cell
        
       
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(self.questionTitle == "religious"){
            //getDenomination(id: id as! String)
            let obj = religiousArr[indexPath.row]
            let id = obj["religion_name"] as? String ?? ""
            if(id == "All"){
                rowsWhichAreChecked.removeAll()
                religiousSelectedArray.removeAll()
                
                if(rowsWhichAreCheckedForAll.contains(indexPath as NSIndexPath) == false){
                    for i in 0...religiousArr.count - 1 {
                        let obj = religiousArr[i]
                        let id1 = obj["religion_name"] as? String ?? ""
                        let indexPath = NSIndexPath(row: i, section: 0)
                        rowsWhichAreCheckedForAll.append(indexPath)
                        rowsWhichAreChecked.append(indexPath)
                        religiousSelectedArray.append(id1 as AnyObject )
                    }
                }else{
                    rowsWhichAreChecked.removeAll()
                    religiousSelectedArray.removeAll()
                    rowsWhichAreCheckedForAll.removeAll()
                }
            }else{
                if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                    rowsWhichAreChecked.append(indexPath as NSIndexPath)
                    religiousSelectedArray.append(id as AnyObject )
                }else{
                    // cell.backgroundColor = UIColor.black
                    // remove the indexPath from rowsWhichAreCheckedArray
                    if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                        rowsWhichAreChecked.remove(at: checkedItemIndex)
                        religiousSelectedArray.remove(at: checkedItemIndex)

   
                        if(religiousSelectedArray.count == 0){
                            
                        }else{
                            for i in 0...religiousSelectedArray.count - 1{
                                
                                let obj = religiousSelectedArray[i] as! String
                                if(obj == "All"){
                                    rowsWhichAreChecked.remove(at: i)
                                    religiousSelectedArray.remove(at: i)
                                    break
                                }else{
                                    
                                }
                                
                            }
                        }
                        
                       
                        
                        
                    }
                }
            }
            
//            var religionString = String()
//            for i in 0...religiousSelectedArray.count-1{
//
//
//                religionString = religionString + "," + (religiousSelectedArray[i] as! String)
//            }
//            religionString.remove(at: religionString.startIndex)
           // getDenomination(id: religionString)
            
            
            
        }else if(self.questionTitle == "far"){
            far = farArr[indexPath.row]
            farlSelectedRow = indexPath.row
            
            
        }else if(self.questionTitle == "distance"){
            distance = distanceArr[indexPath.row]
            farlSelectedRow = indexPath.row
            
            
        }else if(self.questionTitle == "ethinicity"){
            //getDenomination(id: id as! String)
            let obj = ethnicityArr[indexPath.row]
            let id = obj["answer"] as? String ?? ""
            
            
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                rowsWhichAreChecked.append(indexPath as NSIndexPath)
                
                
                ethinicitySelectedArry.append(id as AnyObject )
                
            }else{
                // cell.backgroundColor = UIColor.black
                // remove the indexPath from rowsWhichAreCheckedArray
                if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                    rowsWhichAreChecked.remove(at: checkedItemIndex)
                    ethinicitySelectedArry.remove(at: checkedItemIndex)
                }
                
                
            }
            
            
            
            
            
        }else if(self.questionTitle == "education"){
            //getDenomination(id: id as! String)
            let obj = educationArr[indexPath.row]
            let id = obj["answer"] as? String ?? ""
            
            
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                rowsWhichAreChecked.append(indexPath as NSIndexPath)
                
                
                educationSelectedArray.append(id as AnyObject )
                
            }else{
                // cell.backgroundColor = UIColor.black
                // remove the indexPath from rowsWhichAreCheckedArray
                if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                    rowsWhichAreChecked.remove(at: checkedItemIndex)
                    educationSelectedArray.remove(at: checkedItemIndex)
                }
                
                
            }
            
        }else if(self.questionTitle == "occupation"){
            //getDenomination(id: id as! String)
            let id = occupationArr[indexPath.row]
            
            
            
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                rowsWhichAreChecked.append(indexPath as NSIndexPath)
                
                
                occupationSelectedArray.append(id as AnyObject )
                
            }else{
                // cell.backgroundColor = UIColor.black
                // remove the indexPath from rowsWhichAreCheckedArray
                if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                    rowsWhichAreChecked.remove(at: checkedItemIndex)
                    occupationSelectedArray.remove(at: checkedItemIndex)
                }
                
                
            }
            
        }else if(self.questionTitle == "smoke"){
            //getDenomination(id: id as! String)
            let obj = smokeArr[indexPath.row]
            let id = obj["answer"] as? String ?? ""
            
            
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                rowsWhichAreChecked.append(indexPath as NSIndexPath)
                
                
                smokeSelectedArray.append(id as AnyObject )
                
            }else{
                // cell.backgroundColor = UIColor.black
                // remove the indexPath from rowsWhichAreCheckedArray
                if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                    rowsWhichAreChecked.remove(at: checkedItemIndex)
                    smokeSelectedArray.remove(at: checkedItemIndex)
                }
                
                
            }
            
        }else if(self.questionTitle == "drink"){
            //getDenomination(id: id as! String)
            let obj = drinkArr[indexPath.row]
            let id = obj["answer"] as? String ?? ""
            
            
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                rowsWhichAreChecked.append(indexPath as NSIndexPath)
                
                
                drinkSelectedArray.append(id as AnyObject )
                
            }else{
                // cell.backgroundColor = UIColor.black
                // remove the indexPath from rowsWhichAreCheckedArray
                if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                    rowsWhichAreChecked.remove(at: checkedItemIndex)
                    drinkSelectedArray.remove(at: checkedItemIndex)
                }
                
                
            }
            
        }else if(self.questionTitle == "denomination"){
            //getDenomination(id: id as! String)
            let obj = denominationArr[indexPath.row]
            let id = obj["mtongue_name"] as? String ?? ""
            
            
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                rowsWhichAreChecked.append(indexPath as NSIndexPath)
                denominationSelectedArray.append(id as AnyObject )
                
            }else{
                // cell.backgroundColor = UIColor.black
                // remove the indexPath from rowsWhichAreCheckedArray
                if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                    rowsWhichAreChecked.remove(at: checkedItemIndex)
                    denominationSelectedArray.remove(at: checkedItemIndex)
                }
                
                
            }
            
        }
        profileCollectionView.reloadData()
        nextBtn.isEnabled = true
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        return CGSize(width: self.profileCollectionView.frame.width/2 - 3, height: 55)
        print("screen width is \(screenWidth)")
        //        if(screenWidth == 375){
        //            return CGSize(width: 170, height: 40)
        //        }else if(screenWidth == 414){
        //            return CGSize(width: 190, height: 40)0
        //        }else if(screenWidth == 320) {
        //            return CGSize(width: 143, height: 40)
        //        }else  if(screenWidth == 1024){
        //            return CGSize(width: 494, height: 40)
        //        }else{
        //            return CGSize(width: 143, height: 40)
        //        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 3
    }
    func dynamicProfileValues(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            let parameters = [:] as [String : Any]
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:dynamicProfile, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    self.data = response["data"] as! [AnyObject]
                    let obj = self.data[0]
                    let obj1 = self.data[1]
                    let obj2 = self.data[2]
                    // let obj3 = self.data[3]
                    let obj4 = self.data[4]
                    let obj5 = self.data[5]
                    self.ethnicityArr = obj["answer"] as! [AnyObject]
                    self.educationArr = obj1["answer"] as! [AnyObject]
                    let array = obj2["answer"] as! [AnyObject]
                    for i in 0...array.count-1{
                        let obj = array[i]
                        self.occupationArr.append(obj["answer"] as AnyObject)
                        self.filterd_OccupationArr.append(obj["answer"] as! String)
                    }
                    self.smokeArr = obj4["answer"] as! [AnyObject]
                    self.drinkArr = obj5["answer"] as! [AnyObject]
                    self.farTableView.reloadData()
                    self.profileCollectionView.reloadData()
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func getReligions(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            let parameters = [:] as [String : Any]
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:getReligion, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    self.religiousArr = response["data"] as! [AnyObject]
                    
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    func getDenomination(id: String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            var dict = [String : AnyObject]()
            dict.updateValue(id as AnyObject, forKey: "religion_id")
            print("log in parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:getMultipleCastes, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                   self.denominationArr = response["data"] as! [AnyObject]
                    
                    self.farTableView.reloadData()
                    self.profileCollectionView.reloadData()
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Success", message: result, sender: self)
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func getMotherToungeApi(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            urlService.serviceCallGetMethod(url: mothertongue, completionHandler: { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    self.denominationArr = response["data"] as! [AnyObject]
                    
                    self.farTableView.reloadData()
                    self.profileCollectionView.reloadData()
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            })
        }else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }

    
    @IBAction func scrollAction(_ sender: Any) {
        
        if(self.answerArray.count == 0){
            
        }else{
            let indexPath = NSIndexPath(row: self.answerArray.count-1, section: 0)
            self.farTableView.scrollToRow(at: indexPath as IndexPath, at: .bottom, animated: true)
            self.profileCollectionView.scrollToItem(at: indexPath as IndexPath, at: .bottom, animated: true)
        }
        
      
        
    }
    func scrollToBottom(animated: Bool = true, delay: Double = 0.0) {
        let numberOfRows = farTableView.numberOfRows(inSection: farTableView.numberOfSections - 1) - 1
        guard numberOfRows > 0 else { return }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) { [unowned self] in
            
            let indexPath = IndexPath(
                row: numberOfRows,
                section: self.farTableView.numberOfSections - 1)
            self.farTableView.scrollToRow(at: indexPath, at: .bottom, animated: animated)
            
              self.profileCollectionView.scrollToItem(at: indexPath as IndexPath, at: .bottom, animated: true)
            
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
       
            answerArray.removeAll()
            
        answerArray = filterd_OccupationArr.filter{ $0.localizedCaseInsensitiveContains(textField.text!) } as [AnyObject]
            if answerArray.count == 0 || textField.text == ""{
                
                answerArray = filterd_OccupationArr as [AnyObject]
            }
            else {
                
            }
            DispatchQueue.main.async {
                self.profileCollectionView.reloadData()
            }
        
        
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
}
