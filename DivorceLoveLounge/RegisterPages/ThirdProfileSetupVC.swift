//
//  ThirdProfileSetupVC.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/20/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
class ThirdProfileSetupVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate {
    @IBOutlet var percentLabel: UILabel!
    @IBOutlet weak var profileCollectionView: UICollectionView!
    @IBOutlet var percentSlider: CustomSlider1!
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var scrollBtn: UIButton!
    
    @IBOutlet var scrollView: UIView!
    
    var countryArr = ["WHITE","HISPANIC/LATINO","BLACK/AFRICAN DECENT","ASIAN/PACIFIC ISLANDER","INDIAN","CHINES","NATIVE AMERICA","ARABIC/MIDDLE EASTREN","KOREAN","JAPANESE","OTHERS"]
    var countrySelectedRow = Int()
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    var optionType = String()
    var question = String()
    var religionId = String()
    var questionIncrement = Int()
    var religiousArray = Array<AnyObject>()
    var answerArray = Array<AnyObject>()
    var casteArray = Array<AnyObject>()
    var questionTitle = String()
    var data = [AnyObject]()
    var percentHint = Float()
    
    
    var ethnicityArr = Array<AnyObject>()
    var religiousArr = Array<AnyObject>()
    var educationArr = Array<AnyObject>()
    var occupationArr = Array<AnyObject>()
    var smokeArr = Array<AnyObject>()
    var drinkArr = Array<AnyObject>()
    var denominationArr = Array<AnyObject>()
    var incomeArray = Array<AnyObject>()
    
    
    
    
    
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var countryTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.layer.cornerRadius = scrollView.frame.height/2
        countryTableView.delegate = self
        countryTableView.dataSource = self
        countrySelectedRow = -1
        self.countryTableView.register(UINib(nibName: "myDataTableViewCell", bundle: nil), forCellReuseIdentifier: "myDataTableViewCell")
        self.profileCollectionView.register(UINib(nibName:"gridCollectionCell" , bundle:nil), forCellWithReuseIdentifier: "gridCollectionCell")
        profileCollectionView.isHidden = true
        countryTableView.isHidden = false
        percentSlider.value = percentHint
        let val = Int(percentHint)
        percentLabel.text = String(val) + "%"
        percentSlider.setThumbImage(UIImage(), for: .normal)
        rightBtn.setImage(#imageLiteral(resourceName: "right-arrow-hide"), for: .normal)
        rightBtn.isEnabled = false
//        nextBtn.backgroundColor = buttonHideColor
        nextBtn.isEnabled = false
        self.nextBtn.layer.cornerRadius = 5
        dynamicProfileValues()
        getReligions()
        buttonEnableOrDisableforLeftButton(val: 1)
        getMotherToungeApi()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func skipBtnAction(_ sender: Any) {
        let usermail =  self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginUserName") as? String)
        let userpass =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginPassword") as? String)
        let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard1.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        vc.logIn(fromScreen: "signup", userEmail: usermail! as! String, userPassword: userpass! as! String)
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to skip this section?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { [] (_) in
            let imageUpload = UserDefaults.standard.object(forKey: "imageUpload")
            if(imageUpload is NSNull || imageUpload == nil){
                
                
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "EditProfilePicVC") as! EditProfilePicVC
                vc.fromScreen = "AppDelegate"
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func leftBtnAction(_ sender: Any) {
        //self.navigationController?.popViewController(animated: true)
        countrySelectedRow = -1
        //        leftButton.setImage(#imageLiteral(resourceName: "left-arrow (16)"), for: .normal)
        leftButton.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        questionIncrement = questionIncrement+1
        print("exceeded")
        if(self.questionTitle == "Religious"){
             //percentHint = percentHint - 10
            self.navigationController?.popViewController(animated: true)
            leftButton.isEnabled = false
            buttonEnableOrDisableforLeftButton(val: 0)
        }else if(self.questionTitle == "Denomination"){
             percentHint = percentHint - 10
            getReligions()
            percentSlider.value = Float(percentHint)
            let val = Int(percentHint)
            percentLabel.text = String(val) + "%"
            leftButton.isEnabled = false
            buttonEnableOrDisableforLeftButton(val: 0)
        }else if(self.questionTitle == "ethnicity"){
            nextBtn.isEnabled = true
            self.answerArray = casteArray
            self.questionLabel.text = "What's your Mother Tongue"
            questionLabel.sizeToFit()
            self.questionTitle = "Denomination"
            percentHint = percentHint - 12
            percentSlider.value = Float(percentHint)
            let val = Int(percentHint)
            percentLabel.text = String(val) + "%"
            buttonEnableOrDisableforNextButton(val: 1)
            buttonEnableOrDisableforRightButton(val: 0)
          //  self.questionTitle = "Denomination"
            leftButton.isEnabled = false
            buttonEnableOrDisableforLeftButton(val: 0)
            leftButton.isEnabled = false
            buttonEnableOrDisableforLeftButton(val: 0)
        }else if(self.questionTitle == "education"){
            let obj = self.data[0]
            self.optionType = obj["option_type"] as! String
            self.question = obj["question"] as! String
            self.answerArray = obj["answer"] as! Array
            self.questionLabel.text = self.question
            self.questionTitle = obj["title"] as! String
            percentHint = percentHint - 6
            percentSlider.value = Float(percentHint)
            let val = Int(percentHint)
            percentLabel.text = String(val) + "%"
            questionLabel.sizeToFit()
            leftButton.isEnabled = false
            buttonEnableOrDisableforLeftButton(val: 0)
       
            
        }else if(self.questionTitle == "occupation"){
            let obj = self.data[1]
            self.optionType = obj["option_type"] as! String
            self.question = obj["question"] as! String
            self.answerArray = obj["answer"] as! Array
            self.questionLabel.text = self.question
            self.questionTitle = obj["title"] as! String
            percentHint = percentHint - 6
            percentSlider.value = Float(percentHint)
            let val = Int(percentHint)
            percentLabel.text = String(val) + "%"
            questionLabel.sizeToFit()
            leftButton.isEnabled = false
            buttonEnableOrDisableforLeftButton(val: 0)
       
        }else if(self.questionTitle == "income"){
            let obj = self.data[2]
            self.optionType = obj["option_type"] as! String
            self.question = obj["question"] as! String
            self.answerArray = obj["answer"] as! Array
            self.questionLabel.text = self.question
            self.questionTitle = obj["title"] as! String
            questionLabel.sizeToFit()
            leftButton.isEnabled = false
            buttonEnableOrDisableforLeftButton(val: 0)
            percentHint = percentHint - 6
            percentSlider.value = Float(percentHint)
            let val = Int(percentHint)
            percentLabel.text = String(val) + "%"
            
            
            
        }else if(self.questionTitle == "smoke"){
            let obj = self.data[3]
            self.optionType = obj["option_type"] as! String
            self.question = obj["question"] as! String
            self.answerArray = obj["answer"] as! Array
            self.questionLabel.text = self.question
            self.questionTitle = obj["title"] as! String
            questionLabel.sizeToFit()
            leftButton.isEnabled = false
            buttonEnableOrDisableforLeftButton(val: 0)
            
            percentHint = percentHint - 6
            percentSlider.value = Float(percentHint)
            let val = Int(percentHint)
            percentLabel.text = String(val) + "%"
            
            
            
            
        }else if(self.questionTitle == "drink"){
            let obj = self.data[4]
            self.optionType = obj["option_type"] as! String
            self.question = obj["question"] as! String
            self.answerArray = obj["answer"] as! Array
            self.questionLabel.text = self.question
            self.questionTitle = obj["title"] as! String
            questionLabel.sizeToFit()
            leftButton.isEnabled = false
            buttonEnableOrDisableforLeftButton(val: 0)
            percentHint = percentHint - 6
            percentSlider.value = Float(percentHint)
            let val = Int(percentHint)
            percentLabel.text = String(val) + "%"
     
        }
        
        if(self.questionTitle == "drink"){
            profileCollectionView.isHidden = true
            countryTableView.isHidden = false
        }else if(self.questionTitle == "smoke"){
            profileCollectionView.isHidden = true
            countryTableView.isHidden = false
        }else if(self.questionTitle == "income"){
            profileCollectionView.isHidden = true
            countryTableView.isHidden = false
        }else if(self.questionTitle == "occupation"){
            profileCollectionView.isHidden = false
            countryTableView.isHidden = true
        }else if(self.questionTitle == "education"){
            
            profileCollectionView.isHidden = true
            countryTableView.isHidden = false
            
        }else if(self.questionTitle == "ethnicity"){
            profileCollectionView.isHidden = true
            countryTableView.isHidden = false
        }else if(self.questionTitle == "Religious"){
            profileCollectionView.isHidden = false
            countryTableView.isHidden = true
        }
        
        
        if(answerArray.count > 10){
            // scrollBtn.isHidden = false
            scrollView.isHidden = false
        }else{
            scrollView.isHidden = true
        }
        
        profileCollectionView.reloadData()
        countryTableView.reloadData()
        
    }
    @IBAction func nextBtnAction(_ sender: Any) {
        countrySelectedRow = -1
//        leftButton.setImage(#imageLiteral(resourceName: "left-arrow (16)"), for: .normal)
         // self.profileCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: UICollectionViewScrollPosition.top, animated: false)
        
        self.profileCollectionView?.scrollToItem(at: IndexPath(row: 0, section: 0),
                                          at: .top,
                                          animated: true)
        self.countryTableView.scroll(to: .top, animated: true)
       leftButton.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        questionIncrement = questionIncrement+1
        leftButton.isEnabled = true
        buttonEnableOrDisableforLeftButton(val: 1)
            print("exceeded")
            if(self.questionTitle == "Religious"){
                nextBtn.isEnabled = true
                self.answerArray = casteArray
                self.questionLabel.text = "What's your Mother Tongue"
                questionLabel.sizeToFit()
                self.questionTitle = "Denomination"
                percentHint = percentHint + 10
                percentSlider.value = Float(percentHint)
                let val = Int(percentHint)
                percentLabel.text = String(val) + "%"
                buttonEnableOrDisableforNextButton(val: 1)
                buttonEnableOrDisableforRightButton(val: 0)
            }else if(self.questionTitle == "Denomination"){
                nextBtn.isEnabled = true
                
                let obj = self.data[0]
                self.optionType = obj["option_type"] as! String
                self.question = obj["question"] as! String
                self.answerArray = obj["answer"] as! Array
                self.questionLabel.text = self.question
                questionLabel.sizeToFit()
                self.questionTitle = obj["title"] as! String
                percentHint = percentHint + 12
                percentSlider.value = Float(percentHint)
                let val = Int(percentHint)
                percentLabel.text = String(val) + "%"

                    //leftButton.isEnabled = false
                  //  buttonEnableOrDisableforLeftButton(val: 0)
            }else if(self.questionTitle == "ethnicity"){
                
                let obj = self.data[1]
                self.optionType = obj["option_type"] as! String
                self.question = obj["question"] as! String
                self.answerArray = obj["answer"] as! Array
                self.questionLabel.text = self.question
                self.questionTitle = obj["title"] as! String
                percentHint = percentHint + 6
                percentSlider.value = Float(percentHint)
                let val = Int(percentHint)
                percentLabel.text = String(val) + "%"
                questionLabel.sizeToFit()
               // leftButton.isEnabled = false
               // buttonEnableOrDisableforLeftButton(val: 0)
                
                
                
            }else if(self.questionTitle == "education"){
                
                
                let obj = self.data[2]
                self.optionType = obj["option_type"] as! String
                self.question = obj["question"] as! String
                self.answerArray = obj["answer"] as! Array
                self.questionLabel.text = self.question
                self.questionTitle = obj["title"] as! String
                percentHint = percentHint + 6
                percentSlider.value = Float(percentHint)
                let val = Int(percentHint)
                percentLabel.text = String(val) + "%"
                questionLabel.sizeToFit()
               // leftButton.isEnabled = false
               // buttonEnableOrDisableforLeftButton(val: 0)
                
                
                
            }else if(self.questionTitle == "occupation"){
                
                
                let obj = self.data[3]
                self.optionType = obj["option_type"] as! String
                self.question = obj["question"] as! String
                self.answerArray = obj["answer"] as! Array
                self.questionLabel.text = self.question
                self.questionTitle = obj["title"] as! String
                questionLabel.sizeToFit()
                //leftButton.isEnabled = false
               // buttonEnableOrDisableforLeftButton(val: 0)
                percentHint = percentHint + 6
                percentSlider.value = Float(percentHint)
                let val = Int(percentHint)
                percentLabel.text = String(val) + "%"
                
                
            }else if(self.questionTitle == "income"){
                
                
                let obj = self.data[4]
                self.optionType = obj["option_type"] as! String
                self.question = obj["question"] as! String
                self.answerArray = obj["answer"] as! Array
                self.questionLabel.text = self.question
                self.questionTitle = obj["title"] as! String
                questionLabel.sizeToFit()
               // leftButton.isEnabled = false
              //  buttonEnableOrDisableforLeftButton(val: 0)
                
                percentHint = percentHint + 6
                percentSlider.value = Float(percentHint)
                let val = Int(percentHint)
                percentLabel.text = String(val) + "%"
                
            }else if(self.questionTitle == "smoke"){
                
                
                let obj = self.data[5]
                self.optionType = obj["option_type"] as! String
                self.question = obj["question"] as! String
                self.answerArray = obj["answer"] as! Array
                self.questionLabel.text = self.question
                self.questionTitle = obj["title"] as! String
                questionLabel.sizeToFit()
               // leftButton.isEnabled = false
              //  buttonEnableOrDisableforLeftButton(val: 0)
                percentHint = percentHint + 6
                percentSlider.value = Float(percentHint)
                let val = Int(percentHint)
                percentLabel.text = String(val) + "%"
                
                
            }else if(self.questionTitle == "drink"){
                
                 leftButton.isEnabled = false
                  buttonEnableOrDisableforLeftButton(val: 0)
                                    let secondProfileVc = self.storyboard?.instantiateViewController(withIdentifier: "EighthProfileSetupVC") as! EighthProfileSetupVC
                                    secondProfileVc.percentHint = Float(78)
                                    self.navigationController?.pushViewController(secondProfileVc, animated: true)
                
                
            }
    
        if(self.questionTitle == "drink"){
            profileCollectionView.isHidden = true
            countryTableView.isHidden = false
        }else if(self.questionTitle == "smoke"){
            profileCollectionView.isHidden = true
            countryTableView.isHidden = false
        }else if(self.questionTitle == "income"){
            profileCollectionView.isHidden = true
            countryTableView.isHidden = false
        }else if(self.questionTitle == "occupation"){
            profileCollectionView.isHidden = false
            countryTableView.isHidden = true
        }else if(self.questionTitle == "education"){
            
            profileCollectionView.isHidden = true
            countryTableView.isHidden = false
           
        }else if(self.questionTitle == "ethnicity"){
            profileCollectionView.isHidden = true
            countryTableView.isHidden = false
        }else if(self.questionTitle == "Religious"){
            profileCollectionView.isHidden = false
            countryTableView.isHidden = true
        }
        
        
        if(answerArray.count > 10){
            // scrollBtn.isHidden = false
            scrollView.isHidden = false
        }else{
            scrollView.isHidden = true
        }
        
         profileCollectionView.reloadData()
        countryTableView.reloadData()
//        let secondProfileVc = self.storyboard?.instantiateViewController(withIdentifier: "FourthProfileSetUpVC") as! FourthProfileSetUpVC
//        self.navigationController?.pushViewController(secondProfileVc, animated: true)
    }
    @IBAction func nextBarBtn(_ sender: Any) {
        leftButton.setImage(#imageLiteral(resourceName: "left-arrow (16)"), for: .normal)
        leftButton.isEnabled = false
        let secondProfileVc = self.storyboard?.instantiateViewController(withIdentifier: "FourthProfileSetUpVC") as! FourthProfileSetUpVC
        self.navigationController?.pushViewController(secondProfileVc, animated: true)
        
    }
    @IBAction func scrollAction(_ sender: Any) {
        
        if(self.answerArray.count == 0){
            
        }else{
            let indexPath = NSIndexPath(row: self.answerArray.count-1, section: 0)
            self.countryTableView.scrollToRow(at: indexPath as IndexPath, at: .bottom, animated: true)
            
            self.profileCollectionView.scrollToItem(at: indexPath as IndexPath, at: .bottom, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.answerArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "myDataTableViewCell", for: indexPath) as! myDataTableViewCell
              //  cell.dataLabel.layer.cornerRadius = 5
//                cell.dataLabel.layer.borderWidth = 1
//                cell.dataLabel.layer.borderColor = UIColor.lightGray.cgColor
                cell.dataLabel.textAlignment = .center
       if(countrySelectedRow == indexPath.row){
          cell.backView.backgroundColor = darkBrownColor
          cell.dataLabel.textColor = UIColor.white
        
        }else{
          cell.backView.backgroundColor = UIColor.white
          cell.dataLabel.textColor = UIColor.black
        }
        tableView.separatorStyle = .none
         let obj = answerArray[indexPath.row]
        if(questionTitle == "Religious"){
            cell.dataLabel.text =  (obj["religion_name"] as! String)
        }else if(questionTitle == "Denomination"){
            cell.dataLabel.text =  (obj["mtongue_name"] as! String)
        }else{
            cell.dataLabel.text =  (obj["answer"] as! String)
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 57
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        countrySelectedRow = indexPath.row
       // profileDict["country"] = countryArr[indexPath.row] as AnyObject
        rightBtn.setImage(#imageLiteral(resourceName: "right-arrow-black"), for: .normal)
        rightBtn.isEnabled = true
//        nextBtn.backgroundColor = darkBrownColor
        nextBtn.isEnabled = true
        if(self.questionTitle == "drink"){
            let obj = answerArray[indexPath.row]
            profileDict["drink"] = (obj["answer"] as! String) as AnyObject
        }else if(self.questionTitle == "smoke"){
            let obj = answerArray[indexPath.row]
            profileDict["smoke"] = (obj["answer"] as! String) as AnyObject
        }else if(self.questionTitle == "income"){
            let obj = answerArray[indexPath.row]
            profileDict["income"] = (obj["answer"] as! String) as AnyObject
        }else if(self.questionTitle == "occupation"){
            let obj = answerArray[indexPath.row]
            profileDict["occupation"] = (obj["answer"] as! String) as AnyObject
        }else if(self.questionTitle == "education"){
            let obj = answerArray[indexPath.row]
            profileDict["education"] = (obj["answer"] as! String) as AnyObject
        }else if(self.questionTitle == "ethnicity"){
            let obj = answerArray[indexPath.row]
            profileDict["country"] = (obj["answer"] as! String) as AnyObject
        }else if(self.questionTitle == "ethnicity"){
            let obj = answerArray[indexPath.row]
            profileDict["ethnicity"] = (obj["answer"] as! String) as AnyObject
        }else if(self.questionTitle == "Religious"){
            let obj = answerArray[indexPath.row]
            profileDict["religion"] = (obj["religion_name"] as! String) as AnyObject
            
            let id = obj["religion_id"] as! String
            
           //  getDenomination(id: id)
        }else if(self.questionTitle == "Denomination"){
            let obj = answerArray[indexPath.row]
            profileDict["denomination"] = (obj["mtongue_name"] as! String) as AnyObject
            
        }
       
        buttonEnableOrDisableforNextButton(val: 1)
        buttonEnableOrDisableforRightButton(val: 1)
        
        countryTableView.reloadData()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.answerArray.count < Int(scrollView.contentOffset.y)) {
            // did move up
//            if(self.answerArray.count > 10){
//                 self.scrollView.isHidden = false
//            }else{
//                self.scrollView.isHidden = true
//            }
            self.scrollView.isHidden = true
            
           
        } else if (self.answerArray.count > Int(scrollView.contentOffset.y)) {
            
            
            
            self.scrollView.isHidden = false
        } else {
            // didn't move
        }
    }
    
    
    func buttonEnableOrDisableforLeftButton(val : Int){
        
        if(val == 0){
//            leftButton.setImage(#imageLiteral(resourceName: "left-arrow (16)"), for: .normal)
            leftButton.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        }else{
//            leftButton.setImage(#imageLiteral(resourceName: "left-arrow-black"), for: .normal)
            leftButton.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        }
        
    }
    
    func buttonEnableOrDisableforRightButton(val : Int){
        if(val == 0){
            rightBtn.setImage(#imageLiteral(resourceName: "right-arrow-hide"), for: .normal)
        }else{
            rightBtn.setImage(#imageLiteral(resourceName: "right-arrow-black"), for: .normal)
        }
    }
    func buttonEnableOrDisableforNextButton(val : Int){
        if(val == 0){
//            nextBtn.backgroundColor = buttonHideColor
        }else{
//            nextBtn.backgroundColor = darkBrownColor
        }
    }
    func dynamicProfileValues(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            let parameters = [:] as [String : Any]
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:dynamicProfile, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    self.data = response["data"] as! [AnyObject]
                    let obj = self.data[0]
                    let obj1 = self.data[1]
                    let obj2 = self.data[2]
                    let obj3 = self.data[3]
                    let obj4 = self.data[4]
                    let obj5 = self.data[5]
                    self.ethnicityArr = obj["answer"] as! [AnyObject]
                    self.educationArr = obj1["answer"] as! [AnyObject]
                    let array = obj2["answer"] as! [AnyObject]
                    for i in 0...array.count-1{
                        let obj = array[i]
                        self.occupationArr.append(obj["answer"] as AnyObject)
//                        self.filterd_OccupationArr.append(obj["answer"] as! String)
                    }
                    self.smokeArr = obj4["answer"] as! [AnyObject]
                    self.incomeArray = obj3["answer"] as! [AnyObject]
                    self.drinkArr = obj5["answer"] as! [AnyObject]
            
                    
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func getReligions(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            let parameters = [:] as [String : Any]
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:getReligion, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    var data1 = response["data"] as! [AnyObject]
                    for i in 0..<data1.count{
                        let obj = data1[i]
                        let religion_name = obj["religion_name"] as! String
                        if(religion_name == "All"){
                            data1.remove(at: i)
                            break
                        }
                    }
                    self.answerArray = data1
                    self.questionLabel.text = "What's your Religion"
                    self.questionTitle = "Religious"
//                    self.questionLabel.text = "What's your Religion"
//                    self.questionTitle = "Religious"
                     self.countryTableView.reloadData()
                    self.profileCollectionView.reloadData()
               } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        }else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func getDenomination(id: String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            var dict = [String : AnyObject]()
            dict.updateValue(id as AnyObject, forKey: "religion_id")
            print("log in parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:getCaste, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                   
                     self.casteArray = response["data"] as! [AnyObject]
                    
                    
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    
    func getMotherToungeApi(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            urlService.serviceCallGetMethod(url: mothertongue, completionHandler: { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    self.casteArray = response["data"] as! [AnyObject]
                    
                    
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            })
        }else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }

    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  self.answerArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gridCollectionCell", for: indexPath) as! gridCollectionCell
        
        cell.contentView.layer.cornerRadius = cell.contentView.frame.height/2
        cell.contentView.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.darkGray.cgColor
        if(countrySelectedRow == indexPath.row){
            cell.contentView.backgroundColor = darkBrownColor
            cell.nameLabel.textColor = UIColor.white
            
        }else{
            cell.contentView.backgroundColor = UIColor.white
            cell.nameLabel.textColor = UIColor.black
        }
       
        let obj = answerArray[indexPath.row]
        if(questionTitle == "Religious"){
            cell.nameLabel.text =  (obj["religion_name"] as! String)
        }else if(questionTitle == "Denomination"){
            cell.nameLabel.text =  (obj["mtongue_name"] as! String)
        }else{
            cell.nameLabel.text =  (obj["answer"] as! String)
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        
       
        
        return CGSize(width: self.profileCollectionView.frame.width/2 - 3, height: 50)
        
        print("screen width is \(screenWidth)")
//        if(screenWidth == 375){
//            return CGSize(width: 170, height: 40)
//        }else if(screenWidth == 414){
//            return CGSize(width: 190, height: 40)0
//        }else if(screenWidth == 320) {
//            return CGSize(width: 143, height: 40)
//        }else  if(screenWidth == 1024){
//            return CGSize(width: 494, height: 40)
//        }else{
//            return CGSize(width: 143, height: 40)
//        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        countrySelectedRow = indexPath.row
        // profileDict["country"] = countryArr[indexPath.row] as AnyObject
        rightBtn.setImage(#imageLiteral(resourceName: "right-arrow-black"), for: .normal)
        rightBtn.isEnabled = true
//        nextBtn.backgroundColor = darkBrownColor
        nextBtn.isEnabled = true
        if(self.questionTitle == "drink"){
            let obj = answerArray[indexPath.row]
            profileDict["drink"] = (obj["answer"] as! String) as AnyObject
        }else if(self.questionTitle == "smoke"){
            let obj = answerArray[indexPath.row]
            profileDict["smoke"] = (obj["answer"] as! String) as AnyObject
        }else if(self.questionTitle == "income"){
            let obj = answerArray[indexPath.row]
            profileDict["income"] = (obj["answer"] as! String) as AnyObject
        }else if(self.questionTitle == "occupation"){
            let obj = answerArray[indexPath.row]
            profileDict["occupation"] = (obj["answer"] as! String) as AnyObject
        }else if(self.questionTitle == "education"){
            let obj = answerArray[indexPath.row]
            profileDict["education"] = (obj["answer"] as! String) as AnyObject
        }else if(self.questionTitle == "ethnicity"){
            let obj = answerArray[indexPath.row]
            profileDict["country"] = (obj["answer"] as! String) as AnyObject
        }else if(self.questionTitle == "ethnicity"){
            let obj = answerArray[indexPath.row]
            profileDict["ethnicity"] = (obj["answer"] as! String) as AnyObject
        }else if(self.questionTitle == "Religious"){
            let obj = answerArray[indexPath.row]
            profileDict["religion"] = (obj["religion_name"] as! String) as AnyObject
            
            let id = obj["religion_id"] as! String
            
          //  getDenomination(id: id)
        }else if(self.questionTitle == "Denomination"){
            let obj = answerArray[indexPath.row]
            profileDict["denomination"] = (obj["mtongue_name"] as! String) as AnyObject
            
        }
        
        buttonEnableOrDisableforNextButton(val: 1)
        buttonEnableOrDisableforRightButton(val: 1)
        
        profileCollectionView.reloadData()
    }
}
