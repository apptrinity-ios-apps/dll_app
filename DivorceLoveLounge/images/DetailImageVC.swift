//
//  DetailImageVC.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 25/06/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class DetailImageVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate {
    
    @IBOutlet weak var smallCV: UICollectionView!
    @IBOutlet weak var largeCV: UICollectionView!
    
      var imageArray = [AnyObject]()
    
    var page = Int()
    var index = Int()
    var index2 = Int()
    var select = Int()
    var selectedCurrentPage = Int()
    
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()
        page = 0
        select = 0
        
        // Do any additional setup after loading the view.
         self.smallCV.register(UINib(nibName: "smallDetailImageCell", bundle: nil), forCellWithReuseIdentifier: "smallDetailImageCell")
        
        self.largeCV.register(UINib(nibName: "largeDetailImageCell", bundle: nil), forCellWithReuseIdentifier: "largeDetailImageCell")

    }

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == smallCV){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "smallDetailImageCell", for: indexPath)as! smallDetailImageCell
           //index = indexPath.row
            if indexPath.item == selectedCurrentPage {
                cell.alphaView.isHidden = true
                smallCV.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            } else {
                cell.alphaView.isHidden = false
            }
            let url =   self.themes.checkNullValue(imageArray[indexPath.row] as AnyObject?) as! String
            let imageBaseUrl = ImagebaseUrl + url
            DispatchQueue.main.async {
                cell.imageView.sd_setShowActivityIndicatorView(true)
                cell.imageView.sd_setIndicatorStyle(.whiteLarge)
                cell.imageView.sd_setImage(with: URL(string: imageBaseUrl), placeholderImage:UIImage(named: "defaultPic"), completed: { (image, error, cacheType, url) -> Void in
                    if ((error) != nil) {
                        cell.imageView.image = UIImage(named: "defaultPic")
                        cell.imageView.sd_setShowActivityIndicatorView(true)
                    } else {
                        cell.imageView.image = image
                        cell.imageView.sd_setShowActivityIndicatorView(false)
                    }
                })
            }
          //  cell.imageView.sd_setImage(with: URL(string: imageBaseUrl), placeholderImage: UIImage(named: "defaultPic"))
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "largeDetailImageCell", for: indexPath)as! largeDetailImageCell
            
            index2 = indexPath.item
            let url =   self.themes.checkNullValue(imageArray[indexPath.row] as AnyObject?) as! String
            let imageBaseUrl = ImagebaseUrl + url
            DispatchQueue.main.async {

                cell.imageView.sd_setShowActivityIndicatorView(true)
                cell.imageView.sd_setIndicatorStyle(.whiteLarge)
                cell.imageView.sd_setImage(with: URL(string: imageBaseUrl), placeholderImage:UIImage(named: "defaultPic"), completed: { (image, error, cacheType, url) -> Void in
                    if ((error) != nil) {
                        cell.imageView.image = UIImage(named: "defaultPic")
                        cell.imageView.sd_setShowActivityIndicatorView(true)
                    } else {
                        cell.imageView.image = image
                        cell.imageView.sd_setShowActivityIndicatorView(false)
                    }
                })
            }
           // cell.imageView.sd_setImage(with: URL(string: imageBaseUrl), placeholderImage: UIImage(named: "defaultPic"))
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == smallCV){
            return CGSize(width: 108, height: 108)
        }else{
             return CGSize(width: self.view.frame.width, height: self.view.frame.height - 160)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView == smallCV){
            
            selectedCurrentPage = indexPath.row
            
           

            largeCV.scrollToItem(at: indexPath, at: .left, animated: true)
            smallCV.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.smallCV.reloadData()
        }
//        else if(collectionView == largeCV){
//
//            smallCV.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
//
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if(collectionView == smallCV){
          return 0
        }
        return 0
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (scrollView == largeCV) {
//            let indexPath = IndexPath(row: 0, section: 0)
//            let cell = smallCV.cellForItem(at: indexPath)as? smallDetailImageCell
        let x = scrollView.contentOffset.x
        let w = scrollView.bounds.size.width
        let currentPage = Int(ceil(x/w))
        print(currentPage)
         selectedCurrentPage = currentPage
            DispatchQueue.main.async {
                self.smallCV.reloadData()
            }
        }
 
    }

}
