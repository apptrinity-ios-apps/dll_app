//
//  largeDetailImageCell.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 25/06/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class largeDetailImageCell: UICollectionViewCell,UIScrollViewDelegate {
    @IBOutlet var scrollview: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        scrollview.delegate = self
        scrollview.minimumZoomScale = 1.0
        scrollview.maximumZoomScale = 6.0
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
