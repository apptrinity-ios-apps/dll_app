//
//  TopTabBarVC.swift
//  DivorceLoveLounge
//
//  Created by nagaraj  kumar on 19/06/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class TopTabBarVC: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate{

    
    
    
    @IBOutlet var profileLBL: UILabel!
    
    @IBOutlet var favouritesLBL: UILabel!
    
    @IBOutlet var discoverLBL: UILabel!
    
    @IBOutlet var chatLBL: UILabel!
    
    @IBOutlet var profileview: UIView!
    
    @IBOutlet var inboxLBL: UILabel!
    @IBOutlet var profileImage: UIImageView!
    
    
    @IBOutlet var favouriteview: UIView!
    
    @IBOutlet var favouriteimage: UIImageView!
    
    @IBOutlet var discoverview: UIView!
    
    @IBOutlet var dicoverImage: UIImageView!
    
    
    @IBOutlet var chatview: UIView!
    
    
    @IBOutlet var chatimage: UIImageView!
    @IBOutlet var inboxview: UIView!
    
    @IBOutlet var inboximage: UIImageView!
    
    
    @IBOutlet weak var tabBarView: UIView!
    @IBOutlet private weak var btnTab1: UIButton!
    @IBOutlet private weak var btnTab2: UIButton!
    @IBOutlet private weak var btnTab3: UIButton!
    @IBOutlet private weak var btnTab4: UIButton!
    @IBOutlet private weak var btnTab5: UIButton!    
   
    @IBOutlet var mesgCountLbl: UILabel!
    
    @IBOutlet var favrtCountLbl: UILabel!
    @IBOutlet var chatcount: UILabel!
    
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    
    var fromscreen = String()
    
    
    var tab1VC:ProfileVC! = nil
    var tab2VC:FavoriteVC! = nil
    var tab3VC:HomeVC! = nil
    var tab4VC:MainViewController! = nil
    var tab5VC:MessagesViewController! = nil
    
    
    private var pageController: UIPageViewController!
    

    private var arrVC:[UIViewController] = []
    private var currentPage: Int!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        chatcount.isHidden = true
        
        currentPage = 2
        createPageViewController()
        
        tabBarView.layer.shadowOffset = CGSize(width:0, height: 2)
        tabBarView.layer.shadowOpacity = 2.0
        tabBarView.layer.shadowRadius = 2
        tabBarView.layer.masksToBounds = false
        tabBarView.layer.shadowColor = UIColor.gray.cgColor
        
        
        
        
        mesgCountLbl.layer.cornerRadius = mesgCountLbl.frame.size.height/2
        mesgCountLbl.clipsToBounds = true
        favrtCountLbl.layer.cornerRadius = favrtCountLbl.frame.size.height/2
        favrtCountLbl.clipsToBounds = true
        
      //  chatcount.layer.cornerRadius = chatcount.frame.size.height/2
      //  chatcount.clipsToBounds = true
        
        mesgCountLbl.layer.shadowOffset = CGSize(width:0, height: 0)
        mesgCountLbl.layer.shadowOpacity = 2.0
        mesgCountLbl.layer.shadowRadius = 2
        mesgCountLbl.layer.shadowColor = UIColor.gray.cgColor
        
        
       // chatcount.layer.shadowOffset = CGSize(width:0, height: 0)
       // chatcount.layer.shadowOpacity = 2.0
       // chatcount.layer.shadowRadius = 2
       // chatcount.layer.shadowColor = UIColor.gray.cgColor
        
        favrtCountLbl.layer.shadowOffset = CGSize(width:0, height: 0)
        favrtCountLbl.layer.shadowOpacity = 2.0
        favrtCountLbl.layer.shadowRadius = 2
        favrtCountLbl.layer.shadowColor = UIColor.gray.cgColor
        
        
        
       profileview.layer.cornerRadius = profileview.frame.size.height/2
       profileview.clipsToBounds = true
        
        
        favouriteview.layer.cornerRadius = favouriteview.frame.size.height/2
        favouriteview.clipsToBounds = true
        
        
        discoverview.layer.cornerRadius = discoverview.frame.size.height/2
        discoverview.clipsToBounds = true
        
        
        chatview.layer.cornerRadius = chatview.frame.size.height/2
        chatview.clipsToBounds = true
        
        
        inboxview.layer.cornerRadius = inboxview.frame.size.height/2
        inboxview.clipsToBounds = true
        
        
        
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getUsermessagesCountsApi()
        getUserCountApi()
        fetchPhotoRequest(userID: "")
    }
    
    //MARK: - Custom Methods
    
    private func selectedButton(btn: UIButton) {
        
        btn.setTitleColor(UIColor.black, for: .normal)
        
//        constantViewLeft.constant = btn.frame.origin.x
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        
    }
    
    private func unSelectedButton(btn: UIButton) {
        btn.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
    }
    
    @IBAction private func btnOptionClicked(btn: UIButton) {
//        fromscreen = ""
        pageController.setViewControllers([arrVC[btn.tag-1]], direction: UIPageViewControllerNavigationDirection.reverse, animated: false, completion: {(Bool) -> Void in
        })
        
        resetTabBarForTag(tag: btn.tag-1)
    }
    
    
    @IBAction func profileAction(_ sender: Any) {
        
//        btnTab1.setImage(UIImage(named: "profileSelect"), for: .normal)
//        btnTab2.setImage(UIImage(named: "starDefault"), for: .normal)
//        btnTab3.setImage(UIImage(named: "vibrateDefault"), for: .normal)
//        btnTab4.setImage( UIImage(named: "chatDefault"), for: .normal)
//        btnTab5.setImage(UIImage(named: "mesg_icon_default"), for: .normal)
//
        
        self.shadowView(view: profileview)
        self.unShadowView(view: favouriteview)
        self.unShadowView(view: discoverview)
        self.unShadowView(view: chatview)
        self.unShadowView(view: inboxview)
        
        
        profileLBL.textColor = UIColor.red
        favouritesLBL.textColor = UIColor.black
        discoverLBL.textColor = UIColor.black
        chatLBL.textColor = UIColor.black
        inboxLBL.textColor = UIColor.black
        
        
        getUsermessagesCountsApi()
        getUserCountApi()
        fetchPhotoRequest(userID: "")
        
    }
    @IBAction func favoriteAction(_ sender: Any) {
        
//        btnTab1.setImage(UIImage(named: "profileDefault"), for: .normal)
//        btnTab2.setImage(UIImage(named: "starSelect"), for: .normal)
//        btnTab3.setImage(UIImage(named: "vibrateDefault"), for: .normal)
//        btnTab4.setImage( UIImage(named: "chatDefault"), for: .normal)
//        btnTab5.setImage(UIImage(named: "mesg_icon_default"), for: .normal)
//
        
        
        self.shadowView(view: favouriteview)
        self.unShadowView(view: profileview)
        self.unShadowView(view: discoverview)
        self.unShadowView(view: chatview)
        self.unShadowView(view: inboxview)
        
        
        profileLBL.textColor = UIColor.black
        favouritesLBL.textColor = UIColor.red
        discoverLBL.textColor = UIColor.black
        chatLBL.textColor = UIColor.black
        inboxLBL.textColor = UIColor.black
        
        
        
        getUsermessagesCountsApi()
        getUserCountApi()
         fetchPhotoRequest(userID: "")
    }
    @IBAction func dashBoardAction(_ sender: Any) {
        
//        btnTab1.setImage(UIImage(named: "profileDefault"), for: .normal)
//        btnTab2.setImage(UIImage(named: "starDefault"), for: .normal)
//        btnTab3.setImage(UIImage(named: "vibrateSelect"), for: .normal)
//        btnTab4.setImage( UIImage(named: "chatDefault"), for: .normal)
//        btnTab5.setImage(UIImage(named: "mesg_icon_default"), for: .normal)
        
        
        self.shadowView(view: discoverview)
        self.unShadowView(view: favouriteview)
        self.unShadowView(view: profileview)
        self.unShadowView(view: chatview)
        self.unShadowView(view: inboxview)
        
        
        profileLBL.textColor = UIColor.black
        favouritesLBL.textColor = UIColor.black
        discoverLBL.textColor = UIColor.red
        chatLBL.textColor = UIColor.black
        inboxLBL.textColor = UIColor.black
        
        
        getUsermessagesCountsApi()
        getUserCountApi()
         fetchPhotoRequest(userID: "")
    }
    @IBAction func chatAction(_ sender: Any) {
        
//        btnTab1.setImage(UIImage(named: "profileDefault"), for: .normal)
//        btnTab2.setImage(UIImage(named: "starDefault"), for: .normal)
//        btnTab3.setImage(UIImage(named: "vibrateDefault"), for: .normal)
//        btnTab4.setImage( UIImage(named: "chatSelect"), for: .normal)
//        btnTab5.setImage(UIImage(named: "mesg_icon_default"), for: .normal)
//
        
        self.shadowView(view: chatview)
        self.unShadowView(view: favouriteview)
        self.unShadowView(view: discoverview)
        self.unShadowView(view: profileview)
        self.unShadowView(view: inboxview)
        
        profileLBL.textColor = UIColor.black
        favouritesLBL.textColor = UIColor.black
        discoverLBL.textColor = UIColor.black
        chatLBL.textColor = UIColor.red
        inboxLBL.textColor = UIColor.black
        
        
        getUsermessagesCountsApi()
        getUserCountApi()
         fetchPhotoRequest(userID: "")
    }
    @IBAction func notificationAction(_ sender: Any) {
        
//        btnTab1.setImage(UIImage(named: "profileDefault"), for: .normal)
//        btnTab2.setImage(UIImage(named: "starDefault"), for: .normal)
//        btnTab3.setImage(UIImage(named: "vibrateDefault"), for: .normal)
//        btnTab4.setImage( UIImage(named: "chatDefault"), for: .normal)
//        btnTab5.setImage(UIImage(named: "mesg_icon"), for: .normal)
        
        self.shadowView(view: inboxview)
        self.unShadowView(view: favouriteview)
        self.unShadowView(view: discoverview)
        self.unShadowView(view: chatview)
        self.unShadowView(view: profileview)
        
        
        
        profileLBL.textColor = UIColor.black
        favouritesLBL.textColor = UIColor.black
        discoverLBL.textColor = UIColor.black
        chatLBL.textColor = UIColor.black
        inboxLBL.textColor = UIColor.red
        
        
        
        getUsermessagesCountsApi()
        getUserCountApi()
         fetchPhotoRequest(userID: "")
    }
    
  
    
    
    //MARK: - CreatePagination
    
    private func createPageViewController() {
        
        pageController = UIPageViewController.init(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal, options: nil)
        
      //  pageController = UIPageViewController.init()
        
        pageController.view.backgroundColor = UIColor.clear
        
     //   pageController.delegate = self
      //  pageController.dataSource = self
        
//        for svScroll in pageController.view.subviews as! [UIScrollView] {
//            svScroll.delegate = self
//        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.pageController.view.frame = CGRect(x: 0, y: 110, width: self.view.frame.size.width, height: self.view.frame.size.height-110)
        }
        let homeStoryboard = UIStoryboard(name: "Main", bundle: nil)
        tab1VC = homeStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        tab2VC = homeStoryboard.instantiateViewController(withIdentifier: "FavoriteVC") as! FavoriteVC
        tab3VC = homeStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let XmppStoryboard = UIStoryboard(name: "IM", bundle: nil)
        tab4VC = XmppStoryboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        tab5VC = homeStoryboard.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
        tab5VC.screenFrom = "Tab"
        arrVC = [tab1VC, tab2VC, tab3VC, tab4VC, tab5VC]
        
        
        
        if fromscreen == "default"{
            
//            btnTab1.setImage(UIImage(named: "profileDefault"), for: .normal)
//            btnTab2.setImage(UIImage(named: "starSelect"), for: .normal)
//            btnTab3.setImage(UIImage(named: "vibrateDefault"), for: .normal)
//            btnTab4.setImage( UIImage(named: "chatDefault"), for: .normal)
//            btnTab5.setImage(UIImage(named: "mesg_icon_default"), for: .normal)
//
            self.shadowView(view: discoverview)
            self.unShadowView(view: favouriteview)
            self.unShadowView(view: profileview)
            self.unShadowView(view: chatview)
            self.unShadowView(view: inboxview)
            
            
            profileLBL.textColor = UIColor.black
            favouritesLBL.textColor = UIColor.black
            discoverLBL.textColor = UIColor.red
            chatLBL.textColor = UIColor.black
            inboxLBL.textColor = UIColor.black
            
             pageController.setViewControllers([tab2VC], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
            
            
        }else if fromscreen == "chat"{
            
//            btnTab1.setImage(UIImage(named: "profileDefault"), for: .normal)
//            btnTab2.setImage(UIImage(named: "starDefault"), for: .normal)
//            btnTab3.setImage(UIImage(named: "vibrateDefault"), for: .normal)
//            btnTab4.setImage( UIImage(named: "chatSelect"), for: .normal)
//            btnTab5.setImage(UIImage(named: "mesg_icon_default"), for: .normal)



            self.shadowView(view: chatview)
            self.unShadowView(view: favouriteview)
            self.unShadowView(view: discoverview)
            self.unShadowView(view: profileview)
            self.unShadowView(view: inboxview)
            
            profileLBL.textColor = UIColor.black
            favouritesLBL.textColor = UIColor.black
            discoverLBL.textColor = UIColor.black
            chatLBL.textColor = UIColor.red
            inboxLBL.textColor = UIColor.black
            

            pageController.setViewControllers([tab4VC], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        }
        else if fromscreen == "inbox"{
            self.shadowView(view: inboxview)
            self.unShadowView(view: favouriteview)
            self.unShadowView(view: discoverview)
            self.unShadowView(view: profileview)
            self.unShadowView(view: chatview)
            profileLBL.textColor = UIColor.black
            favouritesLBL.textColor = UIColor.black
            discoverLBL.textColor = UIColor.black
            chatLBL.textColor = UIColor.black
            inboxLBL.textColor = UIColor.red
         
            pageController.setViewControllers([tab5VC], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
         
        }
       
        else {
             pageController.setViewControllers([tab3VC], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        }
        
        
       
        
        self.addChildViewController(pageController)
        self.view.addSubview(pageController.view)
        pageController.didMove(toParentViewController: self)
        
        
        
    }
    
    
    
    private func indexofviewController(viewCOntroller: UIViewController) -> Int {
        if(arrVC .contains(viewCOntroller)) {
            return arrVC.index(of: viewCOntroller)!
        }
        
        return -1
    }
    
    //MARK: - Pagination Delegate Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index - 1
        }
        
        if(index < 0) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index + 1
        }
        
        if(index >= arrVC.count) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController1: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if(completed) {
            currentPage = arrVC.index(of: (pageViewController1.viewControllers?.last)!)
            resetTabBarForTag(tag: currentPage)
        }
    }
    
    //MARK: - Set Top bar after selecting Option from Top Tabbar
    
    private func resetTabBarForTag(tag: Int) {
        
        var sender: UIButton!
        
        if(tag == 0) {
            sender = btnTab1
        }
        else if(tag == 1) {
            sender = btnTab2
        }
        else if(tag == 2) {
            sender = btnTab3
        }
        else if(tag == 3) {
            sender = btnTab4
        }
        else if(tag == 4) {
            sender = btnTab5
        }
        
        currentPage = tag
        
        unSelectedButton(btn: btnTab1)
        unSelectedButton(btn: btnTab2)
        unSelectedButton(btn: btnTab3)
        unSelectedButton(btn: btnTab4)
        unSelectedButton(btn: btnTab5)
        
        
        selectedButton(btn: sender)
        
    }
    func getUsermessagesCountsApi(){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
           
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:getUsermessagesCounts, params: dict as Dictionary<String, Any>) { response in
                print(response)
               
                let success = response["status"] as! String
                if(success == "1"){
                    
                    
                    let mesgCount = self.themes.checkNullValue(response["totalcount"] as AnyObject) as! String
                    if(mesgCount == "" || mesgCount == "0"){
                        self.mesgCountLbl.isHidden = true
                    } else {
                        self.mesgCountLbl.isHidden = false
                        self.mesgCountLbl.text = String(mesgCount)
                    }
                    
                } else {
                    let result = response["result"] as! String
                   
                }
            }
        } else {
           
        }
        
    }
    
    func getUserCountApi(){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
           
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:getUserCounts, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    let allcounts = response["allcounts"] as AnyObject
                   
                   let favouritesMeCount = allcounts["favouritedmecount"] as! Int
                   let intrestedMeCount = allcounts["interestscount"] as! Int
                    
                  let FavrTotalCount = favouritesMeCount + intrestedMeCount
                    if(FavrTotalCount == 0){
                        self.favrtCountLbl.isHidden = true
                    } else {
                        self.favrtCountLbl.isHidden = false
                        self.favrtCountLbl.text = "\(FavrTotalCount)"
                    }
                    
                } 
            
        }
    }
    
    }
 
    func fetchPhotoRequest(userID: String)  {
        
        let userid = self.themes.getUserId()
        let urlString = "https://api.talkjs.com/v1/M5iybBK9/users/\(userid)/conversations?unreadsOnly=true"
        let session = URLSession.shared
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer sk_live_EXkW3yEcNrqQ9nlgg5SnvJ4g", forHTTPHeaderField: "Authorization")
        session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
            
            if let data = data
            {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    let JJSON = json as! [String:Any]
                    print(JJSON["data"])
                    let Jdata = JJSON["data"] as! [Any]
                    DispatchQueue.main.async {
                        if Jdata.count != 0 {
                          //  self.chatcount.isHidden = false
                          //  self.chatcount.text = "\(Jdata.count)"
                          //  self.chatcount.startBlink()
                            
                        }else{
                          //  self.chatcount.isHidden = true
                          //  self.chatcount.stopBlink()
                        }
                    }
                } catch {
                    print(error)
                }
            }
        }).resume()
    }

    func shadowView (view:UIView){
        view.layer.shadowOffset = CGSize(width:0, height: 0)
        view.layer.shadowOpacity = 2.0
        view.layer.shadowRadius = 2
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.gray.cgColor
    }
    
   func unShadowView (view:UIView) {
        view.layer.shadowOffset = CGSize(width:0, height: 0)
        view.layer.shadowOpacity = 2.0
        view.layer.shadowRadius = 2
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.white.cgColor
   }
    
    
    
    
    
}
extension UILabel {
    //MARK: StartBlink
    func startBlink() {
        UIView.animate(withDuration: 0.3,//Time duration
            delay:0.0,
            options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
            animations: { self.alpha = 0 },
            completion: nil)
    }
    
    //MARK: StopBlink
    func stopBlink() {
        layer.removeAllAnimations()
        alpha = 1
    }
}


