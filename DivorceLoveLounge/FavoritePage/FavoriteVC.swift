//
//  FavoriteVC.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 3/15/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
import SDWebImage
class FavoriteVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,backBtnClickDelegate ,UIScrollViewDelegate{
    
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    
      var refreshControl = UIRefreshControl()
    
    
    
    func backAction(from:String){
        if(from == "myfavourite"){
            myFavouritesAPI()
            
      
        }else if(from == "interestme"){
            Interested_meAPI(type: "block")
        }else if(from == "myinterest"){
            MyInterestAPI(type: "block")
        
        }else if(from == "viewedMe"){
            viewed_meAPI(type:"block")
        }else if(from == "favouratedMe"){
           
            favourited_meAPI(type: "block")
        
        }
        
       //  self.favrtCollectionView.reloadData()
    }
    
    @IBOutlet var popupview: UIView!
    @IBOutlet var popupLbl: UILabel!
    
    @IBAction func popupBtnAction(_ sender: Any) {
        
//        let MemberShipViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewMemberShipPlanVC") as! NewMemberShipPlanVC
//        self.navigationController?.pushViewController(MemberShipViewController, animated: true)
  
    }
    
    
    @IBOutlet var topCollectionView: UICollectionView!
    
    var myfavoriteRecordArray = [MyFavoritesRecord]()
    var favouritedMeRecordArray = [FavouritedMeRecord]()
    
    var myfavouriteArray = [AnyObject]()
    var my_InterestArray = [AnyObject]()
    var interest_ME_Array = [AnyObject]()
    var favouritedMeArray = [AnyObject]()
    var viewdMeArray = [AnyObject]()
    var favouritesCount = Int()
    var favouritesMeCount = Int()
    var intrestedMeCount = Int()
    var id = String()
    var fromPushNotify = ""
    
    var UserDataArray = [AnyObject]()

    @IBOutlet var fliterimage: UIImageView!
    
    @IBOutlet weak var favrtCollectionView: UICollectionView!        
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var favouriteSegment: UISegmentedControl!
    @IBOutlet weak var alphaView: UIView!
    @IBOutlet var myIntrstsFilter: UIView!
    @IBOutlet var myIntrstsAllBtn: UIButton!
    @IBOutlet var sendPendingBtn: UIButton!
    @IBOutlet var sendAcceptBtn: UIButton!
    @IBOutlet var sendRejectBtn: UIButton!
    @IBOutlet var myIntrestsDoneBtn: UIButton!
    @IBOutlet var myIntrstsCloseBtn: UIButton!
    @IBOutlet var myIntrestsAllImg: UIImageView!
    @IBOutlet var sendPendingImg: UIImageView!
    @IBOutlet var sendAcceptImg: UIImageView!
    @IBOutlet var sendRejectImg: UIImageView!
    
    @IBOutlet var intrestsMeFilter: UIView!
    @IBOutlet var intrestsMeAllBtn: UIButton!
    @IBOutlet var RecievePendingBtn: UIButton!
    @IBOutlet var RecieveAcceptBtn: UIButton!
    @IBOutlet var RecieveRejectBtn: UIButton!
    @IBOutlet var intrestsMeDoneBtn: UIButton!
    @IBOutlet var intrstsMeCloseBtn: UIButton!
    @IBOutlet var IntrestsMeAllImg: UIImageView!
    @IBOutlet var RecievePendingImg: UIImageView!
    @IBOutlet var RecieveAcceptImg: UIImageView!
    @IBOutlet var RecieveRejectImg: UIImageView!
    
    var selectedIndex = Int()
    var fromscreen = String()
    
    
    let topHeadingArray = ["My Favourites","Favourited Me","My Interests","Interested Me","Viewed Me"]
  
    
    @IBAction func serchAction(_ sender: Any) {
        
        
    }
    
    
    @IBAction func filetrAction(_ sender: Any) {
        self.alphaView.isHidden = false
        let getIndex = selectedIndex
        if getIndex == 3 {
            IntrestedMeView()
        } else if getIndex == 2 {
            MyIntrestsView()
        }
    }
    
    @IBAction func favouriteSegmentAction(_ sender: UISegmentedControl) {
        
        let getIndex = favouriteSegment.selectedSegmentIndex
      //  favouriteSegment.changeUnderlinePosition()
        
        if getIndex == 0 {
          
            self.filterBtn.isHidden = true
            self.fliterimage.isHidden = true
            
            favouriteSegment.setTitleTextAttributes([
                NSAttributedStringKey.font : UIFont(name: "Poppins-SemiBold", size: 12),
                NSAttributedStringKey.foregroundColor: UIColor.lightGray
                ], for: .normal)
            favouriteSegment.setTitleTextAttributes([
                NSAttributedStringKey.font : UIFont(name: "Poppins-SemiBold", size: 12),
                NSAttributedStringKey.foregroundColor: UIColor.white
                ], for: .selected)
            self.UserDataArray = self.myfavouriteArray
            self.favrtCollectionView.reloadData()
            
            
        } else if getIndex == 1 {
           self.filterBtn.isHidden = false
             self.fliterimage.isHidden = false
            favouriteSegment.setTitleTextAttributes([
                NSAttributedStringKey.font : UIFont(name: "Poppins-SemiBold", size: 12),
                NSAttributedStringKey.foregroundColor: UIColor.lightGray
                ], for: .normal)
            favouriteSegment.setTitleTextAttributes([
                NSAttributedStringKey.font : UIFont(name: "Poppins-SemiBold", size: 12),
                NSAttributedStringKey.foregroundColor: UIColor.white
                ], for: .selected)
            
            if self.IntrestsMeAllImg.image == UIImage(named: "radio_button_selected"){
                self.Interest_ME_FilteringArray(status: "ALL")
            }
            else if self.RecievePendingImg.image == UIImage(named: "radio_button_selected"){
                self.Interest_ME_FilteringArray(status: "0")
            }
            else if self.RecieveAcceptImg.image == UIImage(named: "radio_button_selected"){
                self.Interest_ME_FilteringArray(status: "1")
            }
            else if self.RecieveRejectImg.image == UIImage(named: "radio_button_selected"){
                self.Interest_ME_FilteringArray(status: "2")
            }
            self.favrtCollectionView.reloadData()
            
            
        } else  if getIndex == 2 {
            
            self.filterBtn.isHidden = false
             self.fliterimage.isHidden = false
            favouriteSegment.setTitleTextAttributes([
                NSAttributedStringKey.font : UIFont(name: "Poppins-SemiBold", size: 12),
                NSAttributedStringKey.foregroundColor: UIColor.lightGray
                ], for: .normal)
            favouriteSegment.setTitleTextAttributes([
                NSAttributedStringKey.font : UIFont(name: "Poppins-SemiBold", size: 12),
                NSAttributedStringKey.foregroundColor: UIColor.white
                ], for: .selected)
            
            if self.myIntrestsAllImg.image == UIImage(named: "radio_button_selected"){
               self.My_Interest_FilteringArray(status: "ALL")
            }
            else if self.sendPendingImg.image == UIImage(named: "radio_button_selected"){
               self.My_Interest_FilteringArray(status: "0")
            }
            else if self.sendAcceptImg.image == UIImage(named: "radio_button_selected"){
               self.My_Interest_FilteringArray(status: "1")
            }
            else if self.sendRejectImg.image == UIImage(named: "radio_button_selected"){
                self.My_Interest_FilteringArray(status: "2")
            }
            self.favrtCollectionView.reloadData()
    }else {
            
         self.filterBtn.isHidden = true
            favouriteSegment.setTitleTextAttributes([
                NSAttributedStringKey.font : UIFont(name: "Poppins-SemiBold", size: 12),
                NSAttributedStringKey.foregroundColor: UIColor.lightGray
                ], for: .normal)
            favouriteSegment.setTitleTextAttributes([
                NSAttributedStringKey.font : UIFont(name: "Poppins-SemiBold", size: 12),
                NSAttributedStringKey.foregroundColor: UIColor.white
                ], for: .selected)
            
            self.UserDataArray = self.myfavouriteArray
            self.favrtCollectionView.reloadData()
            
        }
     
}
    
    // Filter Close Actions for Interest ME
    @IBAction func intrestsMecloseAction(_ sender: Any) {
        self.alphaView.isHidden = true
        intrestsMeFilter.alpha = 0
    }
    // Filter Close Actions for My Interest
    @IBAction func MyIntrestedcloseAction(_ sender: Any) {
        self.alphaView.isHidden = true
        myIntrstsFilter.alpha = 0
    }
    
    
   // Filter Done Actions for Interest ME
    @IBAction func interest_ME_filterDoneAction(_ sender: Any) {
        
        self.alphaView.isHidden = true
        intrestsMeFilter.alpha = 0
        
        
    }
    // Filter Done Actions for My interest
    @IBAction func MyinterestFilterDoneAction(_ sender: Any) {
        
        self.alphaView.isHidden = true
        myIntrstsFilter.alpha = 0
        
        
    }
   
    
    @IBAction func myIntrestsAction(sender: UIButton) {
    
        
        if sender.tag == 0 {
            
              self.myIntrestsAllImg.image = UIImage(named: "radio_button_selected")
              self.sendPendingImg.image = UIImage(named: "radio_button_default")
              self.sendAcceptImg.image = UIImage(named: "radio_button_default")
              self.sendRejectImg.image = UIImage(named: "radio_button_default")
           self.My_Interest_FilteringArray(status: "ALL")
            print("All clicked")
        } else if sender.tag == 1 {
           
            self.myIntrestsAllImg.image = UIImage(named: "radio_button_default")
            self.sendPendingImg.image = UIImage(named: "radio_button_selected")
            self.sendAcceptImg.image = UIImage(named: "radio_button_default")
            self.sendRejectImg.image = UIImage(named: "radio_button_default")
             self.My_Interest_FilteringArray(status: "0")
            print("send Pending clicked")
        } else if sender.tag == 2 {
            
            self.myIntrestsAllImg.image = UIImage(named: "radio_button_default")
            self.sendPendingImg.image = UIImage(named: "radio_button_default")
            self.sendAcceptImg.image = UIImage(named: "radio_button_selected")
            self.sendRejectImg.image = UIImage(named: "radio_button_default")
            self.My_Interest_FilteringArray(status: "1")
            print("send accept clicked")
        } else {
           
            self.myIntrestsAllImg.image = UIImage(named: "radio_button_default")
            self.sendPendingImg.image = UIImage(named: "radio_button_default")
            self.sendAcceptImg.image = UIImage(named: "radio_button_default")
            self.sendRejectImg.image = UIImage(named: "radio_button_selected")
              self.My_Interest_FilteringArray(status: "2")
            print("send reject clicked")
        }
        
        favrtCollectionView.reloadData()
        
}
   
    
func My_Interest_FilteringArray(status:String){
        
        self.UserDataArray.removeAll()
        for userdat in self.my_InterestArray{
            
            let STATUS = userdat["interest_status"] as! String
            if status == STATUS {
                self.UserDataArray.append(userdat)
            }else if status == "ALL"{
                self.UserDataArray.append(userdat)
            }
        }
    }
    
func Interest_ME_FilteringArray(status:String){
        
        self.UserDataArray.removeAll()
        for userdat in self.interest_ME_Array{
            let STATUS = userdat["interest_status"] as! String
            if status == STATUS {
                self.UserDataArray.append(userdat)
            }else if status == "ALL"{
                self.UserDataArray.append(userdat)
            }
        }
    }
    
    
    
    @IBAction func IntrestedMeAction(sender: UIButton) {
        
        if sender.tag == 0 {
            
             self.IntrestsMeAllImg.image = UIImage(named: "radio_button_selected")
             self.RecievePendingImg.image = UIImage(named: "radio_button_default")
             self.RecieveAcceptImg.image = UIImage(named: "radio_button_default")
             self.RecieveRejectImg.image = UIImage(named: "radio_button_default")
              self.Interest_ME_FilteringArray(status: "ALL")
            print("All clicked")
        } else if sender.tag == 1 {
            self.IntrestsMeAllImg.image = UIImage(named: "radio_button_default")
            self.RecievePendingImg.image = UIImage(named: "radio_button_selected")
            self.RecieveAcceptImg.image = UIImage(named: "radio_button_default")
            self.RecieveRejectImg.image = UIImage(named: "radio_button_default")
              self.Interest_ME_FilteringArray(status: "0")
            print("Recieve Pending clicked")
        } else if sender.tag == 2 {
            self.IntrestsMeAllImg.image = UIImage(named: "radio_button_default")
            self.RecievePendingImg.image = UIImage(named: "radio_button_default")
            self.RecieveAcceptImg.image = UIImage(named: "radio_button_selected")
            self.RecieveRejectImg.image = UIImage(named: "radio_button_default")
              self.Interest_ME_FilteringArray(status: "1")
            print("Recieve accept clicked")
        } else {
            self.IntrestsMeAllImg.image = UIImage(named: "radio_button_default")
            self.RecievePendingImg.image = UIImage(named: "radio_button_default")
            self.RecieveAcceptImg.image = UIImage(named: "radio_button_default")
            self.RecieveRejectImg.image = UIImage(named: "radio_button_selected")
              self.Interest_ME_FilteringArray(status: "2")
            print("Recieve reject clicked")
        }
        self.favrtCollectionView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if fromscreen == "0"{
             selectedIndex = 3
        } else {
             selectedIndex = 0
        }
        
        popupview.isHidden = true
        
        
        self.filterBtn.isHidden = true
        self.fliterimage.isHidden = true
        
     //   favouriteSegment.addUnderlineForSelectedSegment()
        
        myIntrestsDoneBtn.layer.cornerRadius = (myIntrestsDoneBtn.frame.size.height)/2
        intrestsMeDoneBtn.layer.cornerRadius = (intrestsMeDoneBtn.frame.size.height)/2
        myIntrstsFilter.layer.cornerRadius = 5
        intrestsMeFilter.layer.cornerRadius = 5

        self.alphaView.isHidden = true
        
        self.topCollectionView.register(UINib(nibName: "topCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "topCollectionViewCell")
        self.favrtCollectionView.register(UINib(nibName: "UsersCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "UsersCollectionViewCell")
        
        
        self.topCollectionView.delegate = self
        self.topCollectionView.dataSource = self
        
        
//        // segmented control's tintColor
//        favouriteSegment.setTitleTextAttributes([
//            NSAttributedStringKey.font : UIFont(name: "Poppins-SemiBold", size: 12),
//            NSAttributedStringKey.foregroundColor: UIColor.white
//            ], for: .normal)
//        favouriteSegment.setTitleTextAttributes([
//            NSAttributedStringKey.font : UIFont(name: "Poppins-SemiBold", size: 12),
//            NSAttributedStringKey.foregroundColor: UIColor.white
//            ], for: .selected)
//
        

         self.IntrestsMeAllImg.image = UIImage(named: "radio_button_selected")
         self.myIntrestsAllImg.image = UIImage(named: "radio_button_selected")
        
        
        myFavouritesAPI()
        MyInterestAPI(type: "normal")
        Interested_meAPI(type: "normal")
        favourited_meAPI(type: "normal")
        viewed_meAPI(type: "normal")
//        getUserCountApi()
//
        
        
        
        // Scroll to refresh Add target
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(FavoriteVC.refresh(sender:)), for: .allEvents)
        favrtCollectionView.addSubview(refreshControl)

        // Do any additional setup after loading the view.
    }
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        
       
        MyInterestAPI(type: "normal")
        Interested_meAPI(type: "normal")
        favourited_meAPI(type: "normal")
        viewed_meAPI(type: "normal")
        getUserCountApi()
        refreshControl.endRefreshing()
    }
    
    override func viewWillAppear(_ animated: Bool) {
      
//        if fromscreen == "0"{
//            selectedIndex = 3
//        } else {
//            selectedIndex = 0
//        }
//        myFavouritesAPI()
//        MyInterestAPI(type: "normal")
//        Interested_meAPI(type: "normal")
//        favourited_meAPI(type: "normal")
//        viewed_meAPI(type: "normal")
        
        let planSatus = self.themes.checkNullValue( self.themes.getplanStatus()) as! String
        
        if planSatus == "2" {
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopViewController") as! PopViewController
                                      
                        popOverVC.fromScreen = "expirePlan"
                        popOverVC.headingMesg = "Oops ☹️"
                        popOverVC.subMesg = "You need an active membership plan to view the profile. Proceed to membership plans?"
                                      
                        self.addChildViewController(popOverVC)
                        popOverVC.view.center = self.view.frame.center
                        self.view.addSubview(popOverVC.view)
                        popOverVC.didMove(toParentViewController: self)
        } else {
            
        }
        
//
    getUserCountApi()
    }
    
    func MyIntrestsView(){
        myIntrstsFilter.frame = CGRect(x: 20, y: 100, width: Int(UIScreen.main.bounds.width - 40), height: 380)
        myIntrstsFilter.alpha = 1

        self.view.addSubview(myIntrstsFilter)
    }
    func IntrestedMeView(){
        
        intrestsMeFilter.frame = CGRect(x: 20, y: 100, width: Int(UIScreen.main.bounds.width - 40), height: 380)
        intrestsMeFilter.alpha = 1

        self.view.addSubview(intrestsMeFilter)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        
        if collectionView == topCollectionView{
           
            return topHeadingArray.count
        }else{
        
        if self.UserDataArray.count == 0 {
           // self.themes.showToast(message: "No Data is Available", sender: self)
            collectionView.isHidden = true
             return 0
        }else{
            collectionView.isHidden = false
              return self.UserDataArray.count
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
      
        
        
        
        if collectionView == topCollectionView{
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "topCollectionViewCell", for: indexPath)as! topCollectionViewCell
            
            if(indexPath.item == 0){
                 cell.topLbl.text = "My Favourites "
            }else if(indexPath.item == 1){
                if (favouritesMeCount == 0){
                    cell.topLbl.text = "Favourited Me"
                } else {
                cell.topLbl.text = "Favourited Me " + "( " + String(favouritesMeCount) + " )"
                }
            }else if(indexPath.item == 2){
                 cell.topLbl.text = "My Interests"
            }else if(indexPath.item == 3){
                if (intrestedMeCount == 0) {
                    cell.topLbl.text = "Interested Me"
                } else {
                cell.topLbl.text = "Interested Me" + "( " + String(intrestedMeCount) + " )"
                }
            }else if(indexPath.item == 4){
                cell.topLbl.text = "Viewed Me"
            }
            
            
            
         //   cell.topLbl.text = topHeadingArray[indexPath.item]
            
            if selectedIndex == indexPath.item {
                cell.bottomview.backgroundColor = UIColor.yellow
                return cell
            }else{
                cell.bottomview.backgroundColor = UIColor.clear
            }

            return cell
            
            
        }else{
        
            
            // let topHeadingArray = ["My Favourites","Favourited Me","My Intrestes","Intrested Me","Viewed Me"]
            
            
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UsersCollectionViewCell", for: indexPath)as! UsersCollectionViewCell
            
        if selectedIndex == 0 || selectedIndex == 1 || selectedIndex == 4  {
            
            cell.likeBtn.addTarget(self, action: #selector(likeBtnClicked(button:)), for: .touchUpInside)
            cell.likeBtn.tag = indexPath.row
           
            let userdata = self.UserDataArray[indexPath.row]
            let idNum = userdata["id"] as? String
            id = idNum!
            
            let imageUrl = self.themes.checkNullValue(userdata["photo1"] as AnyObject?) as! String
            let photoApproved = self.themes.checkNullValue(userdata["photo1_approve"] as AnyObject?) as! String
            
            if photoApproved == "APPROVED" {
                let fullimageUrl = ImagebaseUrl + imageUrl
//                cell.profPicImg.sd_setImage(with: URL(string: fullimageUrl), placeholderImage: UIImage(named: "defaultPic"))
                DispatchQueue.main.async {
                    cell.userImgView.sd_setShowActivityIndicatorView(true)
                    cell.userImgView.sd_setIndicatorStyle(.whiteLarge)
                    cell.userImgView.sd_setImage(with: URL(string: fullimageUrl), placeholderImage:UIImage(named: "defaultPic"), completed: { (image, error, cacheType, url) -> Void in
                        if ((error) != nil) {
                            cell.userImgView.image = UIImage(named: "defaultPic")
                            cell.userImgView.sd_setShowActivityIndicatorView(true)
                        } else {
                            cell.userImgView.image = image
                            cell.userImgView.sd_setShowActivityIndicatorView(false)
                            
                            
                            if self.themes.getplanStatus() == "1"{
                               
                            }else{
                                let blurEffect = UIBlurEffect(style: .light)
                                let effectView = UIVisualEffectView(effect: blurEffect)
                                effectView.frame =  cell.userImgView.frame
                                effectView.alpha = 0.6
                                cell.userImgView.addSubview(effectView)
                                cell.userImgView.clipsToBounds = true
                            }
                            
                            
                        }
                    })
                }
                
            }else{
                
                cell.userImgView.image = UIImage(named: "defaultPic")
//                cell.profPicImg.image = UIImage(named: "defaultPic")
            }
         
            cell.nameLable.text = self.themes.checkNullValue(userdata["first_name"] as AnyObject?) as? String
            let DOB = self.themes.checkNullValue(userdata["dob"] as AnyObject?) as? String
            if DOB != "" {
                let age = self.themes.getAgeFromDOF(date: DOB!)
                cell.ageLabel.text = "\(age.0)"
            }
             cell.acceptLabel.isHidden = true
            cell.transprntView.isHidden = true
            
          return cell
           
        }
        else if selectedIndex == 2{

            let userdata = self.UserDataArray[indexPath.row]
            let imageUrl = self.themes.checkNullValue(userdata["photo1"] as AnyObject?) as! String
            let fullimageUrl = ImagebaseUrl + imageUrl
            
          //  cell.userImgView.sd_setImage(with: URL(string: fullimageUrl), placeholderImage: UIImage(named: "defaultPic"))
           // cell.profPicImg.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "defaultPic"))
            
            
            cell.nameLable.text = self.themes.checkNullValue(userdata["first_name"] as AnyObject?) as? String
            let DOB = self.themes.checkNullValue(userdata["dob"] as AnyObject?) as? String
            if DOB != "" {
                let age = self.themes.getAgeFromDOF(date: DOB!)
                cell.ageLabel.text = "\(age.0)"
            }
              cell.acceptLabel.isHidden = false
             cell.transprntView.isHidden = false
            let interest_status = self.themes.checkNullValue(userdata["interest_status"] as AnyObject?) as? String
            
            if interest_status == "0"{
                cell.acceptLabel.text = "Pending"
                cell.acceptLabel.textColor = yellow
                
            }else if interest_status == "1"{
                
                cell.acceptLabel.text = "Accepted"
                cell.acceptLabel.textColor = green
                
            }else if interest_status == "2"{
                
                cell.acceptLabel.text = "Rejected"
                cell.acceptLabel.textColor = UIColor.red
                
            }
            
            let photoApproved = self.themes.checkNullValue(userdata["photo1_approve"] as AnyObject?) as! String
            
            if photoApproved == "APPROVED" {
                let fullimageUrl = ImagebaseUrl + imageUrl
//                cell.profPicImg.sd_setImage(with: URL(string: fullimageUrl), placeholderImage: UIImage(named: "defaultPic"))
                DispatchQueue.main.async {
                    cell.userImgView.sd_setShowActivityIndicatorView(true)
                    cell.userImgView.sd_setIndicatorStyle(.whiteLarge)
                    cell.userImgView.sd_setImage(with: URL(string: fullimageUrl), placeholderImage:UIImage(named: "defaultPic"), completed: { (image, error, cacheType, url) -> Void in
                        if ((error) != nil) {
                            cell.userImgView.image = UIImage(named: "defaultPic")
                            cell.userImgView.sd_setShowActivityIndicatorView(true)
                        } else {
                            cell.userImgView.image = image
                            cell.userImgView.sd_setShowActivityIndicatorView(false)
                            
                            
                            if self.themes.getplanStatus() == "1"{
                                
                            }else{
                                let blurEffect = UIBlurEffect(style: .light)
                                let effectView = UIVisualEffectView(effect: blurEffect)
                                effectView.frame =  cell.userImgView.frame
                                effectView.alpha = 0.6
                                cell.userImgView.addSubview(effectView)
                                cell.userImgView.clipsToBounds = true
                            }
                            
                            
                        }
                    })
                }
                
            }else{
                
                cell.userImgView.image = UIImage(named: "defaultPic")
//                cell.profPicImg.image = UIImage(named: "defaultPic")
            }
            
          return cell
        }
        else if selectedIndex == 3 {
            
             cell.likeBtn.addTarget(self, action: #selector(likeBtnClicked(button:)), for: .touchUpInside)
            cell.likeBtn.tag = indexPath.row
            
            
        
            let userdata = self.UserDataArray[indexPath.row]
            let idNum = userdata["id"] as? String
            id = idNum!
            
            let imageUrl = self.themes.checkNullValue(userdata["photo1"] as AnyObject?) as! String
          
            
            cell.nameLable.text = self.themes.checkNullValue(userdata["first_name"] as AnyObject?) as? String
            let DOB = self.themes.checkNullValue(userdata["dob"] as AnyObject?) as? String
            if DOB != "" {
                let age = self.themes.getAgeFromDOF(date: DOB!)
                cell.ageLabel.text = "\(age.0)"
            }
              cell.acceptLabel.isHidden = false
             cell.transprntView.isHidden = false
            let interest_status = self.themes.checkNullValue(userdata["interest_status"] as AnyObject?) as? String
           
            if interest_status == "0"{
                cell.acceptLabel.text = "Pending"
                cell.acceptLabel.textColor = yellow
                
            }else if interest_status == "1"{
                
                cell.acceptLabel.text = "Accepted"
                cell.acceptLabel.textColor = green
                
            }else if interest_status == "2"{
                
                cell.acceptLabel.text = "Rejected"
                cell.acceptLabel.textColor = UIColor.red
                
            }
            
            let photoApproved = self.themes.checkNullValue(userdata["photo1_approve"] as AnyObject?) as! String
            
            if photoApproved == "APPROVED" {
                let fullimageUrl = ImagebaseUrl + imageUrl
//                cell.profPicImg.sd_setImage(with: URL(string: fullimageUrl), placeholderImage: UIImage(named: "defaultPic"))
                DispatchQueue.main.async {
                    cell.userImgView.sd_setShowActivityIndicatorView(true)
                    cell.userImgView.sd_setIndicatorStyle(.whiteLarge)
                    cell.userImgView.sd_setImage(with: URL(string: fullimageUrl), placeholderImage:UIImage(named: "defaultPic"), completed: { (image, error, cacheType, url) -> Void in
                        if ((error) != nil) {
                            cell.userImgView.image = UIImage(named: "defaultPic")
                            cell.userImgView.sd_setShowActivityIndicatorView(true)
                        } else {
                            cell.userImgView.image = image
                            cell.userImgView.sd_setShowActivityIndicatorView(false)
                            
                            
                            if self.themes.getplanStatus() == "1"{
                                
                            }else{
                                let blurEffect = UIBlurEffect(style: .light)
                                let effectView = UIVisualEffectView(effect: blurEffect)
                                effectView.frame =  cell.userImgView.frame
                                effectView.alpha = 0.6
                                cell.userImgView.addSubview(effectView)
                                cell.userImgView.clipsToBounds = true
                            }
                            
                            
                        }
                    })
                }
                
            }else{
                
                cell.userImgView.image = UIImage(named: "defaultPic")
//                cell.profPicImg.image = UIImage(named: "defaultPic")
            }
            
           return cell
       }
         //   else {
//
//            let userdata = self.UserDataArray[indexPath.row]
//
//            let imageUrl = self.themes.checkNullValue(userdata["photo1"] as AnyObject?) as! String
//            let fullimageUrl = ImagebaseUrl + imageUrl
//
//            cell.userImgView.sd_setImage(with: URL(string: fullimageUrl), placeholderImage: UIImage(named: "defaultPic"))
//            cell.profPicImg.sd_setImage(with: URL(string: fullimageUrl), placeholderImage: UIImage(named: "defaultPic"))
//
//
//            cell.nameLable.text = self.themes.checkNullValue(userdata["first_name"] as AnyObject?) as? String
//            let DOB = self.themes.checkNullValue(userdata["dob"] as AnyObject?) as? String
//            if DOB != "" {
//                let age = self.themes.getAgeFromDOF(date: DOB!)
//                cell.ageLabel.text = "\(age.0)"
//            }
//            cell.acceptLabel.isHidden = true
//            cell.transprntView.isHidden = true
//
//
//       }
        
        return cell
    }
            
       
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == topCollectionView{
            if(indexPath.item == 0){
                return CGSize(width: 160, height: 43)
            }else if(indexPath.item == 1){
                return CGSize(width: 160, height: 43)
            }else if(indexPath.item == 2){
               return CGSize(width: 160, height: 43)
            }else if(indexPath.item == 3){
               return CGSize(width: 150, height: 43)
            }else {
                return CGSize(width: 120, height: 43)
            }
        }else{
            let screenSize = UIScreen.main.bounds
            let screenWidth = screenSize.width
            print("screen width is \(screenWidth)")
            if(screenWidth == 375){
                return CGSize(width: 186, height: 186)
            }else if(screenWidth == 414){
                return CGSize(width: 205.5, height: 206)
            }else if(screenWidth == 320) {
                return CGSize(width: 158.5, height: 159)
            }else if(screenWidth == 768) {
                return CGSize(width: 382.5, height: 382)
            }else if(screenWidth == 1024) {
                return CGSize(width: 255, height: 272)
            } else {
                return CGSize(width: 277, height: 277)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        if collectionView == topCollectionView{
          return 5
        }else{
           return 1
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        
        if collectionView == topCollectionView{
            return 5
        }else{
        return 0
       }
 }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == topCollectionView {
            topCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            selectedIndex = indexPath.item
            if selectedIndex == 0 {
                self.filterBtn.isHidden = true
                self.fliterimage.isHidden = true
                self.favrtCollectionView.isHidden = false
                self.UserDataArray = self.myfavouriteArray
                
                backAction(from: "myfavourite")
                
                
             
                self.favrtCollectionView.reloadData()
                  popupview.isHidden = true
                
            } else if selectedIndex == 1 {
                
                self.filterBtn.isHidden = true
                self.fliterimage.isHidden = true
                
                if self.themes.getfavouritedMe() == "No" || self.themes.getfavouritedMe() == "" || self.themes.getplanStatus() == "2" {

                    popupview.isHidden = false
                    popupview.center = self.view.center
                    popupview.alpha = 1
                    
                    popupLbl.text = "An active membership is required to view the profiles for FavouritedMe ."
                    
                    self.view.addSubview(popupview)
                    
                    
                     self.favrtCollectionView.isHidden = true
                    
                    
                }else{
                    
                    backAction(from: "favouratedMe")
                    self.UserDataArray = self.favouritedMeArray
                    self.favrtCollectionView.reloadData()
                      popupview.isHidden = true
                    self.favrtCollectionView.isHidden = false
                }
             
                
            } else  if selectedIndex == 2 {
                
                self.filterBtn.isHidden = false
                self.fliterimage.isHidden = false
                
                if self.myIntrestsAllImg.image == UIImage(named: "radio_button_selected"){
                    self.My_Interest_FilteringArray(status: "ALL")
                }
                else if self.sendPendingImg.image == UIImage(named: "radio_button_selected"){
                    self.My_Interest_FilteringArray(status: "0")
                }
                else if self.sendAcceptImg.image == UIImage(named: "radio_button_selected"){
                    self.My_Interest_FilteringArray(status: "1")
                }
                else if self.sendRejectImg.image == UIImage(named: "radio_button_selected"){
                    self.My_Interest_FilteringArray(status: "2")
                }
                self.favrtCollectionView.reloadData()
                  popupview.isHidden = true
                backAction(from: "myinterest")
                self.favrtCollectionView.isHidden = false
               
            }else if selectedIndex == 3 {
                
                let userDetailsVc = self.storyboard?.instantiateViewController(withIdentifier: "UserDetailsVC") as! UserDetailsVC
                
                userDetailsVc.fromScreen = "IntrestedMe"
                
                self.filterBtn.isHidden = false
                self.fliterimage.isHidden = false
                
                
                
                
                if self.IntrestsMeAllImg.image == UIImage(named: "radio_button_selected"){
                    self.Interest_ME_FilteringArray(status: "ALL")
                }
                else if self.RecievePendingImg.image == UIImage(named: "radio_button_selected"){
                    self.Interest_ME_FilteringArray(status: "0")
                }
                else if self.RecieveAcceptImg.image == UIImage(named: "radio_button_selected"){
                    self.Interest_ME_FilteringArray(status: "1")
                }
                else if self.RecieveRejectImg.image == UIImage(named: "radio_button_selected"){
                    self.Interest_ME_FilteringArray(status: "2")
                }
                self.favrtCollectionView.reloadData()
                 popupview.isHidden = true
                backAction(from: "interestme")
                self.favrtCollectionView.isHidden = false
            }
            else if selectedIndex == 4 {
                
                self.filterBtn.isHidden = true
                self.fliterimage.isHidden = true
                
                  popupview.isHidden = false
                
                if self.themes.getviewedMe() == "No" || self.themes.getviewedMe() == "" || self.themes.getplanStatus() == "2" {
                    popupview.isHidden = false
                      popupview.center = self.view.center
                    popupview.alpha = 1
                    self.view.addSubview(popupview)
                   self.favrtCollectionView.isHidden = true
                       popupLbl.text = "An active membership is required to view the profiles for ViewedMe."
                    
                }else{
                    backAction(from: "viewedMe")
                    self.UserDataArray = self.viewdMeArray
                    self.favrtCollectionView.reloadData()
                     popupview.isHidden = true
                    self.favrtCollectionView.isHidden = false
                }

            }
            
            favrtCollectionView.reloadData()
            topCollectionView.reloadData()
            
        }else{
          
          //  let topHeadingArray = ["My Favourites","My Intrestes","Favourited Me","Intrested Me","Viewed Me"]
            
            
            
            if self.themes.getplanStatus() == "1"{
                
                
                if selectedIndex == 0 || selectedIndex == 1 || selectedIndex == 4 {
                    
                    let userDetailsVc = self.storyboard?.instantiateViewController(withIdentifier: "UserDetailsVC") as! UserDetailsVC
                    
                    let obj = self.UserDataArray[indexPath.row] as! [String : AnyObject]
                    
                    userDetailsVc.userDataDict = self.UserDataArray[indexPath.row] as! [String : AnyObject]
                    userDetailsVc.fromScreen = "myfavourite"
                    
                    
                    if(selectedIndex == 0){
                        userDetailsVc.fromScreen1 = "myfavourite"
                    }else if(selectedIndex == 1){
                         userDetailsVc.fromScreen1 = "favouratedMe"
                    }else if(selectedIndex == 4){
                        userDetailsVc.fromScreen1 = "viewedMe"
                    }
                    
                    
                   userDetailsVc.imageApprove = true
                    userDetailsVc.delegate = self
                    userDetailsVc.match_id = obj["id"] as! String
                    self.navigationController?.pushViewController(userDetailsVc, animated: true)
                    
                    
                }
                else if selectedIndex == 3{
                    
                    let userDetailsVc = self.storyboard!.instantiateViewController(withIdentifier: "UserDetailsVC") as! UserDetailsVC
                    let obj = self.UserDataArray[indexPath.row] as! [String : AnyObject]
                     userDetailsVc.delegate = self
                    userDetailsVc.userDataDict = self.UserDataArray[indexPath.row] as! [String : AnyObject]
                    userDetailsVc.fromScreen = "interestme"
                     userDetailsVc.fromScreen1 = "interestme"
                    
                     userDetailsVc.imageApprove = true
                    
                    userDetailsVc.match_id = obj["id"] as! String
                    self.navigationController?.pushViewController(userDetailsVc, animated: true)
                    
                }
                else if selectedIndex == 2{
                    
                    let userDetailsVc = self.storyboard!.instantiateViewController(withIdentifier: "UserDetailsVC") as! UserDetailsVC
                    let obj = self.UserDataArray[indexPath.row] as! [String : AnyObject]
                     userDetailsVc.delegate = self
                    userDetailsVc.userDataDict = self.UserDataArray[indexPath.row] as! [String : AnyObject]
                     userDetailsVc.imageApprove = true
                    userDetailsVc.fromScreen = "myinterest"
                     userDetailsVc.fromScreen1 = "myinterest"
                    userDetailsVc.match_id = obj["id"] as! String
                    self.navigationController?.pushViewController(userDetailsVc, animated: true)
                }
            }else{
                // Naveen
//                let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopViewController") as! PopViewController
//                popOverVC.fromScreen = ""
//                // popOverVC.ImageStr = ""
//                popOverVC.headingMesg = "Alert"
//                popOverVC.subMesg = "You need an active membership plan to view the profile. Proceed to membership plans?"
//                self.addChildViewController(popOverVC)
//                popOverVC.view.center = self.view.frame.center
//                self.view.addSubview(popOverVC.view)
//                popOverVC.didMove(toParentViewController: self)
               // Naveen
                
                
                
//                let alertController = UIAlertController(title: "Alert", message: "You need an active membership plan to view the profile. Proceed to membership plans?", preferredStyle: .alert)
//                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
//                    print("Ok button tapped");
//                    let MemberShipViewController = self.storyboard?.instantiateViewController(withIdentifier: "MemberShipViewController") as! MemberShipViewController
//                    self.navigationController?.pushViewController(MemberShipViewController, animated: true)
//                }
//                alertController.addAction(OKAction)
//                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
//
//                }
//                alertController.addAction(cancelAction)
//                self.present(alertController, animated: true, completion:nil)
                
                
               }
            getUserCountApi()
        }
 
    }
     @objc func likeBtnClicked(button: UIButton){
        let userid = self.themes.getUserId()
        sendInterestApi(user_id: userid, Id: id)
        let cell =   favrtCollectionView.cellForItem(at:IndexPath(row: button.tag, section: 0)) as! UsersCollectionViewCell
        if (cell.likeBtn.currentImage?.isEqual(UIImage(named: "Love_icon")))! {
            cell.likeBtn.setImage(UIImage(named: "love_default"), for: .normal)
        } else {
            cell.likeBtn.setImage(UIImage(named: "Love_icon"), for: .normal)
        }
 
    }
    
    func sendInterestApi(user_id: String, Id : String){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            
            let parameters = [ "user_id":user_id, "id":Id,
                ] as [String : Any]
            
            print("send interest parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:sendInterest, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    self.viewWillAppear(true)
              
                } else {
                    let result = response["result"] as! String
                   // self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func getUserCountApi(){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:getUserCounts, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    let allcounts = response["allcounts"] as AnyObject
                    self.favouritesCount = allcounts["favouritescount"] as! Int
                    self.favouritesMeCount = allcounts["favouritedmecount"] as! Int
                    self.intrestedMeCount = allcounts["interestscount"] as! Int
                   
                    self.topCollectionView.reloadData()
                    
                } else {
                    //                    let result = response["result"] as! String
                    //                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }

    func myFavouritesAPI(){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:myFavourites, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
              
                    self.myfavouriteArray = response["data"]as! [AnyObject]
                    self.UserDataArray = self.myfavouriteArray
                    self.favrtCollectionView.reloadData()
                   
                    
                } else {
//                    let result = response["result"] as! String
//                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    func MyInterestAPI(type:String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:myInterest, params: dict as Dictionary<String, Any>) { response in
               // print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    if(type == "normal"){
                         self.my_InterestArray = response["data"]as! [AnyObject]
                    }else{
                         self.my_InterestArray = response["data"]as! [AnyObject]
                        if self.myIntrestsAllImg.image == UIImage(named: "radio_button_selected"){
                            self.My_Interest_FilteringArray(status: "ALL")
                        }
                        else if self.sendPendingImg.image == UIImage(named: "radio_button_selected"){
                            self.My_Interest_FilteringArray(status: "0")
                        }
                        else if self.sendAcceptImg.image == UIImage(named: "radio_button_selected"){
                            self.My_Interest_FilteringArray(status: "1")
                        }
                        else if self.sendRejectImg.image == UIImage(named: "radio_button_selected"){
                            self.My_Interest_FilteringArray(status: "2")
                        }
                    }
                    
                   
                    
                    
                   
                   self.favrtCollectionView.reloadData()
                } else {
//          let result = response["result"] as! String
//          self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    
    func Interested_meAPI(type:String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:interestedMe, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    if(type == "normal"){
                        self.interest_ME_Array = response["data"]as! [AnyObject]
                    }else{
                        self.interest_ME_Array = response["data"]as! [AnyObject]
                        if self.IntrestsMeAllImg.image == UIImage(named: "radio_button_selected"){
                            self.Interest_ME_FilteringArray(status: "ALL")
                        }
                        else if self.RecievePendingImg.image == UIImage(named: "radio_button_selected"){
                            self.Interest_ME_FilteringArray(status: "0")
                        }
                        else if self.RecieveAcceptImg.image == UIImage(named: "radio_button_selected"){
                            self.Interest_ME_FilteringArray(status: "1")
                        }
                        else if self.RecieveRejectImg.image == UIImage(named: "radio_button_selected"){
                            self.Interest_ME_FilteringArray(status: "2")
                        }
                    }
                    
                    
                    
                    
                    
                    
                   
                   self.favrtCollectionView.reloadData()
                } else {
//                    let result = response["result"] as! String
//                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    func favourited_meAPI(type:String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:favouritedMe, params: dict as Dictionary<String, Any>) { response in
                // print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    if(type == "normal"){
                        self.favouritedMeArray = response["data"]as! [AnyObject]
                    }else{
                        self.favouritedMeArray = response["data"]as! [AnyObject]
                         self.UserDataArray = self.favouritedMeArray
                    }
                    
                     self.favrtCollectionView.reloadData()
                    
                } else {
                    //  let result = response["result"] as! String
                    //self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    func viewed_meAPI(type:String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:whoViewedMe, params: dict as Dictionary<String, Any>) { response in
                // print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    if(type == "normal"){
                        self.viewdMeArray = response["data"]as! [AnyObject]
                    }else{
                        self.viewdMeArray = response["data"]as! [AnyObject]
                         self.UserDataArray = self.viewdMeArray
                    }
                
                    self.favrtCollectionView.reloadData()
                } else {
                    //       let result = response["result"] as! String
                    //       self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
   
    
  }



extension UISegmentedControl{
    
    func removeBorder(){
        let backgroundImage = UIImage.getColoredRectImageWith(color: UIColor.black.cgColor, andSize: self.bounds.size)
        self.setBackgroundImage(backgroundImage, for: .normal, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .selected, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .highlighted, barMetrics: .default)
        let deviderImage = UIImage.getColoredRectImageWith(color: UIColor.black.cgColor, andSize: CGSize(width: 1.0, height: self.bounds.size.height))
        self.setDividerImage(deviderImage, forLeftSegmentState: .selected, rightSegmentState: .normal, barMetrics: .default)
     
        
    }
    func addUnderlineForSelectedSegment(){
        
        removeBorder()
        let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
        let underlineHeight: CGFloat = 2.0
        let underlineXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth))
        let underLineYPosition = self.bounds.size.height - 1.0
        let underlineFrame = CGRect(x: underlineXPosition+25, y: underLineYPosition, width: underlineWidth+20, height: underlineHeight)
        let underline = UIView(frame: underlineFrame)
        underline.backgroundColor = UIColor(red: 243/255, green: 165/255, blue: 22/255, alpha: 1.0)
        underline.tag = 1
        self.addSubview(underline)
    }
    func changeUnderlinePosition(){
        guard let underline = self.viewWithTag(1) else {return}
        let underlineFinalXPosition = (self.bounds.width / CGFloat(self.numberOfSegments)) * CGFloat(selectedSegmentIndex)
        UIView.animate(withDuration: 0.1, animations: {
            underline.frame.origin.x = underlineFinalXPosition
        })
    }
}
extension UIImage{
    
    class func getColoredRectImageWith(color: CGColor, andSize size: CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let graphicsContext = UIGraphicsGetCurrentContext()
        graphicsContext?.setFillColor(color)
        let rectangle = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        graphicsContext?.fill(rectangle)
        let rectangleImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return rectangleImage!
    }
}

