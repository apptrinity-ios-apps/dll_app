//
//  FavouritedMeRecord.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 24/05/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class FavouritedMeRecord: NSObject {
    
    var id : String = ""
    var date_id : String = ""
    var gender : String = ""
    var lookingfor : String = ""
    var firstName : String = ""
    var email : String = ""
    var zipcode : String = ""
    var state : String = ""
    
    
    
    var referal_from : String =  ""
    var children : String = ""
    var city : String = ""
    var dob : String = ""
    var ethnicity : String = ""
    var religion : String = ""
    var denomination : String = ""
    var education : String = ""
    var occupation : String = ""
    
    
    var income : String = ""
    var smoke : String = ""
    var drink : String = ""
    var height : String = ""
    var passionate : String = ""
    var leisure_time : String = ""
    var thankful_1 : String = ""
    var thankful_2 : String = ""
    var thankful_3 : String = ""
    
    
    var partner_age_min : String = ""
    var partner_age_max : String = ""
    var partner_match_distance : String = ""
    var partner_match_search : String = ""
    var partner_ethnicity : String = ""
    var partner_religion : String = ""
    var partner_education : String = ""
    var partner_occupation : String = ""
    var partner_smoke : String = ""
    
    var  partner_drink : String = ""
    var photo1 : String = ""
    var photo1_approve : String = ""
    var photo2 : String = ""
    var photo2_approve : String = ""
    var photo3 : String = ""
    var photo3_approve : String = ""
    var photo4 : String = ""
    var photo4_approve : String = ""
    var photo5 : String = ""
    var photo5_approve : String = ""
    var photo6 : String = ""
    var photo6_aprove : String = ""
    var status : String = ""
    var signup_step : String = ""
    var fav_id : String = ""
    var fav_from : String = ""
    var fav_to : String = ""
    var interest_from : String = ""
    var interest_to : String = ""
    
    
    init(id : String,date_id : String,gender : String ,lookingfor : String ,firstName : String,email : String ,zipcode : String ,state : String ,referal_from : String ,children : String ,city : String ,dob : String ,ethnicity : String ,religion : String ,denomination : String ,education : String ,occupation : String ,income : String ,smoke : String,drink : String ,height : String ,passionate : String ,leisure_time : String ,thankful_1 : String ,thankful_2 : String ,thankful_3 : String ,partner_age_min : String ,partner_age_max : String ,partner_match_distance : String ,partner_match_search : String ,partner_ethnicity : String ,partner_religion : String ,partner_education : String ,partner_occupation : String ,partner_smoke : String ,partner_drink : String ,photo1 : String ,photo1_approve : String ,photo2 : String ,photo2_approve : String ,photo3 : String ,photo3_approve : String ,photo4 : String ,photo4_approve : String ,photo5 : String ,photo5_approve : String ,photo6 : String ,photo6_aprove : String ,status : String ,signup_step : String ,fav_id : String ,fav_from : String ,fav_to : String ,
         
         interest_from : String ,interest_to : String){
        
        
        
        
        
        
        
        self.id  = id
        
        self.date_id  = date_id
        
        self.gender  = gender
        
        self.lookingfor  = lookingfor
        
        self.firstName  = firstName
        
        self.email  = email
        
        self.zipcode  = zipcode
        
        self.state  = state
        
        
        
        
        
        
        
        self.referal_from  =  referal_from
        
        self.children  = children
        
        self.city  = city
        
        self.dob  = dob
        
        self.ethnicity  = ethnicity
        
        self.religion  = religion
        
        self.denomination  = denomination
        
        self.education  = education
        
        self.occupation  = occupation
        
        
        
        
        
        self.income  = income
        
        self.smoke  = smoke
        
        self.drink  = drink
        
        self.height  = height
        
        self.passionate  = passionate
        
        self.leisure_time  = leisure_time
        
        self.thankful_1  = thankful_1
        
        self.thankful_2  = thankful_2
        
        self.thankful_3  = thankful_3
        
        
        
        
        
        self.partner_age_min  = partner_age_min
        
        self.partner_age_max  = partner_age_max
        
        self.partner_match_distance  = partner_match_distance
        
        self.partner_match_search  = partner_match_search
        
        self.partner_ethnicity  = partner_ethnicity
        
        self.partner_religion  = partner_religion
        
        self.partner_education  = partner_education
        
        self.partner_occupation  = partner_occupation
        
        self.partner_smoke  = partner_smoke
        
        
        
        self.partner_drink  = partner_drink
        
        self.photo1  = photo1
        
        self.photo1_approve  = photo1_approve
        
        self.photo2  = photo2
        
        self.photo2_approve  = photo2_approve
        
        self.photo3  = photo3
        
        self.photo3_approve  = photo3_approve
        
        self.photo4  = photo4
        
        self.photo4_approve  = photo4_approve
        
        self.photo5  = photo5
        
        self.photo5_approve  = photo5_approve
        
        self.photo6  = photo6
        
        self.photo6_aprove  = photo6_aprove
        
        self.status  = status
        
        self.signup_step  = signup_step
        
        self.fav_id  = fav_id
        
        self.fav_from  = fav_from
        self.fav_to  = fav_to
        self.interest_from  = interest_from
        self.interest_to  = interest_to
        
        
    }
}
