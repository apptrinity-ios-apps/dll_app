//
//  UsersCollectionViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 3/19/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class UsersCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var nameLable: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet var transprntView: UIView!
    @IBOutlet var acceptLabel: UILabel!    
    @IBOutlet var gradeView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        transprntView.layer.cornerRadius = transprntView.frame.size.height/2
//     self.gradeView.applyGrade(colors: [UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.1).cgColor, UIColor(red: 79/255, green: 68/255, blue: 68/255, alpha: 1).cgColor])
        // Initialization code
    }

}
//extension UIView
//{
//    func applyGrade(colors: [CGColor])
//    {
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.colors = colors
//        gradientLayer.startPoint = CGPoint(x: 0, y: 1.0)
//        gradientLayer.endPoint = CGPoint(x: 0, y: 0.0)
//        gradientLayer.locations = [0, 1]
//        gradientLayer.frame = self.bounds
//        self.layer.insertSublayer(gradientLayer, at: 0)
//    }
//}
