//
//  Themes.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/9/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
import XMPPFramework


class Themes: UIViewController {
    
    var xmppController:XMPPController!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        self.xmppController = XMPPController.sharedInstance

    }
    
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    // Mark: ActivityIndicator
    // Showing customized activity indicator
    func showActivityIndicator(uiView: UIView) {
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColorFromHex(rgbValue: 0xffffff, alpha: 0.3)
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColorFromHex(rgbValue: 0x444444, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        
        loadingView.addSubview(activityIndicator)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        activityIndicator.startAnimating()
    }
    
    // Mark: Hidding activity indicator
    func hideActivityIndicator(uiView: UIView) {
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
    }
  
    func shadowForView(view: UIView){
        
        view.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowOpacity = 1.0
        view.layer.shadowRadius = 3.0
        view.layer.masksToBounds = false
    }

    
    func savedate_id(_ date_id: String?) {
        
        UserDefaults.standard.set(date_id, forKey: "date_id")
        
        UserDefaults.standard.synchronize()
        
    }
    
    func getdate_id() -> String? {
        
        return UserDefaults.standard.object(forKey: "date_id") as? String
        
    }
    
    func saveChildren(_ children: String?) {
        
        UserDefaults.standard.set(children, forKey: "children")
        
        UserDefaults.standard.synchronize()
        
    }
    
    func getChildren() -> String? {
        
        return UserDefaults.standard.object(forKey: "children") as? String
        
    }
    
    
    
    func saveuserEmail(_ userMail: String?) {
        
        UserDefaults.standard.set(userMail, forKey: "email")
        
        UserDefaults.standard.synchronize()
        
    }
    
    func getuserMail() -> String? {
        
        return UserDefaults.standard.object(forKey: "email") as? String
        
    }
    
    
    
    func savelooking_for(_ looking_for: String?) {
        
        UserDefaults.standard.set(looking_for, forKey: "looking_for")
        
        UserDefaults.standard.synchronize()
        
    }
    
    func getlooking_for() -> String? {
        
        return UserDefaults.standard.object(forKey: "looking_for") as? String
        
    }
    
    
    
    
    
    func saveEthinicity(_ ethnicity: String?) {
        
        UserDefaults.standard.set(ethnicity, forKey: "ethnicity")
        
        UserDefaults.standard.synchronize()
        
    }
    
    func getEthinicity() -> String? {
        
        return UserDefaults.standard.object(forKey: "ethnicity") as? String
        
    }
    
    
    
    func saveFirstName(_ first_name: String?) {
        
        UserDefaults.standard.set(first_name, forKey: "first_name")
        
        UserDefaults.standard.synchronize()
        
    }
    
    func getFirstName() -> String? {
        
        return UserDefaults.standard.object(forKey: "first_name") as? String
        
    }
    
    
    
    func saveGender(_ gender: String?) {
        
        UserDefaults.standard.set(gender, forKey: "gender")
        
        UserDefaults.standard.synchronize()
        
    }
    
    func getGender() -> String? {
        
        return UserDefaults.standard.object(forKey: "gender") as? String
        
    }
    
    
    
    func saveHeight(_ height: String?) {
        
        UserDefaults.standard.set(height, forKey: "height")
        
        UserDefaults.standard.synchronize()
        
    }
    
    func getHeight() -> String? {
        
        return UserDefaults.standard.object(forKey: "height") as? String
        
    }
    
    
    
    
    
    func saveIncome(_ income: String?) {
        
        UserDefaults.standard.set(income, forKey: "income")
        
        UserDefaults.standard.synchronize()
        
    }
    
    func getIncome() -> String? {
        
        return UserDefaults.standard.object(forKey: "income") as? String
        
    }
    
    
    
    func saveReligion(_ religion: String?) {
        
        UserDefaults.standard.set(religion, forKey: "religion")
        
        UserDefaults.standard.synchronize()
        
    }
    
    func getReligion() -> String? {
        
        return UserDefaults.standard.object(forKey: "religion") as? String
        
    }
    
    
    
    func saveZipcode(_ zipcode: String?) {
        
        UserDefaults.standard.set(zipcode, forKey: "zipcode")
        
        UserDefaults.standard.synchronize()
        
    }
    
    func getZipcode() -> String? {
        
        return UserDefaults.standard.object(forKey: "zipcode") as? String
        
    }
    
    
    
    func saveUserID(_ userID: String?) {
        
        UserDefaults.standard.set(userID, forKey: "userID")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getUserId() -> String{
        
        let userID = UserDefaults.standard.object(forKey: "userID")
        let id = checkNullValue(userID) as! String
        if (id == ""){
            return ""
        } else {
        return userID as! String
        }
    }
    
    func saveEmailNotification(_ email_notification_status: Any?) {
        
        UserDefaults.standard.set(email_notification_status, forKey: "email_notification_status")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getEmailNotification() -> Any{
        
        let email_notification_status = UserDefaults.standard.object(forKey:"email_notification_status")
        
        return email_notification_status as Any
        
    }
    
    
    func savePushNotification(_ push_notification_status: String?) {
        
        UserDefaults.standard.set(push_notification_status, forKey: "push_notification_status")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    func getPushNotification() -> String{
        let push_notification_status = UserDefaults.standard.object(forKey: "push_notification_status")
        return push_notification_status as! String
        
    }
   
    func savePhotoApproveStatus(_ savePhotoApproveStatus: Bool?) {
        UserDefaults.standard.set(savePhotoApproveStatus, forKey: "savePhotoApproveStatus")
        UserDefaults.standard.synchronize()
        
    }
    func getPhotoApproveStatus() -> Bool{
        let push_notification_status = UserDefaults.standard.object(forKey: "savePhotoApproveStatus")
        if(push_notification_status ==  nil){
           return false
        }else{
           return push_notification_status as! Bool
        }
    
    }
    
    
    
    
    
    
    
    func saveMobileNumber(_ mobileNumber: String?) {
        
        UserDefaults.standard.set(mobileNumber, forKey: "phone_number")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getmobileNumber() -> String? {
        
        return UserDefaults.standard.object(forKey: "phone_number") as? String
        
    }
    
    func getCurrentCountry() -> String? {
        return UserDefaults.standard.object(forKey: "CurrentCountry") as? String
    }
    func saveCurrentCountry(_ CurrentCountry: String?) {
        UserDefaults.standard.set(CurrentCountry, forKey: "CurrentCountry")
        UserDefaults.standard.synchronize()
    }
    func getCurrentstate() -> String? {
        return UserDefaults.standard.object(forKey: "Currentstate") as? String
    }
    func saveCurrentstate(_ Currentstate: String?) {
        UserDefaults.standard.set(Currentstate, forKey: "Currentstate")
        UserDefaults.standard.synchronize()
    }
    func getCurrentcity() -> String? {
        return UserDefaults.standard.object(forKey: "Currentcity") as? String
    }
    func saveCurrentcity(_ Currentcity: String?) {
        UserDefaults.standard.set(Currentcity, forKey: "Currentcity")
        UserDefaults.standard.synchronize()
    }
    
    
    
    func saveEducation(_ education: String?) {
        
        UserDefaults.standard.set(education, forKey: "education")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func geteducationr() -> String? {
        
        return UserDefaults.standard.object(forKey: "education") as? String
        
    }
    
    
    
    
    
    func saveoccupation(_ occupation: String?) {
        
        UserDefaults.standard.set(occupation, forKey: "occupation")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getoccupation() -> String? {
        
        return UserDefaults.standard.object(forKey: "occupation") as? String
        
    }
    
    
    
    func savePartnerDenomination(_ denomination: String?) {
        
        UserDefaults.standard.set(denomination, forKey: "partnerDenomination")
        
        UserDefaults.standard.synchronize()
        
    }
    
    func getPartnerDenomination() -> String? {
        
        return UserDefaults.standard.object(forKey: "partnerDenomination") as? String
        
    }
    
    func savesmoke(_ smoke: String?) {
        
        UserDefaults.standard.set(smoke, forKey: "smoke")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getsmoke() -> String? {
        
        return UserDefaults.standard.object(forKey: "smoke") as? String
        
    }
    
    
    
    
    
    func savedrink(_ drink: String?) {
        
        UserDefaults.standard.set(drink, forKey: "drink")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getdrink() -> String? {
        
        return UserDefaults.standard.object(forKey: "drink") as? String
        
    }
    
    
    
    
    
    func savepassionate(_ passionate: String?) {
        
        UserDefaults.standard.set(passionate, forKey: "passionate")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getpassionate() -> String? {
        
        return UserDefaults.standard.object(forKey: "passionate") as? String
        
    }
    
    
    
    func saveleisure_time(_ leisure_time: String?) {
        
        UserDefaults.standard.set(leisure_time, forKey: "leisure_time")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getleisure_time() -> String? {
        
        return UserDefaults.standard.object(forKey: "leisure_time") as? String
        
    }
    
    
    
    
    
    func savethankful_1(_ thankful_1: String?) {
        
        UserDefaults.standard.set(thankful_1, forKey: "thankful_1")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getthankful_1() -> String? {
        
        return UserDefaults.standard.object(forKey: "thankful_1") as? String
        
    }
    
    
    
    func savethankful_2(_ thankful_2: String?) {
        
        UserDefaults.standard.set(thankful_2, forKey: "thankful_2")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getthankful_2() -> String? {
        
        return UserDefaults.standard.object(forKey: "thankful_2") as? String
        
    }
    
    
    
    func savethankful_3(_ thankful_3: String?) {
        
        UserDefaults.standard.set(thankful_3, forKey: "thankful_3")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getthankful_3() -> String? {
        
        return UserDefaults.standard.object(forKey: "thankful_3") as? String
        
    }
    
    
    
    
    
    func savepartner_age_min(_ partner_age_min: String?) {
        
        UserDefaults.standard.set(partner_age_min, forKey: "partner_age_min")
        
        UserDefaults.standard.synchronize()
        
    }
    
    func getpartner_age_min() -> String? {
        
        return UserDefaults.standard.object(forKey: "partner_age_min") as? String
        
    }
    
    
    
    
    
    func savepartner_age_max(_ partner_age_max: String?) {
        
        UserDefaults.standard.set(partner_age_max, forKey: "partner_age_max")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getpartner_age_max() -> String? {
        
        return UserDefaults.standard.object(forKey: "partner_age_max") as? String
        
    }
    
    
    
    func savepartner_match_distance(_ partner_match_distance: String?) {
        
        UserDefaults.standard.set(partner_match_distance, forKey: "partner_match_distance")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getpartner_match_distance() -> String? {
        
        return UserDefaults.standard.object(forKey: "partner_match_distance") as? String
        
    }
    
    
    
    func savepartner_match_search(_ partner_match_search: String?) {
        
        UserDefaults.standard.set(partner_match_search, forKey: "partner_match_search")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getpartner_match_search() -> String? {
        
        return UserDefaults.standard.object(forKey: "partner_match_search") as? String
        
    }
    
    
    
    func savepartner_ethnicity(_ partner_ethnicity: String?) {
        
        UserDefaults.standard.set(partner_ethnicity, forKey: "partner_ethnicity")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getpartner_ethnicity() -> String? {
        
        return UserDefaults.standard.object(forKey: "partner_ethnicity") as? String
        
    }
    
    
    
    func savepartner_religion(_ partner_religion: String?) {
        
        UserDefaults.standard.set(partner_religion, forKey: "partner_religion")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getpartner_religion() -> String? {
        
        return UserDefaults.standard.object(forKey: "partner_religion") as? String
        
    }
    
    
    
    func savepartner_education(_ partner_education: String?) {
        
        UserDefaults.standard.set(partner_education, forKey: "partner_education")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getpartner_education() -> String? {
        
        return UserDefaults.standard.object(forKey: "partner_education") as? String
        
    }
    
    func savemTongue(_ mTongue: String?) {
        
        UserDefaults.standard.set(mTongue, forKey: "mTongue")
        
        UserDefaults.standard.synchronize()
        
    }
    func getmTongue() -> String? {
        
        return UserDefaults.standard.object(forKey: "mTongue") as? String
        
    }
    
    func savepartner_mTongue(_ partner_mTongue: String?) {
        
        UserDefaults.standard.set(partner_mTongue, forKey: "partner_mTongue")
        
        UserDefaults.standard.synchronize()
        
    }
    func getpartner_mTongue() -> String? {
        
        return UserDefaults.standard.object(forKey: "partner_mTongue") as? String
        
    }
    func savepartner_occupation(_ partner_occupation: String?) {
        
        UserDefaults.standard.set(partner_occupation, forKey: "partner_occupation")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getpartner_occupation() -> String? {
        
        return UserDefaults.standard.object(forKey: "partner_occupation") as? String
        
    }
    
    
    
    func savepartner_smoke(_ partner_smoke: String?) {
        
        UserDefaults.standard.set(partner_smoke, forKey: "partner_smoke")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getpartner_smoke() -> String? {
        
        return UserDefaults.standard.object(forKey: "partner_smoke") as? String
        
    }
    
    
    
    
    
    func savepartner_drink(_ partner_drink: String?) {
        
        UserDefaults.standard.set(partner_drink, forKey: "partner_drink")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getpartner_drink() -> String? {
        
        return UserDefaults.standard.object(forKey: "partner_drink") as? String
        
    }
    
    
    
    func savephoto1(_ photo1: String?) {
        
        UserDefaults.standard.set(photo1, forKey: "photo1")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getphoto1() -> String? {
        
        return UserDefaults.standard.object(forKey: "photo1") as? String
        
    }
    
    
    
    func savephoto1_approve(_ photo1_approve: String?) {
        
        UserDefaults.standard.set(photo1_approve, forKey: "photo1_approve")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getphoto1_approve() -> String? {
        
        return UserDefaults.standard.object(forKey: "photo1_approve") as? String
        
    }
    
    
    
    func savephoto2(_ photo2: String?) {
        
        UserDefaults.standard.set(photo2, forKey: "photo2")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getphoto2() -> String? {
        
        return UserDefaults.standard.object(forKey: "photo2") as? String
        
    }
    
    
    
    func savephoto2_approve(_ photo2_approve: String?) {
        
        UserDefaults.standard.set(photo2_approve, forKey: "photo2_approve")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getphoto2_approve() -> String? {
        
        return UserDefaults.standard.object(forKey: "photo2_approve") as? String
        
    }
    
    
    
    func savephoto3(_ photo3: String?) {
        
        UserDefaults.standard.set(photo3, forKey: "photo3")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getphoto3() -> String? {
        
        return UserDefaults.standard.object(forKey: "photo3") as? String
        
    }
    
    func savephoto3_approve(_ photo3_approve: String?) {
        
        UserDefaults.standard.set(photo3_approve, forKey: "photo3_approve")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getphoto3_approve() -> String? {
        
        return UserDefaults.standard.object(forKey: "phophoto3_approveto3") as? String
        
    }
    
    
    
    func savephoto4(_ photo4: String?) {
        
        UserDefaults.standard.set(photo4, forKey: "photo4")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getphoto4() -> String? {
        
        return UserDefaults.standard.object(forKey: "photo4") as? String
        
    }
    
    
    
    func savephoto4_approve(_ photo4_approve: String?) {
        
        UserDefaults.standard.set(photo4_approve, forKey: "photo4_approve")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getphoto4_approve() -> String? {
        
        return UserDefaults.standard.object(forKey: "photo4_approve") as? String
        
    }
    
    
    
    func savephoto5(_ photo5: String?) {
        
        UserDefaults.standard.set(photo5, forKey: "photo5")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getphoto5() -> String? {
        
        return UserDefaults.standard.object(forKey: "photo5") as? String
        
    }
    
    
    
    func savephoto5_approve(_ photo5_approve: String?) {
        
        UserDefaults.standard.set(photo5_approve, forKey: "photo5_approve")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getphoto5_approve() -> String? {
        
        return UserDefaults.standard.object(forKey: "photo5_approve") as? String
        
    }
    
    
    
    
    
    func savephoto6(_ photo6: String?) {
        
        UserDefaults.standard.set(photo6, forKey: "photo6")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getphoto6() -> String? {
        
        return UserDefaults.standard.object(forKey: "photo6") as? String
        
    }
    
    func savephoto6_aprove(_ photo6_aprove: String?) {
        
        UserDefaults.standard.set(photo6_aprove, forKey: "photo6_aprove")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getphoto6_aprove() -> String? {
        
        return UserDefaults.standard.object(forKey: "photo6_aprove") as? String
        
    }
    
    func savesignup_step(_ signup_step: String?) {
        
        UserDefaults.standard.set(signup_step, forKey: "signup_step")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getsignup_step() -> String? {
        
        return UserDefaults.standard.object(forKey: "signup_step") as? String
        
    }
    
    func saveChatstatus(_ Chatstatus: String?) {
           UserDefaults.standard.set(Chatstatus, forKey: "Chatstatus")
           UserDefaults.standard.synchronize()
           
       }
    
    
    func saveChatstatusMessage(_ ChatstatusMessage: String?) {
        UserDefaults.standard.set(ChatstatusMessage, forKey: "ChatstatusMessage")
        UserDefaults.standard.synchronize()
        
    }
    
    func getChatstatusMessage() -> String? {
              return UserDefaults.standard.object(forKey: "ChatstatusMessage") as? String
          }
          
    
       func getChatstatus() -> String? {
           return UserDefaults.standard.object(forKey: "Chatstatus") as? String
       }
       
    
    func savestatus(_ status: String?) {
        UserDefaults.standard.set(status, forKey: "status")
        UserDefaults.standard.synchronize()
        
    }
    func getstatus() -> String? {
        return UserDefaults.standard.object(forKey: "status") as? String
    }
    
    
    
    func savestate(_ state: String?) {
        
        UserDefaults.standard.set(state, forKey: "state")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getstate() -> String? {
        
        return UserDefaults.standard.object(forKey: "state") as? String
        
    }
    
    
    
    func savereferal_from(_ referal_from: String?) {
        
        UserDefaults.standard.set(referal_from, forKey: "referal_from")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getreferal_from() -> String? {
        
        return UserDefaults.standard.object(forKey: "referal_from") as? String
        
    }
    
    
    
    func savecity(_ city: String?) {
        
        UserDefaults.standard.set(city, forKey: "city")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getcity() -> String? {
        
        return UserDefaults.standard.object(forKey: "city") as? String
        
    }
    
    
    
    func savecountry(_ country: String?) {
        
        UserDefaults.standard.set(country, forKey: "country")
        
        UserDefaults.standard.synchronize()
        
    }
    

    func getcountry() -> String? {
        
        return UserDefaults.standard.object(forKey: "country") as? String
        
    }
    
    
    
    func savedob(_ dob: String?) {
        
        UserDefaults.standard.set(dob, forKey: "dob")
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
    func getdob() -> String? {
        
        return UserDefaults.standard.object(forKey: "dob") as? String
        
    }
    
    
    
    func savedenomination(_ denomination: String?) {
        
        UserDefaults.standard.set(denomination, forKey: "denomination")
        UserDefaults.standard.synchronize()
        
    }
    
    func getdenomination() -> String? {
        return UserDefaults.standard.object(forKey: "denomination") as? String
    }
    
    func chatting(_ chatting: String?) {
        UserDefaults.standard.set(chatting, forKey: "chatting")
        UserDefaults.standard.synchronize()
    }
    
    func getChatting() -> String? {
        return UserDefaults.standard.object(forKey: "chatting") as? String
    }
    
    
    func viewedMe(_ viewedMe: String?) {
        UserDefaults.standard.set(viewedMe, forKey: "viewedMe")
        UserDefaults.standard.synchronize()
    }
    
    func getviewedMe() -> String? {
        return UserDefaults.standard.object(forKey: "viewedMe") as? String
    }
    
    
    func favouritedMe(_ favouritedMe: String?) {
        UserDefaults.standard.set(favouritedMe, forKey: "favouritedMe")
        UserDefaults.standard.synchronize()
    }
    
    func getfavouritedMe() -> String? {
        return UserDefaults.standard.object(forKey: "favouritedMe") as? String
    }
    
    
    func planStatus(_ planStatus: String?) {
        UserDefaults.standard.set(planStatus, forKey: "planStatus")
        UserDefaults.standard.synchronize()
    }
    
    func getplanStatus() -> String? {
        return UserDefaults.standard.object(forKey: "planStatus") as? String
    }
    
    
    func getXmppUserCredentials() -> String? {
        return UserDefaults.standard.object(forKey: "xmppusercredentialsdectar") as? String
    }
    
    
    func savexmppPassword(_ savexmppPassword: String?) {
        
        UserDefaults.standard.set(savexmppPassword, forKey: "savexmppPassword")
        UserDefaults.standard.synchronize()
        
    }
    
    func getxmppPassword() -> String? {
        return UserDefaults.standard.object(forKey: "savexmppPassword") as? String
    }
    
    func saveXmppUserName(_ saveXmppUserName: String?) {
        
        UserDefaults.standard.set(saveXmppUserName, forKey: "saveXmppUserName")
        UserDefaults.standard.synchronize()
        
    }
    
    func getXmppUserName() -> String? {
        return UserDefaults.standard.object(forKey: "saveXmppUserName") as? String
    }
    
    
   
    
    func ClearUserInfo(){

         UserDefaults.standard.set(nil, forKey: "checkProfile1")
         UserDefaults.standard.set(nil, forKey: "checkProfile2")
         UserDefaults.standard.set(nil, forKey: "checkProfile3")
         UserDefaults.standard.set(nil, forKey: "checkProfile4")
       //  UserDefaults.standard.set(nil, forKey: "checkTips")
        
//        let usermail =   UserDefaults.standard.object(forKey: "LoginUserName") as? String
//        let userpass =    UserDefaults.standard.object(forKey: "LoginPassword") as? String
        
         UserDefaults.standard.set(nil, forKey: "LoginUserName")
         UserDefaults.standard.set(nil, forKey: "imageUpload")
        
       // xmppController.disconnect()
        
        savexmppPassword("")
        saveUserID("")
        saveXmppUserName("")
        savedate_id("")
        
        saveGender("")
        
        savelooking_for("")
        
        saveFirstName("")
        
        saveuserEmail("")
        
        saveZipcode("")
        
        savestate("")
        
        savereferal_from("")
        
        saveChildren("")
        
        savecity("")
        
        savedob("")
        
        saveEthinicity("")
        
        saveReligion("")
        
        savedenomination("")
        
        saveEducation("")
        
        saveoccupation("")
        savemTongue("")
        savepartner_mTongue("")
        saveIncome("")
        
        savesmoke("")
        
        savedrink("")
        
        saveHeight("")
        
        savepassionate("")
        
        saveleisure_time("")
        
        savethankful_1("")
        
        savethankful_2("")
        
        savethankful_3("")
        
        savepartner_age_max("")
        
        savepartner_age_min("")
        
        savepartner_match_distance("")
        
        savepartner_match_search("")
        
        savepartner_ethnicity("")
        
        savepartner_religion("")
        
        savepartner_education("")
        
        savepartner_occupation("")
        
        savepartner_smoke("")
        
        savepartner_drink("")
        
        savephoto1("")
        
        savephoto2("")
        
        savephoto3("")
        
        savephoto4("")
        
        savephoto5("")
        
        savephoto6("")
        
        savephoto1_approve("")
        
        savephoto2_approve("")
        
        savephoto3_approve("")
        
        savephoto4_approve("")
        
        savephoto5_approve("")
        
        savephoto6_aprove("")
        
        savesignup_step("")
        
        savestatus("")
        
        favouritedMe("")
        viewedMe("")
        chatting("")
        planStatus("")

        UserDefaults.standard.set(nil, forKey: "checkLogIn")
       
       // AuthenticationModel.remove()
        
    }
    
    func showToast(message : String,sender:UIViewController) {
        
        let toastLabel = UILabel(frame: CGRect(x: sender.view.frame.size.width/2 - 125, y: sender.view.frame.size.height-100, width: 250, height: 50))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Poppins-Medium", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = toastLabel.frame.height/2;
        toastLabel.clipsToBounds  =  true
        sender.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    func dateFormateConverter(date:String) -> String{
        if(date == ""){
             return ""
        }else{
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "yyyy-MM-dd"
            let showDate = inputFormatter.date(from: date)
            inputFormatter.dateFormat = "MM-dd-yyyy"
            let resultString = inputFormatter.string(from: showDate!)
            return resultString
        }
    }
    
    
    func getAgeFromDOF2(date: String) -> (Int,Int,Int) {
        
        
        if date != ""{
            let dateFormater = DateFormatter()
            dateFormater.dateFormat = "MM/dd/yyyy"
            let dateOfBirth = dateFormater.date(from: date)
            let calender = Calendar.current
            let dateComponent = calender.dateComponents([.year, .month, .day], from:
                dateOfBirth!, to: Date())
            return (dateComponent.year!, dateComponent.month!, dateComponent.day!)
        }else{
            return (0,0,0)
        }
 
    }
    
    
    
    func getAgeFromDOF(date: String) -> (Int,Int,Int) {
        
        if date != ""{
            let dateFormater = DateFormatter()
            dateFormater.dateFormat = "YYYY-MM-dd"
            let dateOfBirth = dateFormater.date(from: date)
            let calender = Calendar.current
            let dateComponent = calender.dateComponents([.year, .month, .day], from:
                dateOfBirth!, to: Date())
            return (dateComponent.year!, dateComponent.month!, dateComponent.day!)
        }else{
            return (0,0,0)
        }
        
        
        
    }
    
    
    func showAlert(title:String , message:String,sender:UIViewController)
    {
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title,
                                          message: message,
                                          preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK",
                                   style: .cancel, handler: nil)
            alert.addAction(ok)
            sender.present(alert, animated: true , completion: nil)
        }
    }
    
    func hasAppDetails() -> Bool {
        
        if !UserDefaults.standard.bool(forKey: "HasLaunchedOnce") {
            UserDefaults.standard.set(true, forKey: "HasLaunchedOnce")
            UserDefaults.standard.synchronize()
            return true
            //1234changed
        } else {
            
//            if UserDefaults.standard.dictionaryRepresentation().keys.contains(kAppInfo) {
//                let data = UserDefaults.standard.object(forKey: kAppInfo) as? Data
//                var myDictionary: [AnyHashable : Any]? = nil
//                if let aData = data {
//                    myDictionary = NSKeyedUnarchiver.unarchiveObject(with: aData) as? [AnyHashable : Any]
//                }
//                if myDictionary != nil {
//                    return true
//                }
//            }
        }
        return false
    }
    
    func clearCache(){
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
        
    }
    
    //    func checkNullValue(value : AnyObject?) -> String? {
    //        if value is NSNull || value == nil {
    //            return ""
    //        } else {
    //            return value as? String
    //        }
    //    }
    
    func checkNullValue(_ value: Any?) -> Any? {
        var value = value
        if (value is NSNull) || value == nil {
            value = ""
        }
        var str: String? = nil
        if let aValue = value {
            str = "\(aValue)"
        }
        return str
    }
    
    
    func setNormalFontFor(_ label: UILabel? , size:Int) -> UILabel? {
        if let aSize = UIFont(name: "Poppins-Regular", size: CGFloat(size)) {
            label?.font = aSize
        }
        return label
    }
    
    func setMediumFontFor(_ label: UILabel? , size:Int) -> UILabel? {
        if let aSize = UIFont(name: "Poppins-Medium", size: CGFloat(size)) {
            label?.font = aSize
        }
        return label
    }
    
    func setBoldFontFor(_ label: UILabel? , size:Int) -> UILabel? {
        if let aSize = UIFont(name: "Poppins-Bold", size: CGFloat(size)) {
            label?.font = aSize
        }
        return label
    }
    
    func setSemiBoldNormFontFor(_ label: UILabel? , size:Int) -> UILabel? {
        if let aSize = UIFont(name: "Poppins-SemiBold", size: CGFloat(size)) {
            label?.font = aSize
        }
        return label
    }
    
    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
//    class func saveAppDetails(_ appInforec: AppInfoRecord?) {
//
//        let data = NSKeyedArchiver.archivedData(withRootObject: self.records(toAppDict: appInforec))
//        UserDefaults.standard.set(data, forKey: kAppInfo)
//    }
    
//    class func records(toAppDict appInforec: AppInfoRecord?) -> [AnyHashable : Any]? {
//        var dict: [AnyHashable : Any] = [:]
//        dict["site_contact_mail"] = appInforec?.serviceContactEmail
//        dict["customer_service_number"] = appInforec?.serviceNumber
//        dict["site_url"] = appInforec?.serviceSiteUrl
//        dict["xmpp_host_url"] = appInforec?.serviceXmppHost
//        dict["xmpp_host_name"] = appInforec?.serviceXmppPort
//        dict["facebook_id"] = appInforec?.serviceFacebookAppId
//        dict["google_plus_app_id"] = appInforec?.serviceGooglePlusId
//        dict["phone_masking_status"] = appInforec?.servicePhoneMaskingStatus
//        dict["ongoing_ride"] = appInforec?.hasPendingRide
//        dict["ongoing_ride_id"] = appInforec?.pendingRideID
//        dict["rating_pending"] = appInforec?.hasPendingRating
//        dict["rating_pending_ride_id"] = appInforec?.pendingRateId
//        return dict
//    }
    
//    class func findCurrencySymbol(byCode _currencyCode: String?) -> String? {
//        var themes = Themes()
//        var _currencyCode = _currencyCode
//        _currencyCode = themes.checkNullValue(_currencyCode) as! String
//        let fmtr = NumberFormatter()
//        let locale: NSLocale? = self.findLocale(byCurrencyCode: _currencyCode)
//        var currencySymbol: String
//        if locale != nil {
//            if let locale = locale {
//                fmtr.locale = locale as Locale
//            }
//        }
//        fmtr.numberStyle = .currency
//        currencySymbol = fmtr.currencySymbol
//
//        if currencySymbol.count > 1 {
//            currencySymbol = (currencySymbol as? NSString)?.substring(to: 1) ?? ""
//        }
//        return currencySymbol
//    }
    
    class func findLocale(byCurrencyCode _currencyCode: String?) -> NSLocale? {
        let locales = NSLocale.availableLocaleIdentifiers
        var locale: NSLocale? = nil
        
        for localeId: String in locales {
            locale = NSLocale(localeIdentifier: localeId)
            let code = locale?.object(forKey: .currencyCode) as? String
            if (code == _currencyCode) {
                break
            } else {
                locale = nil
            }
        }
        return locale
    }
}
open class CustomSlider1 : UISlider {
    @IBInspectable open var trackWidth:CGFloat = 2 {
        didSet {setNeedsDisplay()}
    }
    
    override open func trackRect(forBounds bounds: CGRect) -> CGRect {
        let defaultBounds = super.trackRect(forBounds: bounds)
        return CGRect(
            x: defaultBounds.origin.x,
            y: defaultBounds.origin.y + defaultBounds.size.height/2 - trackWidth/2,
            width: defaultBounds.size.width,
            height: trackWidth
        )
    }
}
extension UILabel {
    
    func setNormalFontFor(_ label: UILabel? , size:Int) -> UILabel? {
        if let aSize = UIFont(name: "Poppins-Regular", size: CGFloat(size)) {
            label?.font = aSize
        }
        return label
    }
    
    func setMediumFontFor(_ label: UILabel? , size:Int) -> UILabel? {
        if let aSize = UIFont(name: "Poppins-Medium", size: CGFloat(size)) {
            label?.font = aSize
        }
        return label
    }
    
    func setBoldFontFor(_ label: UILabel? , size:Int) -> UILabel? {
        if let aSize = UIFont(name: "Poppins-Bold", size: CGFloat(size)) {
            label?.font = aSize
        }
        return label
    }
    
    func setSemiBoldNormFontFor(_ label: UILabel? , size:Int) -> UILabel? {
        if let aSize = UIFont(name: "Poppins-SemiBold", size: CGFloat(size)) {
            label?.font = aSize
        }
        return label
    }    
}

extension UIButton {
    
    func setNormalFontFor(_ btn: UIButton? , size:Int) -> UIButton? {
        if let aSize = UIFont(name: "Poppins-Regular", size: CGFloat(size)) {
            btn?.titleLabel?.font = aSize
        }
        return btn
    }
    
    func setMediumFontFor(_ btn: UIButton? , size:Int) -> UIButton? {
        if let aSize = UIFont(name: "Poppins-Medium", size: CGFloat(size)) {
            btn?.titleLabel?.font = aSize
        }
        return btn
    }
    
    func setBoldFontFor(_ btn: UIButton? , size:Int) -> UIButton? {
        if let aSize = UIFont(name: "Poppins-Bold", size: CGFloat(size)) {
            btn?.titleLabel?.font = aSize
        }
        return btn
    }
    
    func setSemiBoldNormFontFor(_ btn: UIButton? , size:Int) -> UIButton? {
        if let aSize = UIFont(name: "Poppins-SemiBold", size: CGFloat(size)) {
            btn?.titleLabel?.font = aSize
        }
        return btn
    }
}

extension UITextField {
    
    func setNormalFontFor(_ textField: UITextField? , size:Int) -> UITextField? {
        if let aSize = UIFont(name: "Poppins-Regular", size: CGFloat(size)) {
            textField?.font = aSize
        }
        return textField
    }
    
    func setMediumFontFor(_ textField: UITextField? , size:Int) -> UITextField? {
        if let aSize = UIFont(name: "Poppins-Medium", size: CGFloat(size)) {
            textField?.font = aSize
        }
        return textField
    }
    
    func setBoldFontFor(_ textField: UITextField? , size:Int) -> UITextField? {
        if let aSize = UIFont(name: "Poppins-Bold", size: CGFloat(size)) {
            textField?.font = aSize
        }
        return textField
    }
    
    func setSemiBoldNormFontFor(_ textField: UITextField? , size:Int) -> UITextField? {
        if let aSize = UIFont(name: "Poppins-SemiBold", size: CGFloat(size)) {
            textField?.font = aSize
        }
        return textField
    }
}

extension UITextView {
    
    func setNormalFontFor(_ UITextView: UITextView?, size:Int) -> UITextView? {
        UITextView?.font = UIFont(name: "Arial Rounded MT Bold",size: CGFloat(size))
        return UITextView
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        //tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UIView {
    
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat, roundedRect: CGRect) {
        let path = UIBezierPath(roundedRect: roundedRect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
}
  
 
                                                                                                                        
