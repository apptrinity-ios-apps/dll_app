//
//  PopViewController.swift
//  DivorceLoveLounge
//
//  Created by nagaraj  kumar on 30/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class PopViewController: UIViewController {
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    
    @IBOutlet var popUpView: UIView!
    
    @IBOutlet var popUp_BGImage: UIImageView!
    
    @IBOutlet var userImageView: UIImageView!
        
    @IBOutlet var headingLBL: UILabel!
    
    @IBOutlet var SubTextBL: UILabel!
    
    @IBOutlet var ok_Btn: UIButton!
    
    @IBOutlet var strasImg: UIImageView!
    
    var ImageStr = ""
    var id = String()
    var userId = String()
    var headingMesg = ""
    var subMesg = ""
    var fromScreen = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.headingLBL.text = headingMesg
        self.SubTextBL.text = subMesg
//        self.userImageView.image =
        

        self.popUpView.layer.cornerRadius = 10
        self.userImageView.layer.cornerRadius = self.userImageView.frame.height / 2
        self.userImageView.layer.masksToBounds = true
        self.userImageView.layer.borderWidth = 1
        self.userImageView.layer.borderColor = UIColor.white.cgColor
        
        self.popUp_BGImage.layer.cornerRadius = 10
        self.popUp_BGImage.layer.masksToBounds = true
        self.popUp_BGImage.clipsToBounds = true
        
        self.strasImg.layer.cornerRadius = 10
        self.strasImg.layer.masksToBounds = true
        self.strasImg.clipsToBounds = true
        
        self.ok_Btn.layer.cornerRadius = self.ok_Btn.frame.height / 2
        
        
        self.showAnimate()

        let imageUrl =   self.themes.checkNullValue(self.themes.getphoto1()) as! String
        
        let fullimageUrl = ImagebaseUrl + imageUrl
       
            userImageView.sd_setShowActivityIndicatorView(true)
           userImageView.sd_setIndicatorStyle(.whiteLarge)
            userImageView.sd_setImage(with: URL(string: fullimageUrl), placeholderImage:UIImage(named: "defaultPic"), completed: { (image, error, cacheType, url) -> Void in
                if ((error) != nil) {
                    self.userImageView.image = UIImage(named: "defaultPic")
                    self.userImageView.sd_setShowActivityIndicatorView(true)
                    
                } else {
                    
                    self.userImageView.image = image
                     self.userImageView.sd_setShowActivityIndicatorView(true)
                }
            })
        
        // Do any additional setup after loading the view.
    }
    
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
       // self.view.alpha = 3.0;
        UIView.animate(withDuration: 0.25, animations: {
         //   self.view.alpha = 1.8
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
           // self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    
    

    @IBAction func CloseAction(_ sender: Any) {
        
      self.removeAnimate()
        
    }
    
    
    @IBAction func OKAction(_ sender: Any) {
      
     print(ImageStr)
        
        self.removeAnimate()
        
        if fromScreen == "paymentDone" {
            
            
        let userDetailsVc = self.storyboard!.instantiateViewController(withIdentifier: "myPlanVC") as! myPlanVC
            userDetailsVc.fromScreen = "payment"
        self.navigationController?.pushViewController(userDetailsVc, animated: true)
//
        }else if(fromScreen == "SupportScreen"){
            let userDetailsVc = self.storyboard!.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
            self.navigationController?.pushViewController(userDetailsVc, animated: true)
        }
        else if(fromScreen == "loginstatus"){
            logOutAPI()
            
            self.themes.ClearUserInfo()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if(fromScreen == "login"){
          
        }else if(fromScreen == "unBlock"){
            blockUnblockApi(user_id: userId, Id: id)
        } else if(fromScreen == "expirePlan") {
            let memberShipVc = self.storyboard!.instantiateViewController(withIdentifier: "NewMemberShipPlanVC") as! NewMemberShipPlanVC
            self.navigationController?.pushViewController(memberShipVc, animated: true)
        }
        else {
//            let userDetailsVc = self.storyboard!.instantiateViewController(withIdentifier: "NewMemberShipPlanVC") as! NewMemberShipPlanVC
//            self.navigationController?.pushViewController(userDetailsVc, animated: true)
        }
        
    }
    
    
    func blockUnblockApi(user_id: String, Id : String){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
                        themes.showActivityIndicator(uiView: self.view)
            
            let parameters = [ "user_id":user_id, "id":Id,
                ] as [String : Any]
            
            print("favourite parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:blockUnblock, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let home = self.storyboard?.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
                    self.navigationController?.pushViewController(home, animated: true)
                    
                } else {
                    let result = response["result"] as! String
                    
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    
    
    
    
    func logOutAPI(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            let userid = self.themes.getUserId()
            let devicetoken =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "kDeviceToken") )
            let dict = ["user_id":userid, "device_token":devicetoken as! String ] as [String:Any]
            print("logout parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:signOut, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                let success = response["status"] as! String
                if(success == "1"){
                   
                    print("Success")
                }
                
            }
        } else {
            // self.themes.hideActivityIndicator(uiView: self.view)
            // themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    

}
