//
//  URLservices.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/10/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//


import Foundation
import UIKit
import Alamofire
import AlamofireImage
import SystemConfiguration
class URLservices: UIViewController {
    // var userID:UserDefaults!
    //SingleTon
    var ShowDeadAlert = Bool()
    var themes = Themes()
    //let appDelegate = UIApplication.shared.delegate as! AppDelegate
    static let sharedInstance : URLservices = {
        let instance = URLservices()
        return instance
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    func serviceCallGetMethod(url:String,completionHandler:@escaping (_ success: [String : AnyObject]) -> ())
    {
        let urlString: String = url
        let totalUrl =  AppbaseUrl + urlString
        Alamofire.request(totalUrl,method: .get,  encoding: JSONEncoding.default).responseJSON { (response:DataResponse<Any>) in
            // check for errors
            guard response.result.error == nil else {
                // got an error in getting the data, need to handle it
                return
            }
            guard let json = response.result.value as? [String: Any] else {
                return
            }
            //print(json)
            completionHandler(json as [String : AnyObject])
        }
    }
    // Calling PostMethod With Parameters
    
    
    func serviceCallPostMethodWithParams(url:String, params:Dictionary<String,Any> , completionHandler:@escaping (_ success: [String : AnyObject]) -> ())
    {
        let urlString: String = url
        let totalUrl =  AppbaseUrl + urlString
        let params: [String: Any] = params
        // Converting Dictinary to Array formmat and Than Converting to String Formmat
        Alamofire.request(totalUrl,method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { (response:DataResponse<Any>) in
            
            guard response.result.error == nil else {
                print("error calling POST on /todos/1")
                print(response.result.error!)
                
                //  self.themes.hideActivityIndicator(uiView: self.view)
                
                return
            }
            // make sure we got some JSON since that's what we expect
            guard let json = response.result.value as? [String: Any]
                else {
                    print("didn't get todo object as JSON from API")
                    // print("Error: \(String(describing: response.result.error))")
                    return
            }
            print(json)
            completionHandler(json as [String : AnyObject])
        }
    }

    // NetworkReachabilityManager
    func connectedToNetwork()  -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return isReachable && !needsConnection
    }
    
    func Uber_ServiceCallPostMethodWithParams(url:String, params:Dictionary<String,Any> , completionHandler:@escaping (_ success: [String : AnyObject]) -> ())
    {
        let urlString: String = url
        let totalUrl =  AppbaseUrl + urlString
        let params: [String: Any] = params
        // Converting Dictinary to Array formmat and Than Converting to String Formmat
        Alamofire.request(totalUrl,method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { (response:DataResponse<Any>) in
            
            guard response.result.error == nil else {
                print("error calling POST on /todos/1")
                print(response.result.error!)
                //  self.themes.hideActivityIndicator(uiView: self.view)
                return
            }
            // make sure we got some JSON since that's what we expect
            guard let json = response.result.value as? [String: Any]
                else {
                    print("didn't get todo object as JSON from API")
                    // print("Error: \(String(describing: response.result.error))")
                    return
            }
            print(json)
            completionHandler(json as [String : AnyObject])
        }
        
    }
    
    func UberServiceCallGetMethod(url:String,completionHandler:@escaping (_ success: [String : AnyObject]) -> ())
    {
        let urlString: String = url
        let totalUrl = urlString
        Alamofire.request(totalUrl,method: .get,  encoding: JSONEncoding.default).responseJSON { (response:DataResponse<Any>) in
            // check for errors
            guard response.result.error == nil else {
                // got an error in getting the data, need to handle it
                return
            }
            guard let json = response.result.value as? [String: Any] else {
                return
            }
            completionHandler(json as [String : AnyObject])
        }
    }
    
//    func download_Image(urlString: String,type:String,completion: @escaping (_ success: UIImage) -> ()) {
//        var totalUrl = String()
//        totalUrl =  imgBaseUrl + urlString
//        let imgVW = UIImageView()
//        Alamofire.request(totalUrl).responseImage { response in
//            if let image1 = response.result.value {
//                imgVW.image = image1
//            }else{
//
//            }
//            completion(imgVW.image!)
//        }
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func adslistServiceCall(ScreenType:String, completionHandler:@escaping (_ success: [String : AnyObject]) -> ()) {
        var userid = self.themes.getUserId()
        if userid == "" {
            userid = "0"
        }
        let parameters = [ "country": /*"India",*/ self.themes.getCurrentCountry(),
                           "state": /*"Telangana" , */self.themes.getCurrentstate(),
                           "city":/*"Hyderabad",*/self.themes.getCurrentcity(),
                           "page":ScreenType,
                           "user_id":userid
            ] as [String : AnyObject]
        print(parameters)
        self.serviceCallPostMethodWithParams(url: adsList, params: parameters) { response in
            let success = response["status"] as! String
            if(success == "1"){
            }else{
            }
          completionHandler(response as [String : AnyObject])
        }
    }
 
}


extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}
