//
//  EditProfilePicVC.swift
//  DivorceLoveLounge
//
//  Created by nagaraj  kumar on 11/06/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//
import UIKit
import AssetsLibrary
import Photos
protocol profilePicUpdateDelegate:class {
    func myVCDidFinish(image:String)
}
class EditProfilePicVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIActionSheetDelegate,profilePicUpdateDelegate {
    func myVCDidFinish(image: String) {
    }
    let imgArr = ["img1","img2","img3","img1","img2","img3"]
    var infoValue = Int()
     var profileImage = UIImage()
    
    var fromScreen = String()
    weak var delegate : profilePicUpdateDelegate?
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    var imageArray = [AnyObject]()
    var infocloseValue = Int()
    
    
    @IBOutlet var backButton: UIButton!
    @IBOutlet var editProfileCollectionView: UICollectionView!
    @IBOutlet var headingLbl: UILabel!
    override func viewDidLoad(){
        super.viewDidLoad()
        infoValue = -1
        editProfileCollectionView.delegate = self
        editProfileCollectionView.dataSource = self
        self.editProfileCollectionView.register(UINib(nibName: "EditProfileCell", bundle: nil), forCellWithReuseIdentifier: "EditProfileCell")
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
      //  self.editProfileCollectionView.addGestureRecognizer(tapGesture)
        self.view.addGestureRecognizer(tapGesture)
        if(fromScreen == "AppDelegate"){
            backButton.isHidden = true
        }else{
            backButton.isHidden = false
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        getprofileAPi()
    }
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        infoValue = -1
        editProfileCollectionView.reloadData()
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        
        self.navigationController?.popViewController(animated: true)
    }
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.imageArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EditProfileCell", for: indexPath)as! EditProfileCell
        
        let url =   self.themes.checkNullValue(imageArray[indexPath.row] as AnyObject?) as! String
        
        let imageBaseUrl = ImagebaseUrl + url
        
       
//         cell.imgView.sd_setImage(with: URL(string: imageBaseUrl), placeholderImage: UIImage(named: "defaultPic"))
        
        DispatchQueue.main.async {
            cell.imgView.sd_setShowActivityIndicatorView(true)
            cell.imgView.sd_setIndicatorStyle(.whiteLarge)
            cell.imgView.sd_setImage(with: URL(string: imageBaseUrl), placeholderImage:UIImage(named: "defaultPic"), completed: { (image, error, cacheType, url) -> Void in
                if ((error) != nil) {
                    cell.imgView.image = UIImage(named: "defaultPic")
                    cell.imgView.sd_setShowActivityIndicatorView(true)
                } else {
                    cell.imgView.image = image
                    cell.imgView.sd_setShowActivityIndicatorView(false)
                }
                
            })
            
        }

         cell.infoBtn.tag = indexPath.item
         cell.setProfileBtn.tag = indexPath.item
         cell.editBtn.tag = indexPath.item
         cell.infoBtn.addTarget(self, action: #selector(infoBtnClick(button:)), for:.touchUpInside)
        
        
         cell.editBtn.addTarget(self, action: #selector(editProfile(button:)), for:.touchUpInside)
         cell.setProfileBtn.addTarget(self, action: #selector(setProfile(button:)), for:.touchUpInside)
         cell.deleteBtn.addTarget(self, action: #selector(deleteProfile(button:)), for:.touchUpInside)
         cell.deleteBtn.tag = indexPath.item
        
        if infoValue == indexPath.row {
            cell.infoView.isHidden = false
        }else {
            cell.infoView.isHidden = true
        }
        if(infoValue == 0){
            cell.deleteHeight.constant = 0
            cell.heleteLabelHeight.constant = 0
            cell.setPicLabelHeight.constant = 0
            cell.setPicHeight.constant = 0
            cell.deleteBtn.setTitle("", for: .normal)
            
            cell.setProfileBtn.setTitle("", for: .normal)
            
            
        }else{
            if(url == ""){
                cell.deleteHeight.constant = 0
                cell.heleteLabelHeight.constant = 0
                cell.setPicLabelHeight.constant = 0
                cell.setPicHeight.constant = 0
                cell.deleteBtn.setTitle("", for: .normal)
                
                cell.setProfileBtn.setTitle("", for: .normal)
            }else{
                cell.deleteHeight.constant = 35
                cell.heleteLabelHeight.constant = 1
                cell.setPicLabelHeight.constant = 1
                cell.setPicHeight.constant = 35
                cell.deleteBtn.setTitle("Delete", for: .normal)
                
                cell.setProfileBtn.setTitle("Set Profile Pic", for: .normal)
            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        print("screen width is \(screenWidth)")
        if(screenWidth == 375){
            return CGSize(width: 186, height: 186)
        }else if(screenWidth == 414){
            return CGSize(width: 205.5, height: 206)
        }else if(screenWidth == 320) {
            return CGSize(width: 158.5, height: 159)
        }else if(screenWidth == 768) {
            return CGSize(width: 382.5, height: 382)
        }else if(screenWidth == 1024) {
            return CGSize(width: 255, height: 272)
        } else {
            return CGSize(width: 277, height: 277)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 1
    }
    
     @objc func editProfile(button: UIButton) {
        
        
        let indexPath = IndexPath(item: button.tag, section: 0)
        let cell = self.editProfileCollectionView?.cellForItem(at: indexPath) as? EditProfileCell
        cell?.infoView.isHidden = true
        
        
      let actionSheet = UIActionSheet(title: "Choose Option", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera","Gallery")
        actionSheet.show(in: self.view)
        
        
     }
     @objc func setProfile(button: UIButton) {
        
        let indexPath = IndexPath(item: button.tag, section: 0)
        let cell = self.editProfileCollectionView?.cellForItem(at: indexPath) as? EditProfileCell
        
        let url =   self.themes.checkNullValue(imageArray[button.tag] as AnyObject?) as! String
        
        let imageBaseUrl = ImagebaseUrl + url
        
        cell?.infoView.isHidden = true
        setAsProfileApi(photoNum: button.tag + 1, image: imageBaseUrl)
     }
     @objc func deleteProfile(button: UIButton) {
        print(button.tag)
        let indexPath = IndexPath(item: button.tag, section: 0)
        let cell = self.editProfileCollectionView?.cellForItem(at: indexPath) as? EditProfileCell
 
        let photoName =   self.themes.checkNullValue(imageArray[button.tag] as AnyObject?) as! String
        cell?.infoView.isHidden = true
        
        deleteImageApi(imageNumber: button.tag + 1, photoName: photoName)
     }
    
    
    
    @objc func infoBtnClick(button: UIButton) {
        
            infoValue = button.tag
            editProfileCollectionView.reloadData()
      
 }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        switch (buttonIndex){
        case 1:
            cameraClicked()
        case 2:
            galleryClicked()
        default:
            print("Default")
        }
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    func cameraClicked(){
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized
        {
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerControllerSourceType.camera
            image.allowsEditing = true
            self.present(image, animated: true)
        }
        else
        {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted :Bool) -> Void in
                if granted == true
                {
                    let image = UIImagePickerController()
                    image.delegate = self
                    image.sourceType = UIImagePickerControllerSourceType.camera
                    image.allowsEditing = false
                    
                    DispatchQueue.main.async {
                        self.present(image, animated: true)
                    }
                    
                }
                else
                {
                    print("User Rejected")
                    let alertController = UIAlertController(title: "This app does not have access to your camera.",
                                                            message: "Turn on your Camera from the settings to help Access your camera",
                                                            preferredStyle: .alert)
                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alertAction) in
                        if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(appSettings as URL)
                            } else {
                                // Fallback on earlier versions
                            }
                        }
                    }
                    alertController.addAction(settingsAction)
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                    alertController.addAction(cancelAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            });
        }
    }
    func galleryClicked(){
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            // Access has been granted.
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerControllerSourceType.photoLibrary
            image.allowsEditing = true
            
            self.present(image, animated: true)
        }
        else if (status == PHAuthorizationStatus.denied) {
            // Access has been denied.
            print("alert")
            let alertController = UIAlertController(title: "This app does not have access to your Photos.",
                                                    message: "Turn on your Photos from the settings to help Access your Gallery",
                                                    preferredStyle: .alert)
            let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alertAction) in
                if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(appSettings as URL)
                    } else {
                        // Fallback on earlier versions
                    }
                }
            }
            alertController.addAction(settingsAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
        else if (status == PHAuthorizationStatus.notDetermined) {
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                if (newStatus == PHAuthorizationStatus.authorized) {
                    
                }
                else {
                }
            })
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image1 = info[UIImagePickerControllerEditedImage] as? UIImage
        {
            let cropimage = resizeImage(image: image1, targetSize: CGSize(width: 400.0, height: 400.0))
            
            let imageData:NSData = UIImageJPEGRepresentation(cropimage, 0.50)! as NSData //UIImagePNGRepresentation(img)
            let imgString = imageData.base64EncodedString(options: .init(rawValue: 0))
            if let asset = info[UIImagePickerControllerPHAsset] as? PHAsset {
                let assetResources = PHAssetResource.assetResources(for: asset)
            let indexPath = IndexPath(item: infoValue, section: 0)
            let cell = self.editProfileCollectionView?.cellForItem(at: indexPath) as? EditProfileCell
            cell?.infoView.isHidden = true
            cell?.imgView.image = cropimage
              //  let cropimage = resizeImage(image: image1, targetSize: CGSize(width: 400.0, height: 400.0))
                
                print(cropimage.size)
                
                
           profileImage = cropimage
           self.dismiss(animated: true, completion: nil)
           print("stauscode data converting")

          //  print(imgString)
            let photname = info["UIImagePickerControllerImageURL"] as! URL
         //   let photo = "\(photname)"



            let result = PHAsset.fetchAssets(withALAssetURLs: [photname], options: nil)
            let asset = result.firstObject
           // print(asset?.value(forKey: "filename"))
  
            editProfileApi(photoStr: imgString, photoName: assetResources.first!.originalFilename, photoNO: infoValue+1)

                print(assetResources.first!.originalFilename)
            }else{
                
                let indexPath = IndexPath(item: infoValue, section: 0)
                let cell = self.editProfileCollectionView?.cellForItem(at: indexPath) as? EditProfileCell
                cell?.infoView.isHidden = true
                cell?.imgView.image = image1
                editProfileApi(photoStr: imgString, photoName: "camera.jpg", photoNO: infoValue+1)
                
                
                
            }
            
            dismiss(animated: true, completion: nil)
        }else if let img = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            
            let imageData:NSData = UIImageJPEGRepresentation(img, 0.50)! as NSData //UIImagePNGRepresentation(img)
            let imgString = imageData.base64EncodedString(options: .init(rawValue: 0))
            if let asset = info[UIImagePickerControllerPHAsset] as? PHAsset {
                let assetResources = PHAssetResource.assetResources(for: asset)
                let indexPath = IndexPath(item: infoValue, section: 0)
                let cell = self.editProfileCollectionView?.cellForItem(at: indexPath) as? EditProfileCell
                cell?.infoView.isHidden = true
                cell?.imgView.image = img
                let cropimage = resizeImage(image: img, targetSize: CGSize(width: 400.0, height: 400.0))
                print(cropimage.size)
                
            }
            
            
        }

    }
    func getprofileAPi(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
           // print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:getProfilePics, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    self.infoValue = -1
                    self.imageArray.removeAll()
                    let photo = response["photos"] as! [String:AnyObject]
                    self.imageArray.append(photo["photo1"] as AnyObject)
                    self.imageArray.append(photo["photo2"] as AnyObject)
                    self.imageArray.append(photo["photo3"] as AnyObject)
                    self.imageArray.append(photo["photo4"] as AnyObject)
                    self.imageArray.append(photo["photo5"] as AnyObject)
                    self.imageArray.append(photo["photo6"] as AnyObject)
                    self.imageArray.append(photo["photo7"] as AnyObject)
                    self.imageArray.append(photo["photo8"] as AnyObject)
                    self.imageArray.append(photo["photo9"] as AnyObject)
                    self.imageArray.append(photo["photo10"] as AnyObject)
                    self.editProfileCollectionView.reloadData()
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    func editProfileApi(photoStr:String , photoName:String,photoNO:Int){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid,
                        "photo_no":photoNO,
                        "photo":photoStr,
                        "photo_name":photoName] as [String:Any]
            
           
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:changePhoto, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                   self.getprofileAPi()
                   // UserDefaults.standard.object(forKey: "imageUpload")
                    UserDefaults.standard.setValue(true, forKey: "imageUpload")
                    if(self.fromScreen == "AppDelegate"){
                                            let SignVc = self.storyboard?.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
                                            self.navigationController?.pushViewController(SignVc, animated: true)
                    }
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    func setAsProfileApi(photoNum:Int,image:String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid,"photo_no":photoNum] as [String:Any]
           // print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:setProfilePic, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    self.delegate?.myVCDidFinish(image:image)
                    
                    self.navigationController?.popViewController(animated: true)
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    func deleteImageApi(imageNumber:Int, photoName:String){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid,"photo_no":imageNumber,"photo_name": photoName] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:deletePhoto, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    
                      self.getprofileAPi()
                    
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    
}
