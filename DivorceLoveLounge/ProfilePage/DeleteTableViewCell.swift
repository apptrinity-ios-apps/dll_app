//
//  DeleteTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 3/18/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class DeleteTableViewCell: UITableViewCell {

    
    @IBOutlet weak var deleteBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    self.deleteBtn.layer.cornerRadius = (deleteBtn.frame.size.height)/2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
