//
//  EditProfileCell.swift
//  DivorceLoveLounge
//
//  Created by nagaraj  kumar on 11/06/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class EditProfileCell: UICollectionViewCell {

    
    @IBOutlet weak var heleteLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var setPicLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var uploadLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var deleteHeight: NSLayoutConstraint!
    @IBOutlet weak var setPicHeight: NSLayoutConstraint!
    @IBOutlet weak var uploadHeight: NSLayoutConstraint!
    @IBOutlet weak var backView: UIView!
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var infoBtn: UIButton!
    @IBOutlet var infoView: UIView!
    @IBOutlet var editBtn: UIButton!
    @IBOutlet var setProfileBtn: UIButton!
    @IBOutlet var deleteBtn: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        self.backView.layer.borderWidth = 1
//        self.backView.layer.borderColor = UIColor.gray.cgColor
        // Initialization code
    }

}
