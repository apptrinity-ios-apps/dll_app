//
//  ChangePasswordVC.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 02/08/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController,UITextFieldDelegate {

    
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    
    @IBOutlet var backView: UIView!
    @IBOutlet var submitBtn: UIButton!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var newPasswordField: UITextField!
    @IBOutlet weak var reTypePasswordField: UITextField!

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitAction(_ sender: Any) {

        if ValidatedTextField() == true {
            let userid = self.themes.getUserId()
            changePasswordAPI(user_id: userid)
        } else {

        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        passwordField.layer.cornerRadius = passwordField.frame.height/2
        passwordField.delegate = self
        passwordField.layer.borderWidth = 1
        passwordField.layer.borderColor = UIColor.lightGray.cgColor
        // For left side Padding
        passwordField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: passwordField.frame.height))
        passwordField.leftViewMode = .always
        // For Right side Padding
        passwordField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: passwordField.frame.height))
        passwordField.rightViewMode = .always
        
        newPasswordField.layer.cornerRadius = newPasswordField.frame.height/2
        newPasswordField.delegate = self
        newPasswordField.layer.borderWidth = 1
        newPasswordField.layer.borderColor = UIColor.lightGray.cgColor
        // For  left side Padding
        newPasswordField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: newPasswordField.frame.height))
        newPasswordField.leftViewMode = .always
        // For Right side Padding
        newPasswordField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: newPasswordField.frame.height))
        newPasswordField.rightViewMode = .always
        
        reTypePasswordField.layer.cornerRadius = reTypePasswordField.frame.height/2
        reTypePasswordField.delegate = self
        reTypePasswordField.layer.borderWidth = 1
        reTypePasswordField.layer.borderColor = UIColor.lightGray.cgColor
        // For left side Padding
        reTypePasswordField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: reTypePasswordField.frame.height))
        reTypePasswordField.leftViewMode = .always
        // For Right side Padding
        reTypePasswordField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: reTypePasswordField.frame.height))
        reTypePasswordField.rightViewMode = .always
        
        submitBtn.layer.cornerRadius = submitBtn.frame.size.height/2
        submitBtn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        submitBtn.layer.shadowOpacity = 1.0
        submitBtn.layer.shadowRadius = 3
        submitBtn.layer.masksToBounds = false
        submitBtn.layer.shadowColor = UIColor.lightGray.cgColor
        
        backView.layer.cornerRadius = 5
        backView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        backView.layer.shadowOpacity = 1.0
        backView.layer.shadowRadius = 3
        backView.layer.masksToBounds = false
        backView.layer.shadowColor = UIColor.lightGray.cgColor
        
        // Keyboard Action
        NotificationCenter.default.addObserver(self, selector: #selector(ChangePasswordVC.keyboardUp), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChangePasswordVC.keyboardDown), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        passwordField.resignFirstResponder()
        newPasswordField.resignFirstResponder()
        reTypePasswordField.resignFirstResponder()
    }
    @objc func keyboardUp(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            //
            self.view.frame.origin.y = 130
            if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardRectangle = keyboardFrame.cgRectValue
                let keyboardHeight = keyboardRectangle.height
                self.view.frame.origin.y -= keyboardHeight
            }
        }
    }
    @objc func keyboardDown(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            self.view.frame.origin.y = 0
        }
    }
    
    func ValidatedTextField()-> Bool {
        
        if (passwordField.text == ""){
            self.themes.showAlert(title: "Alert", message: "Current Password is Mandatory", sender: self)
            // self.userNameField.becomeFirstResponder()
            return false
        }
        else if (newPasswordField.text == "") {
            self.themes.showAlert(title: "Alert", message: "NewPassword is Mandatory", sender: self)
            // self.passwordField.becomeFirstResponder()
            return false
        } else if (reTypePasswordField.text == "") {
            self.themes.showAlert(title: "Alert", message: "Please Re-Type the NewPassword", sender: self)
            // self.passwordField.becomeFirstResponder()
            return false
        }
        if (newPasswordField.text == reTypePasswordField.text ){
            return true
        } else {
            self.themes.showAlert(title: "Alert", message: "Password Mismatched", sender: self)
            return false
        }
        return true
    }
    
    func changePasswordAPI(user_id: String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            
            let dict = ["user_id":user_id , "old_password": passwordField.text!, "new_password": newPasswordField.text!]
 
            print("change password parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:changePassword, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){

                    let alertController = UIAlertController(title: "Password Updated successfully", message: "Please Login in again", preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            print("Ok button tapped");
                    self.themes.ClearUserInfo()
                    let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                        self.navigationController?.pushViewController(Vc, animated: true)
                                }
                     alertController.addAction(OKAction)
                     self.present(alertController, animated: true, completion:nil)
                } 
                else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
}
