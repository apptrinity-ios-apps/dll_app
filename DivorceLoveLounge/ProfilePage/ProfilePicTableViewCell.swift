//
//  ProfilePicTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 3/18/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class ProfilePicTableViewCell: UITableViewCell {

    @IBOutlet weak var editBgView: UIView!
    @IBOutlet weak var profView: UIView!
    @IBOutlet weak var profImgView: UIImageView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.profView.layer.cornerRadius = (profView.frame.size.height)/2
        self.profView.layer.masksToBounds = true
        self.profImgView.layer.cornerRadius =  (profImgView.frame.size.height)/2
        self.addBtn.layer.cornerRadius =  (addBtn.frame.size.height)/2
        self.editBtn.layer.cornerRadius =  (editBtn.frame.size.height)/2
         self.editBgView.layer.cornerRadius =  (editBgView.frame.size.height)/2
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
