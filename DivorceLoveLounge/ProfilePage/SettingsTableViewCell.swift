//
//  SettingsTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 3/18/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var switchBtn: UISwitch!
    
    @IBOutlet weak var notifyLbl: UILabel!        
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
