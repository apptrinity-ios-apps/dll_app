//
//  ProfileVC.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 3/15/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
import AssetsLibrary
import Photos
import GoogleSignIn
import FBSDKLoginKit
import FBSDKCoreKit

class ProfileVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIActionSheetDelegate,GIDSignInDelegate,GIDSignInUIDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
    }

    var imageUrl = String()
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    
    var IOS_plan_Status = ""
    var messageCount = String()
    
    var shouldCellBeExpanded:Bool = false
    var indexOfExpendedCell:NSInteger = -1
    var informationDown = Bool()
    var locationDown = Bool()
    var settingDown = Bool()
    
    
   
    
    var profileImage = UIImage()
    let fixedArr = ["Email","First Name","Birthday","Gender","Ethnicity","Height","Occupation","Education","Phone Number","Smoke","Drink","Kids"]
    @IBOutlet weak var profileTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        informationDown = true
        locationDown = false
        settingDown = false
        
        
        
        self.profileTableView.register(UINib(nibName: "InfoTableViewCell", bundle: nil), forCellReuseIdentifier: "InfoTableViewCell")
        self.profileTableView.register(UINib(nibName: "PasswordTableViewCell", bundle: nil), forCellReuseIdentifier: "PasswordTableViewCell")
        self.profileTableView.register(UINib(nibName: "SettingsTableViewCell", bundle: nil), forCellReuseIdentifier: "SettingsTableViewCell")
        self.profileTableView.register(UINib(nibName: "DeleteTableViewCell", bundle: nil), forCellReuseIdentifier: "DeleteTableViewCell")
        self.profileTableView.register(UINib(nibName: "HeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "HeaderTableViewCell")
        self.profileTableView.register(UINib(nibName: "ProfilePicTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfilePicTableViewCell")
       
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        getprofileAPi()
        let usermail =  self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginUserName") as? String)
        let userpass =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginPassword") as? String)
      //  logIn(fromScreen: "", userEmail: usermail as! String, userPassword: userpass as! String)
         getUsermessagesCountsApi()
        
    }
    
    
    func logIn(fromScreen:String,userEmail:String ,userPassword:String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            let devicetoken =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "kDeviceToken") )
            let parameters = [ "email":userEmail, "password":userPassword,
                               "device_token":devicetoken as! String , "device_type" : "IOS"
                
                ] as [String : Any]
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:login, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    UserDefaults.standard.set(true, forKey: "checkLogIn")
                    // UserDefaults.standard.set(nil, forKey: "checkLogIn")
                    let userInfo = response["user_info"]as! [String : AnyObject]
                    let id = self.themes.checkNullValue(userInfo["id"])
                    let date_id = self.themes.checkNullValue(userInfo["date_id"])
                    
                    let gender = self.themes.checkNullValue(userInfo["gender"])
                    
                    let lookingfor = self.themes.checkNullValue(userInfo["looking_for"])
                    
                    let firstName = self.themes.checkNullValue(userInfo["first_name"])
                    
                    let email = self.themes.checkNullValue(userInfo["email"])
                    
                    let zipcode = self.themes.checkNullValue(userInfo["zipcode"])
                    
                    let state = self.themes.checkNullValue(userInfo["state"])
                    
                    let phNumber = self.themes.checkNullValue(userInfo["phone_number"])
                    
                    let email_notification = self.themes.checkNullValue(userInfo["email_notification_status"]) as! String
                    
                    let mTounge = self.themes.checkNullValue(userInfo["mtongue_name"]) as! String
                    let partner_mtongue_name = self.themes.checkNullValue(userInfo["partner_mtongue_name"]) as! String
                    
                    
                    
                    let push_notification = self.themes.checkNullValue(userInfo["push_notification_status"]) as! String
                    
                    
                    
                    
                    let referal_from = self.themes.checkNullValue(userInfo["referal_from"])
                    
                    let children = self.themes.checkNullValue(userInfo["children"])
                    
                    let city = self.themes.checkNullValue(userInfo["city"])
                    
                       let country = self.themes.checkNullValue(userInfo["country"])
                    
                    let dob1 = self.themes.checkNullValue(userInfo["dob"])
                    
                    
                    //   let dob = self.themes.dateFormateConverter(date: dob1 as! String)
                    
                    
                    
                    
                    let ethnicity = self.themes.checkNullValue(userInfo["ethnicity"])
                    
                    let religion = self.themes.checkNullValue(userInfo["religion"])
                    
                    let denomination = self.themes.checkNullValue(userInfo["denomination"])
                    
                    let education = self.themes.checkNullValue(userInfo["education"])
                    
                    let occupation = self.themes.checkNullValue(userInfo["occupation"])

                    let income = self.themes.checkNullValue(userInfo["income"])
                    
                    let smoke = self.themes.checkNullValue(userInfo["smoke"])
                    
                    let drink = self.themes.checkNullValue(userInfo["drink"])
                    
                    let heightFt = self.themes.checkNullValue(userInfo["height_ft"])as? String
                    let heightIn = self.themes.checkNullValue(userInfo["height_inc"])as? String
                    let heightCm = self.themes.checkNullValue(userInfo["height_cms"])as? String
                    let height = heightFt! + "'" + heightIn! + "''/" + heightCm!
                    
                    let passionate = self.themes.checkNullValue(userInfo["passionate"])
                    
                    let leisure_time = self.themes.checkNullValue(userInfo["leisure_time"])
                    
                    let thankful_1 = self.themes.checkNullValue(userInfo["thankful_1"])
                    
                    let thankful_2 = self.themes.checkNullValue(userInfo["thankful_2"])
                    
                    let thankful_3 = self.themes.checkNullValue(userInfo["thankful_3"])
                    
                    
                    
                    
                    
                    let partner_age_min = self.themes.checkNullValue(userInfo["partner_age_min"])
                    
                    let partner_age_max = self.themes.checkNullValue(userInfo["partner_age_max"])
                    
                    let partner_match_distance = self.themes.checkNullValue(userInfo["partner_match_distance"])
                    
                    let partner_match_search = self.themes.checkNullValue(userInfo["partner_match_search"])
                    
                    let partner_ethnicity = self.themes.checkNullValue(userInfo["partner_ethnicity"])
                    
                    let partner_religion = self.themes.checkNullValue(userInfo["partner_religion"])
                    
                    let partner_education = self.themes.checkNullValue(userInfo["partner_education"])
                    
                    let partner_occupation = self.themes.checkNullValue(userInfo["partner_occupation"])
                    
                    let partner_smoke = self.themes.checkNullValue(userInfo["partner_smoke"])
                    
                    let partner_denomination = self.themes.checkNullValue(userInfo["partner_denomination"])
                    
                    
                    
                    
                    
                    
                    
                    let partner_drink = self.themes.checkNullValue(userInfo["partner_drink"])
                    
                    let photo1 = self.themes.checkNullValue(userInfo["photo1"])
                    
                    let photo1_approve = self.themes.checkNullValue(userInfo["photo1_approve"])
                    
                    let photo2 = self.themes.checkNullValue(userInfo["photo2"])
                    
                    let photo2_approve = self.themes.checkNullValue(userInfo["photo2_approve"])
                    
                    let photo3 = self.themes.checkNullValue(userInfo["photo3"])
                    UserDefaults.standard.set(self.themes.checkNullValue(userInfo["state"]), forKey: "selectedState")
                    
                    
                    
                    
                    let photo3_approve = self.themes.checkNullValue(userInfo["photo3_approve"])
                    
                    let photo4 = self.themes.checkNullValue(userInfo["photo4"])
                    
                    let photo4_approve = self.themes.checkNullValue(userInfo["photo4_approve"])
                    
                    
                    
                    let photo5 = self.themes.checkNullValue(userInfo["photo5"])
                    
                    let photo5_approve = self.themes.checkNullValue(userInfo["photo5_approve"])
                    
                    let photo6 = self.themes.checkNullValue(userInfo["photo6"])
                    
                    let photo6_aprove = self.themes.checkNullValue(userInfo["photo6_aprove"])
                    
                    let signup_step = self.themes.checkNullValue(userInfo["signup_step"])
                    
                    let status = self.themes.checkNullValue(userInfo["status"])
                    
                    
                    
                    
                    
                    
                    
                    self.themes.saveUserID(id as? String)
                    
                    self.themes.savedate_id(date_id as? String)
                    
                    self.themes.saveGender(gender as? String)
                    
                    self.themes.savelooking_for(lookingfor as? String)
                    
                    self.themes.saveFirstName(firstName as? String)
                    
                    self.themes.saveuserEmail(email as? String)
                    
                    self.themes.saveEmailNotification(email_notification)
                    
                    self.themes.savePushNotification(push_notification)
                    
                    
                   //  self.themes.savePushNotification(mTounge)
                    // self.themes.savePushNotification(partner_mtongue_name)
                    
                    
                    
                    self.themes.saveZipcode(zipcode as? String)
                    
                    self.themes.savestate(state as? String)
                    self.themes.saveMobileNumber(phNumber as? String)
                    
                    self.themes.savereferal_from(referal_from as? String)
                    
                    self.themes.saveChildren(children as? String)
                    
                    self.themes.savecity(city as? String)
                    
                    self.themes.savecountry(country as? String)
                    
                    
                    
                    
                    self.themes.savedob(dob1 as? String)
                    
                    self.themes.saveEthinicity(ethnicity as? String)
                    
                    self.themes.saveReligion(religion as? String)
                    
                    self.themes.savedenomination(denomination as? String)
                    
                    self.themes.saveEducation(education as? String)
                    
                    self.themes.saveoccupation(occupation as? String)
                    
                    self.themes.saveIncome(income as? String)
                    
                    self.themes.savesmoke(smoke as? String)
                    self.themes.savePartnerDenomination(partner_denomination as? String)
                    self.themes.savedrink(drink as? String)
                    
                    self.themes.saveHeight(height as? String)
                    
                    self.themes.savepassionate(passionate as? String)
                    
                    self.themes.saveleisure_time(leisure_time as? String)
                    
                    self.themes.savethankful_1(thankful_1 as? String)
                    
                    self.themes.savethankful_2(thankful_2 as? String)
                    
                    self.themes.savethankful_3(thankful_3 as? String)
                    
                    self.themes.savepartner_age_max(partner_age_max as? String)
                    
                    self.themes.savepartner_age_min(partner_age_min as? String)
                    
                    self.themes.savepartner_match_distance(partner_match_distance as? String)
                    
                    self.themes.savepartner_match_search(partner_match_search as? String)
                    
                    self.themes.savepartner_ethnicity(partner_ethnicity as? String)
                    
                    self.themes.savepartner_religion(partner_religion as? String)
                    
                    self.themes.savepartner_education(partner_education as? String)
                    
                    self.themes.savepartner_occupation(partner_occupation as? String)
                    
                    self.themes.savepartner_smoke(partner_smoke as? String)
                    
                    self.themes.savepartner_drink(partner_drink as? String)
                    
                    self.themes.savephoto1(photo1 as? String)
                    
                    self.themes.savephoto2(photo2 as? String)
                    
                    self.themes.savephoto3(photo3 as? String)
                    
                    self.themes.savephoto4(photo4 as? String)
                    
                    self.themes.savephoto5(photo5 as? String)
                    
                    self.themes.savephoto6(photo6 as? String)
                    
                    self.themes.savephoto1_approve(photo1_approve as? String)
                    
                    self.themes.savephoto2_approve(photo2_approve as? String)
                    
                    self.themes.savephoto3_approve(photo3_approve as? String)
                    self.themes.savephoto4_approve(photo4_approve as? String)
                    self.themes.savephoto5_approve(photo5_approve as? String)
                    self.themes.savephoto6_aprove(photo6_aprove as? String)
                    self.themes.savesignup_step(signup_step as? String)
                    
                    
                    
                    self.themes.savestatus(status as? String)
                    
                } else if success == "3"{
                    
                    self.themes.showAlert(title: "Oops ☹️", message: "The account you are Trying to view has been suspended. \n Please Contact Customer Support \n support@divorcelovelounge.com", sender: self)
                    
                } else {
                    
                   
                    
                }
                
            }
            
        } else {
            
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
            
        }
        
    }
    

    func getprofileAPi(){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:getProfilePics, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    
                    
                    let photo = response["photos"] as! [String:AnyObject]
                    
                    self.imageUrl =  self.themes.checkNullValue(photo["photo1"] as AnyObject) as! String
                    
                    
                    
                 self.IOS_plan_Status = self.themes.checkNullValue(photo["ios_plan_status"] as AnyObject) as! String
                    self.profileTableView.reloadData()
                    
                } else {
                    let result = response["result"] as! String
                   // self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    
    func getUsermessagesCountsApi(){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:getUsermessagesCounts, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    
                    self.messageCount = response["totalcount"] as! String
                    
                   
                    self.profileTableView.reloadData()
                    
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    
    
    
    
    func logOutAPI(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
           // themes.showActivityIndicator(uiView: self.view)
             let userid = self.themes.getUserId()
             let devicetoken =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "kDeviceToken") )
            let dict = ["user_id":userid, "device_token":devicetoken as! String ] as [String:Any]

            print("logout parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:signOut, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
               // self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){

                 print("Success")
                }
                
            }
        } else {
           // self.themes.hideActivityIndicator(uiView: self.view)
           // themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    
    func matchPreferanceUpadate(zipcode: String,state: String,city: String,ethnicity: String,religion: String,denomination: String,education: String,income: String,smoke: String,drink: String,passionate: String,leisure_time: String,occupation: String,partner_age_min: String,partner_age_max: String,partner_match_distance: String,partner_ethnicity: String,partner_denomination: String,partner_education: String,partner_smoke: String,partner_religion: String,partner_drink: String,partner_occupation: String,country:String,matchSearch:String,push_notification: String,email_notification:String){
        
        
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            var dict = [String : AnyObject]()
            dict.updateValue(country as AnyObject, forKey: "country")
            dict.updateValue(themes.getUserId() as AnyObject, forKey: "user_id")
            dict.updateValue(zipcode as AnyObject, forKey: "zipcode")
            dict.updateValue(state as AnyObject, forKey: "state")
            dict.updateValue("" as AnyObject, forKey: "dob")
            dict.updateValue(city as AnyObject, forKey: "city")
            dict.updateValue(ethnicity as AnyObject, forKey: "ethnicity")
            dict.updateValue(religion as AnyObject, forKey: "religion")
            dict.updateValue(denomination as AnyObject, forKey: "mtongue_name")
            dict.updateValue(education as AnyObject, forKey: "education")
            dict.updateValue(income as AnyObject, forKey: "income")
            dict.updateValue(smoke as AnyObject, forKey: "smoke")
            dict.updateValue(drink as AnyObject, forKey: "drink")
            dict.updateValue("" as AnyObject, forKey: "height_ft")
            dict.updateValue("" as AnyObject, forKey: "height_cms")
            dict.updateValue("" as AnyObject, forKey: "height_inc")
            dict.updateValue(passionate as AnyObject, forKey: "passionate")
            dict.updateValue(leisure_time as AnyObject, forKey: "leisure_time")
            dict.updateValue("" as AnyObject, forKey: "thankful_1")
            dict.updateValue("" as AnyObject, forKey: "thankful_2")
            dict.updateValue("" as AnyObject, forKey: "thankful_3")
            dict.updateValue(occupation as AnyObject, forKey: "occupation")
            dict.updateValue(partner_age_min as AnyObject, forKey: "partner_age_min")
            dict.updateValue(partner_age_max as AnyObject, forKey: "partner_age_max")
            dict.updateValue(partner_match_distance as AnyObject, forKey: "partner_match_distance")
            dict.updateValue(partner_ethnicity as AnyObject, forKey: "partner_ethnicity")
            dict.updateValue(partner_religion as AnyObject, forKey: "partner_religion")
            dict.updateValue(partner_denomination as AnyObject, forKey: "partner_mtongue_name")
            dict.updateValue(partner_education as AnyObject, forKey: "partner_education")
            dict.updateValue(partner_occupation as AnyObject, forKey: "partner_occupation")
            dict.updateValue(partner_smoke as AnyObject, forKey: "partner_smoke")
            dict.updateValue(partner_drink as AnyObject, forKey: "partner_drink")
            dict.updateValue(matchSearch as AnyObject, forKey: "partner_match_search")
            dict.updateValue(push_notification as AnyObject, forKey: "push_notification_status")
            dict.updateValue(email_notification as AnyObject, forKey: "email_notification_status")
            print("log in parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:updatematchPreference, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    
                    
                    
                    
                    self.themes.savePushNotification(push_notification)
                    self.themes.saveEmailNotification(email_notification)
                    
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    

    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 16
    }
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return 0
//    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilePicTableViewCell")as! ProfilePicTableViewCell
             cell.addBtn.addTarget(self, action: #selector(addImageBtnClicked(button:)), for:.touchUpInside)
            cell.editBtn.addTarget(self, action: #selector(editBtnClicked(button:)), for:.touchUpInside)
            let imageBaseUrl = ImagebaseUrl + imageUrl
            
             cell.profImgView.sd_setImage(with: URL(string: imageBaseUrl), placeholderImage: UIImage(named: "defaultPic"))
            return cell
        } else if section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell")as! HeaderTableViewCell
            cell.messageCountLabel.isHidden = true
            if(informationDown){
                cell.dropDwnImg.image = UIImage(named: "orangeUpArraow")
                
            }else{
                cell.dropDwnImg.image = UIImage(named: "orengeDownArrow")
            }
          cell.headerLabel.text = "Information"
            cell.headerBtn.tag = section
        cell.headerBtn.addTarget(self, action: #selector(infoBtnClick(button:)), for:.touchUpInside)
            return cell
        }
        else if section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell")as! HeaderTableViewCell
             cell.messageCountLabel.isHidden = true
            if(locationDown){
                cell.dropDwnImg.image = UIImage(named: "orangeUpArraow")
                
            }else{
                cell.dropDwnImg.image = UIImage(named: "orengeDownArrow")
            }
            cell.headerLabel.text = "Location"
            cell.headerBtn.tag = section
            cell.headerBtn.addTarget(self, action: #selector(locationBtnClicked(button:)), for:.touchUpInside)
            return cell
            
        }else if section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell")as! HeaderTableViewCell
             cell.messageCountLabel.isHidden = true
            cell.dropDwnImg.image = UIImage(named: "right-arrow-black")
            cell.headerLabel.text = "Online"
            cell.headerBtn.addTarget(self, action: #selector(onlineListBtnClicked(button:)), for:.touchUpInside)
            return cell
        }else if section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell")as! HeaderTableViewCell
             cell.messageCountLabel.isHidden = true
            cell.dropDwnImg.image = UIImage(named: "right-arrow-black")
            cell.headerLabel.text = "Quiz"
            cell.headerBtn.addTarget(self, action: #selector(quizBtnClicked(button:)), for:.touchUpInside)
            return cell
        }else if section == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell")as! HeaderTableViewCell
            
            cell.dropDwnImg.image = UIImage(named: "right-arrow-black")
            cell.headerLabel.text = "Messages"
            
            if(messageCount == "" || messageCount == "0"){
                 cell.messageCountLabel.isHidden = true
            }else{
                 cell.messageCountLabel.isHidden = false
            }
            cell.messageCountLabel.text = String(messageCount)
            cell.headerBtn.addTarget(self, action: #selector(messageBtnClicked(button:)), for:.touchUpInside)
            return cell
        }else if section == 6 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell")as! HeaderTableViewCell
             cell.messageCountLabel.isHidden = true
            cell.dropDwnImg.image = UIImage(named: "right-arrow-black")
            cell.headerLabel.text = "Contacted Users"
            cell.headerBtn.addTarget(self, action: #selector(contactListBtnClicked(button:)), for:.touchUpInside)
            return cell
        } else if section == 7 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell")as! HeaderTableViewCell
             cell.messageCountLabel.isHidden = true
            cell.dropDwnImg.image = UIImage(named: "right-arrow-black")
            cell.headerLabel.text = "Blocked Users"
            cell.headerBtn.addTarget(self, action: #selector(blockedProfileBtnClicked(button:)), for:.touchUpInside)
            return cell
        }
        else if section == 8 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell")as! HeaderTableViewCell

            cell.dropDwnImg.image = UIImage(named: "right-arrow-black")
            cell.headerLabel.text = "My Membership Plan"
            cell.messageCountLabel.isHidden = true
            cell.headerBtn.addTarget(self, action: #selector(myPlanBtnClicked(button:)), for:.touchUpInside)
            return cell

        }else if section == 9 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell")as! HeaderTableViewCell

                cell.dropDwnImg.image = UIImage(named: "right-arrow-black")
            cell.headerLabel.text = "Upgrade Plan"
            cell.messageCountLabel.isHidden = true

            cell.headerBtn.addTarget(self, action: #selector(membershipBtnClicked(button:)), for:.touchUpInside)
            return cell

        }
        
        else if section == 10 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell")as! HeaderTableViewCell
             cell.messageCountLabel.isHidden = true
            cell.dropDwnImg.image = UIImage(named: "right-arrow-black")
            cell.headerLabel.text = "Change Password"
            cell.headerBtn.addTarget(self, action: #selector(changePasswordBtnClicked(button:)), for:.touchUpInside)
            return cell
        } else if section == 11 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell")as! HeaderTableViewCell
             cell.messageCountLabel.isHidden = true
            cell.dropDwnImg.image = UIImage(named: "right-arrow-black")
            cell.headerLabel.text = "Support"
            cell.headerBtn.addTarget(self, action: #selector(contactUSBtnClicked(button:)), for:.touchUpInside)
            return cell
        }
        else if section == 12 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell")as! HeaderTableViewCell
             cell.messageCountLabel.isHidden = true
            cell.dropDwnImg.image = UIImage(named: "right-arrow-black")
            cell.headerLabel.text = "Privacy Policy"
            cell.headerBtn.addTarget(self, action: #selector(privacyBtnClicked(button:)), for:.touchUpInside)
            return cell
        }
        else if section == 13 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell")as! HeaderTableViewCell
             cell.messageCountLabel.isHidden = true
            cell.dropDwnImg.image = UIImage(named: "right-arrow-black")
            cell.headerLabel.text = "Terms of Use"
            cell.headerBtn.addTarget(self, action: #selector(termsBtnClicked(button:)), for:.touchUpInside)
            return cell
        }
            
    else if section == 14 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell")as! HeaderTableViewCell
             cell.messageCountLabel.isHidden = true
            if(settingDown){
                cell.dropDwnImg.image = UIImage(named: "orangeUpArraow")
                
            }else{
                cell.dropDwnImg.image = UIImage(named: "orengeDownArrow")
            }
            cell.headerLabel.text = "Settings"
            cell.headerBtn.addTarget(self, action: #selector(settingsBtnClicked(button:)), for:.touchUpInside)
            return cell
            
        }
        
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeleteTableViewCell")as! DeleteTableViewCell
            cell.deleteBtn.addTarget(self, action: #selector(deleteBtnAction(button:)), for:.touchUpInside)
            
            return cell
        }
    
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        } else if section == 1 {
            return fixedArr.count
        }
        else if section == 2 {
            return 2
        }  else if section == 3 {
            return 0
        }else if section == 4 {
            return 0
        }else if section == 5 {
            return 0
        }else if section == 6 {
            return 0
        }else if section == 7 {
            return 0
        }else if section == 8 {
            return 0
        }else if section == 9 {
            return 0
        }else if section == 10 {
            return 0
        }else if section == 11 {
            return 0
        }else if section == 12 {
            return 0
        }
        else if section == 13 {
            return 0
        }else if section == 14 {
            return 2
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 1 {
            
            if(informationDown){
                let cell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell", for: indexPath)as! InfoTableViewCell
                if(indexPath.row == 0){
                    cell.dataLabel.text = themes.getuserMail()
                }else  if(indexPath.row == 1){
                    cell.dataLabel.text = themes.getFirstName()
                }else  if(indexPath.row == 2){
                    
                let date = themes.checkNullValue(themes.getdob()) as! String
                    
 
                  //  cell.dataLabel.text = date
                    if (date == "") {
                        print("no date of birth")
                    } else {
                        let characters = Array(date)
                        
                        let arr = characters[2]
                        
                        if(arr == "-" || arr == "/"){
                            cell.dataLabel.text = date
                        }else{
                            let dob = themes.dateFormateConverter(date: date )
                            cell.dataLabel.text = dob
                        }
  
                        
                    }
//                    cell.dataLabel.text = themes.getdob()
//                }else  if(indexPath.row == 3){
//                    cell.dataLabel.text = themes.getstatus()
                }else  if(indexPath.row == 3){
                    cell.dataLabel.text = themes.getGender()
                }else  if(indexPath.row == 4){
                    cell.dataLabel.text = themes.getEthinicity()
                }else  if(indexPath.row == 5){
                    cell.dataLabel.text = themes.getHeight()
                }else  if(indexPath.row == 6){
                    cell.dataLabel.text = themes.getoccupation()
                }else  if(indexPath.row == 7){
                    cell.dataLabel.text = themes.geteducationr()
                }else  if(indexPath.row == 8){
                    cell.dataLabel.text = themes.getmobileNumber()
//                }else  if(indexPath.row == 10){
//                    cell.dataLabel.text = ""
                }else  if(indexPath.row == 9){
                    cell.dataLabel.text = themes.getsmoke()
                }else  if(indexPath.row == 10){
                    cell.dataLabel.text = themes.getdrink()
                }else  if(indexPath.row == 11){
                    cell.dataLabel.text = themes.getChildren()
                }
                cell.selectionStyle = .none
                cell.fixedLbl.text = fixedArr[indexPath.row]
                
                return cell
            }else{
                return UITableViewCell()
            }
            
        } else if indexPath.section == 2 {
            
            if(locationDown){
                let cell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell", for: indexPath)as! InfoTableViewCell
                if indexPath.row == 0 {
                    cell.selectionStyle = .none
                    
                    cell.dataLabel.text = themes.getstate()
                    cell.fixedLbl.text = "State"
                }else if indexPath.row == 1 {
                    cell.selectionStyle = .none
                    cell.dataLabel.text = themes.getcountry()
                    cell.fixedLbl.text = "Country"
                }
                
                return cell
            }else{
                return UITableViewCell()
            }
           
        }
//        else if indexPath.section == 3 {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "PasswordTableViewCell", for: indexPath)as! PasswordTableViewCell
//             cell.selectionStyle = .none
//            if indexPath.row == 0 {
//            cell.pswrdLbl.text = "New Password"
//            return cell
//            } else {
//                cell.pswrdLbl.text = "Retype Password"
//                return cell
//            }
//        }
        else if indexPath.section == 14 {
            if(settingDown){
                let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell", for: indexPath)as! SettingsTableViewCell
                cell.selectionStyle = .none
                if indexPath.row == 0 {
                    cell.notifyLbl.text = "Send Push Notifications"
                    cell.switchBtn.tag = indexPath.row
                    cell.switchBtn.addTarget(self, action: #selector(pushBtnClicked(button:)), for: .touchUpInside)
                    let pushNotf = themes.getPushNotification()
                    if pushNotf == "Yes" {
                        cell.switchBtn.isOn = true
                    } else {
                        cell.switchBtn.isOn = false
                    }
                    return cell
                }
                    //                else if indexPath.row == 1{
                    //                    cell.notifyLbl.text = "Refresh Automatically"
                    //                    return cell
                    //                }
                else {
                    cell.notifyLbl.text = "Send Email Notifications"
                    cell.switchBtn.tag = indexPath.row
                    cell.switchBtn.addTarget(self, action: #selector(emailBtnClicked(button:)), for: .touchUpInside)
                    let emailNotf = themes.getEmailNotification() as! String
                    if emailNotf == "Yes" {
                        cell.switchBtn.isOn = true
                    } else {
                        cell.switchBtn.isOn = false
                    }
                    return cell
                }
            }else{
                return UITableViewCell()
            }
           
        } else {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeleteTableViewCell", for: indexPath)as! DeleteTableViewCell
             cell.selectionStyle = .none
        return cell
       }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        var rowHeight:CGFloat = 0.0
        if section == 0 {
            return 280
        } else if section == 1 {
           return 44
        } else if section == 2 {
            return 44
        }
        else if section == 3 {
            return 44
        }
        else if section == 4 {
            return 44
        }
        else if section == 5 {
            return 44
        }
        else if section == 6 {
            return 44
        }
        else if section == 7 {
            return 44
        }
        else if section == 8 {
            return 44
        }
        else if section == 9 {
            return 44
        }
        else if section == 10 {
            return 44
        }
        else if section == 11 {
            return 44
        }else if section == 12 {
            return 44
        }
        else if section == 13 {
            return 44
        }
        else if section == 14 {
            return 44
        }
        else {
            return 94
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       if indexPath.section == 1 {
        if informationDown {
            return 44
        }
        else {
            return 0
            
        }
        } else if indexPath.section == 2 {
        if locationDown {
            return 44
        }
        else {
            return 0
            
        }
        } else if settingDown {
                return 50
        } else {
           return 0
        }        
    }
    
    
    @objc func pushBtnClicked(button: UISwitch) {
        
        if button.isOn {
            print("button Yes")
            let emailNotify = self.themes.getEmailNotification() as! String
            
            self.matchPreferanceUpadate(zipcode: "", state: "", city: "", ethnicity: "", religion: "", denomination: "", education: "", income: "", smoke: "", drink: "", passionate: "", leisure_time: "", occupation: "", partner_age_min: "", partner_age_max: "", partner_match_distance: "", partner_ethnicity: "", partner_denomination: "", partner_education: "", partner_smoke: "", partner_religion: "", partner_drink: "", partner_occupation: "", country: "", matchSearch: "", push_notification: "Yes", email_notification: emailNotify)
            
        } else {
            print("button No")
            let emailNotify = self.themes.getEmailNotification() as! String
            
            self.matchPreferanceUpadate(zipcode: "", state: "", city: "", ethnicity: "", religion: "", denomination: "", education: "", income: "", smoke: "", drink: "", passionate: "", leisure_time: "", occupation: "", partner_age_min: "", partner_age_max: "", partner_match_distance: "", partner_ethnicity: "", partner_denomination: "", partner_education: "", partner_smoke: "", partner_religion: "", partner_drink: "", partner_occupation: "", country: "", matchSearch: "", push_notification: "No", email_notification: emailNotify)
        }
        
    }
    
    @objc func emailBtnClicked(button: UISwitch) {
        
        if button.isOn {
            print("button Yes")
            let pushNotf = themes.getPushNotification()
            self.matchPreferanceUpadate(zipcode: "", state: "", city: "", ethnicity: "", religion: "", denomination: "", education: "", income: "", smoke: "", drink: "", passionate: "", leisure_time: "", occupation: "", partner_age_min: "", partner_age_max: "", partner_match_distance: "", partner_ethnicity: "", partner_denomination: "", partner_education: "", partner_smoke: "", partner_religion: "", partner_drink: "", partner_occupation: "", country: "", matchSearch: "", push_notification: pushNotf, email_notification: "Yes")
            
            
        } else {
            print("button No")
            
            
            let pushNotf = themes.getPushNotification()
            self.matchPreferanceUpadate(zipcode: "", state: "", city: "", ethnicity: "", religion: "", denomination: "", education: "", income: "", smoke: "", drink: "", passionate: "", leisure_time: "", occupation: "", partner_age_min: "", partner_age_max: "", partner_match_distance: "", partner_ethnicity: "", partner_denomination: "", partner_education: "", partner_smoke: "", partner_religion: "", partner_drink: "", partner_occupation: "", country: "", matchSearch: "", push_notification: pushNotf, email_notification: "No")
        }
    }
    
    
    
    @objc func addImageBtnClicked(button: UIButton) {
       
        
//        let actionSheet = UIActionSheet(title: "Choose Option", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera","Gallery")
//        actionSheet.show(in: self.view)
        
        let editVc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfilePicVC") as! EditProfilePicVC
      
        self.navigationController?.pushViewController(editVc, animated: true)
    }
    @objc func editBtnClicked(button: UIButton) {
        
        let editMatchVc = self.storyboard?.instantiateViewController(withIdentifier: "MatchPreferViewController") as! MatchPreferViewController
        
        self.navigationController?.pushViewController(editMatchVc, animated: true)
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        switch (buttonIndex){
        case 1:
           cameraClicked()
        case 2:
            galleryClicked()
        default:
            print("Default")
        }
    }
    
   
    
    func cameraClicked(){
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized
        {
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerControllerSourceType.camera
            image.allowsEditing = false
            self.present(image, animated: true)
        }
        else
        {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted :Bool) -> Void in
                if granted == true
                {
                    let image = UIImagePickerController()
                    image.delegate = self
                    image.sourceType = UIImagePickerControllerSourceType.camera
                    image.allowsEditing = false
                    
                    self.present(image, animated: true)
                }
                else
                {
                    print("User Rejected")
                    let alertController = UIAlertController(title: "This app does not have access to your camera.",
                                                            message: "Turn on your Camera from the settings to help Access your camera",
                                                            preferredStyle: .alert)
                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alertAction) in
                        if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(appSettings as URL)
                            } else {
                                // Fallback on earlier versions
                            }
                        }
                    }
                    alertController.addAction(settingsAction)
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                    alertController.addAction(cancelAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            });
        }
        
        
    }
    
    func galleryClicked(){
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            // Access has been granted.
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerControllerSourceType.photoLibrary
            image.allowsEditing = false
            
            self.present(image, animated: true)
        }
        else if (status == PHAuthorizationStatus.denied) {
            // Access has been denied.
            print("alert")
            let alertController = UIAlertController(title: "This app does not have access to your Photos.",
                                                    message: "Turn on your Photos from the settings to help Access your Gallery",
                                                    preferredStyle: .alert)
            let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alertAction) in
                if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(appSettings as URL)
                    } else {
                        // Fallback on earlier versions
                    }
                }
            }
            alertController.addAction(settingsAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
            
        else if (status == PHAuthorizationStatus.notDetermined) {
            
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                
                if (newStatus == PHAuthorizationStatus.authorized) {
                    
                }
                    
                else {
                    
                }
            })
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image1 = info[UIImagePickerControllerOriginalImage] as? UIImage
            
        {

           profileImage = image1
            self.dismiss(animated: true, completion: nil)
            profileTableView.reloadData()
            print("stauscode data converting")
        }
    }
    @objc func infoBtnClick(button: UIButton) {
        shouldCellBeExpanded = !shouldCellBeExpanded
        indexOfExpendedCell = button.tag
        if informationDown {
           informationDown = false
           
        }
        else {
           informationDown = true
            locationDown = false
            settingDown = false
        }
        profileTableView.reloadData()
    }
     @objc func quizBtnClicked(button:UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QuizzEditVC") as! QuizzEditVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func messageBtnClicked(button:UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        let vc = storyboard.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
//        vc.screenFrom = "Contact"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func onlineListBtnClicked(button:UIButton) {
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           
           let vc = storyboard.instantiateViewController(withIdentifier: "BlockedUsersVC") as! BlockedUsersVC
            vc.screenFrom = "Online"
           self.navigationController?.pushViewController(vc, animated: true)
       }
    
     @objc func contactListBtnClicked(button:UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "BlockedUsersVC") as! BlockedUsersVC
         vc.screenFrom = "Contact"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func blockedProfileBtnClicked(button:UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "BlockedUsersVC") as! BlockedUsersVC
        vc.screenFrom = "Block"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @objc func locationBtnClicked(button:UIButton) {
        
        shouldCellBeExpanded = !shouldCellBeExpanded
        indexOfExpendedCell = button.tag
        
        if locationDown {
            locationDown = false
            
        }
        else {
            locationDown = true
            informationDown = false
            settingDown = false
        }
        profileTableView.reloadData()
    }
    @objc func passwordBtnClicked(button:UIButton) {
        
    }
    
    
    @objc func privacyBtnClicked(button:UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        vc.fromScreen = "Privacy"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func termsBtnClicked(button:UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        vc.fromScreen = "Terms"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func settingsBtnClicked(button:UIButton) {
        if settingDown {
             settingDown = false
             informationDown = false
             locationDown = false
        }
        else {
            settingDown = true
            informationDown = false
            locationDown = false
        }
        profileTableView.reloadData()
    }
    
    @objc func deleteBtnAction(button:UIButton) {
        
        logOutAPI()
        
       self.themes.ClearUserInfo()
        GIDSignIn.sharedInstance().delegate=self
        GIDSignIn.sharedInstance().uiDelegate=self
        GIDSignIn.sharedInstance()?.signOut()
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logOut()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func contactUSBtnClicked(button:UIButton) {
        
        
        let userDetailsVc = self.storyboard!.instantiateViewController(withIdentifier: "SupportVC") as! SupportVC
        self.navigationController?.pushViewController(userDetailsVc, animated: true)
 
    }
    
    
    @objc func myPlanBtnClicked(button:UIButton) {
        
        
        

        if self.themes.getplanStatus() == "1"{

            let userDetailsVc = self.storyboard!.instantiateViewController(withIdentifier: "myPlanVC") as! myPlanVC
            self.navigationController?.pushViewController(userDetailsVc, animated: true)
        }else{
            let userDetailsVc = self.storyboard!.instantiateViewController(withIdentifier: "NewMemberShipPlanVC") as! NewMemberShipPlanVC
            self.navigationController?.pushViewController(userDetailsVc, animated: true)
        }
        
       
        
    }
    
    
    @objc func membershipBtnClicked(button:UIButton) {
        
        

        if self.IOS_plan_Status == "1"{

            self.themes.showAlert(title: "Alert", message: "This Functionality will be implemented soon", sender: self)

        }else{
            let userDetailsVc = self.storyboard!.instantiateViewController(withIdentifier: "NewMemberShipPlanVC") as! NewMemberShipPlanVC
            self.navigationController?.pushViewController(userDetailsVc, animated: true)
        }
        
        
        
       
        
    }
    @objc func changePasswordBtnClicked(button:UIButton) {
        
        let Vc = self.storyboard!.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        self.navigationController?.pushViewController(Vc, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
