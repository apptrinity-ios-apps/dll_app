//
//  HeaderTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 3/18/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var headerLabel: UILabel!    
    @IBOutlet weak var headerBtn: UIButton!
    @IBOutlet weak var dropDwnImg: UIImageView!
    
    @IBOutlet var messageCountLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        messageCountLabel.layer.cornerRadius = messageCountLabel.frame.size.height/2
        messageCountLabel.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
//    func setExpanded() {
//        dropDwnImg.image = UIImage(named: "Drop_down")
//    }
//    
//    func setCollapsed() {
//         dropDwnImg.image = UIImage(named: "star (1)")
//    }
}
