//
//  ParkPopUpCell.swift
//  BI-IL.GOV
//
//  Created by INDOBYTES on 04/07/18.
//  Copyright © 2018 Indobytes. All rights reserved.
//

import UIKit

class ParkPopUpCell: UITableViewCell {

    @IBOutlet var priceWidth: NSLayoutConstraint!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var imageview: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
