//
//  InfoTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 3/18/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
class InfoTableViewCell: UITableViewCell {
    @IBOutlet weak var fixedLbl: UILabel!
    @IBOutlet weak var dataLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        fixedLbl.intrinsicContentSize.width
        dataLabel.intrinsicContentSize.width
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
