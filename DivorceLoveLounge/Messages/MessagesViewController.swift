//
//  MessagesViewController.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 15/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//
import UIKit
class MessagesViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,backBtnClickDelegate1,backBtnClickDelegate3 {
    
    
    func backAction(from:String){
        if(from == "inbox"){
            inboxList_API(type: "block")
           
        }else if(from == "sent"){
            sentList_API(type: "block")
            
           
        }else if(from == "important"){
            importantList_API(type: "block")
            
        }else if(from == "trash"){
            
            trashList_API(type: "block")
            
        }
    }
    var inboxMessageCount = String()
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    let topHeadingArray = ["Inbox","Sent","Important","Trash"]
    var namesArr = ["WHITE","HISPANIC/LATINO","BLACK/AFRICAN DECENT","ASIAN/PACIFIC ISLANDER","INDIAN","CHINES","NATIVE AMERICA","ARABIC/MIDDLE EASTREN","KOREAN","JAPANESE","OTHERS"]
    
    struct messageList {
        let date_id: String
        let first_name: String
        let msg_content: String
        let msg_date: String
        let msg_from: String
        let msg_id: String
        let msg_important_receiver: String
        let msg_important_sender: String
        let msg_read_status: String
        let msg_status: String
        let msg_to: String
        let photo1: String
        let photo1_approve: String
        let trash_receiver: String
        let trash_sender: String
        let totalcount:String
    }
    var inboxListArr : [messageList] = []
    var filteInboxArr : [messageList] = []
    var inboxListNameArray = [AnyObject]()
    var filterInboxListNameArray = [String]()
    var inboxList1Arr : [messageList] = []
     var sent1Arr : [messageList] = []
    var important1Arr : [messageList] = []
    var trash1Arr : [messageList] = []
    var filterInboxList1Arr : [messageList] = []
    var filtersent1Arr : [messageList] = []
    var filterimportant1Arr : [messageList] = []
    var filtertrash1Arr : [messageList] = []
  
    var selectedIndex = Int()
    var starTag = Int()
    var starValue = Bool()
    var fromPushNotify = ""
    var screenFrom = ""

    var adsListArr = [AnyObject]()
    
    
    @IBOutlet var topCollectionView: UICollectionView!
    @IBOutlet var searchView: UIView!
    @IBOutlet var messageTableView: UITableView!
    @IBOutlet var searchField: UITextField!
    
    @IBOutlet var topBarHeight: NSLayoutConstraint!
    
    
    @IBAction func backAction(_ sender: Any) {
        
        if fromPushNotify == "Push" {
            let mainVc = storyboard?.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
            self.navigationController?.pushViewController(mainVc, animated: true)
        } else {
            self.navigationController?.popViewController(animated: false)
        }
    }
    @IBAction func composeBtnAction(_ sender: Any) {
        
        let composeVc = self.storyboard?.instantiateViewController(withIdentifier: "ComposeVC") as! ComposeVC
        
        if(selectedIndex == 0){
            composeVc.msgType = "inbox"
        }else if(selectedIndex == 1){
            composeVc.msgType = "sent"
        }else if(selectedIndex == 2){
            composeVc.msgType = "important"
        }else if(selectedIndex == 3){
            composeVc.msgType = "trash"
        }
        composeVc.delegate = self
        self.navigationController?.pushViewController(composeVc, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if screenFrom == "Tab"{
            
           self.topBarHeight.constant = 0
        } else {
            
            self.topBarHeight.constant = 55
        }
        
        searchField.autocapitalizationType = .sentences
        selectedIndex = 0
        searchField.delegate = self
        searchView.layer.shadowOffset = CGSize(width:0, height: 2)
        searchView.layer.shadowOpacity = 2.0
        searchView.layer.shadowRadius = 2
        searchView.layer.masksToBounds = false
        searchView.layer.shadowColor = UIColor.gray.cgColor

        topCollectionView.delegate = self
        topCollectionView.dataSource = self
        messageTableView.delegate = self
        messageTableView.dataSource = self
        
        self.topCollectionView.register(UINib(nibName: "topCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "topCollectionViewCell")
        self.messageTableView.register(UINib(nibName: "MessageCell", bundle: nil), forCellReuseIdentifier: "MessageCell")
        
         self.messageTableView.register(UINib(nibName: "AdsBannerTableCell", bundle: nil), forCellReuseIdentifier: "AdsBannerTableCell")
        
        
          inboxList_API(type: "normal")
       
//        sentList_API(type: "normal")
//        importantList_API(type: "normal")
//        trashList_API(type: "normal")

        selectedIndex = 0
        
        // Do any additional setup after loading the view.
       adsListServiceApi(ScreenType: "MessagesInbox")
    
    }
    func adsListServiceApi(ScreenType:String){
        self.urlService.adslistServiceCall(ScreenType: ScreenType) { response in
            let success = response["status"] as! String
            if(success == "1"){
                let data = response["data"] as! [AnyObject]
                self.adsListArr = data
                self.messageTableView.reloadData()
                
            }else{
                
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
   
        
         inboxList_API(type: "normal")
         selectedIndex = 0
        
        
        
        
//        inboxList_API(type: "normal")
//        sentList_API(type: "normal")
//        importantList_API(type: "normal")
//        trashList_API(type: "normal")
//
//        inboxList_API()
//        sentList_API()
//        importantList_API()
//        trashList_API()
        
//        if(selectedIndex == 0){
//
//            inboxList_API()
//            sentList_API()
//            importantList_API()
//            trashList_API()
//        }else if(selectedIndex == 1){
//            if sent1Arr.count == 0 {
//                self.themes.showToast(message: "No Records Found", sender: self)
//                inboxListArr.removeAll()
//                filteInboxArr.removeAll()
//            }else{
//                inboxListArr = sent1Arr
//                filteInboxArr = filtersent1Arr
//
//            }
//        }else if(selectedIndex == 2){
//            if important1Arr.count == 0 {
//                self.themes.showToast(message: "No Records Found", sender: self)
//                inboxListArr.removeAll()
//                filteInboxArr.removeAll()
//            }else{
//                inboxListArr = important1Arr
//                filteInboxArr = filterimportant1Arr
//            }
//        }else if(selectedIndex == 3){
//            if trash1Arr.count == 0 {
//                self.themes.showToast(message: "No Records Found", sender: self)
//                inboxListArr.removeAll()
//                filteInboxArr.removeAll()
//            }else{
//                inboxListArr = trash1Arr
//                filteInboxArr = filtertrash1Arr
//            }
//        }
//
//        topCollectionView.reloadData()
//        messageTableView.reloadData()
 
    }

    
    // Collection view Delegate And data source methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return topHeadingArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "topCollectionViewCell", for: indexPath)as! topCollectionViewCell
        cell.topLbl.text = topHeadingArray[indexPath.item]
        if selectedIndex == indexPath.item {
            cell.bottomview.backgroundColor = barYellow
            return cell
        }else{
            cell.bottomview.backgroundColor = UIColor.clear
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.width
        if(screenWidth == 375){
            return CGSize(width: 94, height: 40)
        }else if(screenWidth == 414){
            return CGSize(width: 100, height: 40)
        }else if(screenWidth == 320) {
            return CGSize(width: 80, height: 40)
        }else if(screenWidth == 768) {
            return CGSize(width: 160, height: 60)
        }else if(screenWidth == 1024) {
            return CGSize(width: 180, height: 70)
        } else {            
            return CGSize(width: 200, height: 80)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        topCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        selectedIndex = indexPath.item
        
        searchField.text = ""
        if selectedIndex == 0 {
            if inboxList1Arr.count == 0 {
                self.themes.showToast(message: "No Records Found", sender: self)
                inboxListArr.removeAll()
                filteInboxArr.removeAll()
            }else{
                inboxListArr = inboxList1Arr
                filteInboxArr = filterInboxList1Arr
            }
            backAction(from: "inbox")
        }else if selectedIndex == 1 {
            if sent1Arr.count == 0 {
                self.themes.showToast(message: "No Records Found", sender: self)
                inboxListArr.removeAll()
                filteInboxArr.removeAll()
            }else{
                inboxListArr = sent1Arr
                filteInboxArr = filtersent1Arr
            }
            backAction(from: "sent")
        }else if selectedIndex == 2 {
            if important1Arr.count == 0 {
                self.themes.showToast(message: "No Records Found", sender: self)
                inboxListArr.removeAll()
                filteInboxArr.removeAll()
            }else{
                inboxListArr = important1Arr
                filteInboxArr = filterimportant1Arr
            }
            backAction(from: "important")
        }else if selectedIndex == 3 {
            if trash1Arr.count == 0 {
                self.themes.showToast(message: "No Records Found", sender: self)
                inboxListArr.removeAll()
                filteInboxArr.removeAll()
            }else{
                inboxListArr = trash1Arr
                filteInboxArr = filtertrash1Arr
            }
             backAction(from: "trash")
        }
       
        self.topCollectionView.reloadData()
        self.messageTableView.reloadData()
        
        
    }
    // Table view Delegate And data source methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//
//        if self.selectedIndex == 0 {
//
//            self.inboxListArr = self.inboxList1Arr
//            self.filteInboxArr = self.filtersent1Arr
//
//
//        }else if selectedIndex == 1 {
//            self.inboxListArr = self.sent1Arr
//            self.filteInboxArr = self.filtersent1Arr
//        }else if selectedIndex == 2 {
//            self.inboxListArr = self.important1Arr
//            self.filteInboxArr = self.filterimportant1Arr
//        }else if selectedIndex == 3 {
//
//            self.inboxListArr = self.trash1Arr
//            self.filteInboxArr = self.filtertrash1Arr
//
//
//        }
        if section == 0 {
            if adsListArr.count > 0 {
                return adsListArr.count
            }else{
                return 0
            }
        }else{
             return inboxListArr.count
        }
        
     
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
          let cell = tableView.dequeueReusableCell(withIdentifier: "AdsBannerTableCell", for: indexPath) as! AdsBannerTableCell
            cell.adsListArr = self.adsListArr
            if self.adsListArr.count > 1 {
            cell.setTimer()
            }
           cell.adsCollectionView.reloadData()
            return cell
        }else{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath)as! MessageCell
        if selectedIndex == 0 {
            cell.starButn.isHidden = false
            cell.starImgView.isHidden = false
            cell.messageCountLbl.isHidden = false
            
            cell.starButn.tag = indexPath.row
            cell.starImgView.tag = indexPath.row
            cell.selectionStyle = .none
            cell.starButn.addTarget(self, action: #selector(starBtnClick(button:)), for:.touchUpInside)
            let obj = inboxListArr[indexPath.row]
            cell.nameLabel.text = obj.first_name
            cell.mesgLabel.text = obj.msg_content
            
            let dateTime = obj.msg_date
            
            
            
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let showDate = inputFormatter.date(from: dateTime)
            inputFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
            let resultString = inputFormatter.string(from: showDate!)
            
            
            
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//            // dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
//            let date = dateFormatter.date(from:dateTime)!
            let DDate = NSDate()
            let timedate =  timeAgoSinceDate( DDate as Date, currentDate: showDate!, numericDates: true)
            print(timedate)
            cell.timeLabel.text = resultString
            
            
            if(obj.totalcount == "" || obj.totalcount == "0"){
                 cell.messageCountLbl.isHidden = true
            }else{
                 cell.messageCountLbl.isHidden = false
            }
           
            cell.messageCountLbl.text = obj.totalcount
            
            let photoApproved = obj.photo1_approve
            let imageUrl = obj.photo1
            
            let msg_from = obj.msg_from
            
            let msg_important_sender = obj.msg_important_sender
            let msg_important_receiver = obj.msg_important_receiver
            
            let my_id = self.themes.getUserId()
            
            if msg_from == my_id {
                if msg_important_sender == "Yes"{
                   
                   cell.starImgView.image = UIImage(named: "star")
                }else{
                      cell.starImgView.image = UIImage(named: "starDefault-1")
                }
            }else{
                if msg_important_receiver == "Yes"{
                    
                    cell.starImgView.image = UIImage(named: "star")
                }else{
                    cell.starImgView.image = UIImage(named: "starDefault-1")
                }
                
            }
           
            if photoApproved == "APPROVED" {
                let fullimageUrl = ImagebaseUrl + imageUrl
                DispatchQueue.main.async {
                    cell.userProfView.sd_setImage(with: URL(string: fullimageUrl), placeholderImage:UIImage(named: "defaultPic"), completed: { (image, error, cacheType, url) -> Void in
                        if ((error) != nil) {
                            cell.userProfView.image = UIImage(named: "defaultPic")
                        } else {
                            cell.userProfView.image = image
                        }
                    })
                }
            }else{
                cell.userProfView.image = UIImage(named: "defaultPic")
            }
            
            return cell
        }
        else if selectedIndex == 1 {
            cell.messageCountLbl.isHidden = true
            cell.starButn.isHidden = false
            cell.starImgView.isHidden = false
            cell.starButn.tag = indexPath.row
             cell.starImgView.tag = indexPath.row
            cell.selectionStyle = .none
            cell.starButn.addTarget(self, action: #selector(starBtnClick(button:)), for:.touchUpInside)
   
            let obj = inboxListArr[indexPath.row]
            cell.nameLabel.text = obj.first_name
            cell.mesgLabel.text = obj.msg_content
            
            let photoApproved = obj.photo1_approve
            let imageUrl = obj.photo1
            
            let msg_from = obj.msg_from
            let dateTime = obj.msg_date
            
            
            
            
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let showDate = inputFormatter.date(from: dateTime)
            inputFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
            let resultString = inputFormatter.string(from: showDate!)
            
            
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//            // dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
//            let date = dateFormatter.date(from:dateTime)!
            let DDate = NSDate()
            let timedate =  timeAgoSinceDate( DDate as Date, currentDate: showDate!, numericDates: true)
            print(timedate)
            
            cell.timeLabel.text = resultString
            
            
            if photoApproved == "APPROVED" {
                let fullimageUrl = ImagebaseUrl + imageUrl
                DispatchQueue.main.async {
                    cell.userProfView.sd_setImage(with: URL(string: fullimageUrl), placeholderImage:UIImage(named: "defaultPic"), completed: { (image, error, cacheType, url) -> Void in
                        if ((error) != nil) {
                            cell.userProfView.image = UIImage(named: "defaultPic")
                        } else {
                            cell.userProfView.image = image
                        }
                    })
                }
            }else{
                cell.userProfView.image = UIImage(named: "defaultPic")
            }
            
            
           
            let msg_important_sender = obj.msg_important_sender
            let msg_important_receiver = obj.msg_important_receiver
            
            let my_id = self.themes.getUserId()
            
            if msg_from == my_id {
                if msg_important_sender == "Yes"{
                    
                    cell.starImgView.image = UIImage(named: "star")
                }else{
                    cell.starImgView.image = UIImage(named: "starDefault-1")
                }
            }else{
                if msg_important_receiver == "Yes"{
                    
                    cell.starImgView.image = UIImage(named: "star")
                }else{
                    cell.starImgView.image = UIImage(named: "starDefault-1")
                }
                
            }
           
            return cell
        }
        else if selectedIndex == 2 {
            cell.messageCountLbl.isHidden = true
            cell.starButn.isHidden = false
            cell.starImgView.isHidden = false
            cell.starButn.tag = indexPath.row
             cell.starImgView.tag = indexPath.row
            cell.selectionStyle = .none
            cell.starButn.addTarget(self, action: #selector(starBtnClick(button:)), for:.touchUpInside)

            let obj = inboxListArr[indexPath.row]
            cell.nameLabel.text = obj.first_name
            cell.mesgLabel.text = obj.msg_content
            
            let photoApproved = obj.photo1_approve
            let imageUrl = obj.photo1
            
            let msg_from = obj.msg_from
            let dateTime = obj.msg_date
            
            
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let showDate = inputFormatter.date(from: dateTime)
            inputFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
            let resultString = inputFormatter.string(from: showDate!)
            
            
            
            
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//            // dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
//            let date = dateFormatter.date(from:dateTime)!
            let DDate = NSDate()
            let timedate =  timeAgoSinceDate( DDate as Date, currentDate: showDate!, numericDates: true)
            print(timedate)
            
            cell.timeLabel.text = resultString
            
 
            if photoApproved == "APPROVED" {
                let fullimageUrl = ImagebaseUrl + imageUrl
                DispatchQueue.main.async {
                    cell.userProfView.sd_setImage(with: URL(string: fullimageUrl), placeholderImage:UIImage(named: "defaultPic"), completed: { (image, error, cacheType, url) -> Void in
                        if ((error) != nil) {
                            cell.userProfView.image = UIImage(named: "defaultPic")
                        } else {
                            cell.userProfView.image = image
                        }
                    })
                }
            }else{
                cell.userProfView.image = UIImage(named: "defaultPic")
            }
            let msg_important_sender = obj.msg_important_sender
            let msg_important_receiver = obj.msg_important_receiver
            let my_id = self.themes.getUserId()
            if msg_from == my_id {
                if msg_important_sender == "Yes"{
                    
                    cell.starImgView.image = UIImage(named: "star")
                }else{
                    cell.starImgView.image = UIImage(named: "starDefault-1")
                }
            }else{
                if msg_important_receiver == "Yes"{
                    cell.starImgView.image = UIImage(named: "star")
                }else{
                    cell.starImgView.image = UIImage(named: "starDefault-1")
                }
                
            }
            return cell
        }
        else if selectedIndex == 3 {
            cell.messageCountLbl.isHidden = true
            cell.starButn.isHidden = true
            cell.starImgView.isHidden = true
            cell.starButn.tag = indexPath.row
             cell.starImgView.tag = indexPath.row
            
            cell.starButn.isHidden = true
            cell.starImgView.isHidden = true
            
            cell.selectionStyle = .none
            cell.starButn.addTarget(self, action: #selector(starBtnClick(button:)), for:.touchUpInside)
            let obj = inboxListArr[indexPath.row]
            cell.nameLabel.text = obj.first_name
            cell.mesgLabel.text = obj.msg_content
            
            let photoApproved = obj.photo1_approve
            let imageUrl = obj.photo1
            
            let msg_from = obj.msg_from
            
            
            let dateTime = obj.msg_date
            
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let showDate = inputFormatter.date(from: dateTime)
            inputFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
            let resultString = inputFormatter.string(from: showDate!)
            
            
            
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//            // dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
//            let date = dateFormatter.date(from:dateTime)!
            let DDate = NSDate()
            let timedate =  timeAgoSinceDate( DDate as Date, currentDate: showDate!, numericDates: true)
            print(timedate)
            
            cell.timeLabel.text = timedate
            let msg_important_sender = obj.msg_important_sender
            let msg_important_receiver = obj.msg_important_receiver
            if photoApproved == "APPROVED" {
                let fullimageUrl = ImagebaseUrl + imageUrl
                DispatchQueue.main.async {
                    cell.userProfView.sd_setImage(with: URL(string: fullimageUrl), placeholderImage:UIImage(named: "defaultPic"), completed: { (image, error, cacheType, url) -> Void in
                        if ((error) != nil) {
                            cell.userProfView.image = UIImage(named: "defaultPic")
                        } else {
                            cell.userProfView.image = image
                        }
                    })
                }
            }else{
                cell.userProfView.image = UIImage(named: "defaultPic")
            }
            
            let my_id = self.themes.getUserId()
            if msg_from == my_id {
                if msg_important_sender == "Yes"{
                    
                    cell.starImgView.image = UIImage(named: "star")
                }else{
                    cell.starImgView.image = UIImage(named: "starDefault-1")
                }
            }else{
                if msg_important_receiver == "Yes"{
                    cell.starImgView.image = UIImage(named: "star")
                }else{
                    cell.starImgView.image = UIImage(named: "starDefault-1")
                }
                
            }
            
            return cell
         }

        else{
            
            return UITableViewCell()
  
        }
        
    }
        

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
 
        
        if indexPath.section == 0 {
         return 130
        }else{
        return 75
        }
    }
    

//    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//
//        if selectedIndex == 0{
//            let action =  UIContextualAction(style: .normal, title: "", handler: { (action,view,completionHandler ) in
//
//
//                var msg_type = ""
//                let obj = self.inboxListArr[indexPath.row]
//                let msg_from = obj.msg_from
//                let msg_id = obj.msg_id
//                let my_id = self.themes.getUserId()
//
//                if msg_from == my_id {
//                    msg_type = "sender"
//                }else{
//                    msg_type = "reciever"
//
//                }
//                self.inboxListArr.remove(at: indexPath.row)
//                self.inboxList1Arr.remove(at: indexPath.row)
//                self.filterInboxList1Arr.remove(at: indexPath.row)
//
//
//
//
//
//                let alert = UIAlertController(title: "Alert", message: "Do you want move this message to trash", preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { [] (_) in
//
//                    self.makeTrash_API(trash_status: "Yes", msg_id: msg_id, msg_type: msg_type)
//
//                }))
//                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//
//
//
//
//
//                completionHandler(true)
//            })
//            action.image = UIImage(named: "white_delete")
//            action.backgroundColor = .red
//            let confrigation = UISwipeActionsConfiguration(actions: [action])
//            // self.messageTableView.reloadData()
//            return confrigation
//        }else if selectedIndex == 1 {
//            let action =  UIContextualAction(style: .normal, title: "", handler: { (action,view,completionHandler ) in
//
//
//                var msg_type = ""
//                let obj = self.inboxListArr[indexPath.row]
//                let msg_from = obj.msg_from
//                let msg_id = obj.msg_id
//                let my_id = self.themes.getUserId()
//
//                if msg_from == my_id {
//                    msg_type = "sender"
//                }else{
//                    msg_type = "reciever"
//
//                }
//                self.inboxListArr.remove(at: indexPath.row)
//                self.sent1Arr.remove(at: indexPath.row)
//                self.filtersent1Arr.remove(at: indexPath.row)
//
//
//
//
//                let alert = UIAlertController(title: "Alert", message: "Do you want move this message to trash", preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { [] (_) in
//
//                    self.makeTrash_API(trash_status: "Yes", msg_id: msg_id, msg_type: msg_type)
//
//                }))
//                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//
//                //do stuff
//                completionHandler(true)
//            })
//            action.image = UIImage(named: "white_delete")
//            action.backgroundColor = .red
//            let confrigation = UISwipeActionsConfiguration(actions: [action])
//            // self.messageTableView.reloadData()
//            return confrigation
//        }else if selectedIndex == 2 {
//            let action =  UIContextualAction(style: .normal, title: "", handler: { (action,view,completionHandler ) in
//
//                var msg_type = ""
//                let obj = self.inboxListArr[indexPath.row]
//                let msg_from = obj.msg_from
//                let msg_id = obj.msg_id
//                let my_id = self.themes.getUserId()
//
//                if msg_from == my_id {
//                    msg_type = "sender"
//                }else{
//                    msg_type = "reciever"
//
//                }
//                self.inboxListArr.remove(at: indexPath.row)
//                self.important1Arr.remove(at: indexPath.row)
//                self.filterimportant1Arr.remove(at: indexPath.row)
//
//
//
//                let alert = UIAlertController(title: "Alert", message: "Do you want move this message to trash", preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { [] (_) in
//
//                    self.makeTrash_API(trash_status: "Yes", msg_id: msg_id, msg_type: msg_type)
//
//                }))
//                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//
//
//                //do stuff
//                completionHandler(true)
//            })
//            action.image = UIImage(named: "white_delete")
//            action.backgroundColor = .red
//            let confrigation = UISwipeActionsConfiguration(actions: [action])
//            // self.messageTableView.reloadData()
//            return confrigation
//
//
//
//        }else if selectedIndex == 3 {
//            let action =  UIContextualAction(style: .normal, title: "", handler: { (action,view,completionHandler ) in
//
//                let alertController = UIAlertController(title: "Alert", message: "Are you sure you want to  Proceed with Delete ?", preferredStyle: .alert)
//                    let OKAction = UIAlertAction(title: "Delete", style: .default) { (action:UIAlertAction!) in
//                                    print("Ok button tapped");
//
//                                    var msg_type = ""
//                                    let obj = self.inboxListArr[indexPath.row]
//                                    let msg_from = obj.msg_from
//                                    let msg_id = obj.msg_id
//                                    let my_id = self.themes.getUserId()
//                                    if msg_from == my_id {
//                                        msg_type = "sender"
//                                    }else{
//                                        msg_type = "reciever"
//
//                                    }
//                                    self.inboxListArr.remove(at: indexPath.row)
//                                    self.trash1Arr.remove(at: indexPath.row)
//                                    self.filtertrash1Arr.remove(at: indexPath.row)
//
//
//                        let alertController = UIAlertController(title: "Alert", message: "Do you want to permanently delete these messages.", preferredStyle: .alert)
//                        let settingsAction = UIAlertAction(title: "OK", style: .default) { (_) -> Void in
//                            self.makeTrash_API(trash_status: "Delete", msg_id: msg_id, msg_type: msg_type)
//                        }
//                        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
//                        alertController.addAction(cancelAction)
//                        alertController.addAction(settingsAction)
//
//                                }
//                                alertController.addAction(OKAction)
//                                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
//
//                                }
//                                alertController.addAction(cancelAction)
//                                self.present(alertController, animated: true, completion:nil)
//
//
//                //do stuff
//                completionHandler(true)
//            })
//            action.image = UIImage(named: "white_delete")
//            action.backgroundColor = .red
//
//
//            let action1 =  UIContextualAction(style: .normal, title: "", handler: { (action,view,completionHandler ) in
//
//                var msg_type = ""
//                let obj = self.inboxListArr[indexPath.row]
//                let msg_from = obj.msg_from
//                let msg_id = obj.msg_id
//                let my_id = self.themes.getUserId()
//
//                if msg_from == my_id {
//                    msg_type = "sender"
//                }else{
//                    msg_type = "reciever"
//
//                }
//                self.inboxListArr.remove(at: indexPath.row)
//                self.trash1Arr.remove(at: indexPath.row)
//                self.filtertrash1Arr.remove(at: indexPath.row)
//
//                self.makeTrash_API(trash_status: "No", msg_id: msg_id, msg_type: msg_type)
//
//                completionHandler(true)
//            })
//            action1.image = UIImage(named: "white_undo")
//            action1.backgroundColor = .green
//            let confrigation = UISwipeActionsConfiguration(actions: [action,action1])
//           //  self.messageTableView.reloadData()
//            return confrigation
//        }else{
//
//           return nil
//
//        }
//
//
//    }
    
//    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//
//
//        if selectedIndex == 3 {
//
//            let action =  UIContextualAction(style: .normal, title: "", handler: { (action,view,completionHandler ) in
//
//                var msg_type = ""
//                let obj = self.inboxListArr[indexPath.row]
//                let msg_from = obj.msg_from
//                let msg_id = obj.msg_id
//                let my_id = self.themes.getUserId()
//
//                if msg_from == my_id {
//                    msg_type = "sender"
//                }else{
//                    msg_type = "reciever"
//
//                }
//                self.inboxListArr.remove(at: indexPath.row)
//                self.makeTrash_API(trash_status: "No", msg_id: msg_id, msg_type: msg_type)
//
//                completionHandler(true)
//            })
//            action.image = UIImage(named: "white_undo")
//            action.backgroundColor = .green
//            let confrigation = UISwipeActionsConfiguration(actions: [action])
//
//
//             self.messageTableView.reloadData()
//            return confrigation
//
//        }else{
//
//            let action =  UIContextualAction(style: .normal, title: "", handler: { (action,view,completionHandler ) in
//                completionHandler(true)
//            })
//            action.image = UIImage(named: "white_undo")
//            action.backgroundColor = .green
//            let confrigation = UISwipeActionsConfiguration(actions: [action])
//            self.messageTableView.reloadData()
//            return confrigation
//
//        }
//
//
//
//
//
//
//    }
//
//
    
    
    
    
    

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {


        if editingStyle == .delete{

            if selectedIndex == 0 {

                var msg_type = ""
                let obj = inboxListArr[indexPath.row]
                let msg_from = obj.msg_from
                let msg_id = obj.msg_id
                let my_id = self.themes.getUserId()

                if msg_from == my_id {
                    msg_type = "sender"
                }else{
                    msg_type = "reciever"

                }
                 self.inboxListArr.remove(at: indexPath.row)
                self.makeTrash_API(trash_status: "Yes", msg_id: msg_id, msg_type: msg_type)


            }else if selectedIndex == 1 {

                var msg_type = ""
                let obj = inboxListArr[indexPath.row]
                let msg_from = obj.msg_from
                let msg_id = obj.msg_id
                let my_id = self.themes.getUserId()

                if msg_from == my_id {
                    msg_type = "sender"
                }else{
                    msg_type = "reciever"

                }
                self.inboxListArr.remove(at: indexPath.row)
                self.makeTrash_API(trash_status: "Yes", msg_id: msg_id, msg_type: msg_type)



            }else if selectedIndex == 2 {

                var msg_type = ""
                let obj = inboxListArr[indexPath.row]
                let msg_from = obj.msg_from
                let msg_id = obj.msg_id
                let my_id = self.themes.getUserId()

                if msg_from == my_id {
                    msg_type = "sender"
                }else{
                    msg_type = "reciever"

                }
                self.inboxListArr.remove(at: indexPath.row)
                self.makeTrash_API(trash_status: "Yes", msg_id: msg_id, msg_type: msg_type)



            }
//            else if selectedIndex == 3 {
//
//
//
//                let alertController = UIAlertController(title: "Alert", message: "Are you sure you want to  Proceed with Delete ?", preferredStyle: .alert)
//                let OKAction = UIAlertAction(title: "Delete", style: .default) { (action:UIAlertAction!) in
//                    print("Ok button tapped");
//
//                    var msg_type = ""
//                    let obj = self.inboxListArr[indexPath.row]
//                    let msg_from = obj.msg_from
//                    let msg_id = obj.msg_id
//                    let my_id = self.themes.getUserId()
//
//                    if msg_from == my_id {
//                        msg_type = "sender"
//                    }else{
//                        msg_type = "reciever"
//
//                    }
//                    self.inboxListArr.remove(at: indexPath.row)
//                    self.makeTrash_API(trash_status: "Delete", msg_id: msg_id, msg_type: msg_type)
//                }
//                alertController.addAction(OKAction)
//                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
//
//                }
//                alertController.addAction(cancelAction)
//                self.present(alertController, animated: true, completion:nil)
//
//            }


            self.messageTableView.reloadData()

        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            
        }else{
            
        
        
        if selectedIndex == 0{
            let messagevc = self.storyboard?.instantiateViewController(withIdentifier: "ChattingVC") as! ChattingVC
             let obj = inboxListArr[indexPath.row]
            messagevc.date_id = obj.date_id
            messagevc.photo = obj.photo1
            messagevc.photoApproved = obj.photo1_approve
            messagevc.msgType = "inbox"
            messagevc.delegate = self
             messagevc.msg_from = obj.msg_from
             messagevc.msg_to = obj.msg_to
             messagevc.first_name = obj.first_name
        
        self.navigationController?.pushViewController(messagevc, animated: true)
            
            
//            messagevc.date_id = obj.date_id
//            messagevc.first_name = obj.first_name
//            messagevc.msg_content = obj.msg_content
//            messagevc.msg_date = obj.msg_date
           
           
//            messagevc.msg_important_receiver = obj.msg_important_receiver
//            messagevc.msg_important_sender = obj.msg_important_sender
//            messagevc.msg_read_status = obj.msg_read_status
//            messagevc.msg_status = obj.msg_status
//
//            messagevc.photo1 = obj.photo1
//            messagevc.photo1_approve = obj.photo1_approve
//            messagevc.trash_receiver = obj.trash_receiver
//            messagevc.trash_sender = obj.trash_sender
//
//
//
//
//            messagevc.msg_Type = "Inbox"
            
            
        }else if selectedIndex == 1 {
//              let messagevc = self.storyboard?.instantiateViewController(withIdentifier: "OpenMessageVC") as! OpenMessageVC
//            let obj = inboxListArr[indexPath.row]
//            messagevc.date_id = obj.date_id
//            messagevc.first_name = obj.first_name
//            messagevc.msg_content = obj.msg_content
//            messagevc.msg_date = obj.msg_date
//            messagevc.msg_from = obj.msg_from
//            messagevc.msg_id = obj.msg_id
//            messagevc.msg_important_receiver = obj.msg_important_receiver
//            messagevc.msg_important_sender = obj.msg_important_sender
//            messagevc.msg_read_status = obj.msg_read_status
//            messagevc.msg_status = obj.msg_status
//            messagevc.msg_to = obj.msg_to
//            messagevc.photo1 = obj.photo1
//            messagevc.photo1_approve = obj.photo1_approve
//            messagevc.trash_receiver = obj.trash_receiver
//            messagevc.trash_sender = obj.trash_sender
//            messagevc.msg_Type = "Sent"
//            self.navigationController?.pushViewController(messagevc, animated: true)
            let messagevc = self.storyboard?.instantiateViewController(withIdentifier: "ChattingVC") as! ChattingVC
            let obj = inboxListArr[indexPath.row]
            messagevc.date_id = obj.date_id
             messagevc.delegate = self
            messagevc.photo = obj.photo1
            messagevc.photoApproved = obj.photo1_approve
            messagevc.msgType = "sent"
                         messagevc.msg_from = obj.msg_from
                         messagevc.msg_to = obj.msg_to
            self.navigationController?.pushViewController(messagevc, animated: true)
        }else if selectedIndex == 2 {
//              let messagevc = self.storyboard?.instantiateViewController(withIdentifier: "OpenMessageVC") as! OpenMessageVC
//            let obj = inboxListArr[indexPath.row]
//            messagevc.date_id = obj.date_id
//            messagevc.first_name = obj.first_name
//            messagevc.msg_content = obj.msg_content
//            messagevc.msg_date = obj.msg_date
//            messagevc.msg_from = obj.msg_from
//            messagevc.msg_id = obj.msg_id
//
//            messagevc.msg_important_receiver = obj.msg_important_receiver
//            messagevc.msg_important_sender = obj.msg_important_sender
//            messagevc.msg_read_status = obj.msg_read_status
//            messagevc.msg_status = obj.msg_status
//            messagevc.msg_to = obj.msg_to
//            messagevc.photo1 = obj.photo1
//            messagevc.photo1_approve = obj.photo1_approve
//            messagevc.trash_receiver = obj.trash_receiver
//            messagevc.trash_sender = obj.trash_sender
//            messagevc.msg_Type = "important"
//            self.navigationController?.pushViewController(messagevc, animated: true)
            
            
            let messagevc = self.storyboard?.instantiateViewController(withIdentifier: "ChattingVC") as! ChattingVC
            let obj = inboxListArr[indexPath.row]
            messagevc.date_id = obj.date_id
            messagevc.photo = obj.photo1
            messagevc.photoApproved = obj.photo1_approve
            messagevc.msgType = "important"
            messagevc.delegate = self
            messagevc.msg_from = obj.msg_from
            messagevc.msg_to = obj.msg_to
            self.navigationController?.pushViewController(messagevc, animated: true)
        }else if selectedIndex == 3 {

            
//            let messagevc = self.storyboard?.instantiateViewController(withIdentifier: "ChattingVC") as! ChattingVC
//            let obj = inboxListArr[indexPath.row]
//            messagevc.msgId = obj.msg_id
//            messagevc.photo = obj.photo1
//            messagevc.photoApproved = obj.photo1_approve
//            messagevc.msgType = "trash"
//            messagevc.delegate = self
//            messagevc.msg_from = obj.msg_from
//            messagevc.msg_to = obj.msg_to
//            self.navigationController?.pushViewController(messagevc, animated: true)
        }
    }
            
            
            
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle
    {
        
        if indexPath.section == 0 {
            return UITableViewCellEditingStyle.none
        }else{
        if selectedIndex == 3 {
            return UITableViewCellEditingStyle.none
        } else {
            return UITableViewCellEditingStyle.delete
        }
            
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
     
        
        if indexPath.section == 0 {
            return nil
            
        }else{
            
        
        
        
        
        if selectedIndex == 0{
            let action =  UIContextualAction(style: .normal, title: "", handler: { (action,view,completionHandler ) in
                var msg_type = ""
                let obj = self.inboxListArr[indexPath.row]
                let msg_from = obj.msg_from
                let msg_id = obj.msg_id
                let my_id = self.themes.getUserId()
                if msg_from == my_id {
                    msg_type = "sender"
                }else{
                    msg_type = "reciever"
                }
                
                self.inboxListArr.remove(at: indexPath.row)
                self.inboxList1Arr.remove(at: indexPath.row)
                self.filterInboxList1Arr.remove(at: indexPath.row)
                
                let alert = UIAlertController(title: "Alert", message: "Do you want move this message to trash", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { [] (_) in
                    self.makeTrash_API(trash_status: "Yes", msg_id: msg_id, msg_type: msg_type)
                  
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
                completionHandler(true)
            })
            action.image = UIImage(named: "white_delete")
            action.backgroundColor = .red
            let confrigation = UISwipeActionsConfiguration(actions: [action])
            // self.messageTableView.reloadData()
            return confrigation
            
        }else if selectedIndex == 1 {
            
            let action =  UIContextualAction(style: .normal, title: "", handler: { (action,view,completionHandler ) in
             
                var msg_type = ""
                let obj = self.inboxListArr[indexPath.row]
                let msg_from = obj.msg_from
                let msg_id = obj.msg_id
                let my_id = self.themes.getUserId()
                
                if msg_from == my_id {
                    
                    msg_type = "sender"
                    
                }else{
                    
                    msg_type = "reciever"
                    
                    
                    
                }
                
                self.inboxListArr.remove(at: indexPath.row)
                
                self.sent1Arr.remove(at: indexPath.row)
                
                self.filtersent1Arr.remove(at: indexPath.row)
                
                
                let alert = UIAlertController(title: "Alert", message: "Do you want move this message to trash", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { [] (_) in
                    
                    self.makeTrash_API(trash_status: "Yes", msg_id: msg_id, msg_type: msg_type)
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                //do stuff
                completionHandler(true)
                
            })
            
            action.image = UIImage(named: "white_delete")
            
            action.backgroundColor = .red
            
            let confrigation = UISwipeActionsConfiguration(actions: [action])
            
            // self.messageTableView.reloadData()
            
            return confrigation
            
        }else if selectedIndex == 2 {
            
            let action =  UIContextualAction(style: .normal, title: "", handler: { (action,view,completionHandler ) in
                
                
                
                var msg_type = ""
                
                let obj = self.inboxListArr[indexPath.row]
                
                let msg_from = obj.msg_from
                
                let msg_id = obj.msg_id
                
                let my_id = self.themes.getUserId()
                
                
                
                if msg_from == my_id {
                    
                    msg_type = "sender"
                    
                }else{
                    
                    msg_type = "reciever"
                    
                    
                    
                }
                
                self.inboxListArr.remove(at: indexPath.row)
                
                self.important1Arr.remove(at: indexPath.row)
                
                self.filterimportant1Arr.remove(at: indexPath.row)
                
              
                
                let alert = UIAlertController(title: "Alert", message: "Do you want move this message to trash", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { [] (_) in
                    
                    self.makeTrash_API(trash_status: "Yes", msg_id: msg_id, msg_type: msg_type)
                  
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
                
               
                //do stuff
                
                completionHandler(true)
                
            })
            
            action.image = UIImage(named: "white_delete")
            
            action.backgroundColor = .red
            
            let confrigation = UISwipeActionsConfiguration(actions: [action])
            
            // self.messageTableView.reloadData()
            
            return confrigation
            
            
            
            
            
            
            
        }else{
            
            
            return nil
            
            
            
        }
        
    }
        
        
        
    }
    @objc func starBtnClick(button: UIButton) {
        
        let cell = messageTableView.cellForRow(at: IndexPath(row: button.tag, section: 0)) as! MessageCell

        if selectedIndex == 0 {
            
            var status = ""
            var msg_type = ""
            if cell.starImgView.image == UIImage(named: "starDefault-1") {
                cell.starImgView.image = UIImage (named: "star")
                status = "Yes"
            }else{
                cell.starImgView.image = UIImage (named: "starDefault-1")
                  status = "No"
            }
            
            let obj = inboxListArr[button.tag]
            let msg_from = obj.msg_from
            let msg_id = obj.msg_id
            let my_id = self.themes.getUserId()
            
            if msg_from == my_id {
              msg_type = "sender"
            }else{
               msg_type = "reciever"
                
            }
             self.makeimportant_API(imp_status: status, msg_id: msg_id, msg_type: msg_type)
            
        }else if selectedIndex == 1{
            
            var status = ""
            var msg_type = ""
            if cell.starImgView.image == UIImage(named: "starDefault-1") {
                cell.starImgView.image = UIImage (named: "star")
                status = "Yes"
            }else{
                cell.starImgView.image = UIImage (named: "starDefault-1")
                status = "No"
            }
            
            let obj = inboxListArr[button.tag]
            let msg_from = obj.msg_from
            let msg_id = obj.msg_id
            let my_id = self.themes.getUserId()
            
            if msg_from == my_id {
                msg_type = "sender"
            }else{
                msg_type = "reciever"
                
            }
            self.makeimportant_API(imp_status: status, msg_id: msg_id, msg_type: msg_type)
            
            
            
        }else if selectedIndex == 2 {
            
            var status = ""
            var msg_type = ""
            if cell.starImgView.image == UIImage(named: "starDefault-1") {
                cell.starImgView.image = UIImage (named: "star")
                status = "Yes"
            }else{
                cell.starImgView.image = UIImage (named: "starDefault-1")
                status = "No"
            }
            
            let obj = inboxListArr[button.tag]
            let msg_from = obj.msg_from
            let msg_id = obj.msg_id
            let my_id = self.themes.getUserId()
            if msg_from == my_id {
                msg_type = "sender"
            }else{
                msg_type = "reciever"
                
            }
            self.makeimportant_API(imp_status: status, msg_id: msg_id, msg_type: msg_type)
            
            
        }else if selectedIndex == 3 {
            
            var status = ""
            var msg_type = ""
            if cell.starImgView.image == UIImage(named: "starDefault-1") {
                cell.starImgView.image = UIImage (named: "star")
                status = "Yes"
            }else{
                cell.starImgView.image = UIImage (named: "starDefault-1")
                status = "No"
            }
            
            let obj = inboxListArr[button.tag]
            let msg_from = obj.msg_from
            let msg_id = obj.msg_id
            let my_id = self.themes.getUserId()
            
            if msg_from == my_id {
                msg_type = "sender"
            }else{
                msg_type = "reciever"
                
            }
            self.makeimportant_API(imp_status: status, msg_id: msg_id, msg_type: msg_type)
            
        }
  
        
    }

    // UITextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        
        let name:String = searchField.text!
        if textField == searchField {
            inboxListArr.removeAll()
            inboxListNameArray.removeAll()
            inboxListArr = filteInboxArr.filter{$0.first_name.contains(name) } as [AnyObject] as! [MessagesViewController.messageList]
            if inboxListArr.count == 0 || searchField.text == ""{
                inboxListArr = filteInboxArr
            }
            else {
            }
       }
        DispatchQueue.main.async {
            self.messageTableView.reloadData()
        }
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    func inboxList_API(type:String){

        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:messagesInboxList, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    if(type == "normal"){
                    self.inboxListArr.removeAll()
                    self.filteInboxArr.removeAll()
                    self.inboxList1Arr.removeAll()
                    self.filterInboxList1Arr.removeAll()
                    let array = response["userlist"] as! [AnyObject]
                    for i in 0...array.count-1{
                        let obj = array[i]
                        let date_id = self.themes.checkNullValue(obj["date_id"] as AnyObject) as! String
                         let first_name = self.themes.checkNullValue(obj["first_name"] as AnyObject) as! String
                         let msg_content = self.themes.checkNullValue(obj["msg_content"] as AnyObject) as! String
                        let msg_date = self.themes.checkNullValue(obj["msg_date"] as AnyObject) as! String
                        let msg_from = self.themes.checkNullValue(obj["msg_from"] as AnyObject) as! String
                        let msg_id = self.themes.checkNullValue(obj["msg_id"] as AnyObject) as! String
                        let msg_important_receiver = self.themes.checkNullValue(obj["msg_important_receiver"] as AnyObject) as! String
                        let msg_important_sender = self.themes.checkNullValue(obj["msg_important_sender"] as AnyObject) as! String
                        let msg_read_status = self.themes.checkNullValue(obj["msg_read_status"] as AnyObject) as! String
                        let msg_status = self.themes.checkNullValue(obj["msg_status"] as AnyObject) as! String
                        let msg_to = self.themes.checkNullValue(obj["msg_to"] as AnyObject) as! String
                        let photo1 = self.themes.checkNullValue(obj["photo1"] as AnyObject) as! String
                        let photo1_approve = self.themes.checkNullValue(obj["photo1_approve"] as AnyObject) as! String
                        let trash_receiver = self.themes.checkNullValue(obj["trash_receiver"] as AnyObject) as! String
                        let trash_sender = self.themes.checkNullValue(obj["trash_sender"] as AnyObject) as! String
                        let totalcount = self.themes.checkNullValue(obj["totalcount"] as AnyObject) as! String
                        self.inboxMessageCount = totalcount
                        let name = first_name.capitalizingFirstLetter()
                        let obj1 = messageList(date_id: date_id,
                                               first_name: name,
                                               msg_content: msg_content,
                                               msg_date: msg_date,
                                               msg_from: msg_from,
                                               msg_id: msg_id,
                                               msg_important_receiver: msg_important_receiver,
                                               msg_important_sender: msg_important_sender,
                                               msg_read_status: msg_read_status,
                                               msg_status: msg_status,
                                               msg_to: msg_to,
                                               photo1: photo1,
                                               photo1_approve: photo1_approve,
                                               trash_receiver: trash_receiver,
                                               trash_sender: trash_sender,
                                               totalcount: totalcount)
                        self.inboxListArr.append(obj1)
                        self.filteInboxArr.append(obj1)
                        self.inboxList1Arr.append(obj1)
                        self.filterInboxList1Arr.append(obj1)
                    }
                    }else{
                        self.inboxListArr.removeAll()
                        self.filteInboxArr.removeAll()
                        self.inboxList1Arr.removeAll()
                        self.filterInboxList1Arr.removeAll()
                        let array = response["userlist"] as! [AnyObject]
                        for i in 0...array.count-1{
                            let obj = array[i]
                            let date_id = self.themes.checkNullValue(obj["date_id"] as AnyObject) as! String
                            let first_name = self.themes.checkNullValue(obj["first_name"] as AnyObject) as! String
                            let msg_content = self.themes.checkNullValue(obj["msg_content"] as AnyObject) as! String
                            let msg_date = self.themes.checkNullValue(obj["msg_date"] as AnyObject) as! String
                            let msg_from = self.themes.checkNullValue(obj["msg_from"] as AnyObject) as! String
                            let msg_id = self.themes.checkNullValue(obj["msg_id"] as AnyObject) as! String
                            let msg_important_receiver = self.themes.checkNullValue(obj["msg_important_receiver"] as AnyObject) as! String
                            let msg_important_sender = self.themes.checkNullValue(obj["msg_important_sender"] as AnyObject) as! String
                            let msg_read_status = self.themes.checkNullValue(obj["msg_read_status"] as AnyObject) as! String
                            let msg_status = self.themes.checkNullValue(obj["msg_status"] as AnyObject) as! String
                            let msg_to = self.themes.checkNullValue(obj["msg_to"] as AnyObject) as! String
                            let photo1 = self.themes.checkNullValue(obj["photo1"] as AnyObject) as! String
                            let photo1_approve = self.themes.checkNullValue(obj["photo1_approve"] as AnyObject) as! String
                            let trash_receiver = self.themes.checkNullValue(obj["trash_receiver"] as AnyObject) as! String
                            let trash_sender = self.themes.checkNullValue(obj["trash_sender"] as AnyObject) as! String
                            let totalcount = self.themes.checkNullValue(obj["totalcount"] as AnyObject) as! String
                            self.inboxMessageCount = totalcount
                            let name = first_name.capitalizingFirstLetter()
                            let obj1 = messageList(date_id: date_id, first_name: name, msg_content: msg_content, msg_date: msg_date, msg_from: msg_from, msg_id: msg_id, msg_important_receiver: msg_important_receiver, msg_important_sender: msg_important_sender, msg_read_status: msg_read_status, msg_status: msg_status, msg_to: msg_to, photo1: photo1, photo1_approve: photo1_approve, trash_receiver: trash_receiver, trash_sender: trash_sender, totalcount: totalcount)
                            self.inboxListArr.append(obj1)
                            self.filteInboxArr.append(obj1)
                            self.inboxList1Arr.append(obj1)
                            self.filterInboxList1Arr.append(obj1)
                        }
                        self.inboxListArr = self.inboxList1Arr
                        self.filteInboxArr = self.filterInboxList1Arr
                    }
                    self.messageTableView.reloadData()
                    self.topCollectionView.reloadData()
                }
                
                else if(success == "2"){
                    self.inboxListArr.removeAll()
                                       self.filteInboxArr.removeAll()
                                       self.inboxList1Arr.removeAll()
                                       self.filterInboxList1Arr.removeAll()
                     self.messageTableView.reloadData()
                    
                }else {
                   // let result = response["result"] as! String
                   // self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func sentList_API(type:String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:messagesSentList, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    if(type == "normal"){
                        
                    
                    
                    self.sent1Arr.removeAll()
                    self.filtersent1Arr.removeAll()
                    
                    let array = response["sentlist"] as! [AnyObject]
                    
                    for i in 0...array.count-1{
                        let obj = array[i]
                        let date_id = self.themes.checkNullValue(obj["date_id"] as AnyObject) as! String
                        let first_name = self.themes.checkNullValue(obj["first_name"] as AnyObject) as! String
                        let msg_content = self.themes.checkNullValue(obj["msg_content"] as AnyObject) as! String
                        let msg_date = self.themes.checkNullValue(obj["msg_date"] as AnyObject) as! String
                        let msg_from = self.themes.checkNullValue(obj["msg_from"] as AnyObject) as! String
                        let msg_id = self.themes.checkNullValue(obj["msg_id"] as AnyObject) as! String
                        let msg_important_receiver = self.themes.checkNullValue(obj["msg_important_receiver"] as AnyObject) as! String
                        let msg_important_sender = self.themes.checkNullValue(obj["msg_important_sender"] as AnyObject) as! String
                        let msg_read_status = self.themes.checkNullValue(obj["msg_read_status"] as AnyObject) as! String
                        let msg_status = self.themes.checkNullValue(obj["msg_status"] as AnyObject) as! String
                        let msg_to = self.themes.checkNullValue(obj["msg_to"] as AnyObject) as! String
                        let photo1 = self.themes.checkNullValue(obj["photo1"] as AnyObject) as! String
                        let photo1_approve = self.themes.checkNullValue(obj["photo1_approve"] as AnyObject) as! String
                        let trash_receiver = self.themes.checkNullValue(obj["trash_receiver"] as AnyObject) as! String
                        let trash_sender = self.themes.checkNullValue(obj["trash_sender"] as AnyObject) as! String
                        
                        let totalcount = self.themes.checkNullValue(obj["totalcount"] as AnyObject) as! String
                        self.inboxMessageCount = totalcount
                        
                         let name = first_name.capitalizingFirstLetter()
                        
                        let obj1 = messageList(date_id: date_id, first_name: name, msg_content: msg_content, msg_date: msg_date, msg_from: msg_from, msg_id: msg_id, msg_important_receiver: msg_important_receiver, msg_important_sender: msg_important_sender, msg_read_status: msg_read_status, msg_status: msg_status, msg_to: msg_to, photo1: photo1, photo1_approve: photo1_approve, trash_receiver: trash_receiver, trash_sender: trash_sender, totalcount: totalcount)
                        
                        
                        
                        self.sent1Arr.append(obj1)
                        self.filtersent1Arr.append(obj1)
                    
                      }
                        
                    }else{
                        
                        self.sent1Arr.removeAll()
                        self.filtersent1Arr.removeAll()
                        
                        let array = response["sentlist"] as! [AnyObject]
                        
                        for i in 0...array.count-1{
                            let obj = array[i]
                            let date_id = self.themes.checkNullValue(obj["date_id"] as AnyObject) as! String
                            let first_name = self.themes.checkNullValue(obj["first_name"] as AnyObject) as! String
                            let msg_content = self.themes.checkNullValue(obj["msg_content"] as AnyObject) as! String
                            let msg_date = self.themes.checkNullValue(obj["msg_date"] as AnyObject) as! String
                            let msg_from = self.themes.checkNullValue(obj["msg_from"] as AnyObject) as! String
                            let msg_id = self.themes.checkNullValue(obj["msg_id"] as AnyObject) as! String
                            let msg_important_receiver = self.themes.checkNullValue(obj["msg_important_receiver"] as AnyObject) as! String
                            let msg_important_sender = self.themes.checkNullValue(obj["msg_important_sender"] as AnyObject) as! String
                            let msg_read_status = self.themes.checkNullValue(obj["msg_read_status"] as AnyObject) as! String
                            let msg_status = self.themes.checkNullValue(obj["msg_status"] as AnyObject) as! String
                            let msg_to = self.themes.checkNullValue(obj["msg_to"] as AnyObject) as! String
                            let photo1 = self.themes.checkNullValue(obj["photo1"] as AnyObject) as! String
                            let photo1_approve = self.themes.checkNullValue(obj["photo1_approve"] as AnyObject) as! String
                            let trash_receiver = self.themes.checkNullValue(obj["trash_receiver"] as AnyObject) as! String
                            let trash_sender = self.themes.checkNullValue(obj["trash_sender"] as AnyObject) as! String
                            
                            let name = first_name.capitalizingFirstLetter()
                            let totalcount = self.themes.checkNullValue(obj["totalcount"] as AnyObject) as! String
                            self.inboxMessageCount = totalcount
                            
                            let obj1 = messageList(date_id: date_id, first_name: name, msg_content: msg_content, msg_date: msg_date, msg_from: msg_from, msg_id: msg_id, msg_important_receiver: msg_important_receiver, msg_important_sender: msg_important_sender, msg_read_status: msg_read_status, msg_status: msg_status, msg_to: msg_to, photo1: photo1, photo1_approve: photo1_approve, trash_receiver: trash_receiver, trash_sender: trash_sender, totalcount: totalcount)
                            
                            
                            
                            self.sent1Arr.append(obj1)
                            self.filtersent1Arr.append(obj1)
                            
                        }
                        
                        self.inboxListArr = self.sent1Arr
                        self.filteInboxArr = self.filtersent1Arr
                    }
                    self.messageTableView.reloadData()
                    
                    
                    
                    
            
                   
                } else {
                    
                  //  let result = response["result"] as! String
                  //  self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
                
            }
            
        } else {
            
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }

    }
    func importantList_API(type:String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:messagesImportantList, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    if(type == "normal"){
                        
                    
                    
                    self.important1Arr.removeAll()
                    self.filterimportant1Arr.removeAll()
                    
                    
                    let array = response["importantlist"] as! [AnyObject]
                    
                    for i in 0...array.count-1{
                        let obj = array[i]
                        let date_id = self.themes.checkNullValue(obj["date_id"] as AnyObject) as! String
                        let first_name = self.themes.checkNullValue(obj["first_name"] as AnyObject) as! String
                        let msg_content = self.themes.checkNullValue(obj["msg_content"] as AnyObject) as! String
                        let msg_date = self.themes.checkNullValue(obj["msg_date"] as AnyObject) as! String
                        let msg_from = self.themes.checkNullValue(obj["msg_from"] as AnyObject) as! String
                        let msg_id = self.themes.checkNullValue(obj["msg_id"] as AnyObject) as! String
                        let msg_important_receiver = self.themes.checkNullValue(obj["msg_important_receiver"] as AnyObject) as! String
                        let msg_important_sender = self.themes.checkNullValue(obj["msg_important_sender"] as AnyObject) as! String
                        let msg_read_status = self.themes.checkNullValue(obj["msg_read_status"] as AnyObject) as! String
                        let msg_status = self.themes.checkNullValue(obj["msg_status"] as AnyObject) as! String
                        let msg_to = self.themes.checkNullValue(obj["msg_to"] as AnyObject) as! String
                        let photo1 = self.themes.checkNullValue(obj["photo1"] as AnyObject) as! String
                        let photo1_approve = self.themes.checkNullValue(obj["photo1_approve"] as AnyObject) as! String
                        let trash_receiver = self.themes.checkNullValue(obj["trash_receiver"] as AnyObject) as! String
                        let trash_sender = self.themes.checkNullValue(obj["trash_sender"] as AnyObject) as! String
                          let name = first_name.capitalizingFirstLetter()
                        let totalcount = self.themes.checkNullValue(obj["totalcount"] as AnyObject) as! String
                        self.inboxMessageCount = totalcount
                        
                        let obj1 = messageList(date_id: date_id, first_name: name, msg_content: msg_content, msg_date: msg_date, msg_from: msg_from, msg_id: msg_id, msg_important_receiver: msg_important_receiver, msg_important_sender: msg_important_sender, msg_read_status: msg_read_status, msg_status: msg_status, msg_to: msg_to, photo1: photo1, photo1_approve: photo1_approve, trash_receiver: trash_receiver, trash_sender: trash_sender, totalcount: totalcount)
                        
                        
                        
                        self.important1Arr.append(obj1)
                        self.filterimportant1Arr.append(obj1)
                        
                        
                    }
                    }else{
                        self.important1Arr.removeAll()
                        self.filterimportant1Arr.removeAll()
                        
                        
                        let array = response["importantlist"] as! [AnyObject]
                        
                        for i in 0...array.count-1{
                            let obj = array[i]
                            let date_id = self.themes.checkNullValue(obj["date_id"] as AnyObject) as! String
                            let first_name = self.themes.checkNullValue(obj["first_name"] as AnyObject) as! String
                            let msg_content = self.themes.checkNullValue(obj["msg_content"] as AnyObject) as! String
                            let msg_date = self.themes.checkNullValue(obj["msg_date"] as AnyObject) as! String
                            let msg_from = self.themes.checkNullValue(obj["msg_from"] as AnyObject) as! String
                            let msg_id = self.themes.checkNullValue(obj["msg_id"] as AnyObject) as! String
                            let msg_important_receiver = self.themes.checkNullValue(obj["msg_important_receiver"] as AnyObject) as! String
                            let msg_important_sender = self.themes.checkNullValue(obj["msg_important_sender"] as AnyObject) as! String
                            let msg_read_status = self.themes.checkNullValue(obj["msg_read_status"] as AnyObject) as! String
                            let msg_status = self.themes.checkNullValue(obj["msg_status"] as AnyObject) as! String
                            let msg_to = self.themes.checkNullValue(obj["msg_to"] as AnyObject) as! String
                            let photo1 = self.themes.checkNullValue(obj["photo1"] as AnyObject) as! String
                            let photo1_approve = self.themes.checkNullValue(obj["photo1_approve"] as AnyObject) as! String
                            let trash_receiver = self.themes.checkNullValue(obj["trash_receiver"] as AnyObject) as! String
                            let trash_sender = self.themes.checkNullValue(obj["trash_sender"] as AnyObject) as! String
                            let name = first_name.capitalizingFirstLetter()
                            let totalcount = self.themes.checkNullValue(obj["totalcount"] as AnyObject) as! String
                            self.inboxMessageCount = totalcount
                            
                            let obj1 = messageList(date_id: date_id, first_name: name, msg_content: msg_content, msg_date: msg_date, msg_from: msg_from, msg_id: msg_id, msg_important_receiver: msg_important_receiver, msg_important_sender: msg_important_sender, msg_read_status: msg_read_status, msg_status: msg_status, msg_to: msg_to, photo1: photo1, photo1_approve: photo1_approve, trash_receiver: trash_receiver, trash_sender: trash_sender, totalcount: totalcount)
                            
                            
                            
                            self.important1Arr.append(obj1)
                            self.filterimportant1Arr.append(obj1)
                            
                            
                        }
                        self.inboxListArr = self.important1Arr
                        self.filteInboxArr = self.filterimportant1Arr
                    }
                    self.messageTableView.reloadData()
  
                } else {
                   // let result = response["result"] as! String
                   // self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func trashList_API(type:String){

        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:messagesTrashList, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
 
                    let array = response["trashlist"] as! [AnyObject]
                    
                    if(type == "normal"){
                        
                    
                    
                    self.trash1Arr.removeAll()
                    self.filtertrash1Arr.removeAll()
                    
                    
                    for i in 0...array.count-1{
                        
                        
                        let obj = array[i]
                        let date_id = self.themes.checkNullValue(obj["date_id"] as AnyObject) as! String
                        let first_name = self.themes.checkNullValue(obj["first_name"] as AnyObject) as! String
                        let msg_content = self.themes.checkNullValue(obj["msg_content"] as AnyObject) as! String
                        let msg_date = self.themes.checkNullValue(obj["msg_date"] as AnyObject) as! String
                        let msg_from = self.themes.checkNullValue(obj["msg_from"] as AnyObject) as! String
                        let msg_id = self.themes.checkNullValue(obj["msg_id"] as AnyObject) as! String
                        let msg_important_receiver = self.themes.checkNullValue(obj["msg_important_receiver"] as AnyObject) as! String
                        let msg_important_sender = self.themes.checkNullValue(obj["msg_important_sender"] as AnyObject) as! String
                        let msg_read_status = self.themes.checkNullValue(obj["msg_read_status"] as AnyObject) as! String
                        let msg_status = self.themes.checkNullValue(obj["msg_status"] as AnyObject) as! String
                        let msg_to = self.themes.checkNullValue(obj["msg_to"] as AnyObject) as! String
                        let photo1 = self.themes.checkNullValue(obj["photo1"] as AnyObject) as! String
                        let photo1_approve = self.themes.checkNullValue(obj["photo1_approve"] as AnyObject) as! String
                        let trash_receiver = self.themes.checkNullValue(obj["trash_receiver"] as AnyObject) as! String
                        let trash_sender = self.themes.checkNullValue(obj["trash_sender"] as AnyObject) as! String
                        let name = first_name.capitalizingFirstLetter()
                        let totalcount = self.themes.checkNullValue(obj["totalcount"] as AnyObject) as! String
                        self.inboxMessageCount = totalcount
                        
                        let obj1 = messageList(date_id: date_id, first_name: name, msg_content: msg_content, msg_date: msg_date, msg_from: msg_from, msg_id: msg_id, msg_important_receiver: msg_important_receiver, msg_important_sender: msg_important_sender, msg_read_status: msg_read_status, msg_status: msg_status, msg_to: msg_to, photo1: photo1, photo1_approve: photo1_approve, trash_receiver: trash_receiver, trash_sender: trash_sender, totalcount: totalcount)
                        
                        
                        
                        self.trash1Arr.append(obj1)
                        self.filtertrash1Arr.append(obj1)
                        
                        
                    }
                    }else{
                        self.trash1Arr.removeAll()
                        self.filtertrash1Arr.removeAll()
                        
                        
                        for i in 0...array.count-1{
                            
                            
                            let obj = array[i]
                            let date_id = self.themes.checkNullValue(obj["date_id"] as AnyObject) as! String
                            let first_name = self.themes.checkNullValue(obj["first_name"] as AnyObject) as! String
                            let msg_content = self.themes.checkNullValue(obj["msg_content"] as AnyObject) as! String
                            let msg_date = self.themes.checkNullValue(obj["msg_date"] as AnyObject) as! String
                            let msg_from = self.themes.checkNullValue(obj["msg_from"] as AnyObject) as! String
                            let msg_id = self.themes.checkNullValue(obj["msg_id"] as AnyObject) as! String
                            let msg_important_receiver = self.themes.checkNullValue(obj["msg_important_receiver"] as AnyObject) as! String
                            let msg_important_sender = self.themes.checkNullValue(obj["msg_important_sender"] as AnyObject) as! String
                            let msg_read_status = self.themes.checkNullValue(obj["msg_read_status"] as AnyObject) as! String
                            let msg_status = self.themes.checkNullValue(obj["msg_status"] as AnyObject) as! String
                            let msg_to = self.themes.checkNullValue(obj["msg_to"] as AnyObject) as! String
                            let photo1 = self.themes.checkNullValue(obj["photo1"] as AnyObject) as! String
                            let photo1_approve = self.themes.checkNullValue(obj["photo1_approve"] as AnyObject) as! String
                            let trash_receiver = self.themes.checkNullValue(obj["trash_receiver"] as AnyObject) as! String
                            let trash_sender = self.themes.checkNullValue(obj["trash_sender"] as AnyObject) as! String
                            let name = first_name.capitalizingFirstLetter()
                            let totalcount = self.themes.checkNullValue(obj["totalcount"] as AnyObject) as! String
                            self.inboxMessageCount = totalcount
                            
                            let obj1 = messageList(date_id: date_id,
                                                   first_name: name,
                                                   msg_content: msg_content,
                                                   msg_date: msg_date,
                                                   msg_from: msg_from,
                                                   msg_id: msg_id,
                                                   msg_important_receiver: msg_important_receiver,
                                                   msg_important_sender: msg_important_sender,
                                                   msg_read_status: msg_read_status,
                                                   msg_status: msg_status,
                                                   msg_to: msg_to,
                                                   photo1: photo1,
                                                   photo1_approve: photo1_approve,
                                                   trash_receiver: trash_receiver,
                                                   trash_sender: trash_sender, totalcount: totalcount)
                            
                            
                            
                            self.trash1Arr.append(obj1)
                            self.filtertrash1Arr.append(obj1)
                            
                            
                        }
                        self.inboxListArr = self.trash1Arr
                        self.filteInboxArr = self.filtertrash1Arr
                    }
                    self.messageTableView.reloadData()
                    
                   
  
                } else {
                
                   // let result = response["result"] as! String
                  //  self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
                
            }
            
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }

    }
    
  
    func makeimportant_API(imp_status:String ,msg_id:String,msg_type:String){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid,
                        "imp_status":imp_status,
                        "msg_id":msg_id,
                        "msg_type":msg_type] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:makeImportant, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    if(self.selectedIndex == 0){
                        self.backAction(from: "inbox")
                    }else if(self.selectedIndex == 1){
                        self.backAction(from: "sent")
                    }else if(self.selectedIndex == 2){
                        self.backAction(from: "important")
                    }else if(self.selectedIndex == 3){
                        self.backAction(from: "trash")
                    }
                   
                } else if(success == "2"){
                    if(self.selectedIndex == 0){
                        self.backAction(from: "inbox")
                    }else if(self.selectedIndex == 1){
                        self.backAction(from: "sent")
                    }else if(self.selectedIndex == 2){
                        self.backAction(from: "important")
                    }else if(self.selectedIndex == 3){
                        self.backAction(from: "trash")
                    }

                }else{
                    
                }
                
            }
            
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    func makeTrash_API(trash_status:String ,msg_id:String,msg_type:String){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid,
                        "trash_status":trash_status,
                        "msg_id":msg_id,
                        "msg_type":msg_type] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:makeTrash, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    if(self.selectedIndex == 0){
                        self.backAction(from: "inbox")
                    }else if(self.selectedIndex == 1){
                          self.backAction(from: "sent")
                    }else if(self.selectedIndex == 2){
                          self.backAction(from: "important")
                    }else if(self.selectedIndex == 3){
                          self.backAction(from: "trash")
                    }

                } else {
                    
                    // let result = response["result"] as! String
                    //  self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
                
            }
            
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    
    
    
    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }

}

//extension Array where Element: Equatable {
//    func indexes(of element: Element) -> [Int] {
//        return self.enumerated().filter({ element == $0.element }).map({ $0.offset })
//    }
//}
extension Array where Element: Equatable {
    func whatFunction(_ value :  Element) -> [Int] {
        return self.indices.filter {self[$0] == value}
    }
}
extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
   
   

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
