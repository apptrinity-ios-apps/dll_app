//
//  MessageCell.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 15/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

   
    @IBOutlet var userProfView: UIImageView!
    @IBOutlet var nameLabel: UILabel!    
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var mesgLabel: UILabel!
    @IBOutlet var starImgView: UIImageView!
    @IBOutlet var starButn: UIButton!
    
    @IBOutlet var messageCountLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        userProfView.layer.cornerRadius = userProfView.frame.size.height/2
        userProfView.layer.masksToBounds = true
        
         messageCountLbl.layer.cornerRadius = messageCountLbl.frame.size.height/2
        messageCountLbl.layer.masksToBounds = true
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
