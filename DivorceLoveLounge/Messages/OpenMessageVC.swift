//
//  OpenMessageVC.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 15/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
protocol backBtnClickDelegate2 {
    
    func backAction(from:String)
    
}
class OpenMessageVC: UIViewController,UITextViewDelegate {

    @IBOutlet var topView: UIView!
    @IBOutlet var userImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var dltBtn: UIButton!
    @IBOutlet var dltImage: UIImageView!
    @IBOutlet var starBtn: UIButton!
    @IBOutlet var starImage: UIImageView!
    @IBOutlet var messageTextView: UITextView!
    @IBOutlet var replyBtn: UIButton!
    @IBOutlet var msg_type: UILabel!
    
     var delegate : backBtnClickDelegate2?
    var themes = Themes()
    var urlService = URLservices.sharedInstance

    
    var data_dict = [String:AnyObject]()
    var msg_Type = ""
    
    
    var date_id = String()
    var first_name = String()
    var msg_content = String()
    var msg_date = String()
    var msg_from = String()
    var msg_id = String()
    var msg_important_receiver = String()
    var msg_important_sender = String()
    var msg_read_status = String()
    var msg_status = String()
    var msg_to = String()
    var photo1 = String()
    var photo1_approve = String()
    var trash_receiver = String()
    var trash_sender = String()
    
    
    @IBAction func backAction(_ sender: Any) {
        
        if(msg_Type == "Important"){
            delegate?.backAction(from: "Important")
        }else if(msg_Type == "Trash"){
             delegate?.backAction(from: "Trash")
        }

        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageTextView.delegate = self
        messageTextView.isEditable = false
        
        
        topView.layer.shadowOffset = CGSize(width:0, height: 2)
        topView.layer.shadowOpacity = 2.0
        topView.layer.shadowRadius = 2
        topView.layer.masksToBounds = false
        topView.layer.shadowColor = UIColor.gray.cgColor
        replyBtn.layer.shadowOffset = CGSize(width:0, height: 2)
        replyBtn.layer.shadowOpacity = 2.0
        replyBtn.layer.shadowRadius = 2
        replyBtn.layer.masksToBounds = false
        replyBtn.layer.shadowColor = UIColor.gray.cgColor
        replyBtn.layer.cornerRadius = replyBtn.frame.size.height/2
        
        userImage.layer.cornerRadius = userImage.frame.size.height/2
        userImage.layer.masksToBounds = true
        userImage.layer.shadowOffset = CGSize(width:0, height: 2)
        userImage.layer.shadowOpacity = 2.0
        userImage.layer.shadowRadius = 2
        userImage.layer.masksToBounds = false
        userImage.layer.shadowColor = UIColor.gray.cgColor

        
        if msg_Type == "Trash"{
            
        replyBtn.isHidden = true
            
            
        starImage.image = UIImage(named: "black_undo")
            
            
            
        }else{
            
            let msg_from = self.msg_from
            let msg_important_sender = self.msg_important_sender
            let msg_important_receiver = self.msg_important_receiver
            let my_id = self.themes.getUserId()
            if msg_from == my_id {
                if msg_important_sender == "Yes"{
                    starImage.image = UIImage(named: "star")
                }else{
                    starImage.image = UIImage(named: "starDefault-1")
                }
            }else{
                if msg_important_receiver == "Yes"{
                    starImage.image = UIImage(named: "star")
                }else{
                    starImage.image = UIImage(named: "starDefault-1")
                }
                
            }
            replyBtn.isHidden = false
        }
        
        
    
        nameLabel.text  = first_name
        messageTextView.text   = msg_content
        msg_type.text = msg_Type
        
        
        let photoApproved = photo1_approve
        let imageUrl = photo1
        if photoApproved == "APPROVED" {
            let fullimageUrl = ImagebaseUrl + imageUrl
            DispatchQueue.main.async {
                self.userImage.sd_setImage(with: URL(string: fullimageUrl), placeholderImage:UIImage(named: "defaultPic"), completed: { (image, error, cacheType, url) -> Void in
                    if ((error) != nil) {
                       self.userImage.image = UIImage(named: "defaultPic")
                    } else {
                       self.userImage.image = image
                    }
                })
            }
        }else{
           self.userImage.image = UIImage(named: "defaultPic")
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func replyAction(_ sender: Any) {
        
       
        let Compose = self.storyboard?.instantiateViewController(withIdentifier: "ComposeVC") as! ComposeVC
        Compose.selectedName = self.first_name

        if msg_Type == "Inbox"{
            Compose.selectedId = self.msg_from
            
        }else if msg_Type == "sent"{
            
            Compose.selectedId = self.msg_to
            
        }else if msg_Type == "Important"{
            if self.msg_from == self.themes.getUserId() {
                
                Compose.selectedId = self.msg_to
            }else{
                
                 Compose.selectedId = self.msg_from
            }
        }
        
        Compose.fromScreen = "reply"
        self.navigationController?.pushViewController(Compose, animated: true)
       
        
    }
    

    
    @IBAction func deleteAction(_ sender: Any) {
      
        var status = ""
        var msg_type = ""
//        if starImage.image == UIImage(named: "starDefault-1") {
//            starImage.image = UIImage (named: "star")
//            status = "Yes"
//        }else{
//            starImage.image = UIImage (named: "starDefault-1")
//            status = "No"
//        }
        
        let msg_from = self.msg_from
        let msg_id = self.msg_id
        let my_id = self.themes.getUserId()
        
        if msg_from == my_id {
            msg_type = "sender"
        }else{
            msg_type = "reciever"
            
        }
        
          if msg_Type == "Trash"{
            
            trash_API(trash_status: "No", msg_id: msg_id, msg_type: msg_type)
            
          }else{
           trash_API(trash_status: "Yes", msg_id: msg_id, msg_type: msg_type)
        }
        
        
        
      trash_API(trash_status: "Yes", msg_id: msg_id, msg_type: msg_type)
        
        
    }
    
    @IBAction func starAction(_ sender: Any) {
        
      var status = ""
        var msg_ty = ""
        
        
        
        let msg_from = self.msg_from
        let msg_id = self.msg_id
        let my_id = self.themes.getUserId()
        
        if msg_from == my_id {
            msg_ty = "sender"
        }else{
            msg_ty = "reciever"
            
        }
        
        
        if msg_Type == "Trash"{
            
            trash_API(trash_status: "No", msg_id: msg_id, msg_type: msg_ty)
            
        }else{
            
            if starImage.image == UIImage(named: "starDefault-1") {
                starImage.image = UIImage (named: "star")
                status = "Yes"
            }else{
                starImage.image = UIImage (named: "starDefault-1")
                status = "No"
            }
            
            
            self.makeimportant_API(imp_status: status, msg_id: msg_id, msg_type: msg_ty)
        }
        
        
        
        //self.makeimportant_API(imp_status: status, msg_id: msg_id, msg_type: msg_type)
        
    }
    
    func trash_API(trash_status:String ,msg_id:String,msg_type:String){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid,
                        "trash_status":trash_status,
                        "msg_id":msg_id,
                        "msg_type":msg_type] as [String:Any]
            
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:makeTrash, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    self.navigationController?.popViewController(animated: false)
                    
                } else {
                    
                    // let result = response["result"] as! String
                    //  self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
                
            }
            
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    
    
    func makeimportant_API(imp_status:String ,msg_id:String,msg_type:String){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid,
                        "imp_status":imp_status,
                        "msg_id":msg_id,
                        "msg_type":msg_type] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:makeImportant, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    
                    
                } else {
                    
                    // let result = response["result"] as! String
                    //  self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
                
            }
            
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    

}
