//
//  ComposeVC.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 15/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
protocol backBtnClickDelegate3 {
    func backAction(from:String)
}
class ComposeVC: UIViewController,UITextFieldDelegate,UITextViewDelegate,UITableViewDelegate,UITableViewDataSource{
   var delegate : backBtnClickDelegate3?
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    var userName = String()
    var msgType = String()
   var window: UIWindow?
    
    var selectedRow = Int()
    @IBOutlet var hideView: UIView!
    @IBOutlet var popupView: UIView!
    @IBOutlet var searchField: UITextField!
    @IBOutlet var namestableView: UITableView!
    @IBOutlet var nameView: UIView!
    @IBOutlet var selectNameField: UITextField!
    @IBOutlet var messageView: UIView!
    @IBOutlet var mesgTextView: UITextView!
    @IBOutlet var sendBtn: UIButton!
    @IBOutlet var doneBtn: UIButton!
    var data = [AnyObject]()
    var namesArray = Array<AnyObject>()
    var fileterNamesArray = Array<AnyObject>()
    var selectedName = String()
    var selectedId = String()
    var fromScreen = String()
    var userID = ""

    @IBAction func backAction(_ sender: Any) {
        if(msgType == "inbox"){
            delegate?.backAction(from: "inbox")
        }else if(msgType == "sent"){
            delegate?.backAction(from: "sent")
        }else if(msgType == "trash"){
            delegate?.backAction(from: "trash")
        }else if(msgType == "important"){
            delegate?.backAction(from: "important")
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedRow = -1
        selectNameField.text = userName
         self.namestableView.register(UINib(nibName: "countryTableCell", bundle: nil), forCellReuseIdentifier: "countryTableCell")
         searchField.autocapitalizationType = .sentences
        selectNameField.delegate = self
        mesgTextView.delegate = self
        nameView.layer.cornerRadius = 3
        nameView.layer.shadowOffset = CGSize(width:0, height: 2)
        nameView.layer.shadowOpacity = 1
        nameView.layer.shadowRadius = 1
        nameView.layer.masksToBounds = false
        nameView.layer.shadowColor = UIColor.gray.cgColor
        messageView.layer.cornerRadius = 3
        messageView.layer.shadowOffset = CGSize(width:0, height: 2)
        messageView.layer.shadowOpacity = 1
        messageView.layer.shadowRadius = 1
        messageView.layer.masksToBounds = false
        messageView.layer.shadowColor = UIColor.gray.cgColor
        sendBtn.layer.cornerRadius = sendBtn.frame.size.height/2
        sendBtn.layer.shadowOffset = CGSize(width:0, height: 2)
        sendBtn.layer.shadowOpacity = 2.0
        sendBtn.layer.shadowRadius = 1
        sendBtn.layer.masksToBounds = false
        sendBtn.layer.shadowColor = UIColor.gray.cgColor
        doneBtn.layer.cornerRadius = doneBtn.frame.height/2
        searchField.layer.cornerRadius = searchField.frame.height/2
        searchField.delegate = self
        searchField.layer.borderWidth = 1
        searchField.layer.borderColor = UIColor.lightGray.cgColor
        // For Right side Padding
        searchField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: searchField.frame.height))
        searchField.leftViewMode = .always
        // For left side Padding
        searchField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: searchField.frame.height))
        searchField.rightViewMode = .always
        
         self.namestableView.register(UINib(nibName: "countryTableCell", bundle: nil), forCellReuseIdentifier: "countryTableCell")

        popupView.layer.cornerRadius = 5
        getComposeUserList()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if fromScreen == "reply"{
            
            self.selectNameField.text = selectedName
           selectNameField.isUserInteractionEnabled = false
            
        }else if(fromScreen == "detailVC"){
            self.selectNameField.text = selectedName
            selectNameField.isUserInteractionEnabled = false
  
        }
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return namesArray.count
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "countryTableCell", for: indexPath)as! countryTableCell
            tableView.separatorColor = UIColor.clear
            cell.selectionStyle = .none
            cell.backView.layer.cornerRadius = cell.backView.frame.size.height/2
            cell.backView.layer.borderWidth = 1
            cell.backView.layer.borderColor = UIColor.lightGray.cgColor
        
      
        cell.countryLbl.text = namesArray[indexPath.row] as! String
            if(selectedRow == indexPath.row){
                cell.backView.backgroundColor = darkBrownColor
                cell.countryLbl.textColor = UIColor.white
            }else{
                cell.backView.backgroundColor = UIColor.clear
                cell.countryLbl.textColor = UIColor.black
            }
            return cell
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRow = indexPath.row
        
        searchField.text = namesArray[indexPath.row] as? String
        
        selectedName = searchField.text!
  
        namestableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 50
    }

    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        
        if fromScreen == "reply"{
           
            popupView.isHidden = true
            hideView.isHidden = true
        }else if(fromScreen == "detailVC"){
            popupView.isHidden = false
            hideView.isHidden = false
        }
        
        
       
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        if textField == searchField {
            namesArray.removeAll()
            
            namesArray = fileterNamesArray.filter{ $0.localizedCaseInsensitiveContains(searchField.text!) }
            if namesArray.count == 0 || searchField.text == ""{
                
                namesArray = fileterNamesArray
            }
            else {
                
            }
            DispatchQueue.main.async {
                self.namestableView.reloadData()
            }
        }
        
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
    
    @IBAction func sendAction(_ sender: Any) {
        
        if self.themes.getplanStatus() == "1"{
            if(selectNameField.text == "" ){
                 themes.showToast(message: "Please select name", sender: self)
            }else if(mesgTextView.text == ""){
                 themes.showToast(message: "Please enter message", sender: self)
            }else{
                compose_API(send_id: selectedId, content: mesgTextView.text)
            }
 
        }else{
            
          // Naveen
//
//            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopViewController") as! PopViewController
//            popOverVC.fromScreen = ""
//            //                    popOverVC.ImageStr = ""
//            popOverVC.headingMesg = "Alert"
//            popOverVC.subMesg = "You need an active membership plan to send Messages. Proceed to membership plans?"
//            self.addChildViewController(popOverVC)
//            popOverVC.view.center = self.view.frame.center
//            self.view.addSubview(popOverVC.view)
//            popOverVC.didMove(toParentViewController: self)
//
            // Naveen
            
//            let alertController = UIAlertController(title: "Alert", message: "You need an active membership plan to send Messages. Proceed to membership plans?", preferredStyle: .alert)
//            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
//                print("Ok button tapped");
//                let MemberShipViewController = self.storyboard?.instantiateViewController(withIdentifier: "MemberShipViewController") as! newme
//                self.navigationController?.pushViewController(MemberShipViewController, animated: true)
//            }
//            alertController.addAction(OKAction)
//            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
//
//            }
//            alertController.addAction(cancelAction)
//            self.present(alertController, animated: true, completion:nil)

        }

    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func compose_API(send_id:String,content:String){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid,
                        "send_to":send_id,
                        "content":content] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:composeMessage, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    if(self.msgType == "inbox"){
                        self.delegate?.backAction(from: "inbox")
                    }else if(self.msgType == "sent"){
                        self.delegate?.backAction(from: "sent")
                    }else if(self.msgType == "trash"){
                        self.delegate?.backAction(from: "trash")
                    }else if(self.msgType == "important"){
                        self.delegate?.backAction(from: "important")
                    }
                    
                  
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
                    vc.fromscreen = "inbox"
                    self.navigationController?.pushViewController(vc, animated: true)

                    
                } else {
                    
                    // let result = response["result"] as! String
                    //  self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
                
            }
            
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    @IBAction func popUpCloseAction(_ sender: Any) {
        selectNameField.text = ""
        popupView.isHidden = true
        hideView.isHidden = true
    }
    @IBAction func popUpDoneAction(_ sender: Any) {
        popupView.isHidden = true
        hideView.isHidden = true
        
        selectNameField.text = selectedName
        
        for i in 0...fileterNamesArray.count-1{
            let obj = fileterNamesArray[i] as! String
            
            if(obj == selectedName){
                
                let obj = data[i]
                selectedId = obj["id"] as! String

            }
        }
    }
    
    @IBAction func nameBtnAction(_ sender: Any) {
        
        
        if fromScreen == "reply"{
          // searchField.text = ""
            popupView.isHidden = true
            hideView.isHidden = true
        }else if(fromScreen == "detailVC"){
            popupView.isHidden = true
            hideView.isHidden = true
        }else{
            searchField.text = ""
            popupView.isHidden = false
            hideView.isHidden = false
        }
    
        

    }
    func getComposeUserList(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid]
            
            
            //            let parameters = ["gender":gender, "looking_for":lookingFor,"first_name":firstName, "zipcode":zipCode,"state":state,"email":mailField.text!,"password":passwordField.text!] as [String : String]
            
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:composeUsersList, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    self.data = response["data"] as! [AnyObject]
                    
                    
                    for i in 0...self.data.count-1{
                        let obj = self.data[i]
                        self.namesArray.append(obj["first_name"] as AnyObject)
                        self.fileterNamesArray.append(obj["first_name"] as AnyObject)
                        
                        
                    }
                    
                    self.namestableView.reloadData()
                    
                    
                
                }else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }

    
   
}
