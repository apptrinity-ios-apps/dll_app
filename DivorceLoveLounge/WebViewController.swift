//
//  WebViewController.swift
//  DivorceLoveLounge
//
//  Created by nagaraj  kumar on 19/08/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class WebViewController: UIViewController,UIWebViewDelegate {
    var fromScreen = String()
    var themes = Themes()
    @IBOutlet var webview: UIWebView!
    
    @IBOutlet var headinglabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if(fromScreen == "Terms"){
            headinglabel.text = "Terms of Use"
            let url = URL (string: "https://divorcelovelounge.com/appTermsOfUse")
            let requestObj = URLRequest(url: url!)
            webview.loadRequest(requestObj)
        }else if(fromScreen == "Privacy"){
             headinglabel.text = "Privacy policy"
            let url = URL (string: "https://divorcelovelounge.com/privacyPolicy")
            let requestObj = URLRequest(url: url!)
            webview.loadRequest(requestObj)
        }
  
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.themes.showActivityIndicator(uiView: self.view)
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.themes.hideActivityIndicator(uiView: self.view)
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.themes.hideActivityIndicator(uiView: self.view)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
