//
//  ContactTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/30/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var mailField: UITextField!
    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var sendBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.nameField.layer.cornerRadius = nameField.frame.size.height/2
        self.nameField.layer.borderWidth = 1
        
        self.nameField.layer.borderColor = UIColor.lightGray.cgColor
        nameField.attributedPlaceholder = NSAttributedString(string: "Name",
       attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: CGFloat(170/255.0), green: CGFloat(170/255.0), blue: CGFloat(170/255.0), alpha: CGFloat(1.0) )])
        // For Right side Padding
        nameField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: nameField.frame.height))
        nameField.leftViewMode = .always
        // For left side Padding
        nameField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: nameField.frame.height))
        nameField.rightViewMode = .always
        
        self.mailField.layer.cornerRadius = mailField.frame.size.height/2
        self.mailField.layer.borderWidth = 1
        self.mailField.layer.borderColor = UIColor.lightGray.cgColor
        mailField.attributedPlaceholder = NSAttributedString(string: "Email",
       attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: CGFloat(170/255.0), green: CGFloat(170/255.0), blue: CGFloat(170/255.0), alpha: CGFloat(1.0) )])
        // For Right side Padding
        mailField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: mailField.frame.height))
        mailField.leftViewMode = .always
        // For left side Padding
        mailField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: mailField.frame.height))
        mailField.rightViewMode = .always
        
        self.phoneNumberField.layer.cornerRadius = phoneNumberField.frame.size.height/2
        self.phoneNumberField.layer.borderWidth = 1
        self.phoneNumberField.layer.borderColor = UIColor.lightGray.cgColor
        phoneNumberField.attributedPlaceholder = NSAttributedString(string: "Phone",
        attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: CGFloat(170/255.0), green: CGFloat(170/255.0), blue: CGFloat(170/255.0), alpha: CGFloat(1.0) )])
        // For Right side Padding
        phoneNumberField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: phoneNumberField.frame.height))
        phoneNumberField.leftViewMode = .always
        // For left side Padding
        phoneNumberField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: phoneNumberField.frame.height))
        phoneNumberField.rightViewMode = .always
        
        self.messageTextView.layer.cornerRadius = 8
        self.messageTextView.layer.borderWidth = 1
        self.messageTextView.layer.borderColor = UIColor.lightGray.cgColor
        self.sendBtn.layer.cornerRadius = 8
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.addGestureRecognizer(tapGesture)
        
        // Initialization code
    }
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        nameField.resignFirstResponder()
        mailField.resignFirstResponder()
        phoneNumberField.resignFirstResponder()
        messageTextView.resignFirstResponder()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)

     //Configure the view for the selected state
    }
    
}

