//
//  UpgradeSignUpCollectionViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 6/10/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
import QuartzCore

protocol membershipPlanDelegate {
    
    func CanReceivePlanData(discount:String,planName:String,amount:String)
    
}


class UpgradeSignUpCollectionViewCell: UICollectionViewCell,UITableViewDelegate,UITableViewDataSource {
    
   
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    var selectedIndex = Int()
    var delegate : membershipPlanDelegate?
    
    @IBOutlet var strikeLabel: UILabel!
    @IBOutlet weak var offerView: UIView!
    @IBOutlet weak var offerPercentLabel: UILabel!
    @IBOutlet weak var planLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var aboveView: UIView!    
    @IBOutlet weak var costLabel: UILabel!    
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var signMeBtn: UIButton!
    @IBOutlet var durationBtn: UIButton!
     @IBOutlet var label1: UILabel!
    @IBOutlet var label2: UILabel!
    @IBOutlet var label3: UILabel!
    @IBOutlet var label4: UILabel!
    @IBOutlet var label5: UILabel!
    @IBOutlet var label6: UILabel!
    @IBOutlet var label7: UILabel!
    @IBOutlet var label8: UILabel!
    
    @IBOutlet weak var dataTableview: UITableView!
    
    var dataDict = [String : AnyObject]()
    var monthArr = [AnyObject]()

    var selectedint = Int()
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        monthArr.removeAll()
        dataTableview.delegate = self
        dataTableview.dataSource = self
        
        discountLabel.intrinsicContentSize.width
        
        self.dataTableview.register(UINib(nibName: "memberShipDataCell", bundle: nil), forCellReuseIdentifier: "memberShipDataCell")
        self.dataTableview.register(UINib(nibName: "months", bundle: nil), forCellReuseIdentifier: "months")
         self.dataTableview.register(UINib(nibName: "popUpCell", bundle: nil), forCellReuseIdentifier: "popUpCell")
        
        offerView.layer.cornerRadius = (offerView.frame.size.height)/2

        backGroundView.layer.cornerRadius = 8
        bgImageView.layer.cornerRadius = 8
        aboveView.layer.cornerRadius = 8
        bgImageView.layer.masksToBounds = true
        signMeBtn.layer.cornerRadius = (signMeBtn.frame.size.height)/2
        signMeBtn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        signMeBtn.layer.shadowOpacity = 1.0
        signMeBtn.layer.shadowRadius = 3
        signMeBtn.layer.masksToBounds = false
        signMeBtn.layer.shadowColor = UIColor.gray.cgColor
        
        

        self.signMeBtn.applyGradient(colors: [UIColor(red: 195/255, green: 41/255, blue: 37/255, alpha: 1).cgColor, UIColor(red: 226/255, green: 98/255, blue: 47/255, alpha: 1).cgColor])
        
        
        if dataDict.isEmpty {
       
        }else{
            
            
            
            
            if self.themes.getplanStatus() == "1" {
                
                let planAmnt = dataDict["plan_amount"] as! String
                let discAmnt = dataDict["discount_amount"] as! String
                
                let packageType = dataDict["package_type"] as! String
                
                if(packageType != "test"){
                    let dict2 = ["month" : "1Month", "amount" : planAmnt, "discount" : discAmnt, "durationId" : "one", "membershipId" : "1"]
                                 monthArr.append(dict2 as AnyObject)
                    
                }
          
            } else {
                let planAmnt = dataDict["plan_amount"] as! String
                let discAmnt = dataDict["discount_amount"] as! String
                let dict = ["month" : "Free", "amount" : "0", "discount" : "0", "durationId" : "one", "membershipId" : "1"]
                let dict2 = ["month" : "1Month", "amount" : planAmnt, "discount" : discAmnt, "durationId" : "one", "membershipId" : "1"]
                 monthArr.append(dict as AnyObject)
                monthArr.append(dict2 as AnyObject)
            }
            let duration = dataDict["durations"] as! AnyObject
            for i in 0..<duration.count {
                let dict = duration.object(at: i)
                monthArr.append(dict as AnyObject)
            }
            
            
        }

        // Initialization code
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            
        let cell = tableView.dequeueReusableCell(withIdentifier: "popUpCell") as! popUpCell
        cell.selectionStyle = .none
        cell.popUpLabel.text = "Choose Months"
        cell.popUpLabel.font = UIFont.init(name: "Poppins-Medium", size: 15)
        
        return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "popUpCell") as! popUpCell
            cell.selectionStyle = .none
            cell.popUpLabel.text = "Select Plans"
            cell.popUpLabel.font = UIFont.init(name: "Poppins-Medium", size: 15)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return monthArr.count
        } else {
        return 8
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
             let cell = tableView.dequeueReusableCell(withIdentifier: "months", for: indexPath) as! months
            cell.selectionStyle = .none
            let obj = monthArr[indexPath.row] as! [String : AnyObject]
            
             if self.themes.getplanStatus() == "1" {
                if indexPath.row >= 1{
                    cell.monthsLbl.text = (obj["month"] as! String) + "Months"
                }else{
                    cell.monthsLbl.text = (obj["month"] as! String)
                }
             }else{
                let some = (obj["month"] as! String)
              if some == "Free" {
                     cell.monthsLbl.text = (obj["month"] as! String)
              } else {
                 if indexPath.row >= 2{
                    cell.monthsLbl.text = (obj["month"] as! String) + " Months"
                 }else{
                    cell.monthsLbl.text = (obj["month"] as! String) 
                }
                
                
                }
            }
            if selectedint == indexPath.row{
                cell.checkImg.image = UIImage(named: "circle_selected")
            }else{
                cell.checkImg.image = UIImage(named: "circle_default")
            }
            return cell
        } else {
         let cell = tableView.dequeueReusableCell(withIdentifier: "memberShipDataCell", for: indexPath) as! memberShipDataCell
        tableView.separatorColor = UIColor.clear
        cell.selectionStyle = .none
        if indexPath.row == 0 {
            let viewedme = dataDict["viewedme"] as! String
            if viewedme == "Yes"{
               cell.nameLabel.text = "See who's viewed you"
                
            }else{
              cell.nameLabel.text = "Can't see who's viewed you"
            }
           
        }else if indexPath.row == 1 {
            
           let favoritedme = dataDict["favoritedme"] as! String
            if favoritedme == "Yes"{
                
                cell.nameLabel.text = "See who's Favourited you"
                
            }else{
                cell.nameLabel.text = "Can't see who's Favourited you"
            }
            
        }
        else if indexPath.row == 2 {
            
            let plan_msg = dataDict["plan_msg"] as! String
                cell.nameLabel.text = "Unlimited  " + " messaging"
            
            
        }
        else if indexPath.row == 3 {
            
            let profile = dataDict["profile"] as! String
            cell.nameLabel.text =  "View " + "Unlimited " + " profiles"
            
            
            
        }
        else if indexPath.row == 4 {
            
            let interest = dataDict["interest"] as! String
            cell.nameLabel.text =  "Sent " + "Unlimited " + " interest"
            
            
            
        }
        else if indexPath.row == 5 {
            
            let plan_contacts = dataDict["plan_contacts"] as! String
            cell.nameLabel.text =  "View " + "Unlimited " + " contacts"
            
            
            
        }
        else if indexPath.row == 6 {
            
            let chat = dataDict["chat"] as! String
            if chat == "Yes"{
                
                cell.nameLabel.text = "Live chat is available"
                
            }else{
                cell.nameLabel.text = "Live chat is Not available"
            }
          
        }
        else if indexPath.row == 7 {
            
        cell.nameLabel.text = "Access more matches"
           
        }
       
        return cell
        
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            
            
            let obj = monthArr[indexPath.row] as! [String : AnyObject]
            
                    let month = obj["month"] as! String
                    let discount = obj["discount"] as! String
                    let amount = obj["amount"] as! String
                 
            delegate?.CanReceivePlanData(discount: discount, planName: month, amount: amount)
            
            selectedint = indexPath.row
            dataTableview.reloadData()
        }
   
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        return 44
    }
    
}
    extension UIButton
    {
        func applyGradient(colors: [CGColor])
        {
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = colors
            gradientLayer.cornerRadius = self.frame.size.height/2
            gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer.endPoint = CGPoint(x: 1, y: 0)
            gradientLayer.frame = self.bounds
            self.layer.insertSublayer(gradientLayer, at: 0)
        }
    }


