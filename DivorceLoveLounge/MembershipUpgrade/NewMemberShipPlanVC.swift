//
//  NewMemberShipPlanVC.swift
//  DivorceLoveLounge
//
//  Created by nagaraj  kumar on 31/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class NewMemberShipPlanVC: UIViewController,UITableViewDelegate,UITableViewDataSource,NewMebershipDataDelegate  {


// Delegates Methods

//UITableViewDelegate,UITableViewDataSource,NewMebershipDataDelegate {

    var themes = Themes()
    var urlService = URLservices.sharedInstance


    @IBOutlet var headerLBL: UILabel!
    @IBOutlet var newMemeberShipTableView: UITableView!


    var planAmount = String()
    var planDiscount = String()

    var PlanStatus = ""
    var palnDetails = [String:AnyObject]()
    var dataArr = Array<AnyObject>()
    var priceArray = [AnyObject?]()
    var fromPushNotify = ""

    var adsListArr = [AnyObject]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.newMemeberShipTableView.register(UINib(nibName: "MyMemberPlanCell", bundle: nil), forCellReuseIdentifier: "MyMemberPlanCell")
        self.newMemeberShipTableView.register(UINib(nibName: "UpGradePlanCell", bundle: nil), forCellReuseIdentifier: "UpGradePlanCell")
        
        
       
        
        self.get_MyPaln_APi()
        self.membershipListAPI()
        self.adsListServiceApi(ScreenType: "Membership")
        
        
    }

    func adsListServiceApi(ScreenType:String){
        self.urlService.adslistServiceCall(ScreenType: ScreenType) { response in
            let success = response["status"] as! String
            if(success == "1"){
                let data = response["data"] as! [AnyObject]
                self.adsListArr = data
                self.newMemeberShipTableView.reloadData()
            }else{
                self.newMemeberShipTableView.reloadData()

            }
        }
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if section == 0 {
             if self.PlanStatus == "1" {
                return 1
              }else{
                return 1
              }

        }else{
           return 1
        }

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if self.PlanStatus == "1"{
                if self.adsListArr.count > 0 {
                        return 340
                }else{
                    return 221
                }
            }else{
             return 221
            }

        }else{
            return 685
        }



    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {


        if indexPath.section == 0 {

         let cell = tableView.dequeueReusableCell(withIdentifier: "MyMemberPlanCell", for: indexPath) as! MyMemberPlanCell

            cell.planNameLbl.text = themes.checkNullValue(palnDetails["package_type"]) as? String
            let days = "\(themes.checkNullValue(palnDetails["plan_duration"]) as? String ?? "") Days"
            cell.durationLbl.text = days
            cell.paymentDateLbl.text = themes.checkNullValue(palnDetails["plan_start_date"]) as? String

            cell.upgradePlanBtn.addTarget(self, action: #selector(upGradePlanBtnClicked(button:)), for:.touchUpInside)

            cell.adsListArr = self.adsListArr
            if self.adsListArr.count > 0 {
                cell.adsHieghtConstant.constant = 120
                if self.adsListArr.count > 1 {
                    cell.setTimer()
                    }
            }else{
                cell.adsHieghtConstant.constant = 0
            }
            cell.adsCollectionView.reloadData()
            
            if self.PlanStatus == "1"{
              cell.backView.isHidden = false
             cell.upgradePlanBtn.isHidden = false
            }else{
               cell.backView.isHidden = true
                cell.upgradePlanBtn.isHidden = true
            }

          return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpGradePlanCell", for: indexPath) as! UpGradePlanCell
          //  cell.backgroundColor = ligtGray
            cell.priceArray = self.priceArray
            cell.dataArr = self.dataArr
            cell.planAmount = self.planAmount
            cell.planDiscount = self.planDiscount
            cell.delegate = self
            cell.planColloctionView.reloadData()
            return cell
         }

      }

      @objc func upGradePlanBtnClicked(button: UIButton) {
           self.newMemeberShipTableView.scrollToRow(at: IndexPath(row: 0, section: 1), at: .bottom, animated: false)
    }


    func Can_Receive_New_Plan_Data(discount: String, planAmount: String, plan_month: String, plan_Duration: String, dataDict: [String : AnyObject]) {


        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MembershipPaymentVC") as! MembershipPaymentVC

            vc.dataDict = dataDict
            vc.discount = discount
            vc.planeamount = planAmount
            vc.monthplan = plan_month
            vc.plan_duration = plan_Duration

        self.navigationController?.pushViewController(vc, animated: true)



    }

    func get_MyPaln_APi(){

        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:myPlan, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){

                    self.palnDetails = response["plandetails"] as! [String:AnyObject]
                    self.PlanStatus = self.palnDetails["status"] as! String
                    self.newMemeberShipTableView.reloadData()


                } else {
                    let result = response["result"] as! String
                    self.themes.showToast(message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }

    }


    func membershipListAPI(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            urlService.serviceCallGetMethod(url: getMembershipList, completionHandler: { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){

                    self.dataArr = response["data"] as! [AnyObject]
                    let obj = self.dataArr[0]
                    self.planAmount = obj["plan_amount"] as! String
                    self.planDiscount = obj["discount_amount"] as! String

                    for i in 0..<self.dataArr.count {
                        self.priceArray.append(nil)
                    }

                    self.newMemeberShipTableView.delegate = self
                    self.newMemeberShipTableView.dataSource = self
                    self.newMemeberShipTableView.reloadData()

                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            })
        }else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }











    @IBAction func backAction(_ sender: Any) {

        if fromPushNotify == "Push" {
            let mainVc = storyboard?.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
            self.navigationController?.pushViewController(mainVc, animated: true)
        } else {
       self.navigationController?.popViewController(animated: false)
        }

    }

    
    
    
    
    

}
