//
//  UpGradePlanCell.swift
//  DivorceLoveLounge
//
//  Created by nagaraj  kumar on 31/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
import MMBannerLayout


protocol NewMebershipDataDelegate {
    
    func Can_Receive_New_Plan_Data(discount:String,planAmount:String,plan_month:String,plan_Duration:String,dataDict:[String:AnyObject])
    
}


class UpGradePlanCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,BannerLayoutDelegate,membershipPlanDelegate {

    
    @IBOutlet var planColloctionView: UICollectionView!
    
    var dataArr = Array<AnyObject>()
    var priceArray = [AnyObject?]()
    
    var planAmount = String()
    var planDiscount = String()
    
     var delegate : NewMebershipDataDelegate?
    
    func CanReceivePlanData(discount: String, planName: String, amount: String) {
        planAmount = amount
        planDiscount = discount
        planColloctionView.reloadData()
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
       self.planColloctionView.register(UINib(nibName: "UpgradeSignUpCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "UpgradeSignUpCollectionViewCell")
        
        
        (planColloctionView.collectionViewLayout as? MMBannerLayout)?.autoPlayStatus = .none
      // self.view.layoutIfNeeded()
        
        planColloctionView.showsHorizontalScrollIndicator = false
        if let layout = planColloctionView.collectionViewLayout as? MMBannerLayout {
            layout.itemSpace = 10
            layout.itemSize = self.planColloctionView.frame.insetBy(dx: 5, dy: 20).size
            layout.minimuAlpha = 0.9
            layout.angle = 0
        }
        
        
        
        
        
        
        self.planColloctionView.delegate = self
        self.planColloctionView.dataSource = self
        
        
    }

    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UpgradeSignUpCollectionViewCell", for: indexPath)as! UpgradeSignUpCollectionViewCell
        cell.layer.masksToBounds = true
        cell.layer.cornerRadius = 8
        cell.delegate = self
        let obj = dataArr[indexPath.row]
        cell.dataDict = obj as! [String : AnyObject]
        cell.planLabel.text = obj["package_type"] as? String
        let pricearr =  priceArray[indexPath.row]
        if pricearr == nil {
            if(self.planAmount == ""){
            }else{
                let plnAmount = self.planAmount
                let discAmount = self.planDiscount
                let amount = Double(plnAmount)
                let disamount = Double(discAmount)
                let discount = disamount!/100
                let n = discount*amount!
                let totalamount = amount!-n
                let doubleStr = String(format: "%.2f", totalamount)
               
                cell.offerPercentLabel.text = "Save   " + "\(discAmount)" + "%"
                cell.costLabel.text = "$ " + plnAmount  + "/month"
                cell.discountLabel.text = "$ " + "\(doubleStr)"
                if disamount == 0 {
                    cell.strikeLabel.isHidden = true
                    cell.costLabel.isHidden = true
                    cell.offerView.isHidden = true
                }else{
                    cell.offerView.isHidden = false
                    cell.costLabel.isHidden = false
                     cell.strikeLabel.isHidden = false
                }
            }
        }else{
            let save = pricearr!["save"] as! String
            let amount = pricearr!["totalamount"] as! String
            let disc = pricearr!["disamount"] as! String
            cell.offerPercentLabel.text = save
            cell.costLabel.text = amount
            cell.discountLabel.text = disc
            if save == "0" {
                cell.offerView.isHidden = true
            }else{
                cell.offerView.isHidden = false
            }
        }
        cell.signMeBtn.tag = indexPath.row
        cell.signMeBtn.addTarget(self, action: #selector(nextBtnClicked(button:)), for:.touchUpInside)
        cell.awakeFromNib()
        cell.dataTableview.reloadData()
        return cell
       
    }
    
    @objc func nextBtnClicked(button: UIButton) {
        let obj = dataArr[button.tag]
        let somearr = priceArray[button.tag]
        //let vc = self.storyboard?.instantiateViewController(withIdentifier: "MembershipPaymentVC") as! MembershipPaymentVC
        let dataDict = obj as! [String:AnyObject]
        if somearr == nil {
           let discount = obj["discount_amount"] as! String
            let planeamount = obj["plan_amount"] as! String
            let monthplan = "1 MonthPlan"
           let plan_duration = "one"
            
            delegate?.Can_Receive_New_Plan_Data(discount: discount, planAmount: planeamount, plan_month: monthplan, plan_Duration: plan_duration, dataDict: dataDict)
        }else{
            
            let discount = somearr!["save"] as! String
            let planeamount = somearr!["totalamount"] as! String
            let monthplan = somearr!["months"] as! String
            let plan_duration = somearr!["duration_id"] as! String
            
           delegate?.Can_Receive_New_Plan_Data(discount: discount, planAmount: planeamount, plan_month: monthplan, plan_Duration: plan_duration, dataDict: dataDict)
        }
       // self.navigationController?.pushViewController(vc, animated: true)
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
   
    
}
