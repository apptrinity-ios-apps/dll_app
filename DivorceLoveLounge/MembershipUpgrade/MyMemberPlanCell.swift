//
//  MyMemberPlanCell.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 30/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
import WebKit

class MyMemberPlanCell: UITableViewCell,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {

    
    
    var adsListArr = [AnyObject]()
    @IBOutlet var adsCollectionView: UICollectionView!
    
    
    @IBOutlet var adsbackview: UIView!
    @IBOutlet var adsHieghtConstant: NSLayoutConstraint!
    @IBOutlet var backView: UIView!
    @IBOutlet var fstView: UIView!
    @IBOutlet var secndView: UIView!
    @IBOutlet var thirdView: UIView!
    @IBOutlet var planNameLbl: UILabel!
    @IBOutlet var durationLbl: UILabel!
    @IBOutlet var paymentDateLbl: UILabel!
       
    @IBOutlet var backImgView: UIImageView!
    @IBOutlet var upgradePlanBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backView.layer.cornerRadius = 5
        fstView.layer.cornerRadius = 3
        secndView.layer.cornerRadius = 3
        thirdView.layer.cornerRadius = 3
        backImgView.layer.cornerRadius = 5
        backImgView.clipsToBounds = true
        upgradePlanBtn.layer.cornerRadius = upgradePlanBtn.frame.size.height/2
        upgradePlanBtn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        upgradePlanBtn.layer.shadowOpacity = 1.0
        upgradePlanBtn.layer.shadowRadius = 3
        upgradePlanBtn.layer.masksToBounds = false
        upgradePlanBtn.layer.shadowColor = UIColor.gray.cgColor
        
         self.adsCollectionView.register(UINib(nibName: "AdsBannerCollecctionCell", bundle: nil), forCellWithReuseIdentifier: "AdsBannerCollecctionCell")
        
        
        
        self.adsCollectionView.delegate = self
        self.adsCollectionView.dataSource = self
        
//        if self.adsListArr.count > 1 {
//            self.setTimer()
//        }else{
//            
//            
//        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTimer() {
        let _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(MyMemberPlanCell.autoScroll), userInfo: nil, repeats: true)
    }
    var x = 0
    @objc func autoScroll() {
        if self.x < self.adsListArr.count {
            let indexPath = IndexPath(item: x, section: 0)
            self.adsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.x = self.x + 1
        }else{
            self.x = 0
            self.adsCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.adsListArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: adsCollectionView.frame.size.width, height: adsCollectionView.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = adsCollectionView.dequeueReusableCell(withReuseIdentifier: "AdsBannerCollecctionCell", for: indexPath) as! AdsBannerCollecctionCell
        let data = self.adsListArr[indexPath.row] as! [String:AnyObject]
        let type = data["add_type"] as! String
        if type == "Custom" {
            cell.adsWebView.isHidden = true
            cell.adsImageView.isHidden = false
            let url = "\(adsUrl)\(data["add_image"] as! String)"
            cell.adsImageView.af_setImage(withURL: URL(string:url)!)
        }else{
            cell.adsWebView.isHidden = false
            cell.adsImageView.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let data = self.adsListArr[indexPath.row] as! [String:AnyObject]
        let url = data["add_link"]
        if let url = URL(string: url as! String) {
            UIApplication.shared.open(url)
        }
    }
    
    
}
