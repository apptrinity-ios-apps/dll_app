//
//  MemPlanFirstCell.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 30/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
import WebKit

class MemPlanFirstCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {

   
    @IBOutlet var adshieghtConstant: NSLayoutConstraint!
   
    @IBOutlet var adsCollectionView: UICollectionView!
    @IBOutlet var adsbackView: UIView!
    @IBOutlet var backView: UIView!
    @IBOutlet var fstView: UIView!
    @IBOutlet var secndView: UIView!
    @IBOutlet var thirdView: UIView!
    @IBOutlet var fourthView: UIView!
    @IBOutlet var fifthView: UIView!
    @IBOutlet var sixthView: UIView!
    
    @IBOutlet var planNameLbl: UILabel!
    @IBOutlet var planTypeLbl: UILabel!
    @IBOutlet var amountLbl: UILabel!
    @IBOutlet var durationLbl: UILabel!
    @IBOutlet var strateDateLbl: UILabel!
    @IBOutlet var expiryLbl: UILabel!
    
    
    
    var adsListArr = [AnyObject]()
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backView.layer.cornerRadius = 5
        fstView.layer.cornerRadius = 2
        secndView.layer.cornerRadius = 2
        thirdView.layer.cornerRadius = 2
        fourthView.layer.cornerRadius = 2
        fifthView.layer.cornerRadius = 2
        sixthView.layer.cornerRadius = 2
        
        
        self.adsCollectionView.delegate = self
        self.adsCollectionView.dataSource = self
        self.adsCollectionView.register(UINib(nibName: "AdsBannerCollecctionCell", bundle: nil), forCellWithReuseIdentifier: "AdsBannerCollecctionCell")
        
      // self.setTimer()
        // Initialization code
        
        if adsListArr.count > 1 {
            setTimer()
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setTimer() {
        let _ = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(MemPlanFirstCell.autoScroll), userInfo: nil, repeats: true)
    }
    var x = 0
    @objc func autoScroll() {
        if self.x < self.adsListArr.count {
            let indexPath = IndexPath(item: x, section: 0)
            self.adsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.x = self.x + 1
        }else{
            self.x = 0
            self.adsCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.adsListArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: adsCollectionView.frame.size.width, height: adsCollectionView.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = adsCollectionView.dequeueReusableCell(withReuseIdentifier: "AdsBannerCollecctionCell", for: indexPath) as! AdsBannerCollecctionCell
        let data = self.adsListArr[indexPath.row] as! [String:AnyObject]
        let type = data["add_type"] as! String
        if type == "Custom" {
            cell.adsWebView.isHidden = true
            cell.adsImageView.isHidden = false
            let url = "\(adsUrl)\(data["add_image"] as! String)"
            cell.adsImageView.af_setImage(withURL: URL(string:url)!)
        }else{
            cell.adsWebView.isHidden = false
            cell.adsImageView.isHidden = true
            
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
           let data = self.adsListArr[indexPath.row] as! [String:AnyObject]
           let url = data["add_link"]
           if let url = URL(string: url as! String) {
               UIApplication.shared.open(url)
           }
       }
   
        
    }
    
    
    

