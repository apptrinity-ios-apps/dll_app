//
//  myPlanVC.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 15/06/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class myPlanVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    var fromScreen = ""
    var adsListArr = [AnyObject]()

    var dataArray = ["Total Profiles","Remaining Profiles","Chat","Total Contacts","Remaining Contacts","Total Interests","Remaining Interests","Total Messages","Remaining Messages"]


   var palnDetails = [String:AnyObject]()


    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var myPlanTableView: UITableView!


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.myPlanTableView.register(UINib(nibName: "MemPlanFirstCell", bundle: nil), forCellReuseIdentifier: "MemPlanFirstCell")
         self.myPlanTableView.register(UINib(nibName: "MemPlanSecondCell", bundle: nil), forCellReuseIdentifier: "MemPlanSecondCell")
        self.myPlanTableView.separatorStyle = .none

        getMyPalnAPi()
        adsListServiceApi(ScreenType: "Membership")
        
        
    }

    @IBAction func backAction(_ sender: Any) {

        if fromScreen == "payment"{

            let TopTabBarV = self.storyboard!.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
            self.navigationController?.pushViewController(TopTabBarV, animated: true)

        }else{
           self.navigationController?.popViewController(animated: true)
        }

    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
        return dataArray.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if self.adsListArr.count > 0 {
                   return 422
            }else{
                 return 292
            }
        } else {
        return 44
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MemPlanFirstCell", for: indexPath) as! MemPlanFirstCell
            cell.selectionStyle = .none

            
            
            cell.planNameLbl.text = themes.checkNullValue(palnDetails["package_type"]) as? String
            cell.planTypeLbl.text = themes.checkNullValue(palnDetails["plan_type"]) as? String
         let amunt = themes.checkNullValue(palnDetails["plan_amount"]) as? String
            cell.amountLbl.text = "$\(amunt!)"
            let days = "\(themes.checkNullValue(palnDetails["plan_duration"]) as? String ?? "") Days"
            cell.durationLbl.text = days

            var strDate = themes.checkNullValue(palnDetails["plan_start_date"]) as? String
            if (strDate == "") {
                print("no start date")
            } else {
                let startDate = themes.dateFormateConverter(date: strDate as! String)
                cell.strateDateLbl.text = startDate
            }
            var expdate = themes.checkNullValue(palnDetails["plan_expiry"]) as? String
            if (expdate == "") {
                print("no start date")
            } else {
                let expryDate = themes.dateFormateConverter(date: expdate as! String)
                cell.expiryLbl.text = expryDate
            }

//         cell.strateDateLbl.text = themes.checkNullValue(palnDetails["plan_start_date"]) as? String
//         cell.expiryLbl.text = themes.checkNullValue(palnDetails["plan_expiry"]) as? String

            
            cell.adsListArr = self.adsListArr
            if self.adsListArr.count > 0 {
                cell.adshieghtConstant.constant = 120
                if self.adsListArr.count > 1 {
                           cell.setTimer()
                       }
                }else{
                cell.adshieghtConstant.constant = 0
            }
            cell.adsCollectionView.reloadData()
            
            
            return cell

            

        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MemPlanSecondCell", for: indexPath) as! MemPlanSecondCell
            cell.selectionStyle = .none
            cell.profileNameLbl.text = dataArray[indexPath.row]
            if indexPath.row == 0 {
                cell.backView.backgroundColor = lightPink
                cell.unlimitedLbl.text = "Unlimted"
                //themes.checkNullValue(palnDetails["profile"]) as? String
            } else if indexPath.row == 1 {
                cell.backView.backgroundColor = lightYellow
                 cell.unlimitedLbl.text = "Unlimted" //themes.checkNullValue(palnDetails["rem_profiles"]) as? String
            } else if indexPath.row == 2 {
                cell.backView.backgroundColor = lightPink
                cell.unlimitedLbl.text = themes.checkNullValue(palnDetails["chat"]) as? String
            } else if indexPath.row == 3 {
                cell.backView.backgroundColor = lightYellow
                 cell.unlimitedLbl.text = "Unlimted" //themes.checkNullValue(palnDetails["plan_contacts"]) as? String
            } else if indexPath.row == 4 {
                cell.backView.backgroundColor = lightPink
                cell.unlimitedLbl.text = "Unlimted" //themes.checkNullValue(palnDetails["rem_contacts"]) as? String
            } else if indexPath.row == 5 {
                cell.backView.backgroundColor = lightYellow
                 cell.unlimitedLbl.text = "Unlimted" //themes.checkNullValue(palnDetails["interest"]) as? String
            } else if indexPath.row == 6 {
                cell.backView.backgroundColor = lightPink
                 cell.unlimitedLbl.text = "Unlimted" //themes.checkNullValue(palnDetails["rem_interests"]) as? String
            } else if indexPath.row == 7 {
                cell.backView.backgroundColor = lightYellow
                cell.unlimitedLbl.text = "Unlimted" //themes.checkNullValue(palnDetails["plan_msg"]) as? String
            } else {
                cell.backView.backgroundColor = lightPink
                cell.unlimitedLbl.text = "Unlimted" //themes.checkNullValue(palnDetails["rem_msgs"]) as? String
            }


            return cell
        }

//        let cell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell", for: indexPath)as! InfoTableViewCell
//        cell.fixedLbl.text = dataArray[indexPath.row]
//
//        if(indexPath.row == 0){
//            cell.dataLabel.text = themes.checkNullValue(palnDetails["package_type"]) as? String
//        }else if(indexPath.row == 1){
//            cell.dataLabel.text = themes.checkNullValue(palnDetails["plan_type"]) as? String
//        }else if(indexPath.row == 2){
//            cell.dataLabel.text = themes.checkNullValue(palnDetails["plan_amount"]) as? String
//        }else if(indexPath.row == 3){
//
//            let days = "\(themes.checkNullValue(palnDetails["plan_duration"]) as? String ?? "") Days"
//            cell.dataLabel.text = days
//
//        }else if(indexPath.row == 4){
//            cell.dataLabel.text = themes.checkNullValue(palnDetails["plan_start_date"]) as? String
//        }else if(indexPath.row == 5){
//            cell.dataLabel.text = themes.checkNullValue(palnDetails["plan_expiry"]) as? String
//        }else if(indexPath.row == 6){
//            cell.dataLabel.text = "Unlimted" //themes.checkNullValue(palnDetails["profile"]) as? String
//        }else if(indexPath.row == 7){
//            cell.dataLabel.text = "Unlimted" //themes.checkNullValue(palnDetails["rem_profiles"]) as? String
//        }else if(indexPath.row == 8){
//            cell.dataLabel.text = themes.checkNullValue(palnDetails["chat"]) as? String
//        }else if(indexPath.row == 9){
//            cell.dataLabel.text = "Unlimted" //themes.checkNullValue(palnDetails["plan_contacts"]) as? String
//        }else if(indexPath.row == 10){
//            cell.dataLabel.text = "Unlimted" //themes.checkNullValue(palnDetails["rem_contacts"]) as? String
//        }else if(indexPath.row == 11){
//            cell.dataLabel.text = "Unlimted" //themes.checkNullValue(palnDetails["interest"]) as? String
//        }else if(indexPath.row == 12){
//            cell.dataLabel.text = "Unlimted" //themes.checkNullValue(palnDetails["rem_interests"]) as? String
//        }else if(indexPath.row == 13){
//            cell.dataLabel.text = "Unlimted" //themes.checkNullValue(palnDetails["plan_msg"]) as? String
//        }else if(indexPath.row == 14){
//            cell.dataLabel.text = "Unlimted" //themes.checkNullValue(palnDetails["rem_msgs"]) as? String
//        }
//        return cell
    }
    func adsListServiceApi(ScreenType:String){
        self.urlService.adslistServiceCall(ScreenType: ScreenType) { response in
            let success = response["status"] as! String
            if(success == "1"){
                let data = response["data"] as! [AnyObject]
                self.adsListArr = data
                
                self.myPlanTableView.reloadData()
            }else{
                
            }
        }
    }
    func getMyPalnAPi(){

        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:myPlan, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){



                    //let photo = response["photos"] as! [String:AnyObject]

                   self.palnDetails = response["plandetails"] as! [String:AnyObject]

                    self.myPlanTableView.reloadData()

                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }

    }
    
}

