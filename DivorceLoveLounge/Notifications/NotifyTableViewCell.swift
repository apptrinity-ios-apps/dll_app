//
//  NotifyTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 4/1/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class NotifyTableViewCell: UITableViewCell {

    @IBOutlet weak var profImgView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var notificLbl: UILabel!
    @IBOutlet weak var starImgView: UIImageView!    
    @IBOutlet weak var colorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        nameLbl.intrinsicContentSize.width
       profImgView.layer.cornerRadius = (profImgView.frame.size.height)/2
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
