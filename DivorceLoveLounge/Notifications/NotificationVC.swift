//
//  NotificationVC.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 3/15/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var namesArr = [AnyObject]()
    var themes = Themes()
    
    var countryarr = [AnyObject]()

    @IBOutlet weak var notificationTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.notificationTableView.delegate = self
        self.notificationTableView.dataSource = self
        self.notificationTableView.separatorColor = UIColor(red: 212/255, green: 220/255, blue: 225/255, alpha: 1)
        self.notificationTableView.register(UINib(nibName: "NotifyTableViewCell", bundle: nil),
            forCellReuseIdentifier: "NotifyTableViewCell")
        
        
        self.themes.showAlert(title: "Alert", message: "No Data is available", sender: self)
        
        
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return namesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotifyTableViewCell", for: indexPath)as! NotifyTableViewCell
        cell.backgroundColor = UIColor(red: 255/255, green: 209/255, blue: 209/255, alpha: 1)
        cell.selectionStyle = .none
        cell.nameLbl.text = namesArr[indexPath.row] as! String
        cell.countryLbl.text = countryarr[indexPath.row] as! String
        cell.notificLbl.text = ""
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.backgroundColor = UIColor.white
//        self.notificationTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
