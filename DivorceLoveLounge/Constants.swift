//  Constants.swift
//  DivorceLoveLounge
//  Created by S s Vali on 5/9/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
import UIKit
import XMPPFramework
import Foundation

public typealias VoidCompletion = () -> ()
public typealias StreamCompletion = ((_ stream: XMPPStream?) -> ())?
public typealias BoolCompletion = (_ success: Bool) -> ()
public typealias RosterCompletion = ((_ result: Bool, _ roster: XMPPRoster) -> Void)
public struct Constants {
    public struct Preferences {
        public static let Authentication = "AuthenticationPreferenceName"
    }
    public struct Notifications {
        public static let StreamControllerWasCreated = "StreamControllerWasCreatedNotificationName"
        public static let RosterWasUpdated = "RosterWasUpdatedNotificationName"
    }
    public static func applicationSupportDirectory() -> String {
        let cacheDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        return cacheDirectories.first!
    }
}
func delay(_ delay: Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}
////Live links
var AppbaseUrl =  "https://divorcelovelounge.com/api/"
var ImagebaseUrl = "https://divorcelovelounge.com/uploads/userprofiles/"
var adsUrl = "https://divorcelovelounge.com/uploads/adds/"
// https://divorcelovelounge.com/api1/matches
//staging links
//var AppbaseUrl =  "http://18.190.90.133/dll/api/"
//var ImagebaseUrl = "http://18.190.90.133/dll/uploads/userprofiles/"
//var adsUrl = "http://18.190.90.133/dll/uploads/adds/"
var getProfilePics = "getProfilePhotos"
var changePhoto = "changePhoto"
var getChatList = "getXmppchatList"
let login = "login"
let replyMessage = "replyMessage"
let Register = "register"
let profileSetup = "profileSetup"
let myFavourites = "myFavourites"
let whoViewedMe = "whoViewedMe"
let matches = "matches"
let partnerProfile = "partnerProfile"
let myPlan = "myPlan"
let makePayment = "makePayment"
let favouritedMe = "FavouritedMe"
let supportTypes = "supportTypes"
let supportInsert = "supportInsert"
let getMsgDeatilview = "getMsgDeatilview"
let memberMessages = "memberMessages"
let quiz = "quiz"
let contactUs = "contactUs"
let myInterest = "myInterest"
let setProfilePic = "setProfilePic"
let deletePhoto = "deletePhoto"
let interestedMe = "interestedMe"
var profileDict = [String:AnyObject]()
var saveQuiz = "saveQuiz"
let profileMatch = "profileMatch"
let dynamicProfile = "registerProfile"
let favourite = "like"
let sendInterest = "sendInterest"
let blockUnblock = "blockUser"
let blockList = "blockList"
let getReligion = "getReligion"
let getCaste = "getCaste"
let interestAccept = "iterestAccept"
let interestReject = "iterestReject"
let getCities = "getCities"
let getMembershipList = "getMembershipList"
let getMultipleCastes = "getMultipleCastes"
let updatematchPreference = "updatematchPreference"
let viewContact = "viewContact"
let contactList = "contactList"
let messagesInboxList = "messengersList"
let messagesSentList = "messagesSentList"
let messagesImportantList = "messagesImportantList"
let messagesTrashList = "messagesTrashList"
let makeImportant = "makeImportant"
let composeMessage = "composeMessage"
let makeTrash = "makeTrash"
let composeUsersList = "composeUsersList"
let getCountriess = "getCountries"
let forgotPassword = "forgotPassword"
let mothertongue = "mothertongue"
let sociallogin = "sociallogin"
let checkPromocode = "checkPromocode"
let socialregister = "socialregister"
let checkStatus = "checkMemberStatus"
let changePassword = "changePassword"
let signOut = "logout"
let quizDisplay = "quizDisplay"
let updateQuiz = "updateQuiz"
let  getUserCounts = "getUserCounts"
let checkMail = "checkEmail"
let getUsermessagesCounts = "getUsermessagesCounts"
let checkUsername = "checkUsername"
let loginStatus = "updateLoginStatus"
let ActiveList = "activeUserList"
let adsList = "addslist"
let chatStatus = "chatStatus"


// Hexa ColorCode
let brownColorDark = UIColor(red: 48/255.0, green: 14/255.0, blue: 5/255.0, alpha: 1.0)
let orangeColor = UIColor(red: 255/255.0, green: 161/255.0, blue: 63/255.0, alpha: 1.0)
let DARKBLUECOLOR = UIColor(red: 4/255.0, green: 68/255.0, blue: 126/255.0, alpha: 1.0)
let BlackColor = UIColor(red: 67/255.0, green: 67/255.0, blue: 67/255.0, alpha: 1.0)
let GREENCOLOR = UIColor(red: 21/255.0, green: 196/255.0, blue: 54/255.0, alpha: 1.0)
let YELLOWCOLOR = UIColor(red: 253/255.0, green: 183/255.0, blue: 20/255.0, alpha: 1.0)
let REDCOLOR = UIColor(red: 255/255.0, green: 74/255.0, blue: 74/255.0, alpha: 1.0)
let BLUECOLOR = UIColor(red: 63/255.0, green: 144/255.0, blue: 195/255.0, alpha: 1.0)
let AMOUNTCOLOR = UIColor(red: 32/255.0, green: 133/255.0, blue: 197/255.0, alpha: 1.0)
let LightRed = UIColor(red: 196/255.0, green: 71/255.0, blue: 57/255.0, alpha: 1.0)
let BrownColor = UIColor(red: 180/255.0, green: 56/255.0, blue: 41/255.0, alpha: 1.0)
let darkBrownColor = UIColor(red: 111/255.0, green: 41/255.0, blue: 31/255.0, alpha: 1.0)
let buttonHideColor = UIColor(red: 234/255.0, green: 229/255.0, blue: 228/255.0, alpha: 1.0)
let green = UIColor(red: 35/255.0, green: 160/255.0, blue: 6/255.0, alpha: 1.0)
let yellow = UIColor(red: 255/255.0, green: 251/255.0, blue: 0/255.0, alpha: 1.0)
let ligtGray = UIColor(red: 154/255.0, green: 154/255.0, blue: 154/255.0, alpha: 1.0)
let lightBrown = UIColor(red: 197/255.0, green: 72/255.0, blue: 58/255.0, alpha: 1.0)
let lightgray = UIColor(red: 214/255.0, green: 214/255.0, blue: 214/255.0, alpha: 1.0)
let textClrgray = UIColor(red: 85/255.0, green: 85/255.0, blue: 85/255.0, alpha: 1.0)
let profilebackColor = UIColor(red: 192/255.0, green: 68/255.0, blue: 45/255.0, alpha: 1.0)
let barYellow = UIColor(red: 249/255.0, green: 203/255.0, blue: 53/255.0, alpha: 1.0)
let MonthYellow =  UIColor(red: 248/255.0, green: 170/255.0, blue: 86/255.0, alpha: 1.0)
let lightPink = UIColor(red: 234/255.0, green: 225/255.0, blue: 224/255.0, alpha: 1.0)
let lightYellow = UIColor(red: 237/255.0, green: 230/255.0, blue: 218/255.0, alpha: 1.0)

let editBlue = UIColor(red: 66/255.0, green: 165/255.0, blue: 245/255.0, alpha: 1.0)

func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
    let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
    let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
    let blue = CGFloat(rgbValue & 0xFF)/256.0
    return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
}

   


         

        
                                  
      
