//
//  QuizzEditVC.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 09/08/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class QuizzEditVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate {
    var window: UIWindow?
    @IBOutlet weak var percentLabel: UILabel!
    @IBOutlet var editBtn: UIButton!
    @IBOutlet var submitBtn: UIButton!
    @IBOutlet weak var percentSlider: CustomSlider1!
    let width = UIScreen.main.bounds.width
    let height = UIScreen.main.bounds.height
    var rowsWhichAreChecked = [NSIndexPath]()
    var optionType = String()
    var radioHint = Int()
    var question = String()
    var answerArray = Array<AnyObject>()
    var questionIncrement = Int()
    var data = [AnyObject]()
    var finalDict = [String:AnyObject]()
    var lastFinalArray = [AnyObject]()
    var percentHint = 0.0
    var dividedVal = 0.0
    var editButtonHint = Bool()
    var questionID = String()
    var selecedNamesArray = Array<AnyObject>()
    var selecedIdArray = Array<AnyObject>()
    var finalscreen = String()
    @IBOutlet weak var scrollBtn: UIButton!
    //  var lastFinalDict = Array<AnyObject>()
    var generalArr = ["WARM","CLEVER","DOMINANT","OUTGOING","QUARRELSOMW","STABLE","ENERGETIC","PREDICTABLE","AFFECTIONATE","INTELLIGENT","ATTRACTIVE","COMPASSIONATE","LOYAL","WITTY","SENSITIVE","GENEROUS","SENSUAL"]
    var physicalArr = ["STYLISH","ATHLETIC","OVERWEIGHT","PLAIN","HEALTHY","SEXY"]
    var happyArr = ["HAPPY"]
    var checkBoxSelection = [AnyObject]()
    var radioSelection = [AnyObject]()
    var radioIdSelection = [AnyObject]()
    var selectedHint = Int()
    var generalSelectedRow = Int()
    var physicalSelectedRow = Int()
    var happySelectedRow = Int()
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet var scrollView: UIView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet var firstView: UIView!
    @IBOutlet weak var generalTableView: UITableView!
    var ratingArray = [AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        percentHint = 10
        submitBtn.isHidden = true
        scrollView.layer.cornerRadius = scrollView.frame.height/2
        nextBtn.layer.cornerRadius = 5
        percentSlider.value = 10
        percentLabel.text = "10%"
        percentSlider.setThumbImage(UIImage(), for: .normal)
        radioHint = -1
        questionLabel.sizeToFit()
        generalTableView.delegate = self
        generalTableView.dataSource = self
        selectedHint = 0
        generalTableView.allowsSelection = true
        generalTableView.allowsMultipleSelection = true
        FirstView(hidden: false)
        leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        self.generalTableView.register(UINib(nibName: "myDataTableViewCell", bundle: nil), forCellReuseIdentifier: "myDataTableViewCell")
        self.generalTableView.register(UINib(nibName: "radioQuizCell", bundle: nil), forCellReuseIdentifier: "radioQuizCell")
        self.generalTableView.register(UINib(nibName: "sliderQuizCell", bundle: nil), forCellReuseIdentifier: "sliderQuizCell")
         buttonEnableOrDisableforleftBtn(val: 0)
        
        editBtn.setTitleColor(darkBrownColor, for: .normal)

        // Do any additional setup after loading the view.
        quizDisplayAPI()
    }
    func buttonEnableOrDisableforleftBtn(val : Int){
        if(val == 0){
            //            leftBtn.setImage(#imageLiteral(resourceName: "left-arrow (16)"), for: .normal)
            leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        }else{
            //            leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-black"), for: .normal)
            leftBtn.setImage(#imageLiteral(resourceName: "left-arrow-white"), for: .normal)
        }
    }
//    func buttonEnableOrDisableforRightButton(val : Int){
//        if(val == 0){
//            rightBtn.setImage(#imageLiteral(resourceName: "right-arrow-hide"), for: .normal)
//        }else{
//            rightBtn.setImage(#imageLiteral(resourceName: "right-arrow-black"), for: .normal)
//        }
//    }
    
    func buttonEnableOrDisableforNextButton(val : Int){
        if(val == 0){
            //          nextBtn.backgroundColor = buttonHideColor
        }else{
            //            nextBtn.backgroundColor = darkBrownColor
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.answerArray.count < Int(scrollView.contentOffset.y)) {
            // did move up
            self.scrollView.isHidden = true
        } else if (self.answerArray.count > Int(scrollView.contentOffset.y)) {
            self.scrollView.isHidden = false
        } else {
            // didn't move
        }
    }
    func scrollToTop(){
        self.generalTableView.setContentOffset(CGPoint(x:0,  y:UIApplication.shared.statusBarFrame.height ), animated: true)
    }
    @IBAction func nextBtnAction(_ sender: Any) {
//        let indexPath = NSIndexPath(row: 0, section: 0)
//        self.generalTableView.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
     //   self.generalTableView.setContentOffset(.zero, animated: true)
        //scrollToTop()
        
        
        self.generalTableView.scroll(to: .top, animated: true)
        answerArray.removeAll()
        rowsWhichAreChecked.removeAll()
        selecedNamesArray.removeAll()
        selecedIdArray.removeAll()
        ratingArray.removeAll()
        lastFinalArray.removeAll()
        percentHint = percentHint + 10
        var finalArray = [AnyObject]()
        rowsWhichAreChecked.removeAll()
        var subDict = [String:AnyObject]()
        questionIncrement = questionIncrement+1
        if(questionIncrement >= data.count){
            print("exceeded")
           // saveQuizAPI()
        }else{
            let obj = self.data[questionIncrement]
            self.optionType = obj["option_type"] as! String
             self.questionID = obj["question_id"] as! String
            self.question = obj["question"] as! String
            self.answerArray = obj["answer_list"] as! Array
            
            if(self.optionType == "rating"){
                
                for j in 0...self.answerArray.count - 1{
                    let obj1 = self.answerArray[j]
                    let userAnswer = obj1["user_answer"] as! String
                    if(userAnswer == ""){
                        self.ratingArray.append("0" as AnyObject)
                    }else{
                        self.ratingArray.append(userAnswer as AnyObject)
                    }
                    
                }
                print(self.ratingArray)
            }
         
            self.questionLabel.text = self.question
            let val = questionIncrement - 1
            let obj1 = self.data[val]
            buttonEnableOrDisableforleftBtn(val: 1)
            buttonEnableOrDisableforNextButton(val: 0)
         
        }
        
        if(answerArray.count > 10){
            // scrollBtn.isHidden = false
            scrollView.isHidden = false
        }else{
            scrollView.isHidden = true
        }
        if(percentHint == 100){
            
          
            
            nextBtn.isHidden = false
            percentSlider.value = 100
            let val = 100
            percentLabel.text = String(val) + "%"
            
            
            
            
            
        }else if (percentHint > 100){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            
            
            
             self.generalTableView.reloadData()
             nextBtn.isHidden = false
            percentSlider.value = Float(percentHint)
            
            let val = Int(percentHint)
            percentLabel.text = String(val) + "%"
        }
        
        
        
        
        
        generalTableView.reloadData()
        
        
        
        
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        nextBtn.isHidden = false
        if(questionIncrement == 0){
            self.navigationController?.popViewController(animated: true)
        }else{
            rowsWhichAreChecked.removeAll()
            selecedNamesArray.removeAll()
            selecedIdArray.removeAll()
            ratingArray.removeAll()
            lastFinalArray.removeAll()
            percentHint = percentHint - 10
            
            percentSlider.value = Float(percentHint)
            let val = Int(percentHint)
            percentLabel.text = String(val) + "%"
            var finalArray = [AnyObject]()
            rowsWhichAreChecked.removeAll()
            var subDict = [String:AnyObject]()
            if(questionIncrement >= data.count){
                print("exceeded")
                // saveQuizAPI()
                
                
                
            }else{
                let obj = self.data[questionIncrement - 1]
                questionIncrement = questionIncrement-1
                
                self.optionType = obj["option_type"] as! String
                self.questionID = obj["question_id"] as! String
                self.question = obj["question"] as! String
                self.answerArray = obj["answer_list"] as! Array
                
                if(self.optionType == "rating"){
                    
                    for j in 0...self.answerArray.count - 1{
                        
                        let obj1 = self.answerArray[j]
                        let userAnswer = obj1["user_answer"] as! String
                        if(userAnswer == ""){
                            self.ratingArray.append("0" as AnyObject)
                        }else{
                            self.ratingArray.append(userAnswer as AnyObject)
                        }
                        
                    }
                    print(self.ratingArray)
                }
                
                self.questionLabel.text = self.question
               
                buttonEnableOrDisableforleftBtn(val: 1)
                buttonEnableOrDisableforNextButton(val: 0)
                
            }
           
            self.generalTableView.reloadData()
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answerArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.separatorColor = UIColor.clear
        if(optionType == "rating"){
            // generalTableView.allowsMultipleSelection = false
            let cell = tableView.dequeueReusableCell(withIdentifier: "sliderQuizCell", for: indexPath) as! sliderQuizCell
            cell.slider.tag = indexPath.row
            //cell.maxLabel.text = String(ratingArray[indexPath.row] as! Float)
            cell.slider.addTarget(self, action: #selector(CompitibilityQuizVC.updateMinMaxValue(sender:)), for: .allEvents)
            // cell.slider.value = 0.0
            cell.selectionStyle = .none
            let obj = answerArray[indexPath.row]
            let text = (obj["answer"] as! String)
            let user_answer = (obj["user_answer"] as! String)
            cell.maxLabel.text = ratingArray[indexPath.row] as? String
            if let quantity = ratingArray[indexPath.row] as? NSNumber {
                cell.slider.value = Float(truncating: quantity)
            }
            else if let quantity = ratingArray[indexPath.row] as? String {
                cell.slider.value = Float(quantity) as! Float
            }
            if(editButtonHint){
                cell.isUserInteractionEnabled = true
                cell.slider.thumbTintColor = UIColor(red: 255/255.0, green: 197/255.0, blue: 17/255.0, alpha: 1.0)
            }else{
             cell.isUserInteractionEnabled = false
             cell.slider.thumbTintColor = UIColor.clear
            }
            cell.nameLabel.text = text.capitalizingFirstLetter()
            return cell
        }else if(optionType == "checkbox"){
            let cell = tableView.dequeueReusableCell(withIdentifier: "myDataTableViewCell", for: indexPath) as! myDataTableViewCell
            // generalTableView.allowsMultipleSelection = true
            tableView.separatorStyle = .none
            let obj = answerArray[indexPath.row]
            let text = (obj["answer"] as! String)
            let id = (obj["ans_id"] as! String)
            cell.dataLabel.text = text.capitalizingFirstLetter()
            cell.selectionStyle = .none
            let user_answer = (obj["user_answer"] as! String)
            if(user_answer == ""){
                cell.backView.backgroundColor = UIColor.white
                cell.dataLabel.textColor = UIColor.black
            }else{
                rowsWhichAreChecked.append(indexPath as NSIndexPath)
                selecedNamesArray.append(text as AnyObject)
                selecedIdArray.append(id as AnyObject)
                cell.backView.backgroundColor = darkBrownColor
                cell.dataLabel.textColor = UIColor.white
            }
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                cell.backView.backgroundColor = UIColor.white
                cell.dataLabel.textColor = UIColor.black
            }else{
                cell.backView.backgroundColor = darkBrownColor
                cell.dataLabel.textColor = UIColor.white
                
            }
            if(editButtonHint){
                cell.isUserInteractionEnabled = true
            }else{
                cell.isUserInteractionEnabled = false
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "myDataTableViewCell", for: indexPath) as! myDataTableViewCell
            tableView.separatorStyle = .none
            // generalTableView.allowsMultipleSelection = false
            let obj = answerArray[indexPath.row]
            let user_answer = (obj["user_answer"] as! String)
             let text = (obj["answer"] as! String)
             let id = (obj["ans_id"] as! String)
            if(user_answer == ""){
                cell.backView.backgroundColor = UIColor.white
                cell.dataLabel.textColor = UIColor.black
            }else{
                radioHint = indexPath.row
                rowsWhichAreChecked.append(indexPath as NSIndexPath)
                selecedNamesArray.append(text as AnyObject)
                selecedIdArray.append(id as AnyObject)
                cell.backView.backgroundColor = darkBrownColor
                cell.dataLabel.textColor = UIColor.white
            }
            if(radioHint == indexPath.row){
                radioSelection.append(text as AnyObject)
                radioIdSelection.append(id as AnyObject)
                cell.backView.backgroundColor = darkBrownColor
                cell.dataLabel.textColor = UIColor.white
            }else{
                radioSelection.removeAll()
                radioIdSelection.removeAll()
                cell.backView.backgroundColor = UIColor.white
                cell.dataLabel.textColor = UIColor.black
            }
            cell.dataLabel.text = text.capitalizingFirstLetter()
            cell.selectionStyle = .none
            if(editButtonHint){
                cell.isUserInteractionEnabled = true
            }else{
                cell.isUserInteractionEnabled = false
            }
            return cell
        }
    }
    @IBAction func scrollAction(_ sender: Any) {
        if(self.answerArray.count == 0){
        }else{
            let indexPath = NSIndexPath(row: self.answerArray.count-1, section: 0)
            self.generalTableView.scrollToRow(at: indexPath as IndexPath, at: .bottom, animated: true)
        }
    }
    @objc func updateMinMaxValue(sender: UISlider!) {
        nextBtn.isEnabled = true
        buttonEnableOrDisableforleftBtn(val: 1)
        buttonEnableOrDisableforNextButton(val: 1)
       // buttonEnableOrDisableforRightButton(val: 1)
        let value = Int(sender.value)
        ratingArray.remove(at: sender.tag)
        ratingArray.insert(Int(sender!.value) as AnyObject, at: sender.tag)
        let cell:sliderQuizCell = generalTableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! sliderQuizCell
        print(ratingArray)
        cell.maxLabel.text = String(value)
        print(value)
        //        DispatchQueue.main.async {
        //            self.kmsLabel.text = "\(value)"
        //            print("Slider value = \(value)")
        //        }
        
        // generalTableView.reloadData()
    }
    @objc func radioBtnClicked(button: UIButton) {
        buttonEnableOrDisableforNextButton(val: 1)
        radioHint = button.tag
        generalTableView.reloadData()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        nextBtn.isEnabled = true
        buttonEnableOrDisableforleftBtn(val: 1)
        buttonEnableOrDisableforNextButton(val: 1)
       // buttonEnableOrDisableforRightButton(val: 1)
        radioHint = indexPath.row
        if(optionType == "checkbox"){
            let cell:myDataTableViewCell = tableView.cellForRow(at: indexPath) as! myDataTableViewCell
            // cross checking for checked rows
            let obj = answerArray[indexPath.row]
            let text = (obj["answer"] as! String)
            let id = (obj["ans_id"] as! String)
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                rowsWhichAreChecked.append(indexPath as NSIndexPath)
                selecedIdArray.append(id as AnyObject)
                selecedNamesArray.append(text as AnyObject)
                print(selecedIdArray,selecedNamesArray)
            }else{
                cell.backView.backgroundColor = UIColor.clear
                cell.dataLabel.textColor = UIColor.black
                // cell.backgroundColor = UIColor.black
                // remove the indexPath from rowsWhichAreCheckedArray
                if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                    rowsWhichAreChecked.remove(at: checkedItemIndex)
                    selecedIdArray.remove(at: checkedItemIndex)
                    selecedNamesArray.remove(at: checkedItemIndex)
                    print(selecedIdArray,selecedNamesArray)
                }
            }
        }else {
            let obj = answerArray[indexPath.row]
            let text = (obj["answer"] as! String)
            let id = (obj["ans_id"] as! String)
            selecedIdArray.append(id as AnyObject)
            selecedNamesArray.append(text as AnyObject)
            
            
        }
        
        
        
        generalTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath){
        if(optionType == "checkbox"){
            //            let cell = tableView.cellForRow(at: indexPath) as! myDataTableViewCell
            //            cell.accessoryType = UITableViewCellAccessoryType.none
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
        }
        generalTableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(optionType == "rating"){
            return 83
        }else{
            return 57
        }
    }
    func FirstView(hidden: Bool){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136, 1334, 1920, 2208:
                print("iPhone 5 or 5S or 5C")
                firstView.frame = CGRect(x: 0, y: 120, width: Int(width), height: Int(self.view.frame.size.height - 170))
                firstView.alpha = 1
                firstView.isHidden = hidden
                self.view.addSubview(firstView)
            case 2436, 2688, 1792:
                print("iPhone X, XS")
                firstView.frame = CGRect(x: 0, y: 140, width: Int(width), height: Int(self.view.frame.size.height - 230))
                firstView.alpha = 1
                firstView.isHidden = hidden
                self.view.addSubview(firstView)
            default:
                print("Unknown")
            }
        }
    }
    func quizDisplayAPI(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid]
            
            
            //            let parameters = ["gender":gender, "looking_for":lookingFor,"first_name":firstName, "zipcode":zipCode,"state":state,"email":mailField.text!,"password":passwordField.text!] as [String : String]
            
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:quizDisplay, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    self.data = response["quizlist"] as! [AnyObject]
                    self.dividedVal = Double(Float(100/self.data.count))
                    let obj = self.data[0]
                    self.optionType = obj["option_type"] as! String
                    self.question = obj["question"] as! String
                    self.answerArray = obj["answer_list"] as! Array
                    self.questionID = obj["question_id"] as! String
                    self.questionLabel.text = self.question
                    self.buttonEnableOrDisableforleftBtn(val: 1)
                    self.buttonEnableOrDisableforNextButton(val: 1)
                 //   self.buttonEnableOrDisableforRightButton(val: 1)
                    if(self.optionType == "rating"){
  
                        for j in 0...self.answerArray.count - 1{
                            
                            let obj1 = self.answerArray[j]
                            let userAnswer = obj1["user_answer"] as! String
                            if(userAnswer == ""){
                                self.ratingArray.append("0" as AnyObject)
                            }else{
                                self.ratingArray.append(userAnswer as AnyObject)
                            }
                            
                        }
                        print(self.ratingArray)
                    }
                    self.generalTableView.reloadData()

                }else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    @IBAction func editAction(_ sender: Any) {
        //generalTableView.allowsSelection = false
        if(editButtonHint){
            editBtn.setTitleColor(darkBrownColor, for: .normal)
           editButtonHint = false
            submitBtn.isHidden = true
          
        }else{
              editBtn.setTitleColor(editBlue, for: .normal)
            submitBtn.isHidden = false
            editButtonHint = true
        }
        generalTableView.reloadData()
    }
    @IBAction func submitAction(_ sender: Any) {
        if(optionType == "rating"){
            
            for i in 0...self.answerArray.count - 1 {
                print(i)
                    let obj3 = self.answerArray[i]
                    let id = obj3["ans_id"]
                var dict = [String:AnyObject]()
                    dict.updateValue(ratingArray[i] as AnyObject, forKey: "answer")
                dict.updateValue(id as AnyObject, forKey: "answer_id")
                lastFinalArray.append(dict as AnyObject)
            }
            submitQuizAPI()
        }else if(optionType == "checkbox"){
            for i in 0...self.selecedNamesArray.count - 1 {
                print(i)
                let name = self.selecedNamesArray[i]
                let id = self.selecedIdArray[i]
               
                var dict = [String:AnyObject]()
                dict.updateValue(name as AnyObject, forKey: "answer")
                dict.updateValue(id as AnyObject, forKey: "answer_id")
                lastFinalArray.append(dict as AnyObject)
            }
             submitQuizAPI()
        }else{
            
            if self.selecedNamesArray.count>0{
                
                let name = self.selecedNamesArray[0]
                let id = self.selecedIdArray[0]
                var dict = [String:AnyObject]()
                dict.updateValue(name as AnyObject, forKey: "answer")
                dict.updateValue(id as AnyObject, forKey: "answer_id")
                lastFinalArray.append(dict as AnyObject)
                submitQuizAPI()
            }else{
               
               self.themes.showToast(message: "Please Choose any one option", sender: self)
                
            }
            
          
            
        }
    }
    func submitQuizAPI(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let urserId = themes.getUserId()
            
            var dict = [String : AnyObject]()
            dict.updateValue(lastFinalArray
                as AnyObject, forKey: "quiz")
            dict.updateValue(urserId as AnyObject, forKey: "user_id")
            dict.updateValue(questionID as AnyObject, forKey: "question_id")
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:updateQuiz, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    if(self.optionType == "rating"){
                       
                    }else if(self.optionType == "checkbox"){
                       
                    }else{
                       
                        let SignVc = self.storyboard?.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
                        self.navigationController?.pushViewController(SignVc, animated: true)
                    }
                    
                    
                    
                    self.themes.showAlert(title: "Success ", message: "Updated successfully.", sender: self)
                } else {
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UITableView {
    
    public func reloadData(_ completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: {
            self.reloadData()
        }, completion:{ _ in
            completion()
        })
    }
    
    func scroll(to: scrollsTo, animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            let numberOfSections = self.numberOfSections
            let numberOfRows = self.numberOfRows(inSection: numberOfSections-1)
            switch to{
            case .top:
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: 0, section: 0)
                    self.scrollToRow(at: indexPath, at: .top, animated: animated)
                }
                break
            case .bottom:
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                    self.scrollToRow(at: indexPath, at: .bottom, animated: animated)
                }
                break
            }
        }
    }
    
    enum scrollsTo {
        case top,bottom
    }
}
