//
//  ChatVC.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 3/15/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
class ChatVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var namesArr = ["Blanche Mann","Harriett Saiazar","Calvin Shelton","Marget Wong","Robert Downy Jr","Chris Hemsworth","Chris Evens","Tobey Magure","Andrew garfeld","Tom Holland","Tom Hidltson","Huge Jackman","Rayn Ronalds","Mark Rufelow","Bendict","Chadwik Boshman"]
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    var chatList = [AnyObject]()
    
    var talkjsChatArr = [AnyObject]()

    @IBOutlet weak var chatTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.chatTableView.register(UINib(nibName: "ChatListTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatListTableViewCell")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
//        let alert = UIAlertController(title: "Alert", message: "Coming Soon!", preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//        self.present(alert, animated: true, completion: nil)
        
        
        let urserId = themes.getUserId()
        self.fetchPhotoRequest(userID: urserId)
        getChatListApi()
    }
    func getChatListApi(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
             let urserId = themes.getUserId()
            var dict = [String : String]()
            dict.updateValue(urserId , forKey: "user_id")
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:getChatList, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                     self.chatList = response["data"]as! [AnyObject]
                     self.chatTableView.reloadData()
                    
                    // self.fetchPhotoRequest(userID: urserId)
                    
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListTableViewCell", for: indexPath)as! ChatListTableViewCell
        
        let userdata = self.chatList[indexPath.row] as! [String:AnyObject]
        
      //  cell.selectionStyle = .none
        cell.namelabel.text = userdata["first_name"] as? String
        let photoUrl = self.themes.checkNullValue(userdata["photo1"] as AnyObject) as! String
        let PhotoFullUrl = ImagebaseUrl + photoUrl
        cell.profileImgView.sd_setImage(with: URL(string: PhotoFullUrl), placeholderImage: UIImage(named: "defaultPic"))
        cell.profileImgView.layer.cornerRadius = cell.profileImgView.frame.size.height/2
        cell.profileImgView.layer.borderColor = UIColor.blue.cgColor
        cell.profileImgView.layer.borderWidth = 0.5
        cell.profileImgView.layer.masksToBounds = true
        
        
        if talkjsChatArr.count > 0 {
            for data in talkjsChatArr{
                let id = userdata["id"] as? String
                let talkJs_lastMessage = data["lastMessage"] as! [String:AnyObject]
                let talkjs_ID = talkJs_lastMessage["senderId"] as! String
                
                if talkjs_ID == id {
                  cell.backview.backgroundColor = UIColor(red: 222/255, green: 231/255, blue: 221/255, alpha: 1.0)
                    return cell
                }else{
                   cell.backview.backgroundColor = UIColor.clear
                    return cell
                }
            }
        }
         cell.backview.backgroundColor = UIColor.clear
       
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 74
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let userdata = self.chatList[indexPath.row] as! [String:AnyObject]
        
        let chatvc = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC_TalkJS") as! ChatVC_TalkJS
        let photoUrl = self.themes.checkNullValue(userdata["photo1"] as AnyObject) as! String
        chatvc.userID = userdata["id"] as! String
        chatvc.userName = userdata["first_name"] as! String
        chatvc.userEmail = userdata["email"] as! String
        chatvc.photoUrl =  photoUrl
        
        self.navigationController?.pushViewController(chatvc, animated: true)
        
 
    }
    
    
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//
//            if editingStyle == .delete{
//                self.namesArr.remove(at: indexPath.row)
//                self.chatTableView.reloadData()
//            }
//        }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    func fetchPhotoRequest(userID: String)  {
        
        let userid = self.themes.getUserId()
        let urlString = "https://api.talkjs.com/v1/M5iybBK9/users/\(userid)/conversations?unreadsOnly=true"
        let session = URLSession.shared
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer sk_live_EXkW3yEcNrqQ9nlgg5SnvJ4g", forHTTPHeaderField: "Authorization")
        session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
            
            if let data = data
            {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    let JJSON = json as! [String:Any]
                   // print(JJSON["data"])
                    
                     self.talkjsChatArr = JJSON["data"] as! [AnyObject]
                     self.chatTableView.reloadData()
                    
                    
                   
                } catch {
                    print(error)
                }
            }
        }).resume()
        
        
        
    }
    
    

}
