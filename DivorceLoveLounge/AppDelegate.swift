//
//  AppDelegate.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 3/8/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//


// Commented the Newmembership planvc , myplanvc , Paymentvc , Stripe integration and its Navigations, constants, StripeKey, upgradeplan and myplan in profile page  and also related APIS , You need an active membership plan to view the profile. Proceed to membership plans?  in all screens.




import UIKit
import FBSDKCoreKit
import GoogleSignIn

import XMPPFramework
import MBProgressHUD
import CoreLocation


import CocoaLumberjack
//import Stripe
import UserNotifications
@UIApplicationMain



class AppDelegate: UIResponder, UIApplicationDelegate,GIDSignInDelegate,UNUserNotificationCenterDelegate,CLLocationManagerDelegate {
    var window: UIWindow?
    
    weak var xmppController: XMPPController!
    var locationManager:CLLocationManager!

    var chatName:String!
    var onlineStr:String!
    
    var allprofiles = [AnyObject]()
    var themes = Themes()
    
    #if MangostaREST
    var mongooseRESTController: MongooseAPI!
    #endif
    
    
    var myfavoriteRecordArray = [MyFavoritesRecord]()
    var favouritedMeRecordArray = [FavouritedMeRecord]()
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            // ...
            return
        }
        
        guard let authentication = user.authentication else { return }
        
    }
    

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    
//    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
//        print("dismissing Google SignIn")
//    }
//
//    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
//        print("presenting Google SignIn")
//    }
    
    
//    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
//        print("presenting Google SignIn")
//    }
    
 func checkForUpdate(completion:@escaping(Bool)->()){

        guard let bundleInfo = Bundle.main.infoDictionary,
            let currentVersion = bundleInfo["CFBundleShortVersionString"] as? String,
            let identifier = bundleInfo["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)")
            else{
            print("some thing wrong")
                completion(false)
            return
           }
        let task = URLSession.shared.dataTask(with: url) {
            (data, resopnse, error) in
            if error != nil{
                 completion(false)
                print("something went wrong")
            }else{
                do{
                    guard let reponseJson = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String:Any],
                    let result = (reponseJson["results"] as? [Any])?.first as? [String: Any],
                    let version = result["version"] as? String
                    else{
                        completion(false)
                        return
                    }
                    print("Current Ver:\(currentVersion)")
                    print("Prev version:\(version)")
                    if currentVersion != version{
                        completion(true)
                    }else{
                        completion(false)
                    }
                }
                catch{
                     completion(false)
                    print("Something went wrong")
                }
            }
        }
        task.resume()
    }
   
    func popupUpdateDialogue(){
         
           let alertMessage = "A new version of DivorceLovelounge Application is available. "
           let alert = UIAlertController(title: "New Version Available", message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
           let okBtn = UIAlertAction(title: "Update", style: .default, handler: {(_ action: UIAlertAction) -> Void in
               if let url = URL(string: "itms-apps://itunes.apple.com/us/app/magento2-mobikul-mobile-app/id1475233389"),
                   UIApplication.shared.canOpenURL(url){
                   if #available(iOS 10.0, *) {
                       UIApplication.shared.open(url, options: [:], completionHandler: nil)
                   } else {
                       UIApplication.shared.openURL(url)
                   }
               }
           })
           let noBtn = UIAlertAction(title:"Skip this Version" , style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
           })
           alert.addAction(okBtn)
           alert.addAction(noBtn)
           self.window?.rootViewController?.present(alert, animated: true, completion: nil)

       }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        var token: String = ""
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        print("device token",token)
        UserDefaults.standard.setValue(token, forKey: "kDeviceToken")
        UserDefaults.standard.synchronize()
        
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("register failed")
        
        
    }
    
    func registerForPushNotifications(application: UIApplication) {
        
        if #available(iOS 10.0, *){
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
                if (granted)
                {
                    DispatchQueue.main.async(execute: {
                        UIApplication.shared.registerForRemoteNotifications()
                    })                 }
                else{
                    //Do stuff if unsuccessful...
                    print("Don't Allow")
                    
                }
            })
        }else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 8 support
        else if #available(iOS 8, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 7 support
        else {
            application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: (UNNotificationPresentationOptions) -> Void) {
        // some other way of handling notification
        completionHandler([.alert, .sound,.alert])
    }
    
//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        
//        let userInfo = response.notification.request.content.userInfo
//        let aps = userInfo[AnyHashable("aps")] as! NSDictionary
//       // let type = aps.object(forKey: "type") as! String
//        
//        
//        
//        let keyExists = aps.object(forKey: "type") != nil
//        if keyExists{
//            print("The key is present in the dictionary")
//            
//            print("The key is not present in the dictionary")
//            let type = aps.object(forKey: "type") as! String
//            let user_id = UserDefaults.standard.object(forKey: "userID")
//            if user_id is NSNull || user_id == nil {
//                DispatchQueue.main.async {
//                    let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                    let initialViewControlleripad : ViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//                    self.window = UIWindow(frame: UIScreen.main.bounds)
//                    self.window?.rootViewController = initialViewControlleripad
//                    self.window?.makeKeyAndVisible()
//                }
//            } else {
//                if type == "message" {
//                    DispatchQueue.main.async {
//                        let appdelegate = UIApplication.shared.delegate as! AppDelegate
//                        
//                        let rootViewController = self.window!.rootViewController as! UINavigationController
//                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                        let MesgViewController = mainStoryboard.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController;
//                        MesgViewController.fromPushNotify = "Push"
//                        rootViewController.pushViewController(MesgViewController, animated: true)
//                        
//                    }
//                } else if type == "intrest" {
//                    DispatchQueue.main.async {
//                        let appdelegate = UIApplication.shared.delegate as! AppDelegate
//                        
//                        let rootViewController = self.window!.rootViewController as! UINavigationController
//                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                        let FavrViewController = mainStoryboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC;
//                        FavrViewController.fromscreen = "default"
//                        
//                        //FavrViewController.fromPushNotify = "Push"
//                        rootViewController.pushViewController(FavrViewController, animated: true)
//                        
//                    }
//                }
//                
//            }
//            
//           
//            
//        }
//        else {
//            DispatchQueue.main.async {
//                let appdelegate = UIApplication.shared.delegate as! AppDelegate
//                let rootViewController = self.window!.rootViewController as! UINavigationController
//                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let FavrViewController = mainStoryboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC;
//                FavrViewController.fromscreen = "chat"
//                rootViewController.pushViewController(FavrViewController, animated: true)
//            }
//        }
//
//        print(" recive notification called")
//    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print(userInfo)
        
        let aps = userInfo[AnyHashable("aps")] as! NSDictionary
        let state = UIApplication.shared.applicationState
        if state == .background {
            // background
            print("app in background state")
            if let user_id = UserDefaults.standard.object(forKey: "userID"){
                if user_id is NSNull || user_id == nil {
                    DispatchQueue.main.async {
                        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let initialViewControlleripad : ViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                        self.window = UIWindow(frame: UIScreen.main.bounds)
                        self.window?.rootViewController = initialViewControlleripad
                        self.window?.makeKeyAndVisible()
                    }
                } else{
                    
                     let keyExists = aps.object(forKey: "type") != nil
                    if keyExists{
                         let type = aps.object(forKey: "type") as! String
                        if type == "messages" {
                            DispatchQueue.main.async {
                                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                                
                                let rootViewController = self.window!.rootViewController as! UINavigationController
                                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let MesgViewController = mainStoryboard.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController;
                                MesgViewController.fromPushNotify = "Push"
                                rootViewController.pushViewController(MesgViewController, animated: true)
                                
                            }
                        } else if type == "like" {
                            DispatchQueue.main.async {
                                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                                
                                let rootViewController = self.window!.rootViewController as! UINavigationController
                                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let FavrViewController = mainStoryboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC;
                                
                                FavrViewController.fromscreen = "default"
                                
                                rootViewController.pushViewController(FavrViewController, animated: true)
                                
                            }
                        }
                        
                    }else {
                        
                        DispatchQueue.main.async {
                            let appdelegate = UIApplication.shared.delegate as! AppDelegate
                            let rootViewController = self.window!.rootViewController as! UINavigationController
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let FavrViewController = mainStoryboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC;
                            FavrViewController.fromscreen = "chat"
                            rootViewController.pushViewController(FavrViewController, animated: true)
                        }
                    }
                  
                }
            }
        }
        else if state == .active {
            
        }
       

    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
     
        registerForPushNotifications(application: application)
        
         let current = UNUserNotificationCenter.current()
        current.getNotificationSettings(completionHandler: { (settings) in
            if settings.authorizationStatus == .notDetermined {
                // Notification permission has not been asked yet, go for it!
                
                print("Notification permission has not been asked yet, go for it!")
            }
            
            if settings.authorizationStatus == .denied {
                // Notification permission was previously denied, go to settings & privacy to re-enable
                print(" Notification permission was previously denied, go to settings & privacy to re-enable")
                
                
                
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                    (granted, error) in
                    
                    // add your own
                    UNUserNotificationCenter.current().delegate = self
                    
                    let alertController = UIAlertController(title: "Notification Alert", message: "Please enable notifications", preferredStyle: .alert)
                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                            return
                        }
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            })
                        }
                    }
                    let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                    alertController.addAction(cancelAction)
                    alertController.addAction(settingsAction)
                    
                    
                    
                    DispatchQueue.main.async {
                        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                        
                    }
                }
            }
            
            
            
            if settings.authorizationStatus == .authorized {
                // Notification permission was already granted
                print("Notification permission was already granted")
            }
        })
        
        
//        let user_id = UserDefaults.standard.object(forKey: "user_id")
//
//        if user_id  != nil {
//
//            let rootViewController = self.window!.rootViewController as! UINavigationController
//            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
//            rootViewController.pushViewController(profileViewController, animated: true)
//
//
//        }else {
//
//
//            let rootViewController = self.window!.rootViewController as! UINavigationController
//            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//            rootViewController.pushViewController(profileViewController, animated: true)
//
//
//        }
        
//
        FBSDKApplicationDelegate.sharedInstance()?.application(application, didFinishLaunchingWithOptions: launchOptions)
        GIDSignIn.sharedInstance().clientID = "606382327519-026k1lp30i0215nij9faa41rvpklc0ak.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        
        
        
        // Override point for customization after application launch.
         let checkLogIn = UserDefaults.standard.object(forKey: "checkLogIn")
         let checkTutorial = UserDefaults.standard.object(forKey: "checkTutorial")
         let checkTips = UserDefaults.standard.object(forKey: "checkTips")
         let imageUpload = UserDefaults.standard.object(forKey: "imageUpload")

        if checkLogIn is NSNull || checkLogIn == nil {

            let rootViewController = self.window!.rootViewController as! UINavigationController
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            rootViewController.pushViewController(profileViewController, animated: true)
        }else if(imageUpload is NSNull || imageUpload == nil){
            let rootViewController = self.window!.rootViewController as! UINavigationController
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "EditProfilePicVC") as! EditProfilePicVC
            profileViewController.fromScreen = "AppDelegate"
            rootViewController.pushViewController(profileViewController, animated: true)
            
        }else{

            let rootViewController = self.window!.rootViewController as! UINavigationController
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
            rootViewController.pushViewController(profileViewController, animated: true)
        }
 
        if checkTutorial is NSNull || checkTutorial == nil {

//            let rootViewController = self.window!.rootViewController as! UINavigationController
//            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "PagingVC") as! PagingVC
//            rootViewController.pushViewController(profileViewController, animated: true)

        }else if checkLogIn is NSNull || checkLogIn == nil {
            
//            let rootViewController = self.window!.rootViewController as! UINavigationController
//            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//            rootViewController.pushViewController(profileViewController, animated: true)
            
        }else if checkTips is NSNull || checkTips == nil {
            
//            let rootViewController = self.window!.rootViewController as! UINavigationController
//            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "TipsVC") as! TipsVC
//            rootViewController.pushViewController(profileViewController, animated: true)
//
        }else{
   
//            let rootViewController = self.window!.rootViewController as! UINavigationController
//            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
//            rootViewController.pushViewController(profileViewController, animated: true)
            
            
            
//            
//            let checkProfile1 = UserDefaults.standard.object(forKey: "checkProfile1")
//            let checkProfile2 = UserDefaults.standard.object(forKey: "checkProfile2")
//            let checkProfile3 = UserDefaults.standard.object(forKey: "checkProfile3")
//             let checkProfile4 = UserDefaults.standard.object(forKey: "checkProfile4")
//
//            if checkProfile1 is NSNull || checkProfile1 == nil {
//                let rootViewController = self.window!.rootViewController as! UINavigationController
//                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
//                rootViewController.pushViewController(profileViewController, animated: true)
////                let rootViewController = self.window!.rootViewController as! UINavigationController
////                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
////                let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
////                rootViewController.pushViewController(profileViewController, animated: true)
//            }else if(checkProfile2 is NSNull || checkProfile2 == nil){
//                let rootViewController = self.window!.rootViewController as! UINavigationController
//                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "ProfileSetUpVC") as! ProfileSetUpVC
//                rootViewController.pushViewController(profileViewController, animated: true)
//            }else if(checkProfile3 is NSNull || checkProfile3 == nil){
//                let rootViewController = self.window!.rootViewController as! UINavigationController
//                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "MatchPreferenceVC") as! MatchPreferenceVC
//                rootViewController.pushViewController(profileViewController, animated: true)
//            }else if(checkProfile4 is NSNull || checkProfile4 == nil){
//                let rootViewController = self.window!.rootViewController as! UINavigationController
//                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "CompitibilityQuizProfileVC") as! CompitibilityQuizProfileVC
//                rootViewController.pushViewController(profileViewController, animated: true)
//            }else {
//                let rootViewController = self.window!.rootViewController as! UINavigationController
//                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
//                rootViewController.pushViewController(profileViewController, animated: true)
//            }

            
            // Naveen Stripe
            
//Stripe.setDefaultPublishableKey("pk_test_MTLciMArdTGMNKAisIxdcGvk00PTzZsTH5")

            let dirPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            print("App Path: \(dirPaths)")
            
            DDLog.add(DDTTYLogger.sharedInstance, with: DDLogLevel.verbose)
            XMPPController.sharedInstance.connect()
            // XMPPController.sharedInstance.pushNotificationsDelegate = self
     
        }
        
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.startUpdatingLocation()
        }
     
        checkForUpdate { (isUpdate) in
                     print("Update needed:\(isUpdate)")
                     if isUpdate{
                         DispatchQueue.main.async {
                             print("new update Available")
                            self.popupUpdateDialogue()
                         }
                     }
                 }
        
         
       return true
    }
    
    
  //   This method is where you handle URL opens if you are using univeral link URLs (eg "https://example.com/stripe_ios_callback")
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        // Naveen Stripe
        
//        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
//            if let url = userActivity.webpageURL {
//                let stripeHandled = Stripe.handleURLCallback(with: url)
//
//                if (stripeHandled) {
//                    return true
//                }
//                else {
//                    // This was not a stripe url, do whatever url handling your app
//                    // normally does, if any.
//                }
//            }
        
  //      }
        return false
    }
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any])
        -> Bool {
            return GIDSignIn.sharedInstance().handle(url,
                                                     sourceApplication:options[UIApplication.OpenURLOptionsKey
                                                        .sourceApplication] as? String,
                                                     annotation: [:])
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: sourceApplication,
                                                 annotation: annotation)
    }
    func applicationWillResignActive(_ application: UIApplication) {
        
         XMPPController.sharedInstance.goOffLine()
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        let userId = self.themes.checkNullValue(themes.getUserId()) as! String
        
        if userId == "" {
            
        } else {
        self.onlineStatusApi(user_id: userId, status: "0")
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
         let userId = self.themes.checkNullValue(themes.getUserId()) as! String
               
               if userId == "" {
                   
               } else {
               self.onlineStatusApi(user_id: userId, status: "1")
               }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        XMPPController.sharedInstance.goOnline()
        
        let userId = self.themes.checkNullValue(themes.getUserId()) as! String
               
               if userId == "" {
                   
               } else {
               self.onlineStatusApi(user_id: userId, status: "1")
               }
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
      let userId = self.themes.checkNullValue(themes.getUserId()) as! String
               
               if userId == "" {
                   
               } else {
               self.onlineStatusApi(user_id: userId, status: "0")
               }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation :CLLocation = locations[0] as CLLocation
     //   print("user latitude = \(userLocation.coordinate.latitude)")
     //   print("user longitude = \(userLocation.coordinate.longitude)")
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count>0{
                let placemark = placemarks![0]
                print(placemark.locality!)
                print(placemark.administrativeArea!)
                print(placemark.country!)
                self.themes.saveCurrentCountry(placemark.country!)
                self.themes.saveCurrentcity(placemark.locality!)
                self.themes.saveCurrentstate(placemark.administrativeArea!)
                self.locationManager.stopUpdatingLocation()
            }
        }
        
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }

    func onlineStatusApi(user_id: String, status : String) {
           let networkRechability = urlService.connectedToNetwork()
           if(networkRechability){

               let dict = ["user_id":user_id , "login_status": status]
    
               print("online status parameters is \(dict)")
               urlService.serviceCallPostMethodWithParams(url:loginStatus, params: dict as Dictionary<String, AnyObject>) { response in
                   print(response)
                   let success = response["status"] as! String
                   if(success == "1"){

                   }
                   else {
                       let result = response["result"] as! String
                   }
               }
           } else {
            
           }
       }
    
    
    func initializeNotificationServices() -> Void {
        
        let settings = UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
        // This is an asynchronous method to retrieve a Device Token
        // Callbacks are in AppDelegate.swift
        // Success = didRegisterForRemoteNotificationsWithDeviceToken
        // Fail = didFailToRegisterForRemoteNotificationsWithError
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func configureAndStartStream() {
        self.xmppController = XMPPController.sharedInstance
        self.xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
        _ = self.xmppController.connect()
        
    }
    

}
extension AppDelegate: XMPPControllerPushNotificationsDelegate {
    
    func xmppControllerDidPrepareForPushNotificationsSupport(_ controller: XMPPController) {
        OperationQueue.main.addOperation {
            self.initializeNotificationServices()
        }
    }
    
    func xmppController(_ controller: XMPPController, didReceivePrivateChatPushNotificationFromContact contact: XMPPUser) {
        OperationQueue.main.addOperation {
            // (self.window?.rootViewController as! TabBarController).handlePrivateChatPushNotification(from: contact)
        }
    }
    
    func xmppController(_ controller: XMPPController, didReceiveGroupChatPushNotificationIn room: XMPPRoomLight) {
        OperationQueue.main.addOperation {
            //  (self.window?.rootViewController as! TabBarController).handleGroupChatPushNotification(in: room)
        }
    }
    
    func xmppController(_ controller: XMPPController, didReceiveChatPushNotificationFromUnknownSenderWithJid senderJid: XMPPJID)
    {
        // TODO: can a room notification arrive before affiliation message?
        // TODO: handle private chat notifications from senders not on the roster
        NSLog("Received a push notification from an unknown sender: \(senderJid.full())")
    }
    
    
    
}

        
