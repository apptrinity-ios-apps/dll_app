//
//  IAPProduct.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 09/01/20.
//  Copyright © 2020 com.Indobytes. All rights reserved.
//

import Foundation

enum IAPProduct: String {
    
    case nonRenewingSubcription = "com.4print.DLL.DLL12MonthsSubscription"
    case nonRenewing1MonthSubcription = "com.4print.DLL.DLL1MonthSubscription"
    case nonRenewingOneMonthSubcription = "com.4print.DLL.1MonthSubscription"
    case nonRenewing3MonthSubcription = "com.4print.DLL.3MonthsSubscription"
    case nonRenewing12MonthSubcription = "com.4print.DLL.12MonthSubscription"
    case nonRenewingTestSubcription = "com.4print.DLL.TestingSubScription"
    
}

