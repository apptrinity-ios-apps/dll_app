//
//  DashBoardCollectionViewCell.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 06/06/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class DashBoardCollectionViewCell: UICollectionViewCell {
    
    var sendIntrstbtnTapAction : (()->())?
    var favrBtnTapAction : (()->())?
  
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var ageLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var favrBtn: UIButton!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var infoBtn: UIButton!
    
   
    @IBAction func likeButtonTapped(_ sender: UIButton) {
        sendIntrstbtnTapAction?()
    }
    @IBAction func favourButtonTapped(_ sender: UIButton) {
        favrBtnTapAction?()
    }
    
}
