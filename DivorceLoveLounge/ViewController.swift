//
//  ViewController.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 3/8/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn

import XMPPFramework
import MBProgressHUD

class ViewController: UIViewController,UITextFieldDelegate,GIDSignInUIDelegate,GIDSignInDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    
//    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
//        
//    }
//    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
//    }
    
    
    weak var xmppController: XMPPController!
    
    
    var adsListArr = [AnyObject]()
    
    @IBOutlet var adBannerView: UIView!
    
    
    @IBOutlet var adsBannerCollectionView: UICollectionView!
    
    
    @IBOutlet var adBannerViewHieghtConstant: NSLayoutConstraint!
    
    
    @IBOutlet weak var forgotPasswordtextField: UITextField!
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    var loginrecord = LoginRecord()
    @IBOutlet weak var gmailSignInBtn: GIDSignInButton!
    @IBOutlet weak var fbBtn: FBSDKLoginButton!
    @IBAction func gmailSignIn(_ sender: Any) {
        UserDefaults.standard.set("GMAIL", forKey: "getSignUpHint")
        GIDSignIn.sharedInstance().delegate=self
        GIDSignIn.sharedInstance().uiDelegate=self
        GIDSignIn.sharedInstance()?.signIn()
    }
      @IBAction func googlePlusBtnTouch(_ sender: Any) {
        GIDSignIn.sharedInstance().delegate=self
        GIDSignIn.sharedInstance().uiDelegate=self
        GIDSignIn.sharedInstance().signIn()
    }
    @IBOutlet weak var resetBtn: UIButton!
    @IBOutlet var forgotPaawordView: UIView!
    @IBOutlet weak var gmailBtn: UIButton!
    @IBOutlet weak var logBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var alphaView: UIView!
    @IBOutlet weak var logPopUpView: UIView!
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var logInBtn: UIButton!
    
    @IBAction func logAction(_ sender: Any) {
        
        self.alphaView.isHidden = false
        self.logPopUpView.isHidden = false
        self.adBannerView.isHidden = false
        
        if self.adsListArr.count > 0 {
            self.adBannerView.isHidden = false
            self.adBannerViewHieghtConstant.constant = 120
            self.setTimer()
        }else{
            self.adBannerView.isHidden = true
            self.adBannerViewHieghtConstant.constant = 0
            
        }
        
    }
    
    
    @IBAction func signUpAction(_ sender: Any) {
        UserDefaults.standard.set("SIGNUP", forKey: "getSignUpHint")
        let SignVc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(SignVc, animated: true)
    }
    @IBAction func faceBkAction(_ sender: Any) {
        UserDefaults.standard.set("FACEBOOK", forKey: "getSignUpHint")
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                // if user cancel the login
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                }
            }
        }
    }
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    print(result)
                    
                 let info = result as! [String : AnyObject]
                    
                    let firstName = info["first_name"]
                    let mail = info["email"]
                    UserDefaults.standard.set(firstName, forKey: "getFacebookGmailName")
                    self.gmailLoginApi(email: mail as! String, name: firstName as! String, type: "facebook")
                    
                    
                    
                }
            })
        }
    }
    @IBAction func googleAction(_ sender: Any) {
        
        
    }
    @IBAction func resetPasswordAction(_ sender: Any) {
        
        if  isValidEmail(testStr: forgotPasswordtextField.text!)
        {
            forgotPasswordAPI()
        } else {
            self.themes.showAlert(title: "Alert", message: "Please enter valid emailId", sender: self)
        }
     }
    @IBAction func popUpDoneAction(_ sender: Any) {
        logPopUpView.isHidden = false
        FirstView(hidden: true)
    }
    @IBAction func loginAction(_ sender: Any) {
        
     //   IAPService.shared.purchase(product: .nonRenewingSubcription)
        if (ValidatedTextField()){
            if  isValidEmail(testStr: userNameField.text!)
              {
            } else {
                self.themes.showAlert(title: "Alert", message: "Please enter valid emailId", sender: self)
            }
            logIn(fromScreen: "login", userEmail: userNameField.text ?? "", userPassword: passwordField.text ?? "")
        }
        else{
        }

        
    }
    @IBAction func logCloseAction(_ sender: Any) {
        
        self.alphaView.isHidden = true
        self.logPopUpView.isHidden = true
        self.adBannerView.isHidden = true
    }
    @IBAction func forgotPaswrdActn(_ sender: Any) {
        
        logPopUpView.isHidden = true
         self.adBannerView.isHidden = true
        FirstView(hidden: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       //fbBtn.readPermissions = ["email"]
        //GIDSignIn.sharedInstance().uiDelegate = self
        
        
     //   IAPService.shared.getProducts()
        
//        GIDSignIn.sharedInstance().signIn()
//        fbBtn.delegate = self
        
        self.alphaView.isHidden = true
        self.logPopUpView.isHidden = true
        self.adBannerView.isHidden = true
        
        self.logBtn.layer.cornerRadius = (logBtn.frame.size.height)/2
        self.signUpBtn.layer.cornerRadius = (signUpBtn.frame.size.height)/2
        
        self.logPopUpView.layer.cornerRadius = 10
        
        self.adBannerView.clipsToBounds = true
        self.adBannerView.layer.cornerRadius = 10
       
        self.forgotPaawordView.layer.cornerRadius = 10
        self.resetBtn.layer.cornerRadius = (resetBtn.frame.size.height)/2
        
        self.logInBtn.layer.cornerRadius = (logInBtn.frame.size.height)/2
        self.userNameField.layer.cornerRadius = 22
        self.userNameField.layer.borderWidth = 1
        self.userNameField.layer.borderColor = UIColor(red: 205/255, green: 205/255, blue: 205/255, alpha: 1.0).cgColor
        userNameField.attributedPlaceholder = NSAttributedString(string: "Enter your Email",
        attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: CGFloat(28/255.0), green: CGFloat(37/255.0), blue: CGFloat(42/255.0), alpha: CGFloat(1.0) )])
        // For Right side Padding
        userNameField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: userNameField.frame.height))
        userNameField.leftViewMode = .always
        
        
        self.forgotPasswordtextField.layer.cornerRadius = 22
        self.forgotPasswordtextField.layer.borderWidth = 1
        self.forgotPasswordtextField.layer.borderColor = UIColor(red: 205/255, green: 205/255, blue: 205/255, alpha: 1.0).cgColor
        forgotPasswordtextField.attributedPlaceholder = NSAttributedString(string: "Email",
                                                                 attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: CGFloat(28/255.0), green: CGFloat(37/255.0), blue: CGFloat(42/255.0), alpha: CGFloat(1.0) )])
        // For Right side Padding
        forgotPasswordtextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: userNameField.frame.height))
        forgotPasswordtextField.leftViewMode = .always
        
        
        // For left side Padding
        userNameField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: userNameField.frame.height))
        userNameField.rightViewMode = .always
        
        self.passwordField.layer.cornerRadius = 22
        self.passwordField.layer.borderWidth = 1
        self.passwordField.layer.borderColor = UIColor(red: 205/255, green: 205/255, blue: 205/255, alpha: 1.0).cgColor
        passwordField.attributedPlaceholder = NSAttributedString(string: "Password",
        attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: CGFloat(28/255.0), green: CGFloat(37/255.0), blue: CGFloat(42/255.0), alpha: CGFloat(1.0) )])
        // For Right side Padding
        passwordField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: passwordField.frame.height))
        passwordField.leftViewMode = .always
        // For left side Padding
        passwordField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: passwordField.frame.height))
        passwordField.rightViewMode = .always
        
        self.userNameField.delegate = self
        self.passwordField.delegate = self
        
        
        self.adsBannerCollectionView.register(UINib(nibName: "AdsBannerCollecctionCell", bundle: nil), forCellWithReuseIdentifier: "AdsBannerCollecctionCell")
        
        
// Keyboard Action
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardUp), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardDown), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
      //  self.view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view, typically from a nib.
//        GIDSignIn.sharedInstance()?.delegate = self
//        GIDSignIn.sharedInstance().uiDelegate = self
//        GIDSignIn.sharedInstance().signIn()
        
        adsListServiceApi(ScreenType: "login")
        
        
    }
    func FirstView(hidden :Bool){
        
            
                forgotPaawordView.center = self.view.center
                forgotPaawordView.alpha = 1
                forgotPaawordView.isHidden = hidden
                self.view.addSubview(forgotPaawordView)
          
        
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            UserDefaults.standard.set(fullName, forKey: "getFacebookGmailName")
            
            gmailLoginApi(email: email!, name: fullName!, type: "google")
            print(userId,idToken,fullName,givenName,familyName,email)
            
        }
    }
    private func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        //myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    private func signIn(signIn: GIDSignIn!,
                presentViewController viewController: UIViewController!) {
       // self.presentViewController(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    private func signIn(signIn: GIDSignIn!,
                dismissViewController viewController: UIViewController!) {
       // self.dismissViewControllerAnimated(true, completion: nil)
    }
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        userNameField.resignFirstResponder()
        passwordField.resignFirstResponder()
    }

    @objc func keyboardUp(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            //
            self.view.frame.origin.y = 200
            if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardRectangle = keyboardFrame.cgRectValue
                let keyboardHeight = keyboardRectangle.height
                self.view.frame.origin.y -= keyboardHeight
            }
        }
    }
    @objc func keyboardDown(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            self.view.frame.origin.y = 0
        }
    }
    
    
    
    
    func logIn(fromScreen:String,userEmail:String ,userPassword:String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
                        themes.showActivityIndicator(uiView: self.view)
            let devicetoken =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "kDeviceToken") )
            let parameters = [ "email":userEmail, "password":userPassword,
                               "device_token":devicetoken as! String , "device_type" : "IOS"
                               ] as [String : Any]
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:login, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                //UserDefaults.standard.set(true, forKey: "checkTips")
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    
                    
                    
                    
                      UserDefaults.standard.set(true, forKey: "checkLogIn")
                    // UserDefaults.standard.set(nil, forKey: "checkLogIn")
                    let userInfo = response["user_info"]as! [String : AnyObject]
                    let id = self.themes.checkNullValue(userInfo["id"])
                    let date_id = self.themes.checkNullValue(userInfo["date_id"])
                    
                    let gender = self.themes.checkNullValue(userInfo["gender"])
                    
                    let lookingfor = self.themes.checkNullValue(userInfo["looking_for"])
                    
                    let firstName = self.themes.checkNullValue(userInfo["first_name"])
                    
                    UserDefaults.standard.set(gender, forKey: "getGender")
                    UserDefaults.standard.set(firstName, forKey: "getUserName")
                    
                    let email = self.themes.checkNullValue(userInfo["email"])
                    
                    let zipcode = self.themes.checkNullValue(userInfo["zipcode"])
                    
                    let state = self.themes.checkNullValue(userInfo["state"])
                    
                    let phNumber = self.themes.checkNullValue(userInfo["phone_number"])
                    
                    let email_notification = self.themes.checkNullValue(userInfo["email_notification_status"]) as! String
                    
                    let push_notification = self.themes.checkNullValue(userInfo["push_notification_status"]) as! String
                    
                    UserDefaults.standard.set(userEmail, forKey: "LoginUserName")
                    UserDefaults.standard.set(userPassword, forKey: "LoginPassword")
                    
                    let referal_from = self.themes.checkNullValue(userInfo["referal_from"])
                    
                    let children = self.themes.checkNullValue(userInfo["children"])
                    
                    let city = self.themes.checkNullValue(userInfo["city"])
                    
                     let country = self.themes.checkNullValue(userInfo["country"])
                    
                    let dob1 = self.themes.checkNullValue(userInfo["dob"])
                    
                 //   let dob = self.themes.dateFormateConverter(date: dob1 as! String)
                    
                    let ethnicity = self.themes.checkNullValue(userInfo["ethnicity"])
                    
                    let religion = self.themes.checkNullValue(userInfo["religion"])
                    
                    let denomination = self.themes.checkNullValue(userInfo["denomination"])
                    
                    let education = self.themes.checkNullValue(userInfo["education"])
                    
                    let occupation = self.themes.checkNullValue(userInfo["occupation"])
                    
                    
                    let income = self.themes.checkNullValue(userInfo["income"])
                    
                    let smoke = self.themes.checkNullValue(userInfo["smoke"])
                    
                    let drink = self.themes.checkNullValue(userInfo["drink"])
                    
                    let heightFt = self.themes.checkNullValue(userInfo["height_ft"])as? String
                    let heightIn = self.themes.checkNullValue(userInfo["height_inc"])as? String
                    let height = heightFt! + "." + heightIn!
                    
                    let passionate = self.themes.checkNullValue(userInfo["passionate"])
                    
                    let leisure_time = self.themes.checkNullValue(userInfo["leisure_time"])
                    
                    let thankful_1 = self.themes.checkNullValue(userInfo["thankful_1"])
                    
                    let thankful_2 = self.themes.checkNullValue(userInfo["thankful_2"])
                    
                    let thankful_3 = self.themes.checkNullValue(userInfo["thankful_3"])
                    
                    let partner_age_min = self.themes.checkNullValue(userInfo["partner_age_min"])
                    
                    let partner_age_max = self.themes.checkNullValue(userInfo["partner_age_max"])
                    
                    let partner_match_distance = self.themes.checkNullValue(userInfo["partner_match_distance"])
                    
                    let partner_match_search = self.themes.checkNullValue(userInfo["partner_match_search"])
                    
                    let partner_ethnicity = self.themes.checkNullValue(userInfo["partner_ethnicity"])
                    
                    let partner_religion = self.themes.checkNullValue(userInfo["partner_religion"])
                    
                    let partner_education = self.themes.checkNullValue(userInfo["partner_education"])
                    
                    let partner_occupation = self.themes.checkNullValue(userInfo["partner_occupation"])
                    
                    let partner_smoke = self.themes.checkNullValue(userInfo["partner_smoke"])
                    
                    let partner_denomination = self.themes.checkNullValue(userInfo["partner_denomination"])
                    
                    
                    let partner_drink = self.themes.checkNullValue(userInfo["partner_drink"])
                    
                    let photo1 = self.themes.checkNullValue(userInfo["photo1"]) as! String
                    
                    let photo1_approve = self.themes.checkNullValue(userInfo["photo1_approve"])
                    
                    let photo2 = self.themes.checkNullValue(userInfo["photo2"])
                    
                    let photo2_approve = self.themes.checkNullValue(userInfo["photo2_approve"])
                    
                    let photo3 = self.themes.checkNullValue(userInfo["photo3"])
                     UserDefaults.standard.set(self.themes.checkNullValue(userInfo["state"]), forKey: "selectedState")
                    
                    
                    let photo3_approve = self.themes.checkNullValue(userInfo["photo3_approve"])
                    
                    let photo4 = self.themes.checkNullValue(userInfo["photo4"])
                    
                    let photo4_approve = self.themes.checkNullValue(userInfo["photo4_approve"])
                    
                    
                    let photo5 = self.themes.checkNullValue(userInfo["photo5"])
                    
                    let photo5_approve = self.themes.checkNullValue(userInfo["photo5_approve"])
                    
                    let photo6 = self.themes.checkNullValue(userInfo["photo6"])
                    
                    let photo6_aprove = self.themes.checkNullValue(userInfo["photo6_aprove"])
                    
                    let signup_step = self.themes.checkNullValue(userInfo["signup_step"])
                    
                    let status = self.themes.checkNullValue(userInfo["status"])
                    
                    self.themes.saveUserID(id as? String)
                    
                    self.themes.savedate_id(date_id as? String)
                    
                    self.themes.saveGender(gender as? String)
                    
                    self.themes.savelooking_for(lookingfor as? String)
                    
                    self.themes.saveFirstName(firstName as? String)
                    
                    self.themes.saveuserEmail(email as? String)
                    
                    self.themes.saveEmailNotification(email_notification)
                    
                    self.themes.savePushNotification(push_notification)
                    
                    self.themes.saveZipcode(zipcode as? String)
                    
                    self.themes.savestate(state as? String)
                    
                    self.themes.saveMobileNumber(phNumber as? String)
                    
                    self.themes.savereferal_from(referal_from as? String)
                    
                    self.themes.saveChildren(children as? String)
                    
                    self.themes.savecity(city as? String)
                    
                    self.themes.savecountry(country as? String)
                    
                    self.themes.savedob(dob1 as? String)
                    
                    self.themes.saveEthinicity(ethnicity as? String)
                    
                    self.themes.saveReligion(religion as? String)
                    
                    self.themes.savedenomination(denomination as? String)
                    
                    self.themes.saveEducation(education as? String)
                    
                    self.themes.saveoccupation(occupation as? String)
                    
                    self.themes.saveIncome(income as? String)
                    
                    self.themes.savesmoke(smoke as? String)
                    
                    self.themes.savePartnerDenomination(partner_denomination as? String)
                    
                    self.themes.savedrink(drink as? String)
                    
                    self.themes.saveHeight(height as? String)
                    
                    self.themes.savepassionate(passionate as? String)
                    
                    self.themes.saveleisure_time(leisure_time as? String)
                    
                    self.themes.savethankful_1(thankful_1 as? String)
                    
                    self.themes.savethankful_2(thankful_2 as? String)
                    
                    self.themes.savethankful_3(thankful_3 as? String)
                    
                    self.themes.savepartner_age_max(partner_age_max as? String)
                    
                    self.themes.savepartner_age_min(partner_age_min as? String)
                    
                    self.themes.savepartner_match_distance(partner_match_distance as? String)
                    
                    self.themes.savepartner_match_search(partner_match_search as? String)
                    
                    self.themes.savepartner_ethnicity(partner_ethnicity as? String)
                    
                    self.themes.savepartner_religion(partner_religion as? String)
                    
                    self.themes.savepartner_education(partner_education as? String)
                    
                    self.themes.savepartner_occupation(partner_occupation as? String)
                    
                    self.themes.savepartner_smoke(partner_smoke as? String)
                    
                    self.themes.savepartner_drink(partner_drink as? String)
                    
                    self.themes.savephoto1(photo1 as? String)
                    
                    self.themes.savephoto2(photo2 as? String)
                    
                    self.themes.savephoto3(photo3 as? String)
                    
                    self.themes.savephoto4(photo4 as? String)
                    
                    self.themes.savephoto5(photo5 as? String)
                    
                    self.themes.savephoto6(photo6 as? String)
                    
                    self.themes.savephoto1_approve(photo1_approve as? String)
                    
                    self.themes.savephoto2_approve(photo2_approve as? String)
                    
                    self.themes.savephoto3_approve(photo3_approve as? String)
                    
                    self.themes.savephoto4_approve(photo4_approve as? String)
                    
                    self.themes.savephoto5_approve(photo5_approve as? String)
                    
                    self.themes.savephoto6_aprove(photo6_aprove as? String)
                    
                    self.themes.savesignup_step(signup_step as? String)
                    
                    let photo_approve = self.themes.checkNullValue(userInfo["photo1_approve"]) as! String
                    
                    if(photo_approve == "UNAPPROVED"){
                       self.themes.savePhotoApproveStatus(false)
                    }else{
                        self.themes.savePhotoApproveStatus(true)
                    }
                    
                    self.themes.savestatus(status as? String)
                    
                    let xmppPassword = self.themes.checkNullValue(userInfo["password"])
                    let XmppUserName = self.themes.checkNullValue(userInfo["username"])
//                    
                    self.themes.saveXmppUserName(XmppUserName as? String)
                    self.themes.savexmppPassword(xmppPassword as? String)
//                    
//                    
//                    
//                    
//                    // for xmpp connecting
//                    var servername = UITextField()
//                    servername.text = "divorcelovelounge.com"
//                    var password = UITextField()
//                    password.text = xmppPassword as? String
//                    var username = UITextField()
//                    username.text = "\(XmppUserName)@divorcelovelounge.com"
//                    if let serverText = servername.text, (servername.text?.characters.count)! > 0 {
//                        let auth = AuthenticationModel(jidString: username.text!, serverName: servername.text!, password: password.text!)
//                        auth.save()
//                    } else {
//                        let auth = AuthenticationModel(jidString: username.text!, password: password.text!)
//                        auth.save()
//                    }
//                    self.configureAndStartStream()
//                    
//                    
                    
                    
                    
                    
                    if fromScreen == "login"{

                        let signUpStep =  self.themes.checkNullValue( userInfo["signup_step"] as! AnyObject) as! String  //userInfo["signup_step"] as! String
                        if(signUpStep == "1"){
                            UserDefaults.standard.set(true, forKey: "checkProfile1")
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            UserDefaults.standard.set(true, forKey: "checkProfile1")
//                            let vc = storyboard.instantiateViewController(withIdentifier: "ProfileSetUpVC") as! ProfileSetUpVC
//                            self.navigationController?.pushViewController(vc, animated: true)
                        }else if(signUpStep == "2"){
                               UserDefaults.standard.set(true, forKey: "checkProfile1")
                               UserDefaults.standard.set(true, forKey: "checkProfile2")
//                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//                            let vc = storyboard.instantiateViewController(withIdentifier: "MatchPreferenceVC") as! MatchPreferenceVC
//
//                            self.navigationController?.pushViewController(vc, animated: true)
                        }else if(signUpStep == "3"){
                             UserDefaults.standard.set(true, forKey: "checkProfile1")
                            UserDefaults.standard.set(true, forKey: "checkProfile2")
                               UserDefaults.standard.set(true, forKey: "checkProfile3")
//                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//                            let vc = storyboard.instantiateViewController(withIdentifier: "CompitibilityQuizProfileVC") as! CompitibilityQuizProfileVC
//                            self.navigationController?.pushViewController(vc, animated: true)
                        }else if(signUpStep == "4"){
                            UserDefaults.standard.set(true, forKey: "checkProfile1")
                            UserDefaults.standard.set(true, forKey: "checkProfile2")
                            UserDefaults.standard.set(true, forKey: "checkProfile3")
                               UserDefaults.standard.set(true, forKey: "checkProfile4")
                            let checkTips = UserDefaults.standard.object(forKey: "checkTips")

//                            if checkTips is NSNull || checkTips == nil {
//                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                                let vc = storyboard.instantiateViewController(withIdentifier: "TipsVC") as! TipsVC
//                                self.navigationController?.pushViewController(vc, animated: true)
//                            }else{
//
//                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                                let vc = storyboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
//                                self.navigationController?.pushViewController(vc, animated: true)
//
//                            }

                        }
                        
                        if(photo1 == ""){
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "EditProfilePicVC") as! EditProfilePicVC
                            vc.fromScreen = "AppDelegate"
                            self.navigationController?.pushViewController(vc, animated: true)
                     
                        }else{
                             UserDefaults.standard.setValue(true, forKey: "imageUpload")
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                    else{
                        
                    }
                 
                } else if success == "2"{
                    self.themes.showAlert(title: "Oops ☹️", message: response["result"] as! String, sender: self)
                    
                }else if success == "3"{
                    
                    let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopViewController") as! PopViewController
                    
                    popOverVC.fromScreen = "login"
                  //popOverVC.ImageStr = ""
                    popOverVC.headingMesg = "Oops ☹️"
                    popOverVC.subMesg = "The account you are Trying to view has been suspended. \n Please Contact Customer Support \n support@divorcelovelounge.com"
                    
                    self.addChildViewController(popOverVC)
                    popOverVC.view.center = self.view.frame.center
                    self.view.addSubview(popOverVC.view)
                    popOverVC.didMove(toParentViewController: self)
                    
//                    self.themes.showAlert(title: "Oops ☹️", message: "The account you are Trying to view has been suspended. \n Please Contact Customer Support \n support@divorcelovelounge.com", sender: self)
//                    
                } else {
                    
                 //   let result = response["result"] as! String
                //  self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    
                }
                
            }
            
        } else {
            
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
            
        }
        
    }
    
    
    
    
    func adsListServiceApi(ScreenType:String){
        self.urlService.adslistServiceCall(ScreenType: ScreenType) { response in
            let success = response["status"] as! String
            if(success == "1"){
                
                let data = response["data"] as! [AnyObject]
                self.adsListArr = data
                self.adsBannerCollectionView.delegate = self
                self.adsBannerCollectionView.dataSource = self
                self.adsBannerCollectionView.reloadData()
                
            }else{
                
            }
        }
    
    }
    
    func setTimer() {
        let _ = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(ViewController.autoScroll), userInfo: nil, repeats: true)
    }
    var x = 0
    @objc func autoScroll() {
        if self.x < self.adsListArr.count {
            let indexPath = IndexPath(item: x, section: 0)
            self.adsBannerCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.x = self.x + 1
        }else{
            self.x = 0
            self.adsBannerCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.adsListArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: adBannerView.frame.size.width, height: adBannerView.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = adsBannerCollectionView.dequeueReusableCell(withReuseIdentifier: "AdsBannerCollecctionCell", for: indexPath) as! AdsBannerCollecctionCell
        let data = self.adsListArr[indexPath.row] as! [String:AnyObject]
        let type = data["add_type"] as! String
        if type == "Custom" {
            cell.adsWebView.isHidden = true
            cell.adsImageView.isHidden = false
            let url = "\(adsUrl)\(data["add_image"] as! String)"
            cell.adsImageView.af_setImage(withURL: URL(string:url)!)
        }else{
            cell.adsWebView.isHidden = false
            cell.adsImageView.isHidden = true
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = self.adsListArr[indexPath.row] as! [String:AnyObject]
        let url = data["add_link"]
        if let url = URL(string: url as! String) {
            UIApplication.shared.open(url)
        }
    }
    
    
    func configureAndStartStream() {
        self.xmppController = XMPPController.sharedInstance
        self.xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
        // TODO: revert to UIActivityIndicatorView.
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud?.labelText = "Please wait"
        _ = self.xmppController.connect()
        
    }
    
    
    func forgotPasswordAPI(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let dict = ["email":forgotPasswordtextField.text]
            //            let parameters = ["gender":gender, "looking_for":lookingFor,"first_name":firstName, "zipcode":zipCode,"state":state,"email":mailField.text!,"password":passwordField.text!] as [String : String]
            
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:forgotPassword, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    self.logPopUpView.isHidden = false
                    self.adBannerView.isHidden = false
                    self.FirstView(hidden: true)
                    let message = response["Message"] as! String
                    self.themes.showAlert(title: "Success", message: message, sender: self)
                }else if success == "3"{
                self.themes.showAlert(title: "Oops ☹️", message: "The account you are Trying to view has been suspended. \n Please Contact Customer Support \n support@divorcelovelounge.com", sender: self)
                }
                else {
                    let result = response["Message"] as! String
                    self.themes.showAlert(title: "Alert", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    func gmailLoginApi(email:String,name:String,type:String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            
            let devicetoken =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "kDeviceToken") )
          
            let dict = ["email":email,"provider":type,"name":name,"device_token":devicetoken as! String, "device_type" : "IOS"]
            
            //            let parameters = ["gender":gender, "looking_for":lookingFor,"first_name":firstName, "zipcode":zipCode,"state":state,"email":mailField.text!,"password":passwordField.text!] as [String : String]
            
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:sociallogin, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1" || success == "2" ){
                    
                    
                    UserDefaults.standard.set(true, forKey: "checkLogIn")
                 // UserDefaults.standard.set(nil, forKey: "checkLogIn")
                    
                    let userInfo = response["data"]as! [String : AnyObject]
                    
                    
                    let id = self.themes.checkNullValue(userInfo["id"])
                    
                    let date_id = self.themes.checkNullValue(userInfo["date_id"])
                    
                    let gender = self.themes.checkNullValue(userInfo["gender"])
                    
                    let lookingfor = self.themes.checkNullValue(userInfo["looking_for"])
                    
                    let firstName = self.themes.checkNullValue(userInfo["first_name"])
                    
                    let email = self.themes.checkNullValue(userInfo["email"])
                    
                    let zipcode = self.themes.checkNullValue(userInfo["zipcode"])
                    
                    let state = self.themes.checkNullValue(userInfo["state"])
                    
                    let phNumber = self.themes.checkNullValue(userInfo["phone_number"])
                    
                    let email_notification_status = self.themes.checkNullValue(userInfo["email_notification_status"])
                    
                    let push_notification_status = self.themes.checkNullValue(userInfo["push_notification_status"])

                    let referal_from = self.themes.checkNullValue(userInfo["referal_from"])
                    
                    let children = self.themes.checkNullValue(userInfo["children"])
                    
                    let city = self.themes.checkNullValue(userInfo["city"])
                    
                     let country = self.themes.checkNullValue(userInfo["country"])
                    
                    let dob1 = self.themes.checkNullValue(userInfo["dob"])
                    
                    UserDefaults.standard.set(email, forKey: "LoginUserName")
                    UserDefaults.standard.set("", forKey: "LoginPassword")
                    
                    // let dob = self.themes.dateFormateConverter(date: dob1 as! String)
                    
                    let ethnicity = self.themes.checkNullValue(userInfo["ethnicity"])
                    
                    let religion = self.themes.checkNullValue(userInfo["religion"])
                    
                    let denomination = self.themes.checkNullValue(userInfo["denomination"])
                    
                    let education = self.themes.checkNullValue(userInfo["education"])
                    
                    let occupation = self.themes.checkNullValue(userInfo["occupation"])
                    
                    let income = self.themes.checkNullValue(userInfo["income"])
                    
                    let smoke = self.themes.checkNullValue(userInfo["smoke"])
                    
                    let drink = self.themes.checkNullValue(userInfo["drink"])
                    
                    let heightFt = self.themes.checkNullValue(userInfo["height_ft"])as? String
                    let heightIn = self.themes.checkNullValue(userInfo["height_inc"])as? String
                    let height = heightFt! + "." + heightIn!
                    
                    let passionate = self.themes.checkNullValue(userInfo["passionate"])
                    
                    let leisure_time = self.themes.checkNullValue(userInfo["leisure_time"])
                    
                    let thankful_1 = self.themes.checkNullValue(userInfo["thankful_1"])
                    
                    let thankful_2 = self.themes.checkNullValue(userInfo["thankful_2"])
                    
                    let thankful_3 = self.themes.checkNullValue(userInfo["thankful_3"])
                    
                    
                    let partner_age_min = self.themes.checkNullValue(userInfo["partner_age_min"])
                    
                    let partner_age_max = self.themes.checkNullValue(userInfo["partner_age_max"])
                    
                    let partner_match_distance = self.themes.checkNullValue(userInfo["partner_match_distance"])
                    
                    let partner_match_search = self.themes.checkNullValue(userInfo["partner_match_search"])
                    
                    let partner_ethnicity = self.themes.checkNullValue(userInfo["partner_ethnicity"])
                    
                    let partner_religion = self.themes.checkNullValue(userInfo["partner_religion"])
                    
                    let partner_education = self.themes.checkNullValue(userInfo["partner_education"])
                    
                    let partner_occupation = self.themes.checkNullValue(userInfo["partner_occupation"])
                    
                    let partner_smoke = self.themes.checkNullValue(userInfo["partner_smoke"])
                    
                    let partner_denomination = self.themes.checkNullValue(userInfo["partner_denomination"])
                    
                    
                    let partner_drink = self.themes.checkNullValue(userInfo["partner_drink"])
                    
                    let photo1 = self.themes.checkNullValue(userInfo["photo1"])
                    
                    let photo1_approve = self.themes.checkNullValue(userInfo["photo1_approve"])
                    
                    let photo2 = self.themes.checkNullValue(userInfo["photo2"])
                    
                    let photo2_approve = self.themes.checkNullValue(userInfo["photo2_approve"])
                    
                    let photo3 = self.themes.checkNullValue(userInfo["photo3"])
                    
                    UserDefaults.standard.set(self.themes.checkNullValue(userInfo["state"]), forKey: "selectedState")
                    
                    
                    let photo3_approve = self.themes.checkNullValue(userInfo["photo3_approve"])
                    
                    let photo4 = self.themes.checkNullValue(userInfo["photo4"])
                    
                    let photo4_approve = self.themes.checkNullValue(userInfo["photo4_approve"])
                    
                    
                    let photo5 = self.themes.checkNullValue(userInfo["photo5"])
                    
                    let photo5_approve = self.themes.checkNullValue(userInfo["photo5_approve"])
                    
                    let photo6 = self.themes.checkNullValue(userInfo["photo6"])
                    
                    let photo6_aprove = self.themes.checkNullValue(userInfo["photo6_aprove"])
                    
                    let signup_step = self.themes.checkNullValue(userInfo["signup_step"])
                    
                    let status = self.themes.checkNullValue(userInfo["status"])
                    
                     let xmppPassword = self.themes.checkNullValue(userInfo["password"])
                    let XmppUserName = self.themes.checkNullValue(userInfo["username"])
                    //
                        self.themes.saveXmppUserName(XmppUserName as? String)
                        self.themes.savexmppPassword(xmppPassword as? String)
                    //
                    
                    self.themes.saveUserID(id as? String)
                    
                    self.themes.savedate_id(date_id as? String)
                    
                    self.themes.saveGender(gender as? String)
                    
                    self.themes.savelooking_for(lookingfor as? String)
                    
                    self.themes.saveFirstName(firstName as? String)
                    
                    self.themes.saveuserEmail(email as? String)
                    
                    self.themes.saveZipcode(zipcode as? String)
                    
                    self.themes.saveEmailNotification(email_notification_status as? String)
                    
                    self.themes.savePushNotification(push_notification_status as? String)
                    
                    
                    self.themes.savestate(state as? String)
                    self.themes.saveMobileNumber(phNumber as? String)
                    
                    self.themes.savereferal_from(referal_from as? String)
                    
                    self.themes.saveChildren(children as? String)
                    
                    self.themes.savecity(city as? String)
                    
                    self.themes.savecountry(country as? String)
                    
                   // self.themes.savedob(dob1 as? String)
                    
                    self.themes.saveEthinicity(ethnicity as? String)
                    
                    self.themes.saveReligion(religion as? String)
                    
                    self.themes.savedenomination(denomination as? String)
                    
                    self.themes.saveEducation(education as? String)
                    
                    self.themes.saveoccupation(occupation as? String)
                    
                    self.themes.saveIncome(income as? String)
                    
                    self.themes.savesmoke(smoke as? String)
                    self.themes.savePartnerDenomination(partner_denomination as? String)
                    self.themes.savedrink(drink as? String)
                    
                    self.themes.saveHeight(height as? String)
                    
                    self.themes.savepassionate(passionate as? String)
                    
                    self.themes.saveleisure_time(leisure_time as? String)
                    
                    self.themes.savethankful_1(thankful_1 as? String)
                    
                    self.themes.savethankful_2(thankful_2 as? String)
                    
                    self.themes.savethankful_3(thankful_3 as? String)
                    
                    self.themes.savepartner_age_max(partner_age_max as? String)
                    
                    self.themes.savepartner_age_min(partner_age_min as? String)
                    
                    self.themes.savepartner_match_distance(partner_match_distance as? String)
                    
                    self.themes.savepartner_match_search(partner_match_search as? String)
                    
                    self.themes.savepartner_ethnicity(partner_ethnicity as? String)
                    
                    self.themes.savepartner_religion(partner_religion as? String)
                    
                    self.themes.savepartner_education(partner_education as? String)
                    
                    self.themes.savepartner_occupation(partner_occupation as? String)
                    
                    self.themes.savepartner_smoke(partner_smoke as? String)
                    
                    self.themes.savepartner_drink(partner_drink as? String)
                    
                    self.themes.savephoto1(photo1 as? String)
                    
                    self.themes.savephoto2(photo2 as? String)
                    
                    self.themes.savephoto3(photo3 as? String)
                    
                    self.themes.savephoto4(photo4 as? String)
                    
                    self.themes.savephoto5(photo5 as? String)
                    
                    self.themes.savephoto6(photo6 as? String)
                    
                    self.themes.savephoto1_approve(photo1_approve as? String)
                    
                    self.themes.savephoto2_approve(photo2_approve as? String)
                    
                    self.themes.savephoto3_approve(photo3_approve as? String)
                    self.themes.savephoto4_approve(photo4_approve as? String)
                    self.themes.savephoto5_approve(photo5_approve as? String)
                    self.themes.savephoto6_aprove(photo6_aprove as? String)
                    self.themes.savesignup_step(signup_step as? String)
                    
                    
                    self.themes.savestatus(status as? String)
                    
                    
                        let signUpStep = userInfo["signup_step"] as! String
                        if(signUpStep == "1"){
                            UserDefaults.standard.set(true, forKey: "checkProfile1")
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            UserDefaults.standard.set(true, forKey: "checkProfile1")
                            let vc = storyboard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
                            vc.registrationType = "social"
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else if(signUpStep == "2"){
                            UserDefaults.standard.set(true, forKey: "checkProfile1")
                            UserDefaults.standard.set(true, forKey: "checkProfile2")
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "MatchPreferenceVC") as! MatchPreferenceVC
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else if(signUpStep == "3"){
                            UserDefaults.standard.set(true, forKey: "checkProfile1")
                            UserDefaults.standard.set(true, forKey: "checkProfile2")
                            UserDefaults.standard.set(true, forKey: "checkProfile3")
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            
                            let vc = storyboard.instantiateViewController(withIdentifier: "CompitibilityQuizProfileVC") as! CompitibilityQuizProfileVC
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else if(signUpStep == "4"){
                            UserDefaults.standard.set(true, forKey: "checkProfile1")
                            UserDefaults.standard.set(true, forKey: "checkProfile2")
                            UserDefaults.standard.set(true, forKey: "checkProfile3")
                            UserDefaults.standard.set(true, forKey: "checkProfile4")
                            let checkTips = UserDefaults.standard.object(forKey: "checkTips")
                            
                            if checkTips is NSNull || checkTips == nil {
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "TipsVC") as! TipsVC
                                self.navigationController?.pushViewController(vc, animated: true)
                            }else{
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
                                
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                        }
                        
                    
                }else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    
    
    func ValidatedTextField()-> Bool {
        
        if (userNameField.text == ""){
            self.themes.showAlert(title: "Alert", message: "Email is Mandatory", sender: self)
            // self.userNameField.becomeFirstResponder()
            return false
        }
        else if (passwordField.text == "") {
            self.themes.showAlert(title: "Alert", message: "Password is Mandatory", sender: self)
            // self.passwordField.becomeFirstResponder()
            return false
        }
        return true
    }    
// UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}






