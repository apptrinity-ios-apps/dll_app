//
//  instaTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 4/25/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class instaTableViewCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    var imgesArry = ["img1","img2","img3","img1","img2","img3","img1","img2","img3","img1","img2","img3","img1","img2","img3","img1","img2","img3"]

    @IBOutlet weak var instaCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        instaCollectionView.delegate = self
        instaCollectionView.dataSource = self
        instaCollectionView.backgroundColor = UIColor.white
        
        self.instaCollectionView.register(UINib(nibName: "InstaImgCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "InstaImgCollectionViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgesArry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InstaImgCollectionViewCell", for: indexPath) as! InstaImgCollectionViewCell
        
        cell.layer.cornerRadius      = 15
        cell.instaImgView.clipsToBounds = true
        cell.layer.masksToBounds     = true
        
        cell.instaImgView.image = UIImage(named: imgesArry[indexPath.row])
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 90)
    }
    
}
