//
//  UserDetailsVC.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 4/25/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
import XMPPFramework
import Chatto
import ChattoAdditions


protocol backBtnClickDelegate {
    
    func backAction(from:String)
    
}

class UserDetailsVC: UIViewController,UITableViewDelegate,UITableViewDataSource,CanReceiveProfileBtnData {
    weak var xmppController: XMPPController!
    var themes = Themes()
    var urlService = URLservices.sharedInstance
     var chatList = [AnyObject]()
    var selectedItem = 0
    var currentItem = 0
    var favourValue = Bool()
    var sendIntrstValue = Bool()
    var delegate : backBtnClickDelegate?
    var headerArray = ["","Basic information","Religion information","Location information","Professional information"]
    
    var favourateCheck = Bool()
    var favourateCheck1 = Bool()
    var fromScreen1 = String()
    var count:Int!
    var fromScreen = ""
    var match_id = ""
    var imageApprove = Bool()
    
    var blockSeleccted = ""
    var ProfileName = ""
    
    var userDataDict = [String:AnyObject]()
    var imageArray = Array<AnyObject>()
    
    var adsListArr = [AnyObject]()
    
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>?
    
    
    @IBOutlet weak var detailsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        favourValue = false
        sendIntrstValue = false
        
       
        
        self.detailsTableView.register(UINib(nibName: "AboutMeTableViewCell", bundle: nil), forCellReuseIdentifier: "AboutMeTableViewCell")
        self.detailsTableView.register(UINib(nibName: "BasicInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "BasicInfoTableViewCell")
        self.detailsTableView.register(UINib(nibName: "ReligionInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "ReligionInfoTableViewCell")
        self.detailsTableView.register(UINib(nibName: "LocationInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "LocationInfoTableViewCell")
        self.detailsTableView.register(UINib(nibName: "ProfessionalInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfessionalInfoTableViewCell")
        self.detailsTableView.register(UINib(nibName: "RequestTableViewCell", bundle: nil), forCellReuseIdentifier: "RequestTableViewCell")
        self.detailsTableView.register(UINib(nibName: "InfoHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "InfoHeaderTableViewCell")
        self.detailsTableView.register(UINib(nibName: "BlockTableViewCell", bundle: nil), forCellReuseIdentifier: "BlockTableViewCell")
        
        self.detailsTableView.register(UINib(nibName: "instaTableViewCell", bundle: nil), forCellReuseIdentifier: "instaTableViewCell")
        self.detailsTableView.register(UINib(nibName: "IntrestsTableViewCell", bundle: nil), forCellReuseIdentifier: "IntrestsTableViewCell")
        self.detailsTableView.register(UINib(nibName: "ProfileVerifyTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileVerifyTableViewCell")
        
        let chatStatus = self.themes.getChatstatus()
        
        if chatStatus == "0"{
         //   self.themes.showAlert(title: "Alert", message: "These Feature is Not Available", sender: self)
        }else if chatStatus == "1"{
        self.xmppController = XMPPController.sharedInstance
        self.xmppController.roomListDelegate = self
        let usenameStr =  self.themes.checkNullValue(self.themes.getXmppUserName()) as! String
        let xmppPassword = self.themes.checkNullValue(self.themes.getxmppPassword()) as! String
        let servername = UITextField()
        servername.text = "divorcelovelounge.com"
        let password = UITextField()
        password.text = (xmppPassword as! String)
        let username = UITextField()
        username.text = "\(usenameStr)@divorcelovelounge.com"
        if let serverText = servername.text, (servername.text?.characters.count)! > 0 {
            let auth = AuthenticationModel(jidString: username.text!, serverName: servername.text!, password: password.text!)
            auth.save()
        } else {
            let auth = AuthenticationModel(jidString: username.text!, password: password.text!)
            auth.save()
        }
        self.configureAndStartStream()

        }
        
        
        
       adsListServiceApi(ScreenType: "Profile")
        
        
        
    }
    
    
    func adsListServiceApi(ScreenType:String){
        self.urlService.adslistServiceCall(ScreenType: ScreenType) { response in
            let success = response["status"] as! String
            if(success == "1"){
                let data = response["data"] as! [AnyObject]
                self.adsListArr = data
                self.detailsTableView.reloadData()
                
                
            }else{
                
            }
        }
    }
    
    
    
    
    func matchesAPI(){
        
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let urserId = themes.getUserId()
            var dict = [String : AnyObject]()
            
            dict.updateValue(urserId as AnyObject, forKey: "user_id")
            dict.updateValue(match_id as AnyObject, forKey: "match_id")
            
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:partnerProfile, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    self.userDataDict = response["user_info"]as! [String : AnyObject]
                    self.detailsTableView.reloadData()
                    
                } else  if(success == "2"){
                    
                    // Naveen
                    
                    
                    //                    let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopViewController") as! PopViewController
                    //                    popOverVC.fromScreen = ""
                    //                    //                    popOverVC.ImageStr = ""
                    //                    popOverVC.headingMesg = "Alert"
                    //                    popOverVC.subMesg = "You need an active membership plan to view the profile. Proceed to membership plans?"
                    //                    self.addChildViewController(popOverVC)
                    //                    popOverVC.view.center = self.view.frame.center
                    //                    self.view.addSubview(popOverVC.view)
                    //                    popOverVC.didMove(toParentViewController: self)
                    
                    // Naveen
                    
                    
                    //                    let alertController = UIAlertController(title: "Alert", message: "You need an active membership plan to view the profile. Proceed to membership plans?", preferredStyle: .alert)
                    //                    let OKAction = UIAlertAction(title: "Proceed", style: .default) { (action:UIAlertAction!) in
                    //                        print("Ok button tapped");
                    //                        let MemberShipViewController = self.storyboard?.instantiateViewController(withIdentifier: "MemberShipViewController") as! MemberShipViewController
                    //                        self.navigationController?.pushViewController(MemberShipViewController, animated: true)
                    //                    }
                    //                    alertController.addAction(OKAction)
                    //                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                    //
                    //                    }
                    //                    alertController.addAction(cancelAction)
                    //                    self.present(alertController, animated: true, completion:nil)
                }else {
                    //                    let result = response["result"] as! String
                    //                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    func configureAndStartStream() {
        
        self.xmppController = XMPPController.sharedInstance
        self.xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
        _ = self.xmppController.connect()
     
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        imageArray.removeAll()
        
        getprofileAPi()
       
        
        
        if fromScreen == "myfavourite" ||  fromScreen == "Block"{
            self.matchesAPI()
        } else {
            
        }
        
        let photo1 = self.themes.checkNullValue(self.userDataDict["photo1"] as AnyObject?) as! String
        
        let photo2 = self.themes.checkNullValue(self.userDataDict["photo2"] as AnyObject?) as! String
        
        let photo3 = self.themes.checkNullValue(self.userDataDict["photo3"] as AnyObject?) as! String
        
        let photo4 = self.themes.checkNullValue(self.userDataDict["photo4"] as AnyObject?) as! String
        
        let photo5 = self.themes.checkNullValue(self.userDataDict["photo5"] as AnyObject?) as! String
        
        let photo6 = self.themes.checkNullValue(self.userDataDict["photo6"] as AnyObject?) as! String
        
        let photo7 = self.themes.checkNullValue(self.userDataDict["photo7"] as AnyObject?) as! String
        
        let photo8 = self.themes.checkNullValue(self.userDataDict["photo8"] as AnyObject?) as! String
        
        let photo9 = self.themes.checkNullValue(self.userDataDict["photo9"] as AnyObject?) as! String
        
        let photo10 = self.themes.checkNullValue(self.userDataDict["photo10"] as AnyObject?) as! String
        
        
        
        let photoApprove1 = self.themes.checkNullValue(self.userDataDict["photo1_approve"] as AnyObject?) as! String
        
        let photoApprove2 = self.themes.checkNullValue(self.userDataDict["photo2_approve"] as AnyObject?) as! String
        
        let photoApprove3 = self.themes.checkNullValue(self.userDataDict["photo3_approve"] as AnyObject?) as! String
        
        let photoApprove4 = self.themes.checkNullValue(self.userDataDict["photo4_approve"] as AnyObject?) as! String
        
        let photoApprove5 = self.themes.checkNullValue(self.userDataDict["photo5_approve"] as AnyObject?) as! String
        
        let photoApprove6 = self.themes.checkNullValue(self.userDataDict["photo6_approve"] as AnyObject?) as! String
        
        let photoApprove7 = self.themes.checkNullValue(self.userDataDict["photo7_approve"] as AnyObject?) as! String
        
        let photoApprove8 = self.themes.checkNullValue(self.userDataDict["photo8_approve"] as AnyObject?) as! String
        
        let photoApprove9 = self.themes.checkNullValue(self.userDataDict["photo9_approve"] as AnyObject?) as! String
        
        let photoApprove10 = self.themes.checkNullValue(self.userDataDict["photo10_approve"] as AnyObject?) as! String
        
        // APPROVED
        
        if(photo1 != "" && photoApprove1 == "APPROVED"){
            imageArray.append(photo1 as AnyObject)
        }else{
            
        }
        
        if(photo2 != "" && photoApprove2 == "APPROVED"){
            imageArray.append(photo2 as AnyObject)
        }else{
            
        }
        if(photo3 != "" && photoApprove3 == "APPROVED" ){
            imageArray.append(photo3 as AnyObject)
        }else{
            
        }
        if(photo4 != "" && photoApprove4 == "APPROVED"){
            imageArray.append(photo4 as AnyObject)
        }else{
            
        }
        if(photo5 != "" && photoApprove5 == "APPROVED"){
            imageArray.append(photo5 as AnyObject)
        }else{
            
        }
        if(photo6 != "" && photoApprove6 == "APPROVED"){
            imageArray.append(photo6 as AnyObject)
        }else{
            
        }
        
        if(photo7 != "" && photoApprove7 == "APPROVED"){
            imageArray.append(photo7 as AnyObject)
        }else{
            
        }
        if(photo8 != "" && photoApprove8 == "APPROVED"){
            imageArray.append(photo8 as AnyObject)
        }else{
            
        }
        if(photo9 != "" && photoApprove9 == "APPROVED"){
            imageArray.append(photo9 as AnyObject)
        }else{
            
        }
        if(photo10 != "" && photoApprove10 == "APPROVED"){
            imageArray.append(photo10 as AnyObject)
        }else{
            
            
        }
        
        
        DispatchQueue.main.async {
            
            let usenameStr =  self.themes.checkNullValue(self.themes.getXmppUserName()) as! String
            let xmppPassword = self.themes.checkNullValue(self.themes.getxmppPassword()) as! String
            
            let servername = UITextField()
            servername.text = "divorcelovelounge.com"
            let password = UITextField()
            password.text = (xmppPassword as! String)
            let username = UITextField()
            username.text = "\(usenameStr)@divorcelovelounge.com"
            if let serverText = servername.text, (servername.text?.characters.count)! > 0 {
                let auth = AuthenticationModel(jidString: username.text!, serverName: servername.text!, password: password.text!)
                auth.save()
            } else {
                let auth = AuthenticationModel(jidString: username.text!, password: password.text!)
                auth.save()
            }
            self.configureAndStartStream()
            
            
            self.getChatListApi()
        }
        
        
        let planSatus = self.themes.checkNullValue( self.themes.getplanStatus()) as! String
        
        if planSatus == "2" {
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopViewController") as! PopViewController
                                      
                        popOverVC.fromScreen = "expirePlan"
                        popOverVC.headingMesg = "Oops ☹️"
                        popOverVC.subMesg = "You need an active membership plan to view the profile. Proceed to membership plans?"
                                      
                        self.addChildViewController(popOverVC)
                        popOverVC.view.center = self.view.frame.center
                        self.view.addSubview(popOverVC.view)
                        popOverVC.didMove(toParentViewController: self)
        } else {
            
        }
        
        
        
        super.viewWillAppear(animated)
        
      
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        try! self.fetchedResultsController?.performFetch()
        super.viewDidAppear(animated)
    }
    
    func CanReceiveProfileBtnData(buttonName: String, selected: Bool) {
        
        let id = self.themes.checkNullValue(self.userDataDict["id"] as AnyObject?) as! String
        
        let myID = self.themes.getUserId()
        
        if (buttonName == "likebtn") && selected == true {
            
            sendIntrestApi(user_id: myID, Id: id)
            
        }else if (buttonName == "starbtn") && selected == true {
            
            // favouritApi(user_id: myID, Id: id)
            
            //        }else if (buttonName == "blockbtn") && selected == true {
            
            //            let id = self.themes.checkNullValue(self.userDataDict["id"] as AnyObject?) as! String
            //
            //            let myID = self.themes.getUserId()
            //            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopViewController") as! PopViewController
            //            popOverVC.fromScreen = "unBlock"
            //            popOverVC.id = id
            //            popOverVC.userId = myID
            //
            //            popOverVC.headingMesg = "Alert"
            //            popOverVC.subMesg = "Do You Want to Block \(ProfileName)"
            //            self.addChildViewController(popOverVC)
            //            popOverVC.view.center = self.view.frame.center
            //            self.view.addSubview(popOverVC.view)
            //            popOverVC.didMove(toParentViewController: self)
            
            
            
        }
        //        else if (buttonName == "blockbtn") && selected == false {
        //            let alertController = UIAlertController(title: "Alert", message: "Do You Want Unblock \(ProfileName) ", preferredStyle: .alert)
        //            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
        //
        ////                self.blockSeleccted = "yes"
        //                self.blockUnblockApi(user_id: myID, Id: id)
        //                let home = self.storyboard?.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
        //                self.navigationController?.pushViewController(home, animated: true)
        //                self.detailsTableView.reloadData()
        //
        //            }
        //            alertController.addAction(OKAction)
        //            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
        ////                self.blockSeleccted = "no"
        //                self.blockUnblockApi(user_id: myID, Id: id)
        //                self.detailsTableView.reloadData()
        //            }
        //            alertController.addAction(cancelAction)
        //            self.present(alertController, animated: true, completion:nil)
        //        }
        
    }
    
    
    @objc func blockButtonAction1(_ sender : UIButton){
        
        
        self.detailsTableView.reloadData()
        let id = self.themes.checkNullValue(self.userDataDict["id"] as AnyObject?) as! String
        
        let myID = self.themes.getUserId()
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopViewController") as! PopViewController
        popOverVC.fromScreen = "unBlock"
        popOverVC.id = id
        popOverVC.userId = myID
        
        popOverVC.headingMesg = "Alert"
        popOverVC.subMesg = "Do You Want to Block \(ProfileName)"
        self.addChildViewController(popOverVC)
        popOverVC.view.center = self.view.frame.center
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    
    @objc func blockButtonAction(_ sender : UIButton){
        
        let id = self.themes.checkNullValue(self.userDataDict["id"] as AnyObject?) as! String
        
        let myID = self.themes.getUserId()
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopViewController") as! PopViewController
        popOverVC.fromScreen = "unBlock"
        popOverVC.id = id
        popOverVC.userId = myID
        
        popOverVC.headingMesg = "Alert"
        popOverVC.subMesg = "Do You Want to Unblock \(ProfileName)"
        self.addChildViewController(popOverVC)
        popOverVC.view.center = self.view.frame.center
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    
    @objc func backBtnClickAction(_ sender : UIButton){
        
        if(fromScreen1 == "myfavourite"){
            self.delegate?.backAction(from: "myfavourite")
        }else if(fromScreen1 == "interestme"){
            self.delegate?.backAction(from: "interestme")
        }else if(fromScreen1 == "myinterest"){
            self.delegate?.backAction(from: "myinterest")
        }else if(fromScreen1 == "viewedMe"){
            self.delegate?.backAction(from: "viewedMe")
        }else if(fromScreen1 == "favouratedMe"){
            self.delegate?.backAction(from: "favouratedMe")
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        } else if section == 1 {
            return 1
        } else if section == 2 {
            return 1
        } else if section == 3 {
            return 1
        } else if section == 4 {
            return 1
        } else if section == 5 {
            return 0
        } else if section == 6 {
            return 0
        } else {
            return 0
        }
    }
    
    
    @objc func detailImageBtnAction(button: UIButton){
        
        
        
        let SignVc = self.storyboard?.instantiateViewController(withIdentifier: "DetailImageVC") as! DetailImageVC
        SignVc.imageArray = imageArray
        self.navigationController?.pushViewController(SignVc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            
            if fromScreen == "myfavourite"{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "AboutMeTableViewCell")as! AboutMeTableViewCell
                cell.detailImgBtn.addTarget(self, action: #selector(detailImageBtnAction(button:)), for:.touchUpInside)
                
                cell.hideBtn.addTarget(self, action: #selector(btnHideAction(button:)), for: .touchUpInside)

                let PHOTOAPPROVE =  self.themes.getPhotoApproveStatus()
                if(PHOTOAPPROVE ){
                    cell.hideBtn.isHidden = true
                }else{
                    cell.hideBtn.isHidden =  false
                }
                
                cell.callBtn.addTarget(self, action: #selector(callButnClick(_:)), for: .touchUpInside)
                cell.composeMesgBtn.addTarget(self, action: #selector(composeBtnClicked), for: .touchUpInside)
                //                cell.blockButton.addTarget(self, action: #selector(blockButtonAction1(_:)), for:.touchUpInside)
                //                cell.blockButton.addTarget(self, action: #selector(blockBtnAction(button:)), for:.touchUpInside)
                cell.delegate  = self
                
                cell.aboutMeLabel.text = (self.themes.checkNullValue(self.userDataDict["passionate"] as AnyObject?) as! String)
                
                cell.distanceLabel.text = self.themes.checkNullValue(self.userDataDict["city"] as AnyObject?) as? String
                let imageUrl = self.themes.checkNullValue(self.userDataDict["photo1"] as AnyObject?) as! String
                
                
                let photoApproved = self.themes.checkNullValue(userDataDict["photo1_approve"] as AnyObject?) as! String
                if photoApproved == "APPROVED" {
                    
                    let fullimageUrl = ImagebaseUrl + imageUrl
                    cell.profileImgView.sd_setImage(with: URL(string: fullimageUrl), placeholderImage: UIImage(named: "defaultPic"))
                    DispatchQueue.main.async {
                        cell.userImgView.sd_setShowActivityIndicatorView(true)
                        cell.userImgView.sd_setIndicatorStyle(.whiteLarge)
                        cell.userImgView.sd_setImage(with: URL(string: fullimageUrl), placeholderImage:UIImage(named: "defaultPic"), completed: { (image, error, cacheType, url) -> Void in
                            if ((error) != nil) {
                                cell.userImgView.image = UIImage(named: "defaultPic")
                                cell.userImgView.sd_setShowActivityIndicatorView(true)
                            } else {
                                cell.userImgView.image = image
                                cell.userImgView.sd_setShowActivityIndicatorView(false)
                            }
                        })
                    }
                    
                }else{
                    
                    cell.userImgView.image = UIImage(named: "defaultPic")
                    cell.profileImgView.image = UIImage(named: "defaultPic")
                }
                
                ProfileName = self.themes.checkNullValue(self.userDataDict["first_name"] as AnyObject?) as! String
                
                cell.nameLabel.text = self.themes.checkNullValue(self.userDataDict["first_name"] as AnyObject?) as? String
                let DOB = self.themes.checkNullValue(self.userDataDict["dob"] as AnyObject?) as? String
                
                let likeValue = self.themes.checkNullValue(self.userDataDict["interest_from"] as AnyObject?) as? String
                
                let favValue = self.themes.checkNullValue(self.userDataDict["fav_from"] as AnyObject?) as? String
                
                let userId = themes.getUserId()
                if(userId == likeValue){
                    cell.likeButton.setSelected(selected: true, animated: false)
                    cell.likeButton.isUserInteractionEnabled = false
                    
                }else{
                    cell.likeButton.isUserInteractionEnabled = true
                    cell.likeButton.setSelected(selected: false, animated: false)
                }
                
                
                if(favourateCheck){
                    
                    
                    if(favourateCheck1){
                        cell.starButton.setSelected(selected: true, animated: false)
                    }else{
                        cell.starButton.setSelected(selected: false, animated: false)
                    }
                }else{
                    if(userId == favValue){
                        favourateCheck1 = true
                        cell.starButton.setSelected(selected: true, animated: false)
                    }else{
                        favourateCheck1 = false
                        cell.starButton.setSelected(selected: false, animated: false)
                    }
                }
                
                
                
                //                if blockSeleccted == "yes"{
                //                    cell.blockButton.setSelected(selected: true, animated: false)
                //                }else{
                //                    cell.blockButton.setSelected(selected: false, animated: false)
                //                }
                
                
                if DOB != "" {
                    let age = self.themes.getAgeFromDOF(date: DOB!)
                    cell.ageLabel.text = "\(age.0)"
                }
                let interestStatus = self.themes.checkNullValue(self.userDataDict["interest_status"] as AnyObject?) as? String
                
                if self.themes.getplanStatus() == "1" {
//
//                                        if self.themes.getChatting() == "Yes"{
//                                            if (interestStatus == "1") {
//
//                                            } else {
//                                                cell.mesgButton.setImage(UIImage(named: "chatDefault"), for: .normal)
//
//                                                cell.mesgButton.isEnabled = false
//                                            }
//                    
//                    
//                    
//                                        }else{
//
//                                        }
                    
                }else{
                    cell.mesgButton.setImage(UIImage(named: "chatDefault"), for: .normal)
                    cell.mesgButton.isEnabled = false
                }
                
                cell.likeButton.isEnabled = true
                cell.starButton.isEnabled = true
                cell.mesgButton.isEnabled = true
                cell.composeMesgBtn.isEnabled = true
                
                cell.mesgButton.setImage(UIImage(named: "chat_activ"), for: .normal)
                cell.mesgButton.isEnabled = true
                
                cell.mesgButton.addTarget(self, action: #selector(chatBtnClickAction(_:)), for: .touchUpInside)
                cell.backBtn.addTarget(self, action: #selector(backBtnClickAction(_:)), for: .touchUpInside)
                cell.starButton.addTarget(self, action: #selector(starButtonClickAction(_:)), for: .touchUpInside)
                
                
                
                cell.adsListArr = self.adsListArr
                if self.adsListArr.count > 0 {
                    cell.adsHeightConstant.constant = 120
                }else{
                    cell.adsHeightConstant.constant = 0
                }
                cell.adsCollectionView.reloadData()
                return cell
                
                
            }else if fromScreen == "Block"{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "AboutMeTableViewCell")as! AboutMeTableViewCell
                // cell.callBtn.addTarget(self, action: #selector(callButnClick(_:)), for: .touchUpInside)
                cell.detailImgBtn.addTarget(self, action: #selector(detailImageBtnAction(button:)), for:.touchUpInside)
                cell.delegate  = self
                
                cell.hideBtn.isHidden = true
            
                
                cell.aboutMeLabel.text = (self.themes.checkNullValue(self.userDataDict["passionate"] as AnyObject?) as! String)
                
                let imageUrl = self.themes.checkNullValue(self.userDataDict["photo1"] as AnyObject?) as! String
                cell.distanceLabel.text = self.themes.checkNullValue(self.userDataDict["city"] as AnyObject?) as? String
                let photoApproved = self.themes.checkNullValue(userDataDict["photo1_approve"] as AnyObject?) as! String
                
                
                //  cell.blockButton.setImage(UIImage(named: <#T##String#>), for: .normal)
                
                //  cell.blockButton.setSelected(selected: true, animated: false)
                
                if photoApproved == "APPROVED" {
                    let fullimageUrl = ImagebaseUrl + imageUrl
                    cell.profileImgView.sd_setImage(with: URL(string: fullimageUrl), placeholderImage: UIImage(named: "defaultPic"))
                    DispatchQueue.main.async {
                        cell.userImgView.sd_setShowActivityIndicatorView(true)
                        cell.userImgView.sd_setIndicatorStyle(.whiteLarge)
                        cell.userImgView.sd_setImage(with: URL(string: fullimageUrl), placeholderImage:UIImage(named: "defaultPic"), completed: { (image, error, cacheType, url) -> Void in
                            if ((error) != nil) {
                                cell.userImgView.image = UIImage(named: "defaultPic")
                                cell.userImgView.sd_setShowActivityIndicatorView(true)
                            } else {
                                cell.userImgView.image = image
                                cell.userImgView.sd_setShowActivityIndicatorView(false)
                            }
                        })
                    }
                    
                }else{
                    cell.userImgView.image = UIImage(named: "defaultPic")
                    cell.profileImgView.image = UIImage(named: "defaultPic")
                }
                
                ProfileName = self.themes.checkNullValue(self.userDataDict["first_name"] as AnyObject?) as! String
                cell.nameLabel.text = self.themes.checkNullValue(self.userDataDict["first_name"] as AnyObject?) as? String
                let DOB = self.themes.checkNullValue(self.userDataDict["dob"] as AnyObject?) as? String
                let likeValue = self.themes.checkNullValue(self.userDataDict["interest_from"] as AnyObject?) as? String
                let favValue = self.themes.checkNullValue(self.userDataDict["fav_from"] as AnyObject?) as? String
                let userId = themes.getUserId()
                cell.likeButton.isEnabled = false
                cell.starButton.isEnabled = false
                cell.mesgButton.isEnabled = false
                cell.composeMesgBtn.isEnabled = false
                
                cell.composeMesgBtn.setImage(UIImage(named: "message_icon_default"), for: .normal)
                cell.mesgButton.setImage(UIImage(named: "chat_default"), for: .normal)
                
                if DOB != "" {
                    let age = self.themes.getAgeFromDOF(date: DOB!)
                    cell.ageLabel.text = "\(age.0)"
                }
                //                if blockSeleccted == "yes"{
                //                    cell.blockButton.setSelected(selected: true, animated: false)
                //                }else{
                //                    cell.blockButton.setSelected(selected: false, animated: false)
                //                }
                
                cell.mesgButton.addTarget(self, action: #selector(chatBtnClickAction(_:)), for: .touchUpInside)
                cell.backBtn.addTarget(self, action: #selector(backBtnClickAction(_:)), for: .touchUpInside)
                
                cell.adsListArr = self.adsListArr
                if self.adsListArr.count > 0 {
                                   cell.adsHeightConstant.constant = 120
                               }else{
                                   cell.adsHeightConstant.constant = 0
                               }
                               cell.adsCollectionView.reloadData()
                
                
                return cell
            } else {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "RequestTableViewCell")as! RequestTableViewCell
                cell.delegate  = self
                
                
                
                let imageUrl = self.themes.checkNullValue(self.userDataDict["photo1"] as AnyObject?) as! String
                
                let fullimageUrl = ImagebaseUrl + imageUrl
                cell.detailImgBtn.addTarget(self, action: #selector(detailImageBtnAction(button:)), for:.touchUpInside)
                cell.aboutTextView.text = (self.themes.checkNullValue(self.userDataDict["passionate"] as AnyObject?) as! String)
                cell.userImgView.sd_setImage(with: URL(string: fullimageUrl), placeholderImage: UIImage(named: "defaultPic"))
                cell.profileImgView.sd_setImage(with: URL(string: fullimageUrl), placeholderImage: UIImage(named: "defaultPic"))
                cell.distanceLabel.text = self.themes.checkNullValue(self.userDataDict["city"] as AnyObject?) as? String
                cell.nameLabel.text = self.themes.checkNullValue(self.userDataDict["first_name"] as AnyObject?) as? String
                ProfileName = self.themes.checkNullValue(self.userDataDict["first_name"] as AnyObject?) as! String
                
                let likeValue = self.themes.checkNullValue(self.userDataDict["interest_from"] as AnyObject?) as? String
                
                let favValue = self.themes.checkNullValue(self.userDataDict["fav_from"] as AnyObject?) as? String
                
                let userId = themes.getUserId()
                if(userId == likeValue){
                    cell.likeBtn.setSelected(selected: true, animated: false)
                    cell.likeBtn.isUserInteractionEnabled = false
                }else{
                    cell.likeBtn.isUserInteractionEnabled = true
                    cell.likeBtn.setSelected(selected: false, animated: false)
                }
                
                if(userId == favValue){
                    cell.starBtn.setSelected(selected: true, animated: false)
                    
                }else{
                    
                    cell.starBtn.setSelected(selected: false, animated: false)
                }
                
                let DOB = self.themes.checkNullValue(self.userDataDict["dob"] as AnyObject?) as? String
                if DOB != "" {
                    let age = self.themes.getAgeFromDOF(date: DOB!)
                    cell.ageLable.text = "\(age.0)"
                }
                
                let interest_status = self.themes.checkNullValue(self.userDataDict["interest_status"] as AnyObject?) as? String
                cell.requestView.isHidden = true
                cell.acceptedLabel.isHidden = false
                
                
                cell.chatBtn.setImage(UIImage(named: "chatDefault"), for: .normal)
                cell.chatBtn.isEnabled = false
                
                if interest_status == "0"{
                    
                    
                    if(fromScreen == "interestme"){
                        cell.requestView.isHidden = false
                    }else if(fromScreen == "myinterest"){
                        cell.requestView.isHidden = true
                    }
                    
                    cell.acceptedLabel.text = "Pending"
                    cell.acceptedLabel.backgroundColor = yellow
                    
                }else if interest_status == "1"{
                    
                    cell.acceptedLabel.text = "Accepted"
                    cell.acceptedLabel.backgroundColor = green
                    
                    
                    
                    cell.likeBtn.setSelected(selected: true, animated: false)
                    cell.likeBtn.isUserInteractionEnabled = false
                    
                    
                    if self.themes.getplanStatus() == "1" {
                        
                        if self.themes.getChatting() == "Yes"{
                            cell.chatBtn.setImage(UIImage(named: "chat_activ"), for: .normal)
                            cell.chatBtn.isEnabled = true
                        }else{
                            cell.chatBtn.setImage(UIImage(named: "chatDefault"), for: .normal)
                            cell.chatBtn.isEnabled = false
                        }
                        
                    }else{
                        
                        cell.chatBtn.setImage(UIImage(named: "chatDefault"), for: .normal)
                        cell.chatBtn.isEnabled = false
                    }
                    
                }else if interest_status == "2" {
                    
                    cell.acceptedLabel.text = "Rejected"
                    cell.acceptedLabel.backgroundColor = UIColor.red
                    
                }
                else {
                    
                    cell.acceptedLabel.text = "Pending"
                    cell.acceptedLabel.backgroundColor = yellow
                    
                }
                
                
                
                let photoApproved = self.themes.checkNullValue(userDataDict["photo1_approve"] as AnyObject?) as! String
                
                if photoApproved == "APPROVED" {
                    let fullimageUrl = ImagebaseUrl + imageUrl
                    cell.profileImgView.sd_setImage(with: URL(string: fullimageUrl), placeholderImage: UIImage(named: "defaultPic"))
                    DispatchQueue.main.async {
                        cell.userImgView.sd_setShowActivityIndicatorView(true)
                        cell.userImgView.sd_setIndicatorStyle(.whiteLarge)
                        cell.userImgView.sd_setImage(with: URL(string: fullimageUrl), placeholderImage:UIImage(named: "defaultPic"), completed: { (image, error, cacheType, url) -> Void in
                            if ((error) != nil) {
                                cell.userImgView.image = UIImage(named: "defaultPic")
                                cell.userImgView.sd_setShowActivityIndicatorView(true)
                            } else {
                                cell.userImgView.image = image
                                cell.userImgView.sd_setShowActivityIndicatorView(false)
                                
                                
                                if self.themes.getplanStatus() == "1"{
                                    
                                }else{
                                    let blurEffect = UIBlurEffect(style: .light)
                                    let effectView = UIVisualEffectView(effect: blurEffect)
                                    effectView.frame =  cell.userImgView.frame
                                    effectView.alpha = 0.6
                                    cell.userImgView.addSubview(effectView)
                                    cell.userImgView.clipsToBounds = true
                                }
                                
                                
                            }
                        })
                    }
                    
                }else{
                    
                    cell.userImgView.image = UIImage(named: "defaultPic")
                    cell.profileImgView.image = UIImage(named: "defaultPic")
                }
                
                //                if blockSeleccted == "yes"{
                //                    cell.blockBtn.setSelected(selected: true, animated: false)
                //                }else{
                //                    cell.blockBtn.setSelected(selected: false, animated: false)
                //                }
                //
                cell.chatBtn.setImage(UIImage(named: "chat_activ"), for: .normal)
                cell.chatBtn.isEnabled = true
                cell.msgComposeBtn.addTarget(self, action: #selector(composeBtnClicked(_:)), for: .touchUpInside)
                cell.callButn.addTarget(self, action: #selector(callButnClick(_:)), for: .touchUpInside)
                cell.acceptBtn.addTarget(self, action: #selector(AccpectBtnClicked(_:)), for: .touchUpInside)
                cell.rejectBtn.addTarget(self, action: #selector(RejectBtnClicked(_:)), for: .touchUpInside)
                cell.chatBtn.addTarget(self, action: #selector(chatBtnClickAction(_:)), for: .touchUpInside)
                
                cell.backBtn.addTarget(self, action: #selector(backBtnClickAction(_:)), for: .touchUpInside)
                
              cell.adsListArr = self.adsListArr
                             if self.adsListArr.count > 0 {
                                 cell.adshieghtConstant.constant = 120
                             }else{
                                 cell.adshieghtConstant.constant = 0
                             }
                             cell.adsCollectionView.reloadData()
                
                return cell
                
            }
            
            
            
        } else if section == 1 {
            
            let header = tableView.dequeueReusableCell(withIdentifier: "InfoHeaderTableViewCell")as! InfoHeaderTableViewCell
            header.headerLabel.text = "Basic Information"
            return header
            
        }else if section == 2 {
            
            let header = tableView.dequeueReusableCell(withIdentifier: "InfoHeaderTableViewCell")as! InfoHeaderTableViewCell
            header.headerLabel.text = "Religion Information"
            return header
            
        }else if section == 3 {
            
            let header = tableView.dequeueReusableCell(withIdentifier: "InfoHeaderTableViewCell")as! InfoHeaderTableViewCell
            header.headerLabel.text = "Location Information"
            return header
            
        }else if section == 4 {
            
            let header = tableView.dequeueReusableCell(withIdentifier: "InfoHeaderTableViewCell")as! InfoHeaderTableViewCell
            header.headerLabel.text = "Professional Information"
            return header
            
        }else if section == 5 {
            
            let header = tableView.dequeueReusableCell(withIdentifier: "BlockTableViewCell")as! BlockTableViewCell
            
            if fromScreen == "Block"{
                header.blockBtn.setTitle("Unblock", for: .normal)
                header.blockBtn.addTarget(self, action: #selector(blockButtonAction(_:)), for:.touchUpInside)
            } else {
                header.blockBtn.setTitle("Block", for: .normal)
                header.blockBtn.addTarget(self, action: #selector(blockButtonAction1(_:)), for:.touchUpInside)
            }
            
            return header
            //            let header = tableView.dequeueReusableCell(withIdentifier: "instaTableViewCell")as! instaTableViewCell
            //            return header
            
        } else if section == 6 {
            
            let header = tableView.dequeueReusableCell(withIdentifier: "IntrestsTableViewCell")as! IntrestsTableViewCell
            return header
            
        } else {
            
            let header = tableView.dequeueReusableCell(withIdentifier: "ProfileVerifyTableViewCell")as! ProfileVerifyTableViewCell
            return header
        }
    }
    
    @objc func callButnClick(_ sender : UIButton){
        
        viewContactNumberApi()
        
    }
    
    @objc func composeBtnClicked(_ sender : UIButton){
        
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ComposeVC")as! ComposeVC
        Vc.selectedName = ProfileName
        Vc.fromScreen = "detailVC"
        Vc.selectedId = match_id
        self.navigationController?.pushViewController(Vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BasicInfoTableViewCell", for: indexPath)as! BasicInfoTableViewCell
            //            let age = self.themes.checkNullValue(self.userDataDict["first_name"] as AnyObject?) as! String
            //            cell.ageLabel.text = "     " + age
            let ethnicity =  self.themes.checkNullValue(self.userDataDict["ethnicity"] as AnyObject?) as! String
            cell.occupationLabel.text = "     " + ethnicity
            let educ = self.themes.checkNullValue(self.userDataDict["education"] as AnyObject?) as! String
            cell.educationLabel.text = "     " + educ
            let heigh = self.themes.checkNullValue(self.userDataDict["height_ft"] as AnyObject?) as! String
            
            let inch = self.themes.checkNullValue(self.userDataDict["height_inc"] as AnyObject?) as! String
            let heightCm = self.themes.checkNullValue(self.userDataDict["height_cms"])as? String
            
            cell.heightLabel.text =  heigh + "'" + inch + "''/" + heightCm!
            let religion = self.themes.checkNullValue(self.userDataDict["religion"] as AnyObject?) as! String
            cell.religionLabel.text = "     " + religion
            let smoke = self.themes.checkNullValue(self.userDataDict["smoke"] as AnyObject?) as! String
            cell.smokingLabel.text = "     " + smoke
            let drink = self.themes.checkNullValue(self.userDataDict["drink"] as AnyObject?) as! String
            cell.drinkingLabel.text = "     " + drink
            let kid = self.themes.checkNullValue(self.userDataDict["children"] as AnyObject?) as! String
            cell.kidsLabel.text = "     " + kid
            
            let DOB = self.themes.checkNullValue(self.userDataDict["dob"] as AnyObject?) as? String
            let age = self.themes.getAgeFromDOF(date: DOB ?? "04-20-1991")
            cell.ageLabel.text = "\(age.0)"
            
            cell.selectionStyle = .none
            return cell
        } else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReligionInfoTableViewCell", for: indexPath)as! ReligionInfoTableViewCell
            let religion = self.themes.checkNullValue(self.userDataDict["religion"] as AnyObject?) as! String
            cell.religionLabel.text = "     " + religion
            let language = self.themes.checkNullValue(self.userDataDict["mtongue_name"] as AnyObject?) as! String
            cell.languageLabel.text = "     " + language
            cell.selectionStyle = .none
            return cell
        } else if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LocationInfoTableViewCell", for: indexPath)as! LocationInfoTableViewCell
            let city = self.themes.checkNullValue(self.userDataDict["city"] as AnyObject?) as! String
            cell.cityLabel.text = "     " + city
            let state = self.themes.checkNullValue(self.userDataDict["state"] as AnyObject?) as! String
            cell.stateLabel.text = "     " + state
            let zipcod = self.themes.checkNullValue(self.userDataDict["zipcode"] as AnyObject?) as! String
            cell.zipCodeLabel.text = "     " + zipcod
            cell.selectionStyle = .none
            return cell
        } else if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfessionalInfoTableViewCell", for: indexPath)as! ProfessionalInfoTableViewCell
            let occupation =  self.themes.checkNullValue(self.userDataDict["occupation"] as AnyObject?) as! String
            cell.occupationLabel.text = "     " + occupation
            let educ = self.themes.checkNullValue(self.userDataDict["education"] as AnyObject?) as! String
            cell.educationLabel.text = "     " + educ
            let income = self.themes.checkNullValue(self.userDataDict["income"] as AnyObject?) as! String
            cell.incomeLabel.text = "     " + income
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 0
        } else if indexPath.section == 1 {
            return 277
        } else if indexPath.section == 2 {
            return 89
        } else if indexPath.section == 3 {
            return 124
        } else if indexPath.section == 4 {
            return 126
        } else if indexPath.section == 5 {
            return 0
        } else if indexPath.section == 6 {
            return 0
        } else {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            
            if fromScreen == "myfavourite"{
                return UITableViewAutomaticDimension
            }else if fromScreen == "Block"{
                return UITableViewAutomaticDimension
            }else{
                return UITableViewAutomaticDimension
            }
        } else if section == 5{
            return 60
        } else if section == 6 {
            return 168
        } else if section == 7 {
            return 100
        } else {
            return 44
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            
            if fromScreen == "myfavourite"{
                return 689
            }else if fromScreen == "Block"{
                return 789
            }else{
                return 567
            }
        }else{
            return 150
        }
    }
    
    @objc func starButtonClickAction(_ sender : UIButton){
        favourateCheck = true
        let id = self.themes.checkNullValue(self.userDataDict["id"] as AnyObject?) as! String
        let myID = self.themes.getUserId()
        favouritApi(user_id: myID, Id: id)
        
        
        
    }
    
    func switchToChat(with chatViewController: MyChatViewController, animated: Bool) {
        
       
        self.navigationController?.pushViewController(chatViewController, animated: true)
        
    }
    
    internal func setupDataSources() {
        
        let rosterContext = self.xmppController.managedObjectContext_roster()
        
        let entity = NSEntityDescription.entity(forEntityName: "XMPPUserCoreDataStorageObject", in: rosterContext)
        let sd1 = NSSortDescriptor(key: "sectionNum", ascending: true)
        let sd2 = NSSortDescriptor(key: "displayName", ascending: true)
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        fetchRequest.entity = entity
        fetchRequest.sortDescriptors = [sd1, sd2]
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: rosterContext, sectionNameKeyPath: "sectionNum", cacheName: nil)
        self.fetchedResultsController?.delegate = self
        
        //  self.tableView.reloadData()
    }
    
    
    @objc func chatBtnClickAction(_ sender : UIButton){
       
        let chatStatus = self.themes.getChatstatus()
        
        if chatStatus == "0"{
            self.themes.showAlert(title: "Alert", message: "These Feature is Not Available", sender: self)
        }else if chatStatus == "1"{
        
        
        if self.xmppController.xmppStream.isAuthenticated() {
            let jidString = self.userDataDict["username"]
            guard let userJIDString = jidString,
                let userJID = XMPPJID.init(string: "\(userJIDString)@divorcelovelounge.com" as? String ) else {
                    return
            }
            self.xmppController?.xmppRoster.addUser(userJID, withNickname:nil)
            self.setupDataSources()
            try! self.fetchedResultsController?.performFetch()
            let privateChatsIndexPath = IndexPath(row: 0, section: 0)
            let user = self.fetchedResultsController?.object(at: privateChatsIndexPath) as! XMPPUser
            print(user)
            let appdelagate = UIApplication.shared.delegate as! AppDelegate
            appdelagate.chatName = self.themes.checkNullValue(self.userDataDict["username"] as AnyObject) as? String
          
            let chatViewController = MyChatViewController(
                titleProvider: XMPPOneToOneChatTitleProvider(user: user),
                chatDataSource: XMPPCoreDataChatDataSource(xmppController: self.xmppController, configuration: .privateChat, jid: userJID.bare()),
                messageSender: self.xmppController.xmppOneToOneChat.session(forUserJID: userJID.bare()),
                additionalActions: [XMPPOneToOneChatMessageHistoryFetchAction(xmppController: self.xmppController, userJid: userJID.bare())]
            )
            self.switchToChat(with: chatViewController, animated: true)
    //  }
        // self.present(alertController, animated: true, completion: nil)
        }else{
           self.themes.showToast(message: "Please Wait your data is Loading..", sender: self)
        }
        
       
        }
        
        
        
    }
    
    func Accepect_InterestAPI(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            
            let userid = self.themes.getUserId()
            let client_id = self.userDataDict["id"] as AnyObject
            let dict = ["user_id":userid,"id":client_id] as [String:AnyObject]
            
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:interestAccept, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    self.navigationController?.popViewController(animated: true)
                    
                    if(self.fromScreen1 == "myfavourite"){
                        self.delegate?.backAction(from: "myfavourite")
                    }else if(self.fromScreen1 == "interestme"){
                        self.delegate?.backAction(from: "interestme")
                    }else if(self.fromScreen1 == "myinterest"){
                        self.delegate?.backAction(from: "myinterest")
                    }else if(self.fromScreen1 == "viewedMe"){
                        self.delegate?.backAction(from: "viewedMe")
                    }else if(self.fromScreen1 == "favouratedMe"){
                        self.delegate?.backAction(from: "favouratedMe")
                    }
                    
                    
                    
                    
                } else {
                    let result = response["result"] as! String
                   // self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    
    func viewContactNumberApi(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            
            let userid = self.themes.getUserId()
            let client_id = self.userDataDict["id"] as AnyObject
            let dict = ["user_id":userid,"id":client_id] as [String:AnyObject]
            
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:viewContact, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    //self.navigationController?.popViewController(animated: true)
                    
                    let number = self.userDataDict["phone_number"] as! String
                    
                    
                    if(number == ""){
                        self.themes.showAlert(title: "Alert ☹️", message: "There is no number", sender: self)
                    }else{
                        if let url = NSURL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url as URL) {
                            UIApplication.shared.openURL(url as URL)
                        }
                    }
                    
                } else {
                    let result = response["result"] as! String
                   // self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    
    
    
    
    func favouritApi(user_id: String, Id : String){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            
            let parameters = [ "user_id":user_id, "id":Id,
                               ] as [String : Any]
            
            print("favourite parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:favourite, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    if(self.favourateCheck1){
                        self.favourateCheck1 = false
                    }else{
                        self.favourateCheck1 = true
                    }
                    self.detailsTableView.reloadData()
                    
                    
                    
                } else {
                    let result = response["result"] as! String
                   // self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func sendIntrestApi(user_id: String, Id : String){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            
            let parameters = [ "user_id":user_id, "id":Id,
                               ] as [String : Any]
            
            print("send interest parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:sendInterest, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    
                } else {
                    let result = response["result"] as! String
                   // self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    func Reject_InterestAPI(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            
            let userid = self.themes.getUserId()
            let client_id = self.userDataDict["id"] as AnyObject
            let dict = ["user_id":userid,"id":client_id] as [String:AnyObject]
            
            
            
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:interestReject, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    self.navigationController?.popViewController(animated: true)
                    if(self.fromScreen1 == "myfavourite"){
                        self.delegate?.backAction(from: "myfavourite")
                    }else if(self.fromScreen1 == "interestme"){
                        self.delegate?.backAction(from: "interestme")
                    }else if(self.fromScreen1 == "myinterest"){
                        self.delegate?.backAction(from: "myinterest")
                    }else if(self.fromScreen1 == "viewedMe"){
                        self.delegate?.backAction(from: "viewedMe")
                    }else if(self.fromScreen1 == "favouratedMe"){
                        self.delegate?.backAction(from: "favouratedMe")
                    }
                } else {
                    let result = response["result"] as! String
                    //self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
        
        
    }
    
    func blockUnblockApi(user_id: String, Id : String){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            
            let parameters = [ "user_id":user_id, "id":Id,
                               ] as [String : Any]
            
            print("favourite parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:blockUnblock, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    
                } else {
                    let result = response["result"] as! String
                    
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    
    
    @objc func AccpectBtnClicked(_ sender : UIButton){
        
        self.Accepect_InterestAPI()
        
    }
    
    
    @objc func RejectBtnClicked(_ sender : UIButton){
        
        
        self.Reject_InterestAPI()
        
        
    }
    
    
    @objc func btnHideAction(button: UIButton) {
        
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopViewController") as! PopViewController
        
        popOverVC.fromScreen = ""
        //                    popOverVC.ImageStr = ""
        popOverVC.headingMesg = "Alert"
        popOverVC.subMesg = "Your account is under progress !"
        
        self.addChildViewController(popOverVC)
        popOverVC.view.center = self.view.frame.center
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
        
    }
    
    func getChatListApi(){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let urserId = themes.getXmppUserName() as! String
            var dict = [String : String]()
            dict.updateValue(urserId , forKey: "username")
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:getChatList, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    self.chatList = response["xmpplist"]as! [AnyObject]
                    
                    for Array in self.chatList{
                        let userdata = Array as! [String:AnyObject]
                        let name = userdata["jid"] as! String
                        let user_JID = name // "\(name)@divorcelovelounge.com"
                        let JID = XMPPJID.init(string: user_JID)
                        self.xmppController?.xmppRoster.addUser(JID, withNickname: name)
                        
                    }
                   
                    self.detailsTableView.reloadData()
                    
                    
                } else {
                    let result = response["result"] as! String
                  //  self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }

    
    func getprofileAPi(){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:getProfilePics, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    let photo = response["photos"] as! [String:AnyObject]
                    let photo_approve = self.themes.checkNullValue(photo["photo1_approve"]) as! String
                    if(photo_approve == "UNAPPROVED"){
                        self.themes.savePhotoApproveStatus(false)
                    }else{
                        self.themes.savePhotoApproveStatus(true)
                    }
                    self.detailsTableView.reloadData()
                    
                } else {
                    let result = response["result"] as! String
                  //  self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    
    
    
    
    
    
    
   
}

extension UserDetailsVC: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
       
    }
}

extension UserDetailsVC: XMPPControllerRoomListDelegate {
    
    func roomListDidChange(in controller: XMPPController) {
        OperationQueue.main.addOperation {
           
        }
    }
}

extension UserDetailsVC: MUCRoomCreateViewControllerDelegate {
    
    func createRoom(_ roomName: String, users: [XMPPJID]?) {
        xmppController.addRoom(withName: roomName, initialOccupantJids: users)
        dismiss(animated: true, completion: nil)
    }
}

private extension XMPPCoreDataChatDataSource {
    
    enum Configuration {
        case privateChat, groupChat
    }
    
    convenience init(xmppController: XMPPController, configuration: Configuration, jid: XMPPJID) {
        let baseMessageModelProvider = XMPPCoreDataChatBaseMessageModelProvider(xmppRetransmission: xmppController.xmppRetransmission)
        let textMessageModelProvider = XMPPCoreDataChatDataSourceTextMessageModelProvider(
            baseProvider: baseMessageModelProvider,
            xmppRoster: xmppController.xmppRoster,
            xmppStream: xmppController.xmppStream,
            correctionRecipientJid: jid
        )
        let photoMessageModelProvider = XMPPCoreDataChatDataSourcePhotoMessageModelProvider(
            baseProvider: baseMessageModelProvider,
            xmppOutOfBandMessaging: xmppController.xmppOutOfBandMessaging,
            xmppOutOfBandMessagingStorage: xmppController.xmppOutOfBandMessagingStorage,
            remotePartyJid: jid
        )
        let chatItemBuilders: [XMPPCoreDataChatDataSourceItemBuilder] = [textMessageModelProvider, photoMessageModelProvider]
        let chatItemEventSources: [XMPPCoreDataChatDataSourceItemEventSource] = [baseMessageModelProvider, photoMessageModelProvider]
        
        
        
        switch configuration {
        case .privateChat:
            self.init(
                fetchedResultsController: XMPPMessageArchiving_Message_CoreDataObject.chatDataSourceFetchedResultsController(with: xmppController.xmppMessageArchivingStorage.mainThreadManagedObjectContext, userJid: jid),
                chatItemBuilders: chatItemBuilders, chatItemEventSources: chatItemEventSources
            )
        case .groupChat:
            self.init(
                fetchedResultsController: XMPPRoomLightMessageCoreDataStorageObject.chatDataSourceFetchedResultsController(with: xmppController.xmppRoomLightCoreDataStorage.mainThreadManagedObjectContext, roomJid: jid),
                chatItemBuilders: chatItemBuilders, chatItemEventSources: chatItemEventSources
            )
        }
        
        
    }
}

         
