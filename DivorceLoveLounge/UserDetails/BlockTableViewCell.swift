//
//  BlockTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 26/09/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class BlockTableViewCell: UITableViewCell {
    
    @IBOutlet weak var blockBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
     self.blockBtn.layer.cornerRadius = blockBtn.frame.size.height/2
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
