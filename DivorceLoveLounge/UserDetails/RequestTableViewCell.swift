//
//  RequestTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by nagaraj  kumar on 28/05/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
import FaveButton



class RequestTableViewCell: UITableViewCell,FaveButtonDelegate,UICollectionViewDelegate,UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    func faveButton(_ faveButton: FaveButton, didSelected selected: Bool) {
        
        
        if( faveButton == starBtn){
            delegate?.CanReceiveProfileBtnData(buttonName: "starbtn", selected: selected)
            
        }else if (faveButton == likeBtn){
            delegate?.CanReceiveProfileBtnData(buttonName: "likebtn", selected: selected)
        }
//        else if (faveButton == blockBtn){
//            delegate?.CanReceiveProfileBtnData(buttonName: "blockbtn", selected: selected)
//        }
        
        
    }
    
    
    var sendIntrstbtnTapAction : (()->())?
    var favrBtnTapAction : (()->())?
    
    @IBOutlet var backBtn: UIButton!
    @IBOutlet var userImgView: UIImageView!
    @IBOutlet var profileImgView: UIImageView!
    @IBOutlet var profView: UIView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var ageLable: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var infoBtn: UIButton!
    @IBOutlet var acceptedLabel: UILabel!
    @IBOutlet var requestView: UIView!
    @IBOutlet var acceptBtn: UIButton!
    @IBOutlet var rejectBtn: UIButton!
    
    
    @IBOutlet var detailImgBtn: UIButton!
    @IBOutlet var chatBtn: UIButton!
    @IBOutlet var aboutTextView: UILabel!    
    @IBOutlet weak var msgComposeBtn: UIButton!
    
    @IBOutlet var likeBtn: FaveButton!
    @IBOutlet var starBtn: FaveButton!
    @IBOutlet var callButn: FaveButton!
    
    
    
    @IBOutlet weak var adshieghtConstant: NSLayoutConstraint!
    
    @IBOutlet weak var adsCollectionView: UICollectionView!
    var adsListArr = [AnyObject]()

    
    var  delegate : CanReceiveProfileBtnData?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
      //  acceptedLabel.isHidden = true
        //nameLabel.intrinsicContentSize.width
        
        
        profView.layer.cornerRadius = (profView.frame.size.height)/2
        profileImgView.layer.cornerRadius = (profileImgView.frame.size.height)/2
        
        profView.layer.masksToBounds = true
        profileImgView.layer.masksToBounds = true
        backBtn.layer.cornerRadius = backBtn.frame.height/2
       
        
        likeBtn.delegate = self
        starBtn.delegate = self
//        blockBtn.delegate = self
        
        
        
        chatBtn.layer.shadowColor = UIColor.gray.cgColor
        chatBtn.layer.shadowOffset = CGSize(width: 0, height: 1)
        chatBtn.layer.masksToBounds = false
        chatBtn.layer.shadowRadius = 1
        chatBtn.layer.shadowOpacity = 0.8
        chatBtn.layer.cornerRadius = chatBtn.frame.size.height / 2

        likeBtn.layer.shadowColor = UIColor.gray.cgColor
        likeBtn.layer.shadowOffset = CGSize(width: 0, height: 1)
        likeBtn.layer.masksToBounds = false
        likeBtn.layer.shadowRadius = 1
        likeBtn.layer.shadowOpacity = 0.8
        likeBtn.layer.cornerRadius = likeBtn.frame.width / 2
        

        starBtn.layer.shadowColor = UIColor.gray.cgColor
        starBtn.layer.shadowOffset = CGSize(width: 0, height: 1)
        starBtn.layer.masksToBounds = false
        starBtn.layer.shadowRadius = 1
        starBtn.layer.shadowOpacity = 0.8
        starBtn.layer.cornerRadius = starBtn.frame.width / 2
        
        callButn.layer.shadowColor = UIColor.gray.cgColor
        callButn.layer.shadowOffset = CGSize(width: 0, height: 1)
        callButn.layer.masksToBounds = false
        callButn.layer.shadowRadius = 1
        callButn.layer.shadowOpacity = 0.8
        callButn.layer.cornerRadius = callButn.frame.width / 2
        
        msgComposeBtn.layer.shadowColor = UIColor.gray.cgColor
        msgComposeBtn.layer.shadowOffset = CGSize(width: 0, height: 1)
        msgComposeBtn.layer.masksToBounds = false
        msgComposeBtn.layer.shadowRadius = 1
        msgComposeBtn.layer.shadowOpacity = 0.8
        msgComposeBtn.layer.cornerRadius = msgComposeBtn.frame.width / 2
 
        
      //  acceptedLabel.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        acceptedLabel.layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        acceptedLabel.layer.cornerRadius = acceptedLabel.frame.size.height/2
        acceptedLabel.layer.shadowOpacity = 1.0
        acceptedLabel.layer.shadowRadius = 1.0
        acceptedLabel.layer.masksToBounds = true
        
       // acceptBtn.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        acceptBtn.layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        acceptBtn.layer.cornerRadius = acceptBtn.frame.size.height/2
        acceptBtn.layer.shadowOpacity = 1.0
        acceptBtn.layer.shadowRadius = 1.0
        acceptBtn.layer.masksToBounds = true
        
      //  rejectBtn.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        rejectBtn.layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        rejectBtn.layer.cornerRadius = rejectBtn.frame.size.height/2
        rejectBtn.layer.shadowOpacity = 1.0
        rejectBtn.layer.shadowRadius = 1.0
        rejectBtn.layer.masksToBounds = true
        
        self.adsCollectionView.register(UINib(nibName: "AdsBannerCollecctionCell", bundle: nil), forCellWithReuseIdentifier: "AdsBannerCollecctionCell")
               
               
               
               self.adsCollectionView.delegate = self
               self.adsCollectionView.dataSource = self
              
              
               if self.adsListArr.count > 0 {
                   self.setTimer()
                 //  self.adsHeightConstant.constant = 120
               }else{
                 //  self.adsHeightConstant.constant = 0

               }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        
        
        // Configure the view for the selected state
    }
    
    @IBAction func sendRequButtonTapped(_ sender: UIButton) {
        sendIntrstbtnTapAction?()
    }
    @IBAction func favrButtonTapped(_ sender: UIButton) {
        favrBtnTapAction?()
    }
    func setTimer() {
          let _ = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(AboutMeTableViewCell.autoScroll), userInfo: nil, repeats: true)
      }
      var x = 0
      @objc func autoScroll() {
          if self.x < self.adsListArr.count {
              let indexPath = IndexPath(item: x, section: 0)
              self.adsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
              self.x = self.x + 1
          }else{
              self.x = 0
              self.adsCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
          }
      }
      func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return self.adsListArr.count
      }
      
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          return CGSize(width: adsCollectionView.frame.size.width, height: adsCollectionView.frame.size.height)
      }
      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = adsCollectionView.dequeueReusableCell(withReuseIdentifier: "AdsBannerCollecctionCell", for: indexPath) as! AdsBannerCollecctionCell
          let data = self.adsListArr[indexPath.row] as! [String:AnyObject]
          let type = data["add_type"] as! String
          if type == "Custom" {
              cell.adsWebView.isHidden = true
              cell.adsImageView.isHidden = false
              let url = "\(adsUrl)\(data["add_image"] as! String)"
              cell.adsImageView.af_setImage(withURL: URL(string:url)!)
          }else{
              cell.adsWebView.isHidden = false
              cell.adsImageView.isHidden = true
          }
          
          return cell
      }
      
      func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          
          let data = self.adsListArr[indexPath.row] as! [String:AnyObject]
          let url = data["add_link"]
          if let url = URL(string: url as! String) {
              UIApplication.shared.open(url)
          }
      }
    
}
