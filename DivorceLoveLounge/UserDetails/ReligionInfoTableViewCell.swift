//
//  ReligionInfoTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/24/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class ReligionInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var religionLabel: UILabel!    
    @IBOutlet weak var languageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
