//
//  MemberShipDetailsVC.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 6/26/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
import Stripe

class MemberShipDetailsVC: UIViewController,UITableViewDelegate,UITableViewDataSource,STPPaymentCardTextFieldDelegate,UITextFieldDelegate {
    
    var window: UIWindow?
    
    
    
    @IBOutlet var popupView: UIView!
    
    @IBOutlet var popuptableView: UITableView!
    
    
    @IBOutlet var backBtn: UIButton!
    
    
    
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    
     var monthBtnValue = Bool()
     var yearBtnValue = Bool()
    var cashBtnValue = Bool()
    var onlineBtnValue = Bool()
    var paymentSelection = ""
    var stripeToken:STPToken!
   
    var dataDict = [String:AnyObject]()
    var monthsArray = [AnyObject]()
    
    
    var planId = ""
    var plan_duration = ""
    var discount = ""
    
        var planeamount = ""
    var monthplan = ""
    
    
    
     var paymentTextField = STPPaymentCardTextField()
    
    
    
    
    
    @IBOutlet weak var membershipDetailsTableView: UITableView!
    
    @IBOutlet weak var makePaymentBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.monthsArray = dataDict["durations"] as! [AnyObject]
        
        
        popupView.isHidden = true
        backBtn.isHidden = true
        popupView.layer.cornerRadius = 10
    
        monthBtnValue = false
        yearBtnValue = false
        cashBtnValue = false
        onlineBtnValue = false
        membershipDetailsTableView.delegate = self
        membershipDetailsTableView.dataSource = self
        makePaymentBtn.layer.cornerRadius = (makePaymentBtn.frame.size.height)/2
        
        
        self.membershipDetailsTableView.register(UINib(nibName: "MembershipCell", bundle: nil), forCellReuseIdentifier: "MembershipCell")
        self.membershipDetailsTableView.register(UINib(nibName: "StripeCellTableViewCell", bundle: nil), forCellReuseIdentifier: "StripeCellTableViewCell")
        
        
        self.popuptableView.register(UINib(nibName: "months", bundle: nil), forCellReuseIdentifier: "months")
        
        
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(MemberShipDetailsVC.keyboardUp), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MemberShipDetailsVC.keyboardDown), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
       
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.membershipDetailsTableView.addGestureRecognizer(tapGesture)
        
        
    let frame1 = CGRect(x: 0, y: 0, width: self.view.frame.width - 50 , height:40 )
                paymentTextField = STPPaymentCardTextField(frame: frame1)
                paymentTextField.layer.cornerRadius = 5
                paymentTextField.layer.borderColor = ligtGray.cgColor
                paymentTextField.delegate = self
        
        
        
        
        
    }
    
    
    @IBAction func popupCloseAction(_ sender: Any) {
        
        popupView.isHidden = true
        backBtn.isHidden = true
    }
    
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        popupView.isHidden = true
        backBtn.isHidden = true
    }
    
    
    
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {

        
        self.resignFirstResponder()
        paymentTextField.resignFirstResponder()
        
        
    }
    
    @objc func keyboardUp(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            //
            self.view.frame.origin.y = 100
            if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardRectangle = keyboardFrame.cgRectValue
                let keyboardHeight = keyboardRectangle.height
                self.view.frame.origin.y -= keyboardHeight
            }
        }
    }
    @objc func keyboardDown(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            self.view.frame.origin.y = 0
        }
    }
    
    
    

   
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func makePymentAction(_ sender: Any) {
        
        
        if paymentSelection == ""{
            
            self.themes.showAlert(title: "Alert", message: "Please Select PaymentType", sender: self)
            
            
        }else if plan_duration == ""{
            
            self.themes.showAlert(title: "Alert", message: "Please Select Plan Duration", sender: self)
            
        }else{
            
            if paymentSelection == "Cash"{
                 makePaymentAPI()
            }else  if paymentSelection == "Online"{
                
                
                if paymentTextField.cardNumber?.count == 16{
                    if paymentTextField.cvc?.count == 3 {
                        getStripeToken(card: paymentTextField.cardParams)
                    }else{
                        self.themes.showAlert(title: "Alert", message: "Please Enter CVC Number", sender: self)
                    }
                }else {
                self.themes.showAlert(title: "Alert", message: "Please Enter Valid Card Number", sender: self)
                 }
                
                
                
                }
            }
          
        }
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if tableView == popuptableView{
            
           return monthsArray.count
            
        }else{
        
        
        if paymentSelection == "Online" {
            
        return 2
            
        }
        else{
       
            return 1
         }
            
            
        }
      
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == popuptableView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "months", for: indexPath) as! months
            
            let array = self.monthsArray[indexPath.row]
            cell.monthsLbl.text = array["month"] as! String  + " Months"
            
            DispatchQueue.main.async {
                
                cell.monthsLbl.layer.masksToBounds = true
                let yourViewBorder = CAShapeLayer()
                yourViewBorder.strokeColor = UIColor.darkGray.cgColor
                yourViewBorder.lineDashPattern = [2, 2]
                yourViewBorder.frame = cell.monthsLbl.bounds
                yourViewBorder.fillColor = nil
                yourViewBorder.path = UIBezierPath(rect:cell.monthsLbl.bounds).cgPath
                cell.monthsLbl.layer.addSublayer(yourViewBorder)
            }
            
            
 
            return cell
            
            
        }else{
        
        if indexPath.row == 0{
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MembershipCell", for: indexPath)as! MembershipCell
        cell.selectionStyle = .none
        
            cell.backView.layer.shadowOffset = CGSize(width: 0.8, height: 0.8)
            cell.backView.layer.shadowColor = UIColor.black.cgColor
            cell.backView.layer.cornerRadius = 5
            cell.backView.layer.shadowOpacity = 1
            cell.backView.layer.shadowRadius = 2
            cell.backView.layer.masksToBounds = false
            
        cell.planNameLbl.text = dataDict["package_type"] as? String
        cell.amountLbl.text = planeamount
        cell.discountLbl.text = discount
            
        planId = dataDict["id"] as! String
            

            DispatchQueue.main.async {
                let yourViewBorder = CAShapeLayer()
                yourViewBorder.strokeColor = UIColor.darkGray.cgColor
                yourViewBorder.lineDashPattern = [2, 2]
                yourViewBorder.frame = cell.monthview.bounds
                yourViewBorder.fillColor = nil
                yourViewBorder.path = UIBezierPath(rect:cell.monthview.bounds).cgPath
                cell.monthview.layer.addSublayer(yourViewBorder)
            }
        
            
        cell.monthBtn.setTitle(monthplan, for: .normal)
        cell.monthamountLbl.text = planeamount
            
            
            
        cell.monthBtn.addTarget(self, action: #selector(monthBtnClick(button:)), for:.touchUpInside)
        cell.cashBtn.addTarget(self, action: #selector(cashBtnClick(button:)), for:.touchUpInside)
        
        if(cashBtnValue){
            cell.cashImg.image = UIImage(named: "circle_selected")
        } else {
            cell.cashImg.image = UIImage(named: "circle_default")
        }
        cell.onlineBtn.addTarget(self, action: #selector(onlineBtnClick(button:)), for:.touchUpInside)
        
        
        if(onlineBtnValue){
            cell.onlineImg.image = UIImage(named: "circle_selected")
        } else {
            cell.onlineImg.image = UIImage(named: "circle_default")
        }
        return cell
            
       
      }else if indexPath.row == 1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "StripeCellTableViewCell", for: indexPath)as! StripeCellTableViewCell
            cell.selectionStyle = .none

            cell.paymentView.addSubview(paymentTextField)
            cell.backView.layer.shadowOffset = CGSize(width: 0.8, height: 0.8)
            cell.backView.layer.shadowColor = UIColor.black.cgColor
            cell.backView.layer.cornerRadius = 5
            cell.backView.layer.shadowOpacity = 1
            cell.backView.layer.shadowRadius = 2
            cell.backView.layer.masksToBounds = false
            
        return cell
        }
            
      return UITableViewCell()
            
        }
            
            
            
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if tableView == popuptableView{
            
            let array = self.monthsArray[indexPath.row]
            let month = array["month"] as! String  + " Months plan"
            let planamount = array["amount"] as! String
            let discoun = array["discount"] as! String
            plan_duration = array["duration_id"] as! String
            
            let amount = Double(planamount)
            let disamount = Double(discoun)
            let discou = disamount!/100
            let n = discou*amount!
            let totalamount = amount!-n
            
            discount =  "\(discoun)" + " %"
            planeamount = "$ " + "\(totalamount)"
            monthplan = month
            
            
            popupView.isHidden = true
            backBtn.isHidden = true
            membershipDetailsTableView.reloadData()
            
        }
        
    }
    
    
    
    
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        
        if textField.isValid{
            if textField.cvc?.count == 3{
                paymentTextField.resignFirstResponder()
            }
        }
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == popuptableView {
            return 65
        }else{
        if indexPath.row == 0 {
            return 417
        }
        else if indexPath.row == 1{
            return 165
        }
        return 0
        }
        
       
    }
    
    
    
    @objc func monthBtnClick(button: UIButton) {
        monthBtnValue = true
        yearBtnValue = false
        
        if self.monthsArray.count>0{
            
            self.popuptableView.delegate = self
            self.popuptableView.dataSource = self
            popupView.center = self.view.frame.center
            popupView.alpha = 1
            popupView.isHidden = false
            backBtn.isHidden = false
            self.view.addSubview(popupView)
            popuptableView.reloadData()
            
        }else{
            plan_duration = "one"
            self.themes.showToast(message: "NO plans are available", sender: self)
        }
        
        membershipDetailsTableView.reloadData()
        
        
        }
    
    @objc func yearBtnClick(button: UIButton) {
        
        monthBtnValue = false
        yearBtnValue = true
        plan_duration = "12"
        membershipDetailsTableView.reloadData()
        
    }
    
    @objc func cashBtnClick(button: UIButton) {
        
        
        paymentSelection = "Cash"
        onlineBtnValue = false
        cashBtnValue = true
         self.view.endEditing(true)
        membershipDetailsTableView.reloadData()
        let indexpath = IndexPath(row: 0, section: 0)
        membershipDetailsTableView.scrollToRow(at: indexpath, at: .none, animated: false)
        
        
        
        
        
    }
    
    @objc func onlineBtnClick(button: UIButton)
    {
       
        paymentSelection = "Online"
        onlineBtnValue = true
        cashBtnValue = false
        membershipDetailsTableView.reloadData()
      
        let indexpath = IndexPath(row: 1, section: 0)
        membershipDetailsTableView.scrollToRow(at: indexpath, at: .top, animated: false)
         self.view.endEditing(true)
      
    }
    
    
    
    func makePaymentAPI(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let urserId = themes.getUserId()
            
           
            var dict = [String : AnyObject]()
            
            if paymentSelection == "Online"{
                
                dict.updateValue(urserId as AnyObject, forKey: "user_id")
                dict.updateValue(paymentSelection as AnyObject, forKey: "payment_type")
                dict.updateValue(plan_duration as AnyObject, forKey: "duration_id")
                dict.updateValue(planId as AnyObject, forKey: "plan_id")
                 dict.updateValue(stripeToken.tokenId as AnyObject, forKey: "stripeToken")
            }else{
                
                dict.updateValue(urserId as AnyObject, forKey: "user_id")
                dict.updateValue(paymentSelection as AnyObject, forKey: "payment_type")
                dict.updateValue(plan_duration as AnyObject, forKey: "duration_id")
                dict.updateValue(planId as AnyObject, forKey: "plan_id")
                
                
            }
            
            
            
            
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:makePayment, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    self.themes.showAlert(title: "Success", message: "Your membership plan activaated", sender: self)
                   
                    let TopTabBarVC = self.storyboard?.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
                    self.navigationController?.pushViewController(TopTabBarVC, animated: true)
                    
                } else {
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    
    
    
    func getStripeToken(card:STPCardParams) {
        // get stripe token for current card
        STPAPIClient.shared().createToken(withCard: card) { token, error in
            if let token = token {
                print(token)
               // token.tokenId
                self.stripeToken = token
                self.makePaymentAPI()
                
            } else {
                print(error)
                self.themes.showAlert(title: "Alert", message: error!.localizedDescription, sender: self)
               
            }
        }
    }
    
    
    
    
    
    

    
}
