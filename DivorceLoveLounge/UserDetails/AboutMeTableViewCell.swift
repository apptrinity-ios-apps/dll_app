//
//  AboutMeTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 4/25/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
import FaveButton



protocol CanReceiveProfileBtnData {
    
    func CanReceiveProfileBtnData(buttonName:String,selected:Bool)
    
}

class AboutMeTableViewCell: UITableViewCell,FaveButtonDelegate ,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource{
    
    func faveButton(_ faveButton: FaveButton, didSelected selected: Bool) {
      
        if( faveButton == starButton){

            delegate?.CanReceiveProfileBtnData(buttonName: "starbtn", selected: selected)
            
        }else if (faveButton == likeButton){
            delegate?.CanReceiveProfileBtnData(buttonName: "likebtn", selected: selected)
        }
//        else if (faveButton == blockButton){
//
//         delegate?.CanReceiveProfileBtnData(buttonName: "blockbtn", selected: selected)
//
//        }
  
    }
    
    
    var sendIntrstbtnTapAction : (()->())?
    var favrBtnTapAction : (()->())?
    
    
    @IBOutlet var adsBannerView: UIView!
    
    @IBOutlet var adsCollectionView: UICollectionView!
    
    @IBOutlet var adsHeightConstant: NSLayoutConstraint!
    
    @IBOutlet var hideBtn: UIButton!
    @IBOutlet weak var detailImgBtn: UIButton!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var mesgButton: UIButton!
    @IBOutlet weak var likeButton: FaveButton!
    @IBOutlet weak var starButton: FaveButton!    
    @IBOutlet weak var composeMesgBtn: UIButton!
    @IBOutlet var callBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet var aboutmeTxtView: UITextView!
    
    @IBOutlet var aboutMeLabel: UILabel!
   
    var adsListArr = [AnyObject]()
    
    
    var delegate:CanReceiveProfileBtnData?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backBtn.layer.cornerRadius = backBtn.frame.height / 2
        self.profileView.layer.cornerRadius = profileView.frame.size.height/2
        self.profileImgView.layer.cornerRadius = profileImgView.frame.size.height/2
        profileView.layer.masksToBounds = true
        profileImgView.layer.masksToBounds = true
      
        
        likeButton.delegate = self
        starButton.delegate = self
//        blockButton.delegate = self
        
       
        
        mesgButton.layer.shadowColor = UIColor.gray.cgColor
        mesgButton.layer.shadowOffset = CGSize(width: 0, height: 1)
        mesgButton.layer.masksToBounds = false
        mesgButton.layer.shadowRadius = 1.5
        mesgButton.layer.shadowOpacity = 1
        mesgButton.layer.cornerRadius = mesgButton.frame.size.height / 2
        
                
        likeButton.layer.shadowColor = UIColor.gray.cgColor
        likeButton.layer.shadowOffset = CGSize(width: 0, height: 1)
        likeButton.layer.masksToBounds = false
        likeButton.layer.shadowRadius = 1.5
        likeButton.layer.shadowOpacity = 1
        likeButton.layer.cornerRadius = likeButton.frame.size.height / 2
        
    
        starButton.layer.shadowColor = UIColor.gray.cgColor
        starButton.layer.shadowOffset = CGSize(width: 0, height: 1)
        starButton.layer.masksToBounds = false
        starButton.layer.shadowRadius = 1.5
        starButton.layer.shadowOpacity = 1
        starButton.layer.cornerRadius = starButton.frame.size.height / 2
        
        callBtn.layer.shadowColor = UIColor.gray.cgColor
        callBtn.layer.shadowOffset = CGSize(width: 0, height: 1)
        callBtn.layer.masksToBounds = false
        callBtn.layer.shadowRadius = 1.5
        callBtn.layer.shadowOpacity = 1
        callBtn.layer.cornerRadius = callBtn.frame.size.height / 2
       

        composeMesgBtn.layer.shadowColor = UIColor.gray.cgColor
        composeMesgBtn.layer.shadowOffset = CGSize(width: 0, height: 1)
        composeMesgBtn.layer.masksToBounds = false
        composeMesgBtn.layer.shadowRadius = 1.5
        composeMesgBtn.layer.shadowOpacity = 1
        composeMesgBtn.layer.cornerRadius = composeMesgBtn.frame.size.height / 2
        
        self.adsCollectionView.register(UINib(nibName: "AdsBannerCollecctionCell", bundle: nil), forCellWithReuseIdentifier: "AdsBannerCollecctionCell")
        
        
        
        self.adsCollectionView.delegate = self
        self.adsCollectionView.dataSource = self
       
       
        if self.adsListArr.count > 0 {
            self.setTimer()
          //  self.adsHeightConstant.constant = 120
        }else{
          //  self.adsHeightConstant.constant = 0

        }
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTimer() {
        let _ = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(AboutMeTableViewCell.autoScroll), userInfo: nil, repeats: true)
    }
    var x = 0
    @objc func autoScroll() {
        if self.x < self.adsListArr.count {
            let indexPath = IndexPath(item: x, section: 0)
            self.adsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.x = self.x + 1
        }else{
            self.x = 0
            self.adsCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.adsListArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: adsCollectionView.frame.size.width, height: adsCollectionView.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = adsCollectionView.dequeueReusableCell(withReuseIdentifier: "AdsBannerCollecctionCell", for: indexPath) as! AdsBannerCollecctionCell
        let data = self.adsListArr[indexPath.row] as! [String:AnyObject]
        let type = data["add_type"] as! String
        if type == "Custom" {
            cell.adsWebView.isHidden = true
            cell.adsImageView.isHidden = false
            let url = "\(adsUrl)\(data["add_image"] as! String)"
            cell.adsImageView.af_setImage(withURL: URL(string:url)!)
        }else{
            cell.adsWebView.isHidden = false
            cell.adsImageView.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let data = self.adsListArr[indexPath.row] as! [String:AnyObject]
        let url = data["add_link"]
        if let url = URL(string: url as! String) {
            UIApplication.shared.open(url)
        }
    }
    
    
    
}
