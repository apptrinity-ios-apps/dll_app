//
//  MembershipCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 6/26/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class MembershipCell: UITableViewCell {

    @IBOutlet weak var planNameLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var monthBtn: UIButton!
    @IBOutlet weak var monthImg: UIImageView!
    @IBOutlet weak var yearBtn: UIButton!
    @IBOutlet weak var yearImg: UIImageView!
    @IBOutlet weak var cashBtn: UIButton!
    @IBOutlet weak var cashImg: UIImageView!
    @IBOutlet weak var onlineBtn: UIButton!
    @IBOutlet weak var onlineImg: UIImageView!
    
    
    
    
    @IBOutlet var monthview: UIView!
    
    @IBOutlet var yearamountLbl: UILabel!
    @IBOutlet var monthamountLbl: UILabel!
    
    
    
    @IBOutlet var backView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
