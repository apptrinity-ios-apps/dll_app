//
//  LocationInfoTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/24/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class LocationInfoTableViewCell: UITableViewCell {

    @IBOutlet weak internal var cityLabel: UILabel!
    @IBOutlet weak internal var stateLabel: UILabel!
    @IBOutlet weak internal var zipCodeLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
