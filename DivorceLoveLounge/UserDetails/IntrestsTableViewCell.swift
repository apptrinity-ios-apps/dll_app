//
//  IntrestsTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 4/25/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class IntrestsTableViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var intrestArray = ["new York","Swimming","Seafood","Yoga","Foot ball","Dance","Action","Fishing","Bycycling","Pizza","Guita"]

    @IBOutlet weak var intrestCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        intrestCollectionView.delegate = self
        intrestCollectionView.dataSource = self
        self.intrestCollectionView.register(UINib(nibName: "IntrestsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "IntrestsCollectionViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return intrestArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntrestsCollectionViewCell", for: indexPath)as! IntrestsCollectionViewCell
        cell.intrstLabel.layer.cornerRadius = 5
        cell.intrstLabel.layer.borderWidth = 0.3
        cell.intrstLabel.layer.borderColor = UIColor.lightGray.cgColor
        cell.intrstLabel.text = intrestArray[indexPath.row]
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let screenWidth = UIScreen.main.bounds.width
//        let scaleFactor = (screenWidth / 3)
        
//        return CGSize(width: scaleFactor, height: 43)
        return CGSize(width: 90, height: 38)
    }
  
}
