//
//  InfoHeaderTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/24/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class InfoHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var headerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
