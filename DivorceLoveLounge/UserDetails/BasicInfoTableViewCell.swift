//
//  BasicInfoTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 4/25/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class BasicInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var occupationLabel: UILabel!
    @IBOutlet weak var educationLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var religionLabel: UILabel!
    @IBOutlet weak var smokingLabel: UILabel!
    @IBOutlet weak var drinkingLabel: UILabel!
    @IBOutlet weak var kidsLabel: UILabel!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        
        ageLabel.intrinsicContentSize.width
        occupationLabel.intrinsicContentSize.width
        educationLabel.intrinsicContentSize.width
        heightLabel.intrinsicContentSize.width
        religionLabel.intrinsicContentSize.width
        smokingLabel.intrinsicContentSize.width
        drinkingLabel.intrinsicContentSize.width
        kidsLabel.intrinsicContentSize.width
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
