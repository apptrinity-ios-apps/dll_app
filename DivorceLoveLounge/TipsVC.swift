//
//  TipsVC.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 18/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class TipsVC: UIViewController {

    var urlService = URLservices.sharedInstance
    
    @IBOutlet var tipsImgView: UIImageView!
    @IBOutlet var goBtn: UIButton!
    
    @IBAction func goBtnAction(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "checkTips")
        tipsImgView.isHidden = true
        goBtn.isHidden = true
        let imageUpload = UserDefaults.standard.object(forKey: "imageUpload")
        if(imageUpload is NSNull || imageUpload == nil){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "EditProfilePicVC") as! EditProfilePicVC
            vc.fromScreen = "AppDelegate"
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TopTabBarVC") as! TopTabBarVC
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        goBtn.layer.cornerRadius = goBtn.frame.size.height/2
        
        // Do any additional setup after loading the view.
    }

}
