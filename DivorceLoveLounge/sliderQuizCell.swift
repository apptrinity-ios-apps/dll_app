//
//  sliderQuizCell.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 28/05/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class sliderQuizCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var maxLabel: UILabel!
    
    
    @IBOutlet weak var slider: CustomSlider!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //slider.thumbTintColor = UIColor.blue
        //slider.transform = CGAffineTransform(scaleX: 0.65, y: 0.65)
        slider.value = 0.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
