//
//  HomeVC.swift
//  Facebook Demo
//
//  Created by INDOBYTES on 07/06/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit
import MMBannerLayout

import XMPPFramework
import MBProgressHUD

import WebKit
class HomeVC: UIViewController,CanReceiveButtonaDATA,UIPickerViewDelegate,UIPickerViewDataSource,UIScrollViewDelegate {
    
    @IBOutlet var adscancelbtn: UIButton!
    
     var adsListArr = [AnyObject]()
    weak var xmppController: XMPPController!

    var scrollHint = Int()
    let countryPicker = UIPickerView()
    let countryArray = ["Alaska",
                  "Alabama",
                  "Arkansas",
                  "American Samoa",
                  "Arizona",
                  "California",
                  "Colorado",
                  "Connecticut",
                  "District of Columbia",
                  "Delaware",
                  "Florida",
                  "Georgia",
                  "Guam",
                  "Hawaii",
                  "Iowa",
                  "Idaho",
                  "Illinois",
                  "Indiana",
                  "Kansas",
                  "Kentucky",
                  "Louisiana",
                  "Massachusetts",
                  "Maryland",
                  "Maine",
                  "Michigan",
                  "Minnesota",
                  "Missouri",
                  "Mississippi",
                  "Montana",
                  "North Carolina",
                  " North Dakota",
                  "Nebraska",
                  "New Hampshire",
                  "New Jersey",
                  "New Mexico",
                  "Nevada",
                  "New York",
                  "Ohio",
                  "Oklahoma",
                  "Oregon",
                  "Pennsylvania",
                  "Puerto Rico",
                  "Rhode Island",
                  "South Carolina",
                  "South Dakota",
                  "Tennessee",
                  "Texas",
                  "Utah",
                  "Virginia",
                  "Virgin Islands",
                  "Vermont",
                  "Washington",
                  "Wisconsin",
                  "West Virginia",
                  "Wyoming"]
    
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet var alphaView: UIView!
    @IBOutlet var filterView: UIView!
    @IBOutlet var yesOrNoView: UIView!
    @IBOutlet var noBtn: UIButton!
    @IBOutlet var yesBtn: UIButton!
    @IBOutlet var minAgeLbl: UILabel!
    @IBOutlet var maxAgeLbl: UILabel!
    @IBOutlet var minHeightLbl: UILabel!
    @IBOutlet var maxHeightLbl: UILabel!
    @IBOutlet weak var countryField: UITextField!
    @IBOutlet var doneBtn: UIButton!
    @IBOutlet var ageSliderView: RangeSlider!
    @IBOutlet var heightSliderView: RangeSlider!
    
    @IBOutlet var myProfileLbl: UILabel!
    @IBOutlet var myProfileBtn: UIButton!
    
    @IBOutlet var percentageLbl: UILabel!
    @IBOutlet var myProfileView: UIView!
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var alertTextView: UITextView!
    @IBOutlet weak var alertCancelBtn: UIButton!
    @IBOutlet weak var alertSubmitBtn: UIButton!
    
    
    @IBOutlet var adsBackView: UIView!
    
    @IBOutlet var adsPopupView: UIView!
    
    @IBOutlet var adsCollectionview: UICollectionView!
    
    
    var photoApprove = Bool()
    var selectedItem = 0
    var currentItem = 0
    var favourValue = Bool()
    var sendIntrstValue = Bool()
    var thisWidth:CGFloat = 0
    
    var infoTag = Int()
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    var matchesData = Array<AnyObject>()
    var buttonId = Int()
    
    var myPrefernce = ""
    var minage = ""
    var maxage = ""
    var minHeight = ""
    var maxHeight = ""
    var countryName = ""
    
    var activityIndicator = UIActivityIndicatorView()
    
    @IBAction func adsCloseAction(_ sender: UIButton) {
        
         self.adsBackView.isHidden = true
         self.adscancelbtn.isHidden = true
        self.alphaView.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
       scrollHint = 0
        
        alertView.layer.cornerRadius = 12
        alertView.isHidden = true
        
        
        self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        self.activityIndicator.center = self.view.center
        self.activityIndicator.hidesWhenStopped = true
        view.addSubview(self.activityIndicator)
        
        
        alphaView.isHidden = true
        filterView.isHidden = true
        filterView.layer.cornerRadius = 8
        doneBtn.layer.cornerRadius = (doneBtn.frame.size.height)/2
        doneBtn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        doneBtn.layer.shadowOpacity = 1.0
        doneBtn.layer.shadowRadius = 3
        doneBtn.layer.masksToBounds = false
        doneBtn.layer.shadowColor = UIColor.gray.cgColor
        yesOrNoView.layer.cornerRadius = (yesOrNoView.frame.size.height)/2
        noBtn.layer.cornerRadius = 20
        yesBtn.layer.cornerRadius = 20
        noBtn.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        yesBtn.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        yesBtn.backgroundColor = lightBrown
        yesBtn.setTitleColor(UIColor.white, for: .normal)
        noBtn.backgroundColor = lightgray
        noBtn.setTitleColor(textClrgray, for: .normal)

        noBtn.backgroundColor = lightBrown
        noBtn.setTitleColor(UIColor.white, for: .normal)
        yesBtn.backgroundColor = lightgray
        yesBtn.setTitleColor(textClrgray, for: .normal)
        
        myPrefernce = "No"

        countryPicker.backgroundColor = UIColor.lightGray
        countryPicker.dataSource = self
        countryPicker.delegate = self
//        countryField.layer.cornerRadius = 10
//        countryField.layer.borderWidth = 1
//        countryField.layer.borderColor = ligtGray.cgColor
        countryField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: countryField.frame.height))
        countryField.rightViewMode = .always
        
        // Picker view Done Cancel Actions
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(SecondRegistrationVC.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(SecondRegistrationVC.cancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        countryField
            .inputView = countryPicker
        countryField.inputAccessoryView = toolBar
        
        // Do any additional setup after loading the view.
        infoTag = -1
        favourValue = false
        sendIntrstValue = false
         self.collection.register(UINib(nibName: "DiscoverCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DiscoverCollectionViewCell")
        (collection.collectionViewLayout as? MMBannerLayout)?.autoPlayStatus = .none
        
//        if #available(iOS 11.0, *) {
            self.view.layoutIfNeeded()
            collection.showsHorizontalScrollIndicator = false
            if let layout = collection.collectionViewLayout as? MMBannerLayout {
                layout.itemSpace = 10
                layout.itemSize = self.collection.frame.insetBy(dx: 30, dy: 60).size
                layout.minimuAlpha = 0.8
               // layout.angle = 15
            }

        
        self.adsCollectionview.register(UINib(nibName: "AdsBannerCollecctionCell", bundle: nil), forCellWithReuseIdentifier: "AdsBannerCollecctionCell")
        
      
        
        
        
        
        
        
        
        
    }
    
    @IBAction func myProfileBtnAction(_ sender: Any) {
        
        let checkProfile1 = UserDefaults.standard.object(forKey: "checkProfile1")
        let checkProfile2 = UserDefaults.standard.object(forKey: "checkProfile2")
        let checkProfile3 = UserDefaults.standard.object(forKey: "checkProfile3")
        let checkProfile4 = UserDefaults.standard.object(forKey: "checkProfile4")

        if(checkProfile2 is NSNull || checkProfile2 == nil){
            
            let ProfileSetUpVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileSetUpVC") as! ProfileSetUpVC
            self.navigationController?.pushViewController(ProfileSetUpVC, animated: true)
            
        }else if(checkProfile3 is NSNull || checkProfile3 == nil){
            
            let MatchPreferenceVC = self.storyboard?.instantiateViewController(withIdentifier: "MatchPreferenceVC") as! MatchPreferenceVC
            self.navigationController?.pushViewController(MatchPreferenceVC, animated: true)
            
        }else if(checkProfile4 is NSNull || checkProfile4 == nil){
            
            let CompitibilityQuizProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "CompitibilityQuizProfileVC") as! CompitibilityQuizProfileVC
            self.navigationController?.pushViewController(CompitibilityQuizProfileVC, animated: true)
            
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        let usermail =  self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginUserName") as? String)
        let userpass =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "LoginPassword") as? String)
      //  logIn(fromScreen: "", userEmail: usermail as! String, userPassword: userpass as! String)
         self.matchesAPI(boolVal: "0")
        self.myProfileView.layer.cornerRadius = self.myProfileView.frame.size.width / 2
        
        let circlePath = UIBezierPath(arcCenter: CGPoint (x: self.myProfileView.frame.size.width / 2, y: self.myProfileView.frame.size.height / 2),
                                      
                                      radius: self.myProfileView.frame.size.width / 2,
                                      startAngle: CGFloat(-0.5 * .pi),
                                      endAngle: CGFloat(1.5 * .pi),
                                      clockwise: true)
        
        // circle shape
        
        let circleShape = CAShapeLayer()
        circleShape.path = circlePath.cgPath
        circleShape.strokeColor = profilebackColor.cgColor
        circleShape.lineWidth = 5
        
        //set start and end values
        circleShape.strokeStart = 0.0
        // Naveen
        
        let checkProfile1 = UserDefaults.standard.object(forKey: "checkProfile1")
        let checkProfile2 = UserDefaults.standard.object(forKey: "checkProfile2")
        let checkProfile3 = UserDefaults.standard.object(forKey: "checkProfile3")
        let checkProfile4 = UserDefaults.standard.object(forKey: "checkProfile4")
        
        if checkProfile1 is NSNull || checkProfile1 == nil  {
            
            myProfileView.isHidden = false
            myProfileBtn.isHidden = false
            myProfileLbl.isHidden = false
        }else if(checkProfile2 is NSNull || checkProfile2 == nil){
            
            myProfileView.isHidden = false
            myProfileBtn.isHidden = false
            myProfileLbl.isHidden = false
            
            let amountText = NSMutableAttributedString.init(string: "25%")
            amountText.setAttributes([NSAttributedStringKey.font: UIFont.systemFont(ofSize: 10),
                                      NSAttributedStringKey.foregroundColor:percentageLbl.textColor ],
                                     range: NSMakeRange(2, 1))
            
            percentageLbl.attributedText = amountText
            circleShape.strokeEnd = 0.25
            
        }else if(checkProfile3 is NSNull || checkProfile3 == nil){
            
            myProfileView.isHidden = false
            myProfileBtn.isHidden = false
            myProfileLbl.isHidden = false
            
            let amountText = NSMutableAttributedString.init(string: "50%")
            amountText.setAttributes([NSAttributedStringKey.font: UIFont.systemFont(ofSize: 10),
                                      NSAttributedStringKey.foregroundColor:percentageLbl.textColor ],
                                     range: NSMakeRange(2, 1))
            
            percentageLbl.attributedText = amountText
            circleShape.strokeEnd = 0.50
            
        }else if(checkProfile4 is NSNull || checkProfile4 == nil){
            
            myProfileView.isHidden = false
            myProfileBtn.isHidden = false
            myProfileLbl.isHidden = false
            
            let amountText = NSMutableAttributedString.init(string: "75%")
            amountText.setAttributes([NSAttributedStringKey.font: UIFont.systemFont(ofSize: 10),
                                      
                                      NSAttributedStringKey.foregroundColor:percentageLbl.textColor ],
                                     
                                     range: NSMakeRange(2, 1))
            
            percentageLbl.attributedText = amountText
            
            circleShape.strokeEnd = 0.75
            
        }
        else{
            
            myProfileLbl.isHidden = true
            myProfileBtn.isHidden = true
            myProfileView.isHidden = true
            
        }
        self.myProfileView.layer.addSublayer(circleShape)
        self.myProfileView.addSubview(percentageLbl)
        
        
        alphaView.isHidden = true
        filterView.isHidden = true
        alertView.isHidden = true
         self.getMyPalnAPi()
        let userId = themes.getUserId()
        checkStatusApi(user_id: userId)

        getprofileAPi()
        
        
      //  UIView.animate(withDuration: 0.25) {
          //  self.adsPopupView.isHidden = false
           // self.adsPopupView.frame = CGRect(x:self.view.frame.minX + 20 , y: self.view.frame.minY + 20, width:self.view.frame.size.width - 50, height:self.view.frame.size.height - 150)
            //self.adsPopupView.center = self.view.center
//self.view.addSubview(self.adsPopupView)
       // }
        
      //  adsListServiceApi(ScreenType: "Home")
        
       
        self.adsBackView.layer.shadowColor = UIColor.black.cgColor
        self.adsBackView.layer.shadowOpacity = 1
        self.adsBackView.layer.shadowOffset = .zero
        self.adsBackView.layer.shadowRadius = 10
        
        self.adscancelbtn.layer.cornerRadius = self.adscancelbtn.frame.size.height / 2
        self.adscancelbtn.backgroundColor = UIColor.white
        self.adscancelbtn.layer.shadowColor = UIColor.black.cgColor
        self.adscancelbtn.layer.shadowOpacity = 1
        self.adscancelbtn.layer.shadowOffset = .zero
        self.adscancelbtn.layer.shadowRadius = 5
        
        self.adsBackView.isHidden = true
        self.adscancelbtn.isHidden = true
        
      // self.view.bringSubview(toFront: self.adscancelbtn)
       ChatStatusAPI()
      
    }
    
    func getprofileAPi(){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:getProfilePics, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    let photo = response["photos"] as! [String:AnyObject]
                    let photo_approve = self.themes.checkNullValue(photo["photo1_approve"]) as! String
                    if(photo_approve == "UNAPPROVED"){
                        self.themes.savePhotoApproveStatus(false)
                    }else{
                        self.themes.savePhotoApproveStatus(true)
                    }
                    
                    self.collection.reloadData()
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    
    @IBAction func filterBtnAction(_ sender: Any) {
        
        
        alphaView.isHidden = false
        filterView.isHidden = false
   
    }
    
    
    @IBAction func closeAction(_ sender: Any) {
        alphaView.isHidden = true
        filterView.isHidden = true
       
        myPrefernce = ""
        countryName = ""
        maxHeight = ""
        minHeight = ""
        minage = ""
        maxage = ""
        
    //  self.matchesAPI(boolVal: "0")
        
        
    }
    
    
    @IBAction func noBtnAction(_ sender: Any) {
        
        noBtn.backgroundColor = lightBrown
        noBtn.setTitleColor(UIColor.white, for: .normal)
        yesBtn.backgroundColor = lightgray
        yesBtn.setTitleColor(textClrgray, for: .normal)
        
        myPrefernce = "No"
       
    }
    
    @IBAction func yesBtnAction(_ sender: Any) {
        
        yesBtn.backgroundColor = lightBrown
        yesBtn.setTitleColor(UIColor.white, for: .normal)
        noBtn.backgroundColor = lightgray
        noBtn.setTitleColor(textClrgray, for: .normal)
        
         myPrefernce = "Yes"
        
    }
    
    
    
    @IBAction func ageSliderAction(_ sender: Any) {
        
       let minag = Int(ageSliderView.lowerValue)
       let maxag = Int(ageSliderView.upperValue)
        
        minage = "\(minag)"
        maxage = "\(maxag)"
        minAgeLbl.text = "\(minag) -"
        maxAgeLbl.text = maxage
        
     
        
    }
    
    @IBAction func heoghtSliderAction(_ sender: Any) {
        
      
        // MIN HEIGHT
        let twoDecimalPlaces = String(format: "%.1f",heightSliderView.lowerValue)
        let number:Float = Float(twoDecimalPlaces)!
        let numberString = String(number)
        let numberComponent = numberString.components(separatedBy :".")
        let integerNumber = Int(numberComponent [0])
        let fractionalNumber = Int(numberComponent [1])
        
        
//        print(integerNumber!)
//        print(fractionalNumber!)
//        
        let val = integerNumber!
        let val1 = fractionalNumber!
        let feetValue = Double(val)
        let inchValue = Double(val1)
        let cmValue = (30.48 * feetValue) + (2.54 * inchValue)
        let cmVal = Int(cmValue)
        
        minHeight = String(cmVal)
        minHeightLbl.text = twoDecimalPlaces + " -"
        
       // MAX HEIGHT
        
        let twoDecimalPlaces1 = String(format: "%.1f",heightSliderView.upperValue)
        let number1:Float = Float(twoDecimalPlaces1)!
        let numberString1 = String(number1)
        let numberComponent1 = numberString1.components(separatedBy :".")
        let integerNumber1 = Int(numberComponent1 [0])
        let fractionalNumber1 = Int(numberComponent1 [1])
        
       
        let val_1 = integerNumber1!
        let val11 = fractionalNumber1!
        let feetValue1 = Double(val_1)
        let inchValue1 = Double(val11)
        let cmValue1 = (30.48 * feetValue1) + (2.54 * inchValue1)
        let cmVal1 = Int(cmValue1)
        maxHeight = String(cmVal1)
        
        maxHeightLbl.text = twoDecimalPlaces1
        
    }
    
    
    
    @IBAction func fliterDoneAction(_ sender: Any) {
        
        
      self.alphaView.isHidden = true
      self.filterView.isHidden = true
       
          self.matchesAPI(boolVal: "1")
        
        myPrefernce = ""
        countryName = ""
        maxHeight = ""
        minHeight = ""
        minage = ""
        maxage = ""
        
      
    }

    @objc func donePicker() {
        
        countryField.resignFirstResponder()
    }
    
    @objc func cancelPicker() {
        
        countryField.text = ""
        countryField.resignFirstResponder()
   
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countryArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countryArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        countryField.text = countryArray[row]
        
        countryName = countryField.text!
   
    }
    
    func CanReceiveButtonData(tag: Int, buttonName: String, selected: Bool) {
        
        let obj = self.matchesData[tag]
        let id = themes.checkNullValue(obj["id"] ?? "") as! String
        let userID = self.themes.getUserId()
        if buttonName == "sendreqbtn" && selected == true{
            sendInterestApi(user_id: userID, Id: id)
        }else if buttonName == "favouritebtn"{
          // favouriteApi(user_id: userID, Id: id)
        }else if buttonName == "infobtn" && selected == true{
             let cell =   collection.cellForItem(at:IndexPath(row: tag, section: 0)) as? DiscoverCollectionViewCell
             cell?.infoView.isHidden = false
        }else if buttonName == "infobtn" && selected == false{
            let cell =   collection.cellForItem(at:IndexPath(row: tag, section: 0)) as? DiscoverCollectionViewCell
            cell?.infoView.isHidden = true
        }
      
        
        self.collection.reloadData()
        
    }
    
    func checkStatusApi(user_id: String){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            
            let parameters = [ "user_id":user_id] as [String : Any]
            
            print("send interest parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:checkStatus, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    
                } else if (success == "2"){

                    let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopViewController") as! PopViewController
                    
                    popOverVC.fromScreen = "loginstatus"
                    //                    popOverVC.ImageStr = ""
                    popOverVC.headingMesg = "Oops ☹️"
                    popOverVC.subMesg = "The account you are Trying to view has been suspended. \n Please Contact Customer Support \n support@divorcelovelounge.com"
                    
                    self.addChildViewController(popOverVC)
                    popOverVC.view.center = self.view.frame.center
                    self.view.addSubview(popOverVC.view)
                    popOverVC.didMove(toParentViewController: self)
       
//         let alertController = UIAlertController(title: "Oops ☹️", message: "The account you are Trying to view has been suspended. \n Please Contact Customer Support \n support@divorcelovelounge.com", preferredStyle: .alert)
//        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
//         print("Ok button tapped");
//            self.themes.ClearUserInfo()
//
//        let ViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//        self.navigationController?.pushViewController(ViewController, animated: true)
//            }
//             alertController.addAction(OKAction)
//         let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
//
//         }
//             alertController.addAction(cancelAction)
//            self.present(alertController, animated: true, completion:nil)
//
                    
        }
        
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
   
  
    func matchesAPI(boolVal:String){

        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
             let urserId = themes.getUserId()
            var dict = [String : AnyObject]()

            if boolVal == "1"{
 
            dict.updateValue(urserId as AnyObject, forKey: "user_id")
            dict.updateValue(myPrefernce as AnyObject, forKey: "mypreference")
            dict.updateValue(maxHeight as AnyObject, forKey: "height_max")
            dict.updateValue(minHeight as AnyObject, forKey: "height_min")
            dict.updateValue(countryName as AnyObject, forKey: "city")
            dict.updateValue(minage as AnyObject, forKey: "min_age")
            dict.updateValue(maxage as AnyObject, forKey: "max_age")
                
            dict.updateValue(self.themes.getCurrentCountry() as AnyObject, forKey: "current_country")
            dict.updateValue( self.themes.getCurrentstate() as AnyObject, forKey: "current_state")
            dict.updateValue(self.themes.getCurrentcity() as AnyObject, forKey: "current_city")
            dict.updateValue("Home" as AnyObject, forKey: "page")
                
            }else{
                dict.updateValue(urserId as AnyObject, forKey: "user_id")
                 dict.updateValue(self.themes.getCurrentCountry() as AnyObject, forKey: "current_country")
                           dict.updateValue( self.themes.getCurrentstate() as AnyObject, forKey: "current_state")
                           dict.updateValue(self.themes.getCurrentcity() as AnyObject, forKey: "current_city")
                           dict.updateValue("Home" as AnyObject, forKey: "page")
            }
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:matches, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    let data = response["data"]as! [String : AnyObject]
                    
                    self.matchesData = data["match_data"] as! [AnyObject]
                    
                    
                    if self.matchesData.count <= 0 {
                       self.themes.showAlert(title: "Oops ☹️", message:"No Profiles Found", sender: self)
                    }
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.allprofiles = self.matchesData
                    
                       self.collection.reloadData()
                    
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func favouriteApi(user_id: String, Id : String){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            
            let parameters = [ "user_id":user_id, "id":Id,
                ] as [String : Any]
            
            print("favourite parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:favourite, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    self.matchesAPI(boolVal: "0")
                    
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func sendInterestApi(user_id: String, Id : String){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            
            let parameters = [ "user_id":user_id, "id":Id,
                ] as [String : Any]
            
            print("send interest parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:sendInterest, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                        self.matchesAPI(boolVal: "0")
                    
                } else {
                    let result = response["result"] as! String
                   // self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    
                  // Naveen
                    
//                    let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopViewController") as! PopViewController
//
//                    popOverVC.fromScreen = ""
//                    //                    popOverVC.ImageStr = ""
//                    popOverVC.headingMesg = "Alert"
//                    popOverVC.subMesg = "You need an active membership plan to view the profile. Proceed to membership plans?"
//
//                    self.addChildViewController(popOverVC)
//                    popOverVC.view.center = self.view.frame.center
//                    self.view.addSubview(popOverVC.view)
//                    popOverVC.didMove(toParentViewController: self)
//
                   self.collection.reloadData()
                    
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
        
    @objc func btnHideAction(button: UIButton) {
       
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopViewController") as! PopViewController
        
                            popOverVC.fromScreen = ""
                            //                    popOverVC.ImageStr = ""
                            popOverVC.headingMesg = "Alert"
                            popOverVC.subMesg = "Your account is under progress !"
        
                            self.addChildViewController(popOverVC)
                            popOverVC.view.center = self.view.frame.center
                            self.view.addSubview(popOverVC.view)
                            popOverVC.didMove(toParentViewController: self)
        
        
    }
    
    
    @objc func infoBtnClick(button: UIButton) {
        
        scrollHint = button.tag
        let cell =   collection.cellForItem(at:IndexPath(row: button.tag, section: 0)) as? DiscoverCollectionViewCell

        if cell?.infoView.isHidden == true  {
            cell?.infoView.isHidden = false
            cell?.infoBtn.setImage(UIImage(named:"info_active"), for: .normal)
            
        }else{
            
            cell?.infoView.isHidden = true
            cell?.infoBtn.setImage(UIImage(named:"info_default"), for: .normal)
        }

    }
    
    @IBAction func alertCancelAction(_ sender: Any) {
        alertView.isHidden = true
        alphaView.isHidden = true
        self.view.endEditing(true)
    }
    
    @IBAction func alertSubmitAction(_ button: UIButton) {
        alertView.isHidden = true
        alphaView.isHidden = true
        self.view.endEditing(true)
        
        let buton = buttonId
        let userdata = self.matchesData[buton] as! [String:AnyObject]
        let id = self.themes.checkNullValue(userdata["id"] as AnyObject?) as! String
        let myID = self.themes.getUserId()

        self.blockUnblockApi(user_id: myID, Id: id, type: "Report")
        self.collection.reloadData()
 //   self.newsBlockServiceCall(id: id)
        
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
         let cell =   collection.cellForItem(at:IndexPath(row: scrollHint, section: 0)) as? DiscoverCollectionViewCell
        cell?.infoView.isHidden = true
        
    }
    
    
    @objc func sendRequestBtnClick(button: UIButton) {
        
        infoTag = button.tag
        if sendIntrstValue ==  true {
            sendIntrstValue = false
        } else{
            sendIntrstValue = true
            let userId = self.themes.getUserId()
            let userData =  self.matchesData[button.tag]
            let id = themes.checkNullValue(userData["id"] ?? "") as! String
            print(id)
            sendInterestApi(user_id: userId, Id: id)
        }
        self.collection.reloadData()
    }
    @objc func favouriteBtnAction(button: UIButton) {
        infoTag = button.tag
        if favourValue ==  true {
            favourValue = false
            let userId = self.themes.getUserId()
            let userData =  self.matchesData[button.tag]
            let id = themes.checkNullValue(userData["id"] ?? "") as! String
            favouriteApi(user_id: userId, Id: id)
        } else{
            favourValue = true
            let userId = self.themes.getUserId()
            let userData =  self.matchesData[button.tag]
            let id = themes.checkNullValue(userData["id"] ?? "") as! String
            favouriteApi(user_id: userId, Id: id)
        }
        
        //self.collection.reloadData()
    }

    
    func getMyPalnAPi(){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:myPlan, params: dict as Dictionary<String, Any>) { response in
                print(response)
                let success = response["status"] as! String
                if(success == "1"){
                   
                   let obj = response["plandetails"] as! [String:AnyObject]
                    
                    let chatting = self.themes.checkNullValue(obj["chat"]) as! String
                    let favouritedMe = self.themes.checkNullValue(obj["favoritedme"]) as! String
                    let viewedMe = self.themes.checkNullValue(obj["viewedme"]) as! String
                    let planStatus = self.themes.checkNullValue(obj["status"]) as! String
                    
                    let server_date = self.themes.checkNullValue(obj["server_date"]) as! String
                    let plan_expiry = self.themes.checkNullValue(obj["plan_expiry"]) as! String
                    
                    self.themes.chatting(chatting)
                    self.themes.favouritedMe(favouritedMe)
                    self.themes.viewedMe(viewedMe)
                    self.themes.planStatus(planStatus)
                    
                    let dateFormater = DateFormatter()
                    dateFormater.dateFormat = "YYYY-MM-dd"
                    let dat1 = dateFormater.date(from: server_date)
                    var currentdate = Date()
                    currentdate = dat1!
                    
                    let dateFormater2 = DateFormatter()
                    dateFormater2.dateFormat = "YYYY-MM-dd"
                    let dat = dateFormater2.date(from: plan_expiry)
                    var expirydate = Date()
                    expirydate = dat!
                    
                    if currentdate <= expirydate{
                        self.themes.planStatus(planStatus)
                    }else{
                      self.themes.planStatus("2")
                    }
                    
                    
                } else {
//                    let result = response["result"] as! String
//                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    func blockUnblockApi(user_id: String, Id : String,type:String){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            
            let parameters = [ "user_id":user_id, "id":Id,
                ] as [String : Any]
            
            print("favourite parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:blockUnblock, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    if(type == "Report"){
                        
                    let alertController = UIAlertController(title: "Thanks for reporting this post", message: "We use spam reports as a signal to understand problem.Our admin team will look in to the this post.", preferredStyle: .alert)
                        
                    let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                        
                        }
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion:nil)
     
                    }
                    
                     self.matchesAPI(boolVal:"0")
                     self.collection.reloadData()
                    
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    func adsListServiceApi(ScreenType:String){
        self.urlService.adslistServiceCall(ScreenType: ScreenType) { response in
            let success = response["status"] as! String
            if(success == "1"){
                
                let data = response["data"] as! [AnyObject]
                self.adsListArr = data
                self.adsCollectionview.delegate = self
                self.adsCollectionview.dataSource = self
                self.adsCollectionview.reloadData()
                
                if self.adsListArr.count > 0 {
                    
                    self.adsBackView.isHidden = false
                    self.adscancelbtn.isHidden = false
                     self.alphaView.isHidden = false
                    
                    self.view.bringSubview(toFront: self.adsBackView)
                    self.view.bringSubview(toFront: self.adscancelbtn)
                    self.setTimer()
                }else{
                    self.adsBackView.isHidden = true
                    self.adscancelbtn.isHidden = true
                    self.alphaView.isHidden = true
                    
                }
                
            }else{
                
            }
        }
        
    }
    func setTimer() {
        let _ = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(HomeVC.autoScroll), userInfo: nil, repeats: true)
    }
    var x = 1
    @objc func autoScroll() {
        if self.x < self.adsListArr.count {
            let indexPath = IndexPath(item: x, section: 0)
            self.adsCollectionview.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.x = self.x + 1
        }else{
            self.x = 0
            self.adsCollectionview.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    
    
    func ChatStatusAPI () {
        self.urlService.serviceCallGetMethod(url: chatStatus) { response in
            let success = response["status"] as! String
                       if(success == "1"){
                       let chat_status = response["chat_status"] as! String
                        let chatMessage = response["message"] as! String
                     //  let chat_status = "1"
                        if chat_status == "0"{
                            self.themes.saveChatstatus(chat_status)
                            self.themes.saveChatstatusMessage(chatMessage)
                            
                        }else if chat_status == "1"{
                          self.themes.saveChatstatus(chat_status)
                            
                            let xmppPassword = self.themes.getxmppPassword() as! String
                            let XmppUserName = self.themes.getXmppUserName() as! String
                           // self.themes.saveXmppUserName(XmppUserName)
                           // self.themes.savexmppPassword(xmppPassword)
                         
                                            // for xmpp connecting
                            var servername = UITextField()
                            servername.text = "divorcelovelounge.com"
                            var password = UITextField()
                            password.text = xmppPassword as? String
                            var username = UITextField()
                            username.text = "\(XmppUserName)@divorcelovelounge.com"
                            if let serverText = servername.text, (servername.text?.characters.count)! > 0 {
                            let auth = AuthenticationModel(jidString: username.text!, serverName: servername.text!, password: password.text!)
                                auth.save()
                            } else {
                                let auth = AuthenticationModel(jidString: username.text!, password: password.text!)
                                     auth.save()
                            }
                            self.configureAndStartStream()
                        }
                       }else{
                       }
        }
    }
    func configureAndStartStream() {
        self.xmppController = XMPPController.sharedInstance
        self.xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
        // TODO: revert to UIActivityIndicatorView.
     //   let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
     //   hud?.labelText = "Please wait"
        _ = self.xmppController.connect()
        
    }
    
    
    
    

}
extension HomeVC: BannerLayoutDelegate {
    
    func collectionView(_ collectionView: UICollectionView, focusAt indexPath: IndexPath?) {
        print("Focus At \(indexPath)")
    }
}
extension HomeVC: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
 
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.adsCollectionview {
         return self.adsListArr.count
        }else{
        return matchesData.count
        }
    }
        
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
      
            
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DiscoverCollectionViewCell", for: indexPath)as! DiscoverCollectionViewCell
         
         let obj = self.matchesData[indexPath.item]
        let viewType =  self.themes.checkNullValue(obj["view_type"] as AnyObject) as! String
        
        if viewType == "add" {
          
            cell.adsBackView.isHidden = false
                   let type = obj["add_type"] as! String
                   if type == "Custom" {
                       cell.adsImageView.isHidden = false
                       let url = "\(adsUrl)\(obj["add_image"] as! String)"
                       cell.adsImageView.af_setImage(withURL: URL(string:url)!)
                   }else{
                       cell.adsImageView.isHidden = true
                   }
            
            return cell
        }
        else{
            
            cell.adsBackView.isHidden = true
            
        
        
        
        cell.clipsToBounds = false
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 1, height: 0)
        cell.layer.shadowRadius = 3
        cell.layer.shadowOpacity = 1
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath

          cell.sendRequestBtn?.tag = indexPath.row
          cell.favouriteBtn?.tag = indexPath.row
          cell.infoBtn.tag = indexPath.row
          cell.infoBtn.setImage(UIImage(named:"info_default"), for: .normal)
        
         cell.infoBtn.addTarget(self, action: #selector(infoBtnClick(button:)), for:.touchUpInside)
         cell.btnHide.addTarget(self, action: #selector(btnHideAction(button:)), for: .touchUpInside)
          cell.delegate = self
       
        cell.userNameLbl.text = themes.checkNullValue(obj["first_name"] ?? "") as? String
//     self.activityIndicator.startAnimating()
        let imageUrl = self.themes.checkNullValue(obj["photo1"] as AnyObject?) as! String
        cell.distanceLbl.text = self.themes.checkNullValue(obj["city"] as AnyObject?) as? String
        let DOB = self.themes.checkNullValue(obj["dob"] as AnyObject?) as? String
                  if DOB != "" {
                          let age = self.themes.getAgeFromDOF(date: DOB!)
                          cell.ageLbl.text = "\(age.0)"
                      }
            
        let photoApproved = self.themes.checkNullValue(obj["photo1_approve"] as AnyObject?) as! String
        // APPROVED
        if photoApproved == "APPROVED" {
            let fullimageUrl = ImagebaseUrl + imageUrl
            cell.profileImg.sd_setImage(with: URL(string: fullimageUrl), placeholderImage: UIImage(named: "defaultPic"))
            DispatchQueue.main.async {
                cell.userImgView.sd_setShowActivityIndicatorView(true)
                cell.userImgView.sd_setIndicatorStyle(.whiteLarge)
                cell.userImgView.sd_setImage(with: URL(string: fullimageUrl), placeholderImage:UIImage(named: "defaultPic"), completed: { (image, error, cacheType, url) -> Void in
                    if ((error) != nil) {
                        cell.userImgView.image = UIImage(named: "defaultPic")
                        cell.userImgView.sd_setShowActivityIndicatorView(true)
                    } else {
                        cell.userImgView.image = image
                        cell.userImgView.sd_setShowActivityIndicatorView(false)
                        if self.themes.getplanStatus() == "1"{
                            cell.sendRequestBtn?.isEnabled = true
                           cell.blurImage.isHidden = true

                        } else if self.themes.getplanStatus() == "2"{
//                            cell.sendRequestBtn?.isEnabled = true
                            cell.blurImage.isHidden = true
                        } else {
                           cell.blurImage.isHidden = false
                        }
                       
                    }
                })
                
            }
            
        }else{

            cell.userImgView.image = UIImage(named: "defaultPic")
            cell.profileImg.image = UIImage(named: "defaultPic")
            DispatchQueue.main.async {
            cell.blurImage.isHidden = true
            }
            
   
        }

        let fav_from = self.themes.checkNullValue(obj["fav_from"] as AnyObject?) as! String
        let interest_from = self.themes.checkNullValue(obj["interest_from"] as AnyObject?) as! String
        
        let myID = self.themes.getUserId()
        
        if fav_from == myID {
            cell.favouriteBtn?.setSelected(selected: true, animated: false)
        }else{
            cell.favouriteBtn?.setSelected(selected: false, animated: false)
        }
        if interest_from == myID {
            cell.sendRequestBtn?.setSelected(selected: true, animated: false)
        }else{
            cell.sendRequestBtn?.setSelected(selected: false, animated: false)
        }
        cell.viewProfileBtn.tag = indexPath.row
        cell.messageBtn.tag = indexPath.row
        cell.blockUserBtn.tag = indexPath.row
        cell.reportUserBtn.tag = indexPath.row
        cell.favouriteBtn?.tag = indexPath.row
        
          cell.favouriteBtn!.addTarget(self, action: #selector(favouriteBtnAction(button:)), for:.touchUpInside)
          cell.viewProfileBtn.addTarget(self, action: #selector(profileBtnAction(button:)), for:.touchUpInside)
          cell.messageBtn.addTarget(self, action: #selector(ChatBtnAction(button:)), for:.touchUpInside)
          cell.blockUserBtn.addTarget(self, action: #selector(blockBtnAction(button:)), for: .touchUpInside)
        
          cell.reportUserBtn.addTarget(self, action: #selector(reportUserBtnAction(button:)), for: .touchUpInside)
        
       let PHOTOAPPROVE =  self.themes.getPhotoApproveStatus()
        
        if(PHOTOAPPROVE ){
            cell.btnHide.isHidden = true
          
        }else{
            cell.btnHide.isHidden =  false
        }
        
        return cell
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if collectionView == self.adsCollectionview {
             return CGSize(width: self.adsBackView.frame.size.width, height: adsBackView.frame.size.height)
        }
        return CGSize(width: 0.0, height: 0.0)

    }
   
    
    @objc func profileBtnAction(button: UIButton){
        
        
        let cell =   collection.cellForItem(at:IndexPath(row: button.tag, section: 0)) as? DiscoverCollectionViewCell
        cell?.infoView.isHidden = true
  
        if self.themes.getplanStatus() == "1" || self.themes.getplanStatus() == "2" {
//            let userDetailsVc = self.storyboard?.instantiateViewController(withIdentifier: "UserDetailsVC") as! UserDetailsVC
//            let obj = matchesData[button.tag]
//            userDetailsVc.fromScreen = "myfavourite"
//            userDetailsVc.userDataDict = obj as! [String : AnyObject]
//            userDetailsVc.match_id = obj["id"] as! String
//            self.navigationController?.pushViewController(userDetailsVc, animated: true)
            
            
            let userDetailsVc = self.storyboard?.instantiateViewController(withIdentifier: "UserDetailsVC") as! UserDetailsVC
            let obj = matchesData[button.tag]
            userDetailsVc.fromScreen = "myfavourite"
            userDetailsVc.userDataDict = obj as! [String : AnyObject]
            userDetailsVc.match_id = obj["id"] as! String
            userDetailsVc.imageApprove = true
            self.navigationController?.pushViewController(userDetailsVc, animated: true)
        }else{
       
            
            //Naveen
//            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopViewController") as! PopViewController
//
//            popOverVC.fromScreen = ""
//            //                    popOverVC.ImageStr = ""
//            popOverVC.headingMesg = "Alert"
//            popOverVC.subMesg = "You need an active membership plan to view the profile. Proceed to membership plans?"
//
//            self.addChildViewController(popOverVC)
//            popOverVC.view.center = self.view.frame.center
//            self.view.addSubview(popOverVC.view)
//            popOverVC.didMove(toParentViewController: self)
            
            
            //Naveen

//            let alertController = UIAlertController(title: "Alert", message: "You need an active membership plan to view the profile. Proceed to membership plans?", preferredStyle: .alert)
//            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
//                print("Ok button tapped");
//                let MemberShipViewController = self.storyboard?.instantiateViewController(withIdentifier: "MemberShipViewController") as! MemberShipViewController
//                self.navigationController?.pushViewController(MemberShipViewController, animated: true)
//            }
//            alertController.addAction(OKAction)
//            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
//
//            }
//            alertController.addAction(cancelAction)
//            self.present(alertController, animated: true, completion:nil)
        }
    }
    @objc func ChatBtnAction(button: UIButton){


        let cell =   collection.cellForItem(at:IndexPath(row: button.tag, section: 0)) as? DiscoverCollectionViewCell
        cell?.infoView.isHidden = true

        if self.themes.getplanStatus() == "1" && self.themes.getChatting() == "Yes" {

            let userdata = self.matchesData[button.tag] as! [String:AnyObject]

            let composevc = self.storyboard?.instantiateViewController(withIdentifier: "ComposeVC") as! ComposeVC
            composevc.userID = userdata["id"] as! String
            composevc.userName = userdata["first_name"] as! String
            composevc.selectedId = userdata["id"] as! String
            
            
//            chatvc.userEmail = userdata["email"] as! String
            self.navigationController?.pushViewController(composevc, animated: true)

        }else{
 // Naveen
//            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopViewController") as! PopViewController
//
//            popOverVC.fromScreen = ""
//            //                    popOverVC.ImageStr = ""
//            popOverVC.headingMesg = "Alert"
//            popOverVC.subMesg = "You need an active membership plan to view the profile. Proceed to membership plans?"
//
//            self.addChildViewController(popOverVC)
//            popOverVC.view.center = self.view.frame.center
//            self.view.addSubview(popOverVC.view)
//            popOverVC.didMove(toParentViewController: self)

            // Naveen
            
//            let alertController = UIAlertController(title: "Alert", message: "Please Upgrade Your Plan", preferredStyle: .alert)
//            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
//                print("Ok button tapped");
//                let MemberShipViewController = self.storyboard?.instantiateViewController(withIdentifier: "MemberShipViewController") as! MemberShipViewController
//                self.navigationController?.pushViewController(MemberShipViewController, animated: true)
//            }
//            alertController.addAction(OKAction)
//            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
//
//            }
//            alertController.addAction(cancelAction)
//            self.present(alertController, animated: true, completion:nil)
   
        }
 
    }
    
    
     @objc func reportUserBtnAction(button: UIButton){
        let cell =   collection.cellForItem(at:IndexPath(row: button.tag, section: 0)) as? DiscoverCollectionViewCell
        cell?.infoView.isHidden = true
        
        self.buttonId = button.tag

        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "It's Spam", style: .destructive , handler:{ (UIAlertAction)in
            print("User click Approve button")
            self.alphaView.isHidden = false
            self.alertView.isHidden = false
//
///// /            Create the alert controller
//            let alertController = UIAlertController(title: "Report User", message: nil, preferredStyle: .alert)
//            alertController.addTextField { (textField) in
//                let heightConstraint = NSLayoutConstraint(item: textField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
//                textField.addConstraint(heightConstraint)
//                textField.placeholder = "Comment"
//            }
//////             Create the actions
//            let okAction = UIKit.UIAlertAction(title: "Submit", style: UIAlertActionStyle.default) {
//                UIAlertAction in
//                NSLog("OK Pressed")
//
//                let textField = alertController.textFields![0] // Force unwrapping because we know it exists.
//                print("Text field: \(textField.text)")
//
//                let userdata = self.matchesData[button.tag] as! [String:AnyObject]
//                let id = self.themes.checkNullValue(userdata["id"] as AnyObject?) as! String
//                let myID = self.themes.getUserId()
//
//                self.blockUnblockApi(user_id: myID, Id: id, type: "Report")
//                self.collection.reloadData()
//                //   self.newsBlockServiceCall(id: id)
//
//            }
//            let cancelAction = UIKit.UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) {
//                UIAlertAction in
//                NSLog("Cancel Pressed")
//            }
//
//            // Add the actions
//            alertController.addAction(okAction)
//            alertController.addAction(cancelAction)
//
//            // Present the controller
//            self.present(alertController, animated: true, completion: nil)

            
        }))
        
        
        alert.addAction(UIAlertAction(title: "It's Inappropriate", style: .destructive , handler:{ (UIAlertAction)in
            print("User click Approve button")
            self.alphaView.isHidden = false
            self.alertView.isHidden = false
//            let alertController = UIAlertController(title: "Report User", message: nil, preferredStyle: .alert)
//
//            alertController.addTextField { (textField) in
//                let heightConstraint = NSLayoutConstraint(item: textField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
//                textField.addConstraint(heightConstraint)
//                textField.placeholder = "Comment"
//            }
//            // Create the actions
//            let okAction = UIKit.UIAlertAction(title: "Submit", style: UIAlertActionStyle.default) {
//                UIAlertAction in
//                NSLog("OK Pressed")
//
//                let textField = alertController.textFields![0] // Force unwrapping because we know it exists.
//                print("Text field: \(textField.text)")
//
//                let userdata = self.matchesData[button.tag] as! [String:AnyObject]
//                let id = self.themes.checkNullValue(userdata["id"] as AnyObject?) as! String
//                let myID = self.themes.getUserId()
//
//                self.blockUnblockApi(user_id: myID, Id: id, type: "Report")
//                self.collection.reloadData()
//                //   self.newsBlockServiceCall(id: id)
//
//            }
//            let cancelAction = UIKit.UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) {
//                UIAlertAction in
//                NSLog("Cancel Pressed")
//            }
//
//            // Add the actions
//            alertController.addAction(okAction)
//            alertController.addAction(cancelAction)
//
//            // Present the controller
//            self.present(alertController, animated: true, completion: nil)
    
        }))
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    func logIn(fromScreen:String,userEmail:String ,userPassword:String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            let devicetoken =   self.themes.checkNullValue(UserDefaults.standard.object(forKey: "kDeviceToken") )
            let parameters = [ "email":userEmail, "password":userPassword,
                               "device_token":devicetoken as! String , "device_type" : "IOS"
                
                ] as [String : Any]
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:login, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    UserDefaults.standard.set(true, forKey: "checkLogIn")
                    // UserDefaults.standard.set(nil, forKey: "checkLogIn")
                    let userInfo = response["user_info"]as! [String : AnyObject]
                   
                    let photo1_approve = self.themes.checkNullValue(userInfo["photo1_approve"]) as! String
                    
                    if(photo1_approve == "UNAPPROVED"){
                        
                        self.photoApprove = false
                        
                    }else {
                        
                        self.photoApprove = true
                        
                    }
                    
                    self.collection.reloadData()
           
                } else if success == "3" {
                    
                    self.themes.showAlert(title: "Oops ☹️", message: "The account you are Trying to view has been suspended. \n Please Contact Customer Support \n support@divorcelovelounge.com", sender: self)
                    
                }else{
                    
                    
                }
                
            }
            
        } else {
            
            self.themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
            
        }
        
    }
    @objc func blockBtnAction(button: UIButton){

        let cell =   collection.cellForItem(at:IndexPath(row: button.tag, section: 0)) as? DiscoverCollectionViewCell
        cell?.infoView.isHidden = true

        //  self.themes.showAlert(title: "Message", message: "Coming Soon!!", sender: self)
        let alert = UIAlertController(title: "Alert", message: "If You Select Not Interested/Block,The Selected Content NO more Visible to you. ", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Not Interested /Block", style: .destructive , handler:{ (UIAlertAction)in
            print("User click Approve button")
            
            let userdata = self.matchesData[button.tag] as! [String:AnyObject]
            let id = self.themes.checkNullValue(userdata["id"] as AnyObject?) as! String
            let myID = self.themes.getUserId()
            
            self.blockUnblockApi(user_id: myID, Id: id, type: "Block")
            self.collection.reloadData()
         //   self.newsBlockServiceCall(id: id)
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        self.present(alert, animated: true, completion: {
            print("completion block")
        })

    }

    public func resize(image: UIImage, toScaleSize:CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(toScaleSize, true, image.scale)
        image.draw(in: CGRect(x: 0, y: 0, width: toScaleSize.width, height: toScaleSize.height));                 let scaledImage = UIGraphicsGetImageFromCurrentImageContext();                 UIGraphicsEndImageContext()
        return scaledImage!
        
    }
    
//    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: 0, height: self.view.frame.height - 80)
//    }
//    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: 0, height: self.view.frame.height - 80)
//    }
         
   public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        let obj = self.matchesData[indexPath.item]
        let viewType =  self.themes.checkNullValue(obj["view_type"] as AnyObject) as! String
        if viewType == "add" {
                   let type = obj["add_type"] as! String
                   if type == "Custom" {
                    let url = obj["add_link"]
                          if let url = URL(string: url as! String) {
                              UIApplication.shared.open(url)
                          }
                   }else{
                    
                   }
            
        }
        else{
            
            
            
    
        
   
    if self.themes.getplanStatus() == "1" || self.themes.getplanStatus() == "2"{
        let userDetailsVc = self.storyboard?.instantiateViewController(withIdentifier: "UserDetailsVC") as! UserDetailsVC
        let obj = matchesData[indexPath.item]
        userDetailsVc.fromScreen = "myfavourite"
        userDetailsVc.userDataDict = obj as! [String : AnyObject]
        userDetailsVc.match_id = obj["id"] as! String
        userDetailsVc.imageApprove = true
        self.navigationController?.pushViewController(userDetailsVc, animated: true)

    }else{
//        naveen
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopViewController") as! PopViewController
        popOverVC.fromScreen = ""
      //popOverVC.ImageStr = ""
        popOverVC.headingMesg = "Alert"
        popOverVC.subMesg = "You need an active membership plan to view the profile. Proceed to membership plans?"
        self.addChildViewController(popOverVC)
        popOverVC.view.center = self.view.frame.center
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
//        naveen

//
//        let alertController = UIAlertController(title: "Alert", message: "You need an active membership plan to view the profile. Proceed to membership plans?", preferredStyle: .alert)
//        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
//            print("Ok button tapped");
//            let MemberShipViewController = self.storyboard?.instantiateViewController(withIdentifier: "MemberShipViewController") as! MemberShipViewController
//            self.navigationController?.pushViewController(MemberShipViewController, animated: true)
//        }
//        alertController.addAction(OKAction)
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
//
//        }
//        alertController.addAction(cancelAction)
//        self.present(alertController, animated: true, completion:nil)
        
      }
    
      
    }
    
}
    
    
    
}



extension UIButton {
    
    func pulsate() {
        
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.2
        pulse.fromValue = 0.95
        pulse.toValue = 1.0
        pulse.autoreverses = true
        pulse.repeatCount = 2
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        
        layer.add(pulse, forKey: "pulse")
    }
    
    
    func shake() {
        
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 1
        shake.repeatCount = 2
        shake.autoreverses = true
        
        let fromPoint = CGPoint(x: center.x - 5, y: center.y)
        let fromValue = NSValue(cgPoint: fromPoint)
        
        let toPoint = CGPoint(x: center.x + 5, y: center.y)
        let toValue = NSValue(cgPoint: toPoint)
        
        shake.fromValue = fromValue
        shake.toValue = toValue
        
        layer.add(shake, forKey: "position")
    }
}

extension UIImage {
    enum ContentMode
    { case contentFill
        case contentAspectFill
        case contentAspectFit
        
    }
    func resize(withSize size: CGSize, contentMode: ContentMode = .contentFill) -> UIImage? {
        let aspectWidth = size.width / self.size.width;
        let aspectHeight = size.height / self.size.height;
        switch contentMode
        {
        case .contentFill: return resize(withSize: size)
        case .contentAspectFit:
            let aspectRatio = min(aspectWidth, aspectHeight)
            return resize(withSize: CGSize(width: self.size.width * aspectRatio, height: self.size.height * aspectRatio))
        case .contentAspectFill:
            let aspectRatio = max(aspectWidth, aspectHeight)
            return resize(withSize: CGSize(width: self.size.width * aspectRatio, height: self.size.height * aspectRatio))
            
        }
        
    }
    private func resize(withSize size: CGSize) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, 1)
        defer {
            UIGraphicsEndImageContext()
            
        }
        draw(in: CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height))
        return UIGraphicsGetImageFromCurrentImageContext()
        
    }
    
    
    
}


extension Float {
    var cleanValue: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}

 
         
