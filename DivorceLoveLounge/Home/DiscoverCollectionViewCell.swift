//
//  DiscoverCollectionViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/3/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
import FaveButton
protocol CanReceiveButtonaDATA {
    func CanReceiveButtonData(tag:Int,buttonName:String,selected:Bool)
    }

class DiscoverCollectionViewCell: UICollectionViewCell,FaveButtonDelegate {
    
    func faveButton(_ faveButton: FaveButton, didSelected selected: Bool) {
        print(selected)
        
  
        if( faveButton == sendRequestBtn){
        //self.sendRequestBtn?.setSelected(selected: true, animated: false)
        delegate?.CanReceiveButtonData(tag: sendRequestBtn?.tag ?? 0, buttonName: "sendreqbtn",selected:selected )
        }else if (faveButton == favouriteBtn){
            delegate?.CanReceiveButtonData(tag: sendRequestBtn?.tag ?? 0, buttonName: "favouritebtn",selected:selected )
        }else if (faveButton == infoBtn){
            delegate?.CanReceiveButtonData(tag: sendRequestBtn?.tag ?? 0, buttonName: "infobtn",selected:selected )
        }
    }

    
    @IBOutlet weak var adsBackView: UIView!
    
    @IBOutlet weak var adsImageView: UIImageView!
    @IBOutlet var info_btnview: UIView!
    @IBOutlet var like_View: UIView!
    @IBOutlet var fave_View: UIView!
    
  var delegate : CanReceiveButtonaDATA?
    
    
    
    @IBOutlet var gradientView: UIView!
    @IBOutlet var favouriteBtn: FaveButton?
    @IBOutlet var sendRequestBtn: FaveButton?
    @IBOutlet var infoBtn: UIButton!
    @IBOutlet weak var blurImage: UIVisualEffectView!
    
    
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var ageLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!    
    @IBOutlet var btnHide: UIButton!
    
    
    
   
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var viewProfileBtn: UIButton!
    @IBOutlet weak var messageBtn: UIButton!
    @IBOutlet weak var blockUserBtn: UIButton!
    @IBOutlet weak var reportUserBtn: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        favouriteBtn?.delegate = self
        sendRequestBtn?.delegate = self
       // infoBtn?.delegate = self
        
        
        self.infoView.layer.cornerRadius = 10
        
        
        self.info_btnview.layer.cornerRadius = self.info_btnview.frame.height/2
        self.info_btnview.layer.masksToBounds = true
        
        self.like_View.layer.cornerRadius = self.like_View.frame.height/2
        self.like_View.layer.masksToBounds = true
        
        self.fave_View.layer.cornerRadius = self.fave_View.frame.height/2
        self.fave_View.layer.masksToBounds = true
        
        
        self.profileImg.layer.cornerRadius = self.profileImg.frame.height/2
        self.profileImg.layer.masksToBounds = true
        
        self.profileView.layer.cornerRadius = self.profileView.frame.height/2
        self.profileView.layer.masksToBounds = true
        
        
//        self.gradientView.applyGradient11(colors: [UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1).cgColor, UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.1).cgColor])

    }

}
//extension UIView{
//    func applyGradient11(colors: [CGColor])
//    {
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.colors = colors
//        gradientLayer.startPoint = CGPoint(x: 0, y: 1)
//        gradientLayer.endPoint = CGPoint(x: 0, y: 0)
//        gradientLayer.frame = self.bounds
//        self.layer.insertSublayer(gradientLayer, at: 1)
//    }
//}
