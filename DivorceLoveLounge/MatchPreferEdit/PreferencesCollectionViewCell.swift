//
//  PreferencesCollectionViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 6/24/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class PreferencesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backColorView: UIView!    
    @IBOutlet weak var imagView: UIImageView!    
    @IBOutlet weak var preferLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
