//
//  DistanceTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 6/24/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class DistanceTableViewCell: UITableViewCell {

    @IBOutlet weak var myProfileLbl: UILabel!
    @IBOutlet weak var myMatchLbl: UILabel!
    
    @IBOutlet weak var zipCodeLbl: UILabel!
    @IBOutlet weak var stateLbl: UILabel!    
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var distancePopUpBtn: UIButton!
    @IBOutlet weak var zipcodeBtn: UIButton!    
    @IBOutlet weak var stateBtn: UIButton!
    @IBOutlet weak var distanceLbl: UILabel!    
    @IBOutlet var distanceBtnLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        myProfileLbl.layer.cornerRadius = 5
        myMatchLbl.layer.cornerRadius = 5
        myProfileLbl.layer.masksToBounds = true
        myMatchLbl.layer.masksToBounds = true
        distancePopUpBtn.layer.cornerRadius = 5
         distancePopUpBtn.layer.masksToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
