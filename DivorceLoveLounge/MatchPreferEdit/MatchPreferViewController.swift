//
//  MatchPreferViewController.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 6/24/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class MatchPreferViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource
{
//    let datePicker = UIDatePicker()
    var i = Int()
    var rowsWhichAreChecked = [NSIndexPath]()
    let themes = Themes()
    var matchArray = ["Distance & Geography","Smoking & Drinking","Ethnicity","Age","Religion & Mother Tongue","Education"]
    var imgArray = ["Distance_default","Smoking_Drinking_default","Ethnicity_default","ageDefault","Religion_default","Income_default"]
    var distanceArr  = ["WITHIN 30 MILES","WITHIN 60 MILES (RECOMMENDED)","WITHIN 120 MILES","WITHIN 300 MILES","IN MY COUNTRY","ANYWHERE IN THE WORLD"]
    var selectedDate = String()
    var states = [String]()
    var occupationArray = Array<AnyObject>()
     var rowsWhichAreCheckedForAll = [NSIndexPath]()
    @IBOutlet weak var selctHeadingLabel: UILabel!
    @IBOutlet weak var countryPopUpBtn: UIButton!
    var myOccupation = String()
    var selectedHint = Int()
    var filterCityArray = Array<AnyObject>()
    var citiesArray = Array<AnyObject>()
    var matchReligionArray = Array<AnyObject>()
    var states1 = ["alaska",
                   "alabama",
                   "arkansas",
                   "american Samoa",
                   "arizona",
                   "california",
                   "colorado",
                   "connecticut",
                   "district of Columbia",
                   "delaware",
                   "florida",
                   "georgia",
                   "guam",
                   "hawaii",
                   "iowa",
                   "idaho",
                   "illinois",
                   "indiana",
                   "kansas",
                   "kentucky",
                   "louisiana",
                   "massachusetts",
                   "maryland",
                   "maine",
                   "michigan",
                   "minnesota",
                   "missouri",
                   "mississippi",
                   "montana",
                   "north Carolina",
                   "north Dakota",
                   "nebraska",
                   "new Hampshire",
                   "new Jersey",
                   "new Mexico",
                   "nevada",
                   "new York",
                   "ohio",
                   "oklahoma",
                   "oregon",
                   "pennsylvania",
                   "puerto Rico",
                   "rhode Island",
                   "south Carolina",
                   "south Dakota",
                   "tennessee",
                   "texas",
                   "utah",
                   "virginia",
                   "virgin Islands",
                   "vermont",
                   "washington",
                   "wisconsin",
                   "west Virginia",
                   "wyoming"]
    
    let statesIDS1 = [ "AK",
                       "AL",
                       "AR",
                       "AS",
                       "AZ",
                       "CA",
                       "CO",
                       "CT",
                       "DC",
                       "DE",
                       "FL",
                       "GA",
                       "GU",
                       "HI",
                       "IA",
                       "ID",
                       "IL",
                       "IN",
                       "KS",
                       "KY",
                       "LA",
                       "MA",
                       "MD",
                       "ME",
                       "MI",
                       "MN",
                       "MO",
                       "MS",
                       "MT",
                       "NC",
                       "ND",
                       "NE",
                       "NH",
                       "NJ",
                       "NM",
                       "NV",
                       "NY",
                       "OH",
                       "OK",
                       "OR",
                       "PA",
                       "PR",
                       "RI",
                       "SC",
                       "SD",
                       "TN",
                       "TX",
                       "UT",
                       "VA",
                       "VI",
                       "VT",
                       "WA",
                       "WI",
                       "WV",
                       "WY"]
    
    var minAgeArray = Array<AnyObject>()
    var maxAgeArray = Array<AnyObject>()
    var minAge = String()
    var maxAge = String()
    var countryArray = Array<AnyObject>()
    var filterCountryArray = Array<AnyObject>()
     var data = [AnyObject]()
    var filter_customersArr = [String]()
    var selectedRow = Int()
    var popCountry = String()
    var religionArr = Array<AnyObject>()
    var smokeArr = Array<AnyObject>()
    var drinkArr = Array<AnyObject>()
    var selectedRow2 = Int()
    var ethnicityArr = Array<AnyObject>()
    var denominationArr = Array<AnyObject>()
     var educationArr = Array<AnyObject>()
      var distanceImpArr  = ["Not at all important \n Search outside my range","Search outside my range \n Search slightly outside my range","Somewhat important \n Search only within my range"]
     var countryArr = ["India","Indonesia","Bangladesh","Japan","Philippines","VietNam","Turkey","Iran","SouthKorea","Iraq","Afghanistan","SaudiArabia","Uzbekistan","Malaysia","Nepal","Yemen","China"]
    var myReligionId = String()
    var myReligion = String()
    var mySmoke = String()
    var myDrink = String()
    var matchDist = String()
    var occupationSelection = [String]()
    var matchImpDist = String()
    var myDenomination = String()
    var myEducation = String()
    var myEthinicity = String()
    var myDenominationId = String()
    var multipleSelctedEthinicityArray = Array<AnyObject>()
    var multipleSelctedsmokeArray = Array<AnyObject>()
    var multipleSelctedDrinkArray = Array<AnyObject>()
    var multipleSelctedReligionArray = Array<AnyObject>()
    var multipleSelctedDenominationArray = Array<AnyObject>()
    var multipleSelctedEducationArray = Array<AnyObject>()
    var multipleSelctedDistanceArray = Array<AnyObject>()
    var urlService = URLservices.sharedInstance
    let countryPicker = UIPickerView()
    var clickButton = String()
    
    var adsListArr = [AnyObject]()
    
    
    @IBOutlet weak var cityBtn: UIButton!
    @IBOutlet weak var stateBtn: UIButton!
    @IBOutlet weak var matchCollectionView: UICollectionView!
    @IBOutlet weak var matchTableView: UITableView!
    @IBOutlet var firstPopView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var popTableView: UITableView!    
    @IBOutlet weak var saveBtn: UIButton! 
    @IBOutlet weak var hideView: UIView!
    @IBOutlet var zipcodeView: UIView!
    @IBOutlet weak var zipcodeField: UITextField!
    @IBOutlet weak var zipSaveBtn: UIButton!
    
    
    @IBOutlet var ageView: UIView!
    @IBOutlet var minAgeBtn: UIButton!
    @IBOutlet var minAgeField: UITextField!
    @IBOutlet weak var minAgeTableView: UITableView!
    @IBOutlet var maxAgeBtn: UIButton!
    @IBOutlet var maxAgeFiled: UITextField!
    @IBOutlet weak var maxAgeTableView: UITableView!
    @IBOutlet var ageSaveBtn: UIButton!
     @IBAction func minAgeAction(_ sender: Any) {
        minAgeTableView.isHidden = false
//        maxAgeTableView.isHidden = true
    }
     @IBAction func maxAgeAction(_ sender: Any) {
//        minAgeTableView.isHidden = false
        maxAgeTableView.isHidden = false
    }
    
    @IBAction func ageCloseAction(_ sender: Any) {
        hideView.isHidden = true
        FourthPopView(hidden: true)
        minAgeTableView.isHidden = true
        maxAgeTableView.isHidden = true
        
        
        
        
        
    }
    
    @IBAction func zipCloseAction(_ sender: Any) {
        SecondPopView(hidden : true)
        hideView.isHidden = true
    }
    @IBOutlet var stateView: UIView!    
    @IBOutlet var countryField: UITextField!
    @IBOutlet weak var stateField: UITextField!    
    @IBOutlet var cityField: UITextField!
    @IBAction func countryTextFileld(_ sender: Any) {
    }
    @IBOutlet var countryTableView: UITableView!
    @IBOutlet weak var stateSaveBtn: UIButton!
    @IBAction func stateCloseAction(_ sender: Any) {
        ThirdPopView(hidden : true)
        hideView.isHidden = true
    }
    @IBAction func colseBtnAction(_ sender: Any) {
       FirstPopView(hidden : true)
        hideView.isHidden = true
    }
   @IBAction func ageSaveAction(_ sender: Any) {
    hideView.isHidden = true
    FourthPopView(hidden: true)
    minAgeTableView.isHidden = true
    maxAgeTableView.isHidden = true
    
    
    let minAge1:Int = Int(minAgeField.text!) ?? 18
    let maxAge1:Int = Int(maxAgeFiled.text!) ?? 24
    
    
    
    if(minAge1 < maxAge1){
       // let age = maxAge1 - minAge1
        
      //  if(age >= 6){
            
             self.matchPreferanceUpadate(zipcode: "", state: "", city: "", ethnicity: "", religion: "", denomination: "", education: "", income: "", smoke: "", drink: "", passionate: "", leisure_time: "", occupation: "", partner_age_min: minAgeField.text!, partner_age_max: maxAgeFiled.text!, partner_match_distance: "", partner_ethnicity: "", partner_denomination: "", partner_education: "", partner_smoke: "", partner_religion: "", partner_drink: "", partner_occupation: "", country: "", matchSearch: "")
            
            
//        }else {
//            themes.showAlert(title: "Alert", message: "Age between min and amx age should be greater than 6 years", sender: self)
//
//        }
    }else{
        themes.showAlert(title: "Alert", message: "Max age should be greater than min age.", sender: self)
    }
    
    
    }
    @IBAction func zipSaveBtnAction(_ sender: Any) {
        zipcodeView.isHidden = true
        hideView.isHidden = true
        if(zipcodeField.text == ""){
            themes.showAlert(title: "Alert", message: "Please enter zipcode", sender: self)
        }else{
            self.matchPreferanceUpadate(zipcode: zipcodeField.text!, state: "", city: "", ethnicity: "", religion: "", denomination: "", education: "", income: "", smoke: "", drink: "", passionate: "", leisure_time: "", occupation: "", partner_age_min: "", partner_age_max: "", partner_match_distance: "", partner_ethnicity: "", partner_denomination: "", partner_education: "", partner_smoke: "", partner_religion: "", partner_drink: "", partner_occupation: "", country: "", matchSearch: "")
        }
    }
    @IBAction func stateSaveBtnAction(_ sender: Any) {
        ThirdPopView(hidden: true)
        matchPreferanceUpadate(zipcode: "",state: stateField.text!,city: cityField.text!,ethnicity: "",religion: "",denomination: "",education: "",income: "",smoke: "",drink: "",passionate: "",leisure_time: "",occupation: "",partner_age_min: "",partner_age_max: "",partner_match_distance: "",partner_ethnicity: "",partner_denomination: "",partner_education: "",partner_smoke: "",partner_religion: "",partner_drink: "", partner_occupation: "", country: countryField.text!, matchSearch: "")
        
        
        
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet var selectStatePop: UIView!
    @IBOutlet var countrySearchField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        datePicker.datePickerMode = UIDatePickerMode.date
//        datePicker.addTarget(self, action: #selector(MatchPreferViewController.datePickerValueChanged(sender:)), for: UIControlEvents.valueChanged)
   
        zipcodeField.text = themes.getZipcode()
        countryField.text = themes.getcountry()
        stateField.text = themes.getstate()
        cityField.text = themes.getcity()
        i = 0
        selectStatePop.layer.cornerRadius = 5
        countryPopUpBtn.layer.cornerRadius = 5
        countrySearchField.autocapitalizationType = .sentences
        countrySearchField.layer.cornerRadius = countrySearchField.frame.height/2
        countrySearchField.delegate = self
        countrySearchField.layer.borderWidth = 1
        countrySearchField.layer.borderColor = UIColor.lightGray.cgColor
        
        // For Right side Padding
        countrySearchField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: countrySearchField.frame.height))
        countrySearchField.leftViewMode = .always
        // For left side Padding
        countrySearchField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: countrySearchField.frame.height))
        countrySearchField.rightViewMode = .always
        countryTableView.separatorColor = UIColor.clear
        selectedRow2 = -1
        selectStatePop.isHidden = true
        self.matchCollectionView.delegate = self
        self.matchCollectionView.dataSource = self
        matchTableView.delegate = self
        matchTableView.dataSource = self
        popTableView.delegate = self
        popTableView.dataSource = self
        popTableView.separatorColor = UIColor.clear
        matchTableView.separatorColor = UIColor.clear
        selectedRow = -1
        hideView.isHidden = true
        FirstPopView(hidden : true)
        firstPopView.layer.cornerRadius = 5
        zipcodeField.delegate = self
        stateField.delegate = self
        countryField.delegate = self
        cityField.delegate = self
        SecondPopView(hidden: true)
        saveBtn.layer.cornerRadius = saveBtn.frame.size.height/2
        zipcodeView.layer.cornerRadius = 5
        zipSaveBtn.layer.cornerRadius = zipSaveBtn.frame.size.height/2
        zipSaveBtn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        zipSaveBtn.layer.shadowOpacity = 1.0
        zipSaveBtn.layer.shadowRadius = 3
        zipSaveBtn.layer.masksToBounds = false
        zipSaveBtn.layer.shadowColor = UIColor.gray.cgColor
        zipcodeField.layer.cornerRadius = zipcodeField.frame.size.height/2
        zipcodeField.layer.borderWidth = 1
        zipcodeField.layer.borderColor = UIColor.lightGray.cgColor
        ThirdPopView(hidden: true)
        stateView.layer.cornerRadius = 5
        stateSaveBtn.layer.cornerRadius = stateSaveBtn.frame.size.height/2
        stateSaveBtn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        stateSaveBtn.layer.shadowOpacity = 1.0
        stateSaveBtn.layer.shadowRadius = 3
        stateSaveBtn.layer.masksToBounds = false
        stateSaveBtn.layer.shadowColor = UIColor.gray.cgColor
        stateField.layer.cornerRadius = stateField.frame.size.height/2
        stateField.layer.borderWidth = 1
        stateField.layer.borderColor = UIColor.lightGray.cgColor
        FourthPopView(hidden: true)
        ageView.layer.cornerRadius = 5
        minAgeTableView.layer.cornerRadius = 5
        maxAgeTableView.layer.cornerRadius = 5
        minAgeTableView.delegate = self
        maxAgeTableView.delegate = self
        minAgeTableView.dataSource = self
        maxAgeTableView.dataSource = self

        minAgeTableView.isHidden = true
        maxAgeTableView.isHidden = true
        minAgeTableView.separatorStyle = .none
        maxAgeTableView.separatorStyle = .none
        minAgeField.layer.cornerRadius = minAgeField.frame.size.height/2
        minAgeField.layer.borderWidth = 1
        minAgeField.layer.borderColor = UIColor.lightGray.cgColor
        minAgeField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: minAgeField.frame.height))
        minAgeField.leftViewMode = .always
        maxAgeFiled.layer.cornerRadius = maxAgeFiled.frame.size.height/2
        maxAgeFiled.layer.borderWidth = 1
        maxAgeFiled.layer.borderColor = UIColor.lightGray.cgColor
        maxAgeFiled.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: maxAgeFiled.frame.height))
        maxAgeFiled.leftViewMode = .always
        ageSaveBtn.layer.cornerRadius = ageSaveBtn.frame.size.height/2
        ageSaveBtn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        ageSaveBtn.layer.shadowOpacity = 1.0
        ageSaveBtn.layer.shadowRadius = 3
        ageSaveBtn.layer.masksToBounds = false
        ageSaveBtn.layer.shadowColor = UIColor.gray.cgColor
        for i in 18...99{
            minAgeArray.append(i as AnyObject)
        }
        for i in 18...99{
            maxAgeArray.append(i as AnyObject)
        }
        
        zipcodeField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: zipcodeField.frame.height))
        zipcodeField.leftViewMode = .always
        // For Right side Padding
        stateField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: stateField.frame.height))
        stateField.leftViewMode = .always
        // For left side Padding
        stateField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: stateField.frame.height))
        stateField.rightViewMode = .always
        countryField.layer.cornerRadius = countryField.frame.size.height/2
        countryField.layer.borderWidth = 1
        countryField.layer.borderColor = UIColor.lightGray.cgColor
        cityField.layer.cornerRadius = cityField.frame.size.height/2
        cityField.layer.borderWidth = 1
        cityField.layer.borderColor = UIColor.lightGray.cgColor
        // For Right side Padding
        countryField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: countryField.frame.height))
        countryField.leftViewMode = .always
        
        cityField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: cityField.frame.height))
        cityField.leftViewMode = .always
        
        // For left side Padding
        countryField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: countryField.frame.height))
        countryField.rightViewMode = .always
        
        countryPicker.delegate = self
        countryPicker.dataSource = self
        countryPicker.backgroundColor = UIColor.white
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(MatchPreferViewController.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(MatchPreferViewController.cancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        zipcodeField.inputAccessoryView = toolBar

        self.zipcodeField.delegate = self
        self.stateField.delegate = self
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
//        self.view.addGestureRecognizer(tapGesture)
        
        self.matchCollectionView.register(UINib(nibName: "PreferencesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PreferencesCollectionViewCell")
        self.matchTableView.register(UINib(nibName: "AgeEthncityTableViewCell", bundle: nil), forCellReuseIdentifier: "AgeEthncityTableViewCell")
        self.matchTableView.register(UINib(nibName: "ReligionSmokeCell", bundle: nil), forCellReuseIdentifier: "ReligionSmokeCell")
        self.matchTableView.register(UINib(nibName: "DistanceTableViewCell", bundle: nil), forCellReuseIdentifier: "DistanceTableViewCell")
        self.popTableView.register(UINib(nibName: "popUpCell", bundle: nil), forCellReuseIdentifier: "popUpCell")
        self.countryTableView.register(UINib(nibName: "countryTableCell", bundle: nil), forCellReuseIdentifier: "countryTableCell")
        self.minAgeTableView.register(UINib(nibName: "ParkPopUpCell", bundle: nil), forCellReuseIdentifier: "ParkPopUpCell")
        self.maxAgeTableView.register(UINib(nibName: "ParkPopUpCell", bundle: nil), forCellReuseIdentifier: "ParkPopUpCell")

        getCountries()
        getReligions()
        dynamicValuesApi()
        getMotherToungeApi()
        // Do any additional setup after loading the view.
        
        
        //  self.matchPreferanceUpadate(zipcode: "", state: "", city: "", ethnicity: "", religion: "", denomination: "", education: "", income: "", smoke: "", drink: "", passionate: "", leisure_time: "", occupation: "", partner_age_min: "", partner_age_max: "", partner_match_distance: "", partner_ethnicity: "", partner_denomination: "", partner_education: "", partner_smoke: "", partner_religion: "", partner_drink: "", partner_occupation: "", country: "", matchSearch: "")
        
       adsListServiceApi(ScreenType: "Profile")
    }
    func getCountries(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            urlService.serviceCallGetMethod(url: getCountriess, completionHandler: { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    self.data = (response["data"] as AnyObject) as! [AnyObject]
                    
                    for i in 0...self.data.count - 1{
                        
                        let obj = self.data[i]
                        
                        let name = obj["country_name"] as! String
                        self.countryArray.append(name as AnyObject)
                        self.filterCountryArray.append(name as AnyObject)
                    }
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            })
        }else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func donePicker() {
        zipcodeField.resignFirstResponder()
    }
    @objc func cancelPicker() {
        stateField.text = ""
        stateField.resignFirstResponder()
    }
    // Picker View delegate & data source methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return countryArr.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return countryArr[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            stateField.text = countryArr[row]
    }
    
    // Collection view delegate & data source methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return matchArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PreferencesCollectionViewCell", for: indexPath)as! PreferencesCollectionViewCell
        cell.layer.cornerRadius = 5
         cell.preferLabel.text = matchArray[indexPath.row]
        cell.imagView.image = UIImage(named: imgArray[indexPath.row])
     
        if i == indexPath.row {
            cell.imagView.setImageColor(color: UIColor.white)
               cell.backColorView.backgroundColor = orangeColor
        } else {
            cell.imagView.setImageColor(color: UIColor(red: 200/255.0, green: 126/255.0, blue: 107/255.0, alpha: 1.0))
            cell.backColorView.backgroundColor = brownColorDark
        }
      //  cell.imagView.image = UIImage(named: imgArray[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? PreferencesCollectionViewCell {
            i = indexPath.row
            
            matchCollectionView.reloadData()
            matchTableView.reloadData()
        }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? PreferencesCollectionViewCell else {
            return
        }
       //cell.backColorView.backgroundColor = brownColorDark
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 120)
    }
    // Tableview delegate & data source methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == popTableView {
            if i == 3 {
                return 0
            } else if i == 4 {
                if (clickButton == "MyReligion") {
                    return religionArr.count
                } else if (clickButton == "MyDenomination") {
                    return denominationArr.count
                } else if (clickButton == "MatchReligion") {
                    return matchReligionArray.count
                } else if (clickButton == "MatchDenomination") {
                    return denominationArr.count
                }
            } else if i == 5 {
                
                
                if(clickButton == "MyOcupation"){
                    return occupationArray.count
                }else if(clickButton == "MatchOcuaption"){
                    return occupationArray.count
                }else if(clickButton == "MyEducation"){
                    return educationArr.count
                }else{
                    return educationArr.count
                }
                
            } else if i == 0 {
                
                if(clickButton == "MatchDistanceImp"){
                    return distanceImpArr.count
                }else{
                   return distanceArr.count
                }
                
            } else if i == 1 {
                if (clickButton == "MySmoke") {
                    return smokeArr.count
                } else if (clickButton == "MyDrink") {
                    return drinkArr.count
                } else if (clickButton == "MatchSmoke") {
                    return smokeArr.count
                } else if (clickButton == "MatchDrink") {
                    return drinkArr.count
                }
            } else {
                return ethnicityArr.count
            }
            
        } else if tableView ==  matchTableView {
            
        return 1
            
        }else if tableView ==  countryTableView {
            
            return filter_customersArr.count
            
        }else if tableView == minAgeTableView {
            
            return minAgeArray.count
            
        } else if tableView == maxAgeTableView {
            
            return maxAgeArray.count
            
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         if tableView == matchTableView {
        if i == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AgeEthncityTableViewCell", for: indexPath)as! AgeEthncityTableViewCell
            createPickerView(sender:  (cell.dobTextField)!)
            createToolbar(sender:  (cell.dobTextField)!)
            cell.dobTextField.tag = indexPath.row
            cell.dobTextField.text = themes.dateFormateConverter(date: themes.getdob()!)
            var date = themes.checkNullValue(themes.getdob()) as! String
                if (date == "") {
                } else {
                let dob = themes.dateFormateConverter(date: date as! String)
             cell.dobTextField.text = dob
                }
//            cell.dobTextField.text = themes.checkNullValue(themes.getdob()) as! String
            
            let year = themes.getAgeFromDOF(date: themes.getdob()!)
//            let year = themes.checkNullValue(themes.getdob()!) as! Date
//            let year = themes.getdob() as! String
            cell.profileDataLbl.text = String(year.0)
            let age = "\(themes.getpartner_age_min()!) to \(themes.getpartner_age_max()!)"
            cell.detailLbl.text = age
            cell.popUpBtn.setTitle(age, for: .normal)
  
            
            cell.selectionStyle = .none
            cell.profileLbl.text = "My Age"
            cell.matchLbl.text = "Age"
            //cell.mat
            cell.matchDataLbl.text = "I'm looking for someone between the ages of... "
            cell.popUpBtn.tag = indexPath.row
            cell.dobBtn.tag = indexPath.row
            cell.popUpBtn.addTarget(self, action: #selector(matchAgeBtnClick(button:)), for:.touchUpInside)
            cell.dobBtn.addTarget(self, action: #selector(dobBtnClick(button:)), for:.touchUpInside)
            
            return cell
        } else if i == 4 {
             let cell = tableView.dequeueReusableCell(withIdentifier: "ReligionSmokeCell", for: indexPath)as! ReligionSmokeCell
            cell.selectionStyle = .none
            cell.profileLbl1.text = "Religion"
            cell.matchDataLbl1.text = "What best describes your partner's religious beliefs or spirituality?"
            
            cell.profileLbl2.text = "Mother tongue"
            cell.matchLbl1.text = "Religion"
            cell.matchDatalbl2.text = "What best describes your partner's denominations beliefs or spirituality?"
            
            cell.matchDetailLbl1.text = themes.getpartner_religion()
            cell.matchDetailLbl2.text = themes.getPartnerDenomination()
            cell.profileBtn1.setTitle(themes.getReligion(), for: .normal)
            cell.profileBtn2.setTitle(themes.getmTongue(), for: .normal)
            cell.match1BtnLbl.text = themes.getpartner_religion()
            cell.match2BtnLbl.text = themes.getpartner_mTongue()
            cell.matchDetailLbl2.text = themes.getpartner_mTongue()
            cell.matchLbl2.text = "Mother tongue"
            cell.profileBtn1.tag = indexPath.row
            cell.profileBtn2.tag = indexPath.row
            cell.matchBtn1.tag = indexPath.row
            cell.matchBtn2.tag = indexPath.row
            cell.profileBtn1.addTarget(self, action: #selector(profileBtn1Click(button:)), for:.touchUpInside)
            cell.profileBtn2.addTarget(self, action: #selector(profileBtn2Click(button:)), for:.touchUpInside)
            cell.matchBtn1.addTarget(self, action: #selector(matchBtn1Click(button:)), for:.touchUpInside)
            cell.matchBtn2.addTarget(self, action: #selector(matchBtn2Click(button:)), for:.touchUpInside)
            cell.adsListArr = self.adsListArr
            cell.adsCollectionView.reloadData()
            
            return cell
        } else if i == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReligionSmokeCell", for: indexPath)as! ReligionSmokeCell
            cell.selectionStyle = .none
            cell.profileLbl1.text = "Education"
            cell.profileLbl2.text = "Occupation"
            cell.matchLbl1.text = "Education"
            cell.matchLbl2.text = "Occupation"
            cell.matchDataLbl1.text = "Which describes your partner's level of education?"
            cell.matchDatalbl2.text = "What do you expect your Partner to do?"

//            cell.profileDataLbl.text = themes.geteducationr()
            cell.matchDetailLbl1.text = themes.getpartner_education()
            cell.match1BtnLbl.text = themes.getpartner_education()
            cell.matchDetailLbl2.text = themes.getpartner_occupation()
            cell.match2BtnLbl.text = themes.getpartner_occupation()
            cell.profileBtn1.setTitle(themes.geteducationr(), for: .normal)
            cell.profileBtn2.setTitle(themes.getoccupation(), for: .normal)

            cell.profileBtn1.tag = indexPath.row
            cell.profileBtn2.tag = indexPath.row
            cell.matchBtn1.tag = indexPath.row
            cell.matchBtn2.tag = indexPath.row
            
            cell.profileBtn1.addTarget(self, action: #selector(profileBtn1Click(button:)), for:.touchUpInside)
            cell.profileBtn2.addTarget(self, action: #selector(profileBtn2Click(button:)), for:.touchUpInside)
            cell.matchBtn1.addTarget(self, action: #selector(matchBtn1Click(button:)), for:.touchUpInside)
            cell.matchBtn2.addTarget(self, action: #selector(matchBtn2Click(button:)), for:.touchUpInside)
            cell.adsListArr = self.adsListArr
            cell.adsCollectionView.reloadData()
            return cell
        } else if i == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReligionSmokeCell", for: indexPath)as! ReligionSmokeCell
            cell.selectionStyle = .none
            
            cell.profileLbl1.text = "Zip Code"
            cell.profileLbl2.text = "State"
            cell.matchLbl1.text = "Match Distance"
            cell.matchLbl2.text = "Match Distance Importance"
            cell.matchDataLbl1.text = "How far should we search for your matches?"
            cell.matchDatalbl2.text = "How importance is the distance of your match?"
            
        cell.profileBtn1.setTitle(themes.getZipcode(), for: .normal)
            
            
        cell.profileBtn2.setTitle(themes.getstate(), for: .normal)
            cell.matchDetailLbl1.text = themes.getpartner_match_distance()
            cell.match1BtnLbl.text = themes.getpartner_match_distance()
            cell.matchDetailLbl2.text = themes.getpartner_match_search()
            cell.match2BtnLbl.text = themes.getpartner_match_search()
            
            cell.profileBtn1.tag = indexPath.row
            cell.profileBtn2.tag = indexPath.row
            cell.matchBtn1.tag = indexPath.row
            cell.matchBtn2.tag = indexPath.row
            
            cell.profileBtn1.addTarget(self, action: #selector(profileBtn1Click(button:)), for:.touchUpInside)
            cell.profileBtn2.addTarget(self, action: #selector(profileBtn2Click(button:)), for:.touchUpInside)
            cell.matchBtn1.addTarget(self, action: #selector(matchBtn1Click(button:)), for:.touchUpInside)
            cell.matchBtn2.addTarget(self, action: #selector(matchBtn2Click(button:)), for:.touchUpInside)
            
//            cell.zipCodeLbl.text = themes.getZipcode()
//            cell.stateLbl.text = themes.getstate()
//            cell.distanceLbl.text = themes.getpartner_match_distance()
//            cell.distancePopUpBtn.addTarget(self, action: #selector(distanceBtnClick(button:)), for:.touchUpInside)
//            cell.zipcodeBtn.addTarget(self, action: #selector(zipcodeBtnClick(button:)), for:.touchUpInside)
//            cell.stateBtn.addTarget(self, action: #selector(stateBtnClick(button:)), for:.touchUpInside)
            cell.adsListArr = self.adsListArr
            cell.adsCollectionView.reloadData()
            return cell
        } else if i == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReligionSmokeCell", for: indexPath)as! ReligionSmokeCell
            cell.selectionStyle = .none
            cell.profileLbl1.text = "How often do you smoke?"
            cell.matchDataLbl1.text = "Please indicate the most that you would accept that your ideal match smoke"
            cell.profileLbl2.text = "How ofetn do you drink?"
            cell.matchLbl1.text = "Smoking"
            cell.matchDatalbl2.text = "Please indicate the most that you would accept that your ideal match drink"
            
            cell.matchDetailLbl1.text = themes.getpartner_smoke()
            cell.matchDetailLbl2.text = themes.getpartner_drink()
            cell.profileBtn1.setTitle(themes.getsmoke(), for: .normal)
            cell.profileBtn2.setTitle(themes.getdrink(), for: .normal)
            cell.match1BtnLbl.text = themes.getpartner_smoke()
            cell.match2BtnLbl.text = themes.getpartner_drink()
            cell.matchLbl2.text = "Drinking"
            cell.profileBtn1.tag = indexPath.row
            cell.profileBtn2.tag = indexPath.row
            cell.matchBtn1.tag = indexPath.row
            cell.matchBtn2.tag = indexPath.row
            cell.profileBtn1.addTarget(self, action: #selector(profileBtn1Click(button:)), for:.touchUpInside)
            cell.profileBtn2.addTarget(self, action: #selector(profileBtn2Click(button:)), for:.touchUpInside)
            cell.matchBtn1.addTarget(self, action: #selector(matchBtn1Click(button:)), for:.touchUpInside)
            cell.matchBtn2.addTarget(self, action: #selector(matchBtn2Click(button:)), for:.touchUpInside)
            
            
            cell.adsListArr = self.adsListArr
            cell.adsCollectionView.reloadData()
            
            
            return cell

        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DistanceTableViewCell", for: indexPath)as! DistanceTableViewCell
            cell.selectionStyle = .none
            cell.zipCodeLbl.text = themes.getEthinicity()
            cell.distanceLbl.text = themes.getpartner_ethnicity()
            cell.distanceBtnLbl.text = themes.getpartner_ethnicity()
            cell.zipcodeBtn.tag = indexPath.row
            cell.distancePopUpBtn.tag = indexPath.row
            cell.zipcodeBtn.addTarget(self, action: #selector(myEthnictyBtnClick(button:)), for:.touchUpInside)
            cell.distancePopUpBtn.addTarget(self, action: #selector(matchEtnicityBtnClick(button:)), for:.touchUpInside)
            
            return cell
            }
        } else if tableView == popTableView {
            if i == 3 {
                
                
            } else if i == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "popUpCell", for: indexPath)as! popUpCell
                cell.backView.layer.cornerRadius = 8
                cell.selectionStyle = .none
                if (clickButton == "MyReligion") {
                    headerLabel.text = "Religion"
                    let obj = religionArr[indexPath.row]
                    cell.popUpLabel.text = obj["religion_name"] as? String
                   
                    if(selectedRow == indexPath.row){
                         cell.backView.backgroundColor = orangeColor
                        cell.popUpLabel.textColor = UIColor.white
                    }else{
                        cell.backView.backgroundColor = UIColor.clear
                          cell.popUpLabel.textColor = UIColor.black
                    }
                } else if (clickButton == "MyDenomination") {
                    headerLabel.text = "Mother Tongue"
                    let obj = denominationArr[indexPath.row]
                    cell.popUpLabel.text = obj["mtongue_name"] as? String
                    if(selectedRow == indexPath.row){
                        cell.backView.backgroundColor = orangeColor
                        cell.popUpLabel.textColor = UIColor.white
                    }else{
                        cell.backView.backgroundColor = UIColor.clear
                        cell.popUpLabel.textColor = UIColor.black
                    }
                } else if (clickButton == "MatchReligion") {
                    headerLabel.text = "Religion"
                    let obj = matchReligionArray[indexPath.row]
                    cell.popUpLabel.text = obj["religion_name"] as? String
                    
                                        if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                                           
                                            
                                            cell.backView.backgroundColor = UIColor.clear
                                            cell.popUpLabel.textColor = UIColor.black
                                        }else{
                                            cell.backView.backgroundColor = orangeColor
                                            cell.popUpLabel.textColor = UIColor.white
                                        }
                } else if (clickButton == "MatchDenomination") {
                    headerLabel.text = "Mother Tongue"
                    let obj = denominationArr[indexPath.row]
                    cell.popUpLabel.text = obj["mtongue_name"] as? String
                    if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                        cell.backView.backgroundColor = UIColor.clear
                        cell.popUpLabel.textColor = UIColor.black
                    }else{
                        cell.backView.backgroundColor = orangeColor
                        cell.popUpLabel.textColor = UIColor.white
                    }
                }
                return cell
            } else if i == 5 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "popUpCell", for: indexPath)as! popUpCell
                cell.backView.layer.cornerRadius = 8
                cell.selectionStyle = .none

                if(clickButton == "MyEducation"){
                    headerLabel.text = "Education"
                    let obj = educationArr[indexPath.row]
                    cell.popUpLabel.text = obj["answer"] as? String
                    if(selectedRow == indexPath.row){
                        cell.backView.backgroundColor = orangeColor
                        cell.popUpLabel.textColor = UIColor.white
                    }else{
                        cell.backView.backgroundColor = UIColor.clear
                        cell.popUpLabel.textColor = UIColor.black
                    }
                }else if(clickButton == "MatchEducation"){
                    headerLabel.text = "Education"
                    let obj = educationArr[indexPath.row]
                    cell.popUpLabel.text = obj["answer"] as? String
                    if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                        cell.backView.backgroundColor = UIColor.clear
                        cell.popUpLabel.textColor = UIColor.black
                    }else{
                        cell.backView.backgroundColor = orangeColor
                        cell.popUpLabel.textColor = UIColor.white
                    }
                }else if(clickButton == "MatchOcupation"){
                    headerLabel.text = "Occupation"
                    let obj = occupationArray[indexPath.row]
                    cell.popUpLabel.text = obj["answer"] as? String
                    if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                        cell.backView.backgroundColor = UIColor.clear
                        cell.popUpLabel.textColor = UIColor.black
                    }else{
                        cell.backView.backgroundColor = orangeColor
                        cell.popUpLabel.textColor = UIColor.white
                    }
                }else if(clickButton == "MyOcupation"){
                    headerLabel.text = "Occupation"
                    let obj = occupationArray[indexPath.row]
                    cell.popUpLabel.text = obj["answer"] as? String
                    if(selectedRow == indexPath.row){
                        cell.backView.backgroundColor = orangeColor
                        cell.popUpLabel.textColor = UIColor.white
                    }else{
                        cell.backView.backgroundColor = UIColor.clear
                        cell.popUpLabel.textColor = UIColor.black
                    }
                }
                
                
                
              //  cell.popUpLabel.text = educationArr[indexPath.row] as? String
                return cell
            } else if i == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "popUpCell", for: indexPath)as! popUpCell
                cell.backView.layer.cornerRadius = 8
                cell.selectionStyle = .none
                if(clickButton == "MatchDistanceImp"){
                    headerLabel.text = "Distance Importance"
                    cell.popUpLabel.text = distanceImpArr[indexPath.row]
                    if(selectedRow == indexPath.row){
                        cell.backView.backgroundColor = orangeColor
                        cell.popUpLabel.textColor = UIColor.white
                    }else{
                        cell.backView.backgroundColor = UIColor.clear
                        cell.popUpLabel.textColor = UIColor.black
                    }
                    
                }else{
                    headerLabel.text = "Distance"
                    cell.popUpLabel.text = distanceArr[indexPath.row]
                    if(selectedRow == indexPath.row){
                        cell.backView.backgroundColor = orangeColor
                        cell.popUpLabel.textColor = UIColor.white
                    }else{
                        cell.backView.backgroundColor = UIColor.clear
                        cell.popUpLabel.textColor = UIColor.black
                    }
                }
              
               return cell
                
            } else if i == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "popUpCell", for: indexPath)as! popUpCell
                cell.selectionStyle = .none
                cell.backView.layer.cornerRadius = 8
                if (clickButton == "MySmoke") {
                     headerLabel.text = "Smoke"
                    let obj = smokeArr[indexPath.row]
                    cell.popUpLabel.text = obj["answer"] as? String
                    if(selectedRow == indexPath.row){
                        cell.backView.backgroundColor = orangeColor
                        cell.popUpLabel.textColor = UIColor.white
                    }else{
                        cell.backView.backgroundColor = UIColor.clear
                        cell.popUpLabel.textColor = UIColor.black
                    }
                } else if (clickButton == "MyDrink") {
                    headerLabel.text = "Drink"
                    let obj = drinkArr[indexPath.row]
                    cell.popUpLabel.text = obj["answer"] as? String
                    if(selectedRow == indexPath.row){
                        cell.backView.backgroundColor = orangeColor
                        cell.popUpLabel.textColor = UIColor.white
                    }else{
                        cell.backView.backgroundColor = UIColor.clear
                        cell.popUpLabel.textColor = UIColor.black
                    }
                } else if (clickButton == "MatchSmoke") {
                    headerLabel.text = "Smoke"
                    let obj = smokeArr[indexPath.row]
                    cell.popUpLabel.text = obj["answer"] as? String
                    if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                        
                        cell.backView.backgroundColor = UIColor.clear
                        cell.popUpLabel.textColor = UIColor.black
                    }else{
                        cell.backView.backgroundColor = orangeColor
                        cell.popUpLabel.textColor = UIColor.white
                    }
                } else if (clickButton == "MatchDrink") {
                    headerLabel.text = "Drink"
                    let obj = drinkArr[indexPath.row]
                    cell.popUpLabel.text = obj["answer"] as? String
                    if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                        
                        cell.backView.backgroundColor = UIColor.clear
                        cell.popUpLabel.textColor = UIColor.black
                    }else{
                        cell.backView.backgroundColor = orangeColor
                        cell.popUpLabel.textColor = UIColor.white
                    }
                }
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "popUpCell", for: indexPath)as! popUpCell
                cell.backView.layer.cornerRadius = 8
                cell.selectionStyle = .none
                headerLabel.text = "Ethnicity"
                let obj = ethnicityArr[indexPath.row]
                cell.popUpLabel.text = obj["answer"] as? String
                if(clickButton == "MyEthinicity"){
                    if(selectedRow == indexPath.row){
                        cell.backView.backgroundColor = orangeColor
                        cell.popUpLabel.textColor = UIColor.white
                    }else{
                        cell.backView.backgroundColor = UIColor.clear
                        cell.popUpLabel.textColor = UIColor.black
                    }
                }else if(clickButton == "MatchEthinicity"){
                    if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                        cell.backView.backgroundColor = UIColor.clear
                        cell.popUpLabel.textColor = UIColor.black
                    }else{
                        cell.backView.backgroundColor = orangeColor
                        cell.popUpLabel.textColor = UIColor.white
                    }
                }
                return cell
            }
          } else if tableView == countryTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "countryTableCell", for: indexPath)as! countryTableCell
            cell.selectionStyle = .none
            cell.backView.layer.cornerRadius = cell.backView.frame.size.height/2
            cell.backView.layer.borderWidth = 1
            cell.backView.layer.borderColor = UIColor.lightGray.cgColor
            cell.countryLbl.text = filter_customersArr[indexPath.row]
            if(selectedRow2 == indexPath.row){
                cell.backView.backgroundColor = darkBrownColor
                cell.countryLbl.textColor = UIColor.white
            }else{
                cell.backView.backgroundColor = UIColor.clear
                cell.countryLbl.textColor = UIColor.black
            }
            return cell
         } else if tableView == minAgeTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ParkPopUpCell", for: indexPath) as! ParkPopUpCell
            
            let obj:Int = minAgeArray[indexPath.row] as! Int
            cell.nameLabel.text = String(obj)
            cell.selectionStyle = .none
            return cell

         } else if tableView == maxAgeTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ParkPopUpCell", for: indexPath) as! ParkPopUpCell
            
            let obj:Int = maxAgeArray[indexPath.row] as! Int
            cell.nameLabel.text = String(obj)
            cell.selectionStyle = .none
            return cell
  
        }
        return UITableViewCell()
}
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == matchTableView {
        if i == 3 {
            return 310
        } else if i == 4 {
            return UITableViewAutomaticDimension
        } else if i == 5 {
            return UITableViewAutomaticDimension
        } else if i == 0 {
            return UITableViewAutomaticDimension
        } else if i == 1 {
            return UITableViewAutomaticDimension
        } else {
            return UITableViewAutomaticDimension
        }
    } else if tableView == popTableView {
            return 44
        } else if tableView == countryTableView {
            return 44
        } else {
            return 44
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == matchTableView {
        if i == 3 {
            return 310
        } else if i == 4 {
            return 529
        } else if i == 5 {
            return 529
        } else if i == 0 {
            return 529
        } else if i == 1 {
            return 529
        } else {
            return 257
        }
        }else if tableView == popTableView {
            return 44
        } else if tableView == countryTableView {
            return 44
        } else {
            return 44
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == countryTableView {
            countrySearchField.text = filter_customersArr[indexPath.row]
           // countryField.text = filter_customersArr[indexPath.row]
            selectedRow2 = indexPath.row
            if(selectedHint == 1){
                //countryField.text = popCountry
                if(countrySearchField.text == "USA"){
                    stateBtn.isHidden = false
                }else{
                    stateBtn.isHidden = true
                }
            }else if(selectedHint == 2){
                //stateField.text = popCountry
                let index = states.index(of: popCountry)
                let stateID = statesIDS1[index ?? 0]
                getCitiesAPI(state:stateID)
            }else{
               
            }
            countryTableView.reloadData()
            
        } else if tableView == minAgeTableView {
            let obj:Int = minAgeArray[indexPath.row] as! Int
            minAge = String(obj)
            minAgeTableView.isHidden = true
            minAgeField.text = String(obj)
//            ageMinBtn.setTitle(String(obj), for: .normal)

        } else if tableView == maxAgeTableView {
            let obj:Int = maxAgeArray[indexPath.row] as! Int
            maxAge = String(obj)
             maxAgeTableView.isHidden = true
            maxAgeFiled.text = String(obj)
//            ageMaxBtn.setTitle(String(obj), for: .normal)
        }
         if (clickButton == "MyReligion") {
            let obj = religionArr[indexPath.row]
            let id = obj["religion_id"]
             let name = obj["religion_name"]
            selectedRow = indexPath.row
            myReligionId = id as! String
            myReligion = name as! String
         }else if(clickButton == "MyDenomination"){
            let obj = denominationArr[indexPath.row]
            let id = obj["mtongue_id"]
            
            myDenomination = obj["mtongue_name"] as! String
            myDenominationId = id as! String
            selectedRow = indexPath.row
         }else if(clickButton == "MatchReligion"){
            let obj = matchReligionArray[indexPath.row]
            let religion_name = obj["religion_name"]
            let id = obj["religion_name"] as? String ?? ""
            if(id == "All"){
                rowsWhichAreChecked.removeAll()
                multipleSelctedReligionArray.removeAll()
                
                if(rowsWhichAreCheckedForAll.contains(indexPath as NSIndexPath) == false){
                    for i in 0...matchReligionArray.count - 1 {
                        let obj = matchReligionArray[i]
                        let id1 = obj["religion_name"] as? String ?? ""
                        let indexPath = NSIndexPath(row: i, section: 0)
                        rowsWhichAreCheckedForAll.append(indexPath)
                        rowsWhichAreChecked.append(indexPath)
                        multipleSelctedReligionArray.append(id1 as AnyObject )
                    }
                }else{
                    rowsWhichAreChecked.removeAll()
                    multipleSelctedReligionArray.removeAll()
                    rowsWhichAreCheckedForAll.removeAll()
                }
            }else{
                if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                    rowsWhichAreChecked.append(indexPath as NSIndexPath)
                    multipleSelctedReligionArray.append(id as AnyObject )
                }else{
                    // cell.backgroundColor = UIColor.black
                    // remove the indexPath from rowsWhichAreCheckedArray
                    if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                        rowsWhichAreChecked.remove(at: checkedItemIndex)
                        multipleSelctedReligionArray.remove(at: checkedItemIndex)
                        
                        
                        if(multipleSelctedReligionArray.count == 0){
                            
                        }else{
                            for i in 0...multipleSelctedReligionArray.count - 1{
                                
                                let obj = multipleSelctedReligionArray[i] as! String
                                if(obj == "All"){
                                    rowsWhichAreChecked.remove(at: i)
                                    multipleSelctedReligionArray.remove(at: i)
                                    break
                                }else{
                                    
                                }
                                
                            }
                        }
                    }
                }
            }
 
         }else if(clickButton == "MatchDenomination"){
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                rowsWhichAreChecked.append(indexPath as NSIndexPath)
                 let obj = denominationArr[indexPath.row]
                 let caste_name = obj["mtongue_name"]
                 multipleSelctedDenominationArray.append(caste_name as AnyObject )
            }else{
                // cell.backgroundColor = UIColor.black
                // remove the indexPath from rowsWhichAreCheckedArray
                if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                    rowsWhichAreChecked.remove(at: checkedItemIndex)
                    multipleSelctedDenominationArray.remove(at: checkedItemIndex)
                }
            }
        }else if (clickButton == "MyEducation") {
            let obj = educationArr[indexPath.row]
            let name = obj["answer"]
            selectedRow = indexPath.row
            myEducation = name as! String
         }else if(clickButton == "MatchEducation"){
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                rowsWhichAreChecked.append(indexPath as NSIndexPath)
                let obj = educationArr[indexPath.row]
                let caste_name = obj["answer"]
                multipleSelctedEducationArray.append(caste_name as AnyObject )
            }else{
                // cell.backgroundColor = UIColor.black
                // remove the indexPath from rowsWhichAreCheckedArray
                if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                    rowsWhichAreChecked.remove(at: checkedItemIndex)
                    multipleSelctedEducationArray.remove(at: checkedItemIndex)
                }
            }
         }else if (clickButton == "MyEthinicity") {
            let obj = ethnicityArr[indexPath.row]
            let name = obj["answer"]
            selectedRow = indexPath.row
            myEthinicity = name as! String
         }else if(clickButton == "MatchEthinicity"){
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                rowsWhichAreChecked.append(indexPath as NSIndexPath)
                let obj = ethnicityArr[indexPath.row]
                let caste_name = obj["answer"]
                multipleSelctedEthinicityArray.append(caste_name as AnyObject )
            }else{
                // cell.backgroundColor = UIColor.black
                // remove the indexPath from rowsWhichAreCheckedArray
                if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                    rowsWhichAreChecked.remove(at: checkedItemIndex)
                    multipleSelctedEthinicityArray.remove(at: checkedItemIndex)
                }
            }//occupationSelection
         }else if (clickButton == "MyOcupation") {
            let obj = occupationArray[indexPath.row]
            let name = obj["answer"]
            selectedRow = indexPath.row
            myOccupation = name as! String
         }else if (clickButton == "MySmoke") {
            let obj = smokeArr[indexPath.row]
            let name = obj["answer"]
            selectedRow = indexPath.row
            mySmoke = name as! String
         }else if (clickButton == "MyDrink") {
            let obj = smokeArr[indexPath.row]
            let name = obj["answer"]
            selectedRow = indexPath.row
            myDrink = name as! String
         }else if(clickButton == "MatchSmoke"){
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                rowsWhichAreChecked.append(indexPath as NSIndexPath)
                let obj = smokeArr[indexPath.row]
                let caste_name = obj["answer"]
                multipleSelctedsmokeArray.append(caste_name as AnyObject )
            }else{
                // cell.backgroundColor = UIColor.black
                // remove the indexPath from rowsWhichAreCheckedArray
                if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                    rowsWhichAreChecked.remove(at: checkedItemIndex)
                    multipleSelctedsmokeArray.remove(at: checkedItemIndex)
                }
            }
         }else if(clickButton == "MatchDrink"){
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                rowsWhichAreChecked.append(indexPath as NSIndexPath)
                let obj = drinkArr[indexPath.row]
                let caste_name = obj["answer"]
                multipleSelctedDrinkArray.append(caste_name as AnyObject )
            }else{
                // cell.backgroundColor = UIColor.black
                // remove the indexPath from rowsWhichAreCheckedArray
                if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                    rowsWhichAreChecked.remove(at: checkedItemIndex)
                    multipleSelctedDrinkArray.remove(at: checkedItemIndex)
                }
            }
         } else if(clickButton == "MatchDistance") {
            selectedRow = indexPath.row
            let dist = distanceArr[indexPath.row]
            matchDist = dist
         }else if(clickButton == "MatchOcupation") {
            if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
                rowsWhichAreChecked.append(indexPath as NSIndexPath)
                let obj = occupationArray[indexPath.row]
                let caste_name = obj["answer"]
                occupationSelection.append(caste_name as! String)
            }else{
                // cell.backgroundColor = UIColor.black
                // remove the indexPath from rowsWhichAreCheckedArray
                if let checkedItemIndex = rowsWhichAreChecked.index(of: indexPath as NSIndexPath){
                    rowsWhichAreChecked.remove(at: checkedItemIndex)
                    occupationSelection.remove(at: checkedItemIndex)
                }
            }
         }else if(clickButton == "MatchDistanceImp") {
            selectedRow = indexPath.row
            let dist = distanceImpArr[indexPath.row]
            matchImpDist = dist
         }else {
         }
        popTableView.reloadData()
    }
    
  /////////        Date picker functions at age cell
    func createPickerView(sender: UITextField){
        let datePickerView : UIDatePicker = UIDatePicker()
        
        
        
        let calendar = Calendar(identifier: .gregorian)

        let currentDate = Date()
        var components = DateComponents()
        components.calendar = calendar

        components.year = -19
        components.month = 12
        let maxDate = calendar.date(byAdding: components, to: currentDate)!

        components.year = -150
        let minDate = calendar.date(byAdding: components, to: currentDate)!

        datePickerView.minimumDate = minDate
        datePickerView.maximumDate = maxDate

        
        
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        sender.inputView = datePickerView
        datePickerView.tag = sender.tag
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged(caller:)), for: UIControlEvents.valueChanged)
    }
    
    @objc func datePickerValueChanged(caller: UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium

        let indexRow = (caller.tag)
       let cell = matchTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! AgeEthncityTableViewCell
        cell.dobTextField.text = dateFormatter.string(from: caller.date)
        selectedDate = dateFormatter.string(from: caller.date)
        
    }
    func createToolbar(sender: UITextField){
        let datePickerToolbar = UIToolbar()
        datePickerToolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(MatchPreferViewController.donePicker2))
        doneButton.tag = sender.tag
        
        datePickerToolbar.setItems([doneButton], animated: false)
        datePickerToolbar.isUserInteractionEnabled = true
        sender.inputAccessoryView = datePickerToolbar
    }
    @objc func donePicker2() {
        view.endEditing(true)
       
        if(selectedDate == ""){
            
        }else{
            matchPreferanceUpadate(zipcode: "",state: "",city: "",ethnicity: "",religion: "",denomination: "",education: "",income: "",smoke: "",drink: "",passionate: "",leisure_time: "",occupation: "",partner_age_min: "",partner_age_max: "",partner_match_distance: "",partner_ethnicity: "",partner_denomination: "",partner_education: "",partner_smoke: "",partner_religion: "",partner_drink: "", partner_occupation: "", country: "", matchSearch: "")
        }
        
    }
    
    @objc func matchAgeBtnClick(button: UIButton) {
            print("Match age clicked")
            clickButton = "MatchAge"
        
        minAgeField.text = themes.getpartner_age_min()
        maxAgeFiled.text = themes.getpartner_age_min()
        FourthPopView(hidden: false)
        hideView.isHidden = false
    }
    @objc func dobBtnClick(button: UIButton) {
            print("dob clicked")
            clickButton = "dob"
    }
    
    @objc func dataBtnClick(button: UIButton) {
        selectedRow = -1
         rowsWhichAreChecked.removeAll()
        if i == 3 {
            print("my age button clicked")
            clickButton = "MyAge"
        } else if i == 5 {
//            clickButton = "MyEducation"
//            print("my eduaction button clicked")
//            FirstPopView(hidden : false)
        } else if i == 2 {
            clickButton = "MyEthinicity"
            print("my ethnicity button clicked")
            
            
            let ethinicity:String = themes.getEthinicity()!
            
            if(ethinicity == ""){
                
                
                
            }else{
                myEthinicity = ethinicity
                
                for i in 0...ethnicityArr.count - 1{
                    
                    
                    let obj = ethnicityArr[i]
                    
                    let ethini = obj["answer"] as! String
                    if(ethini == myEthinicity){
                        selectedRow = i
                    }else{
                        
                    }
                    
                    
                    
                }
                
                
            }
            
            
            
            
            FirstPopView(hidden : false)
            popTableView.reloadData()
        }
        popTableView.reloadData()
        
   
    }
    
    @objc func distanceBtnClick(button: UIButton) {
        rowsWhichAreChecked.removeAll()
        print("Match distance button clicked")
        clickButton = "MatchDistance"
        FirstPopView(hidden : false)
        popTableView.reloadData()
    }
    @objc func zipcodeBtnClick(button: UIButton) {
//        hideView.isHidden = false
//        print("zipcode button clicked")
//        SecondPopView(hidden: false)
        
        
        
     }
    
    @objc func profileBtn1Click(button: UIButton) {
        selectedRow = -1
        rowsWhichAreChecked.removeAll()
        hideView.isHidden = false
        if i == 4 {
            print("my Religiuon button clicked")
             clickButton = "MyReligion"
            
            
            
            
            let reli:String = themes.getReligion()!
            
            if(reli == ""){
                
                
                
            }else{
                myReligion = reli
                
                for i in 0...religionArr.count - 1{
                    
                    
                    let obj = religionArr[i]
                    
                    let religion = obj["religion_name"] as! String
                    if(religion == myReligion){
                        selectedRow = i
                    }else{
                        
                    }
                    
                }
                
                
            }
            FirstPopView(hidden : false)
          
            print("match distance button clicked")
            FirstPopView(hidden : false)
            popTableView.reloadData()
        } else if i == 1 {
            print("my smoke button clicked")
            clickButton = "MySmoke"
            let smoke:String = themes.getsmoke()!
            if(smoke == ""){
                
                
                
            }else{
                mySmoke = smoke
                
                for i in 0...smokeArr.count - 1{
                    
                    
                    let obj = smokeArr[i]
                    
                    let smoke = obj["answer"] as! String
                    if(smoke == mySmoke){
                        selectedRow = i
                    }else{
                        
                    }
                    
                    
                    
                }
            
               
            }
            
            
            
            
            FirstPopView(hidden : false)
            popTableView.reloadData()
        } else if i == 0 {
             clickButton = "myZipCode"
            hideView.isHidden = false
            print("zipcode button clicked")
            SecondPopView(hidden: false)
            
        } else if i == 5 {
            clickButton = "MyEducation"
            print("my eduaction button clicked")
            let edu:String = themes.geteducationr()!
            
            if(edu == ""){
                
                
                
            }else{
                myEducation = edu
                
                for i in 0...educationArr.count - 1{
                    
                    
                    let obj = educationArr[i]
                    
                    let occupation = obj["answer"] as! String
                    if(occupation == myEducation){
                        selectedRow = i
                    }else{
                        
                    }
                    
                }
                
                
            }
            
            
            
            FirstPopView(hidden : false)
        }
         popTableView.reloadData()
    }
    @objc func profileBtn2Click(button: UIButton) {
        rowsWhichAreChecked.removeAll()
         selectedRow = -1
        hideView.isHidden = false
        selectedRow = -1
        if i == 4 {
            print("my denomination button clicked")
            clickButton = "MyDenomination"
            
            let denom:String = themes.getmTongue()!
            
            if(denom == ""){
                
            }else{

                myDenomination = denom
                
                for i in 0...denominationArr.count - 1{
                    
                    
                    let obj = denominationArr[i]
                    
                    let religion = obj["mtongue_name"] as! String
                    if(religion == myDenomination){
                        selectedRow = i
                    }else{
                        
                    }
                }
            }
            popTableView.reloadData()
            FirstPopView(hidden : false)
        } else if i == 1 {
            print("my drink button clicked")
            clickButton = "MyDrink"
            let drink:String = themes.getdrink()!
            if(drink == ""){
              
                
            }else{
                myDrink = drink
                for i in 0...drinkArr.count - 1{
                    
                    let obj = drinkArr[i]
                    
                    let drink = obj["answer"] as! String
                    if(drink == myDrink){
                        selectedRow = i
                    }else{
                        
                    }
                   
                }
                
                
            }
            FirstPopView(hidden : false)
        }  else if i == 0 {
             clickButton = "myState"
            hideView.isHidden = false
            print("state button clicked")
            ThirdPopView(hidden: false)
    } else if i == 5 {
            clickButton = "MyOcupation"
            print("my ocupation button clicked")
            
            
            let occu:String = themes.getoccupation()!
            
            if(occu == ""){
                
                
                
            }else{
                myOccupation = occu
                
                for i in 0...occupationArray.count - 1{
                    
                    
                    let obj = occupationArray[i]
                    
                    let occupation = obj["answer"] as! String
                    if(occupation == myOccupation){
                        selectedRow = i
                    }else{
                        
                    }
                    
                }
                
                
            }
            
          
            FirstPopView(hidden : false)
    }
         popTableView.reloadData()
    }
    @objc func matchBtn1Click(button: UIButton) {
        rowsWhichAreChecked.removeAll()
        selectedRow = -1
        hideView.isHidden = false
        if i == 4 {
            print("match religion button clicked")
            clickButton = "MatchReligion"
            
            let reli = self.themes.getpartner_religion()
            if(reli == ""){
                
            }else{
                let sayHello = reli
                let result = sayHello!.split(separator: ",") as Array<AnyObject>
                self.multipleSelctedReligionArray = result as! [String] as [AnyObject]
                for i in 0...multipleSelctedReligionArray.count - 1{
                    let obj = multipleSelctedReligionArray[i] as! String
                    
                    for j in 0...religionArr.count - 1{
                        
                        let obj2 = religionArr[j]
                        
                        let name = obj2["religion_name"] as! String
                        
                        if(name == obj){
                            let indexPath = NSIndexPath(row: j, section: 0)
                            
                            rowsWhichAreChecked.append(indexPath)
                        }else{
                            
                            
                            
                        }
                        
                    }
                    
                }
                print(result)
            }
            popTableView.reloadData()
            FirstPopView(hidden : false)
        } else if i == 1 {
            print("match smoke button clicked")
            clickButton = "MatchSmoke"
                        let matchSmoke = self.themes.getpartner_smoke()
                        if(matchSmoke == ""){
            
                        }else{
                            let sayHello = matchSmoke
                            let result = sayHello!.split(separator: ",") as Array<AnyObject>
                            self.multipleSelctedsmokeArray = result as! [String] as [AnyObject]
                            
                            for i in 0...multipleSelctedsmokeArray.count - 1{
                                let obj = multipleSelctedsmokeArray[i] as! String
                                for j in 0...smokeArr.count - 1{
                                    let obj2 = smokeArr[j]
                                    let name = obj2["answer"] as! String
                                    if(name == obj){
                                        let indexPath = NSIndexPath(row: j, section: 0)
                                        rowsWhichAreChecked.append(indexPath)
                                    }else{
                                    }
                                }
                            }
                            print(result)
                        }
              popTableView.reloadData()
            FirstPopView(hidden : false)
        }  else if i == 0 {
            clickButton = "MatchDistance"
          let dist = themes.getpartner_match_distance()
          if(dist == ""){

          }else{
           
              let sayHello = dist
                               
                           
                             for i in 0...distanceArr.count-1{
                                 let obj = distanceArr[i]
                             
                                 if(sayHello == obj){
                                     selectedRow = i
                                     
                                 }
                             }
          }
            print("match distance button clicked")
            FirstPopView(hidden : false)
            popTableView.reloadData()
        } else if i == 5 {
            print("match education button clicked")
            clickButton = "MatchEducation"
            let reli = self.themes.getpartner_education()
            if(reli == ""){
            }else{
                let sayHello = reli
                let result = sayHello!.split(separator: ",") as Array<AnyObject>
                self.multipleSelctedEducationArray = result as! [String] as [AnyObject]
                for i in 0...multipleSelctedEducationArray.count - 1{
                    let obj = multipleSelctedEducationArray[i] as! String
                    for j in 0...educationArr.count - 1{
                        let obj2 = educationArr[j]
                        let name = obj2["answer"] as! String
                        if(name == obj){
                            let indexPath = NSIndexPath(row: j, section: 0)
                            rowsWhichAreChecked.append(indexPath)
                        }else{
                            
                        }
                    }
                }
                print(result)
            }
            popTableView.reloadData()
            FirstPopView(hidden : false)
        }
         popTableView.reloadData()
    }
    @objc func myEthnictyBtnClick(button: UIButton) {
        selectedRow = -1
        rowsWhichAreChecked.removeAll()
        clickButton = "MyEthinicity"
        let ethinicity:String = themes.getEthinicity()!
        
        if(ethinicity == ""){
            
            
            
        }else{
            myEthinicity = ethinicity
            
            for i in 0...ethnicityArr.count - 1{
                
                
                let obj = ethnicityArr[i]
                
                let ethini = obj["answer"] as! String
                if(ethini == myEthinicity){
                    selectedRow = i
                }else{
                    
                }

            }

        }
       
        FirstPopView(hidden : false)
        popTableView.reloadData()
        
        
         hideView.isHidden = false
        print("my ethnicity button clicked")
       
        
    }
    @objc func matchEtnicityBtnClick(button: UIButton) {
        selectedRow = -1
        rowsWhichAreChecked.removeAll()
        clickButton = "MatchEthinicity"
        print("match ethnicity button clicked")
        hideView.isHidden = false
        let matchEthinicity = self.themes.getpartner_ethnicity()
        if(matchEthinicity == ""){
            
        }else{
            let sayHello = matchEthinicity
            let result = sayHello!.split(separator: ",") as Array<AnyObject>
            self.multipleSelctedEthinicityArray = result as! [String] as [AnyObject]
            for i in 0...multipleSelctedEthinicityArray.count - 1{
                let obj = multipleSelctedEthinicityArray[i] as! String
                for j in 0...ethnicityArr.count - 1{
                    let obj2 = ethnicityArr[j]
                    let name = obj2["answer"] as! String
                    if(name == obj){
                        let indexPath = NSIndexPath(row: j, section: 0)
                        rowsWhichAreChecked.append(indexPath)
                    }else{
                        
                        
                    }
                }
            }
            print(result)
        }
        popTableView.reloadData()
        FirstPopView(hidden : false)
    }
    @objc func matchBtn2Click(button: UIButton) {
        hideView.isHidden = false
        rowsWhichAreChecked.removeAll()
          selectedRow = -1
        if i == 4 {
            print("match denomination button clicked")
            clickButton = "MatchDenomination"
            let reli = self.themes.getpartner_mTongue()
            if(reli == ""){
                
            }else{
                let sayHello = reli
                let result = sayHello!.split(separator: ",") as Array<AnyObject>
                self.multipleSelctedDenominationArray = result as! [String] as [AnyObject]
                for i in 0...multipleSelctedDenominationArray.count - 1{
                    let obj = multipleSelctedDenominationArray[i] as! String
                    
                    for j in 0...denominationArr.count - 1{
                        
                        let obj2 = denominationArr[j]
                        
                        let name = obj2["mtongue_name"] as! String
                        
                        if(name == obj){
                            let indexPath = NSIndexPath(row: j, section: 0)
                            
                            rowsWhichAreChecked.append(indexPath)
                        }else{
                            
                            
                            
                        }
                        
                    }
                    
                    
                }

                print(result)
                
            }
            
            
            
            popTableView.reloadData()
            
            FirstPopView(hidden : false)
        } else if i == 1 {
            print("match drink button clicked")
            clickButton = "MatchDrink"
            
            
            let matchDrint = self.themes.getpartner_drink()
            if(matchDrint == ""){
                
            }else{
                let sayHello = matchDrint
                let result = sayHello!.split(separator: ",") as Array<AnyObject>
                self.multipleSelctedDrinkArray = result as! [String] as [AnyObject]
                
                
                for i in 0...multipleSelctedDrinkArray.count - 1{
                    let obj = multipleSelctedDrinkArray[i] as! String
                    
                    for j in 0...drinkArr.count - 1{
                        
                        let obj2 = drinkArr[j]
                        
                        let name = obj2["answer"] as! String
                        
                        if(name == obj){
                            let indexPath = NSIndexPath(row: j, section: 0)
                            
                            rowsWhichAreChecked.append(indexPath)
                        }else{
                            
                            
                            
                        }
                        
                    }
                    
                    
                }
              
                print(result)
                
            }
             popTableView.reloadData()
            FirstPopView(hidden : false)
        }  else if i == 0 {
            
            
            let matchImpDistance = self.themes.getpartner_match_search()
            if(matchImpDistance == ""){

            }else{
                let sayHello = matchImpDistance!.prefix(3)
             
              
                for i in 0...distanceImpArr.count-1{
                    
                    let obj = distanceImpArr[i]
                    
                    let sayHello1 = obj.prefix(3)
                    
                    
                    
                    
                    if(sayHello == sayHello1){
                        selectedRow = i
                        
                    }
                    
                    
                    
                    
                    
                    
                }
                



            }
          
         
            clickButton = "MatchDistanceImp"
            print("match DistanceImp button clicked")
            FirstPopView(hidden : false)
           // popTableView.reloadData()
        } else if i == 5 {
            print("match ocupation button clicked")
            clickButton = "MatchOcupation"
            let reli = self.themes.getpartner_occupation()
            if(reli == ""){
                
            }else{
                let sayHello = reli
                let result = sayHello!.split(separator: ",") as Array<AnyObject>
                self.occupationSelection = result as! [String]
                
                
                for i in 0...occupationSelection.count - 1{
                    let obj = occupationSelection[i]
                    
                    for j in 0...occupationArray.count - 1{
                        
                        let obj2 = occupationArray[j]
                        
                        let name = obj2["answer"] as! String
                        
                        if(name == obj){
                            let indexPath = NSIndexPath(row: j, section: 0)
                            
                            rowsWhichAreChecked.append(indexPath)
                        }else{
                            
                            
                            
                        }
                        
                    }
                    
                    
                }
                
                
                
                
                
                
                print(result)
                
            }
            
            
            
            popTableView.reloadData()
            
            FirstPopView(hidden : false)
        }
         popTableView.reloadData()
    }
    
    func FirstPopView(hidden : Bool){
        firstPopView.frame = CGRect(x: 15, y: 120, width: (UIScreen.main.bounds.width)-30, height: 350)
        firstPopView.alpha = 1
        firstPopView.isHidden = hidden
        self.view.addSubview(firstPopView)
    }
    func SecondPopView(hidden : Bool){
        zipcodeView.frame = CGRect(x: 15, y: 120, width: (UIScreen.main.bounds.width)-30, height: 200)
        zipcodeView.alpha = 1
        zipcodeView.isHidden = hidden
        self.view.addSubview(zipcodeView)
    }
    func ThirdPopView(hidden : Bool){
        stateView.frame = CGRect(x: 15, y: 120, width: (UIScreen.main.bounds.width)-30, height: 380)
        stateView.alpha = 1
        stateView.isHidden = hidden
        self.view.addSubview(stateView)
    }
    func FourthPopView(hidden : Bool){
        ageView.center = self.view.frame.center
//        ageView.frame = CGRect(x: 15, y: 120, width: (UIScreen.main.bounds.width)-30, height: 350)
        ageView.alpha = 1
        ageView.isHidden = hidden
        self.view.addSubview(ageView)
    }
    
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        if textField == countrySearchField {
            filter_customersArr.removeAll()
            filter_customersArr = states.filter{ $0.localizedCaseInsensitiveContains(countrySearchField.text!) }
            if filter_customersArr.count == 0 || countrySearchField.text == ""{
                filter_customersArr = states
            }
            else {

            }
            DispatchQueue.main.async {
                self.countryTableView.reloadData()
            }
            countryTableView.reloadData()
        }else if(textField == zipcodeField){
            let maxLength = 5
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
    
    @IBAction func popUpSaveAction(_ sender: Any) {
        if(clickButton == "MyReligion"){
            
            if(myReligionId == ""){
                 self.themes.showAlert(title: "Alert ☹️", message: "Please select religion", sender: self)
            }else{
               // getDenomination(id: myReligionId)
                matchPreferanceUpadate(zipcode: "",state: "",city: "",ethnicity: "",religion: myReligion,denomination: "",education: "",income: "",smoke: "",drink: "",passionate: "",leisure_time: "",occupation: "",partner_age_min: "",partner_age_max: "",partner_match_distance: "",partner_ethnicity: "",partner_denomination: "",partner_education: "",partner_smoke: "",partner_religion: "",partner_drink: "", partner_occupation: "", country: "", matchSearch: "")
            }
        }else if(clickButton == "MatchReligion"){
            if(multipleSelctedReligionArray.count == 0){
            }else{
                var religionString = String()
                for i in 0...multipleSelctedReligionArray.count-1{
                    
                    
                    religionString = religionString + "," + (multipleSelctedReligionArray[i] as! String)
                   
                }
                religionString.remove(at: religionString.startIndex)
                
                getMultipleDenomination(id: religionString)
                
                
                    matchPreferanceUpadate(zipcode: "",state: "",city: "",ethnicity: "",religion: "",denomination: "",education: "",income: "",smoke: "",drink: "",passionate: "",leisure_time: "",occupation: "",partner_age_min: "",partner_age_max: "",partner_match_distance: "",partner_ethnicity: "",partner_denomination: "",partner_education: "",partner_smoke: "",partner_religion: religionString,partner_drink: "", partner_occupation: "", country: "", matchSearch: "")
                // let stringRepresentation = multipleSelctedReligionArray.
            }
           
        }else if(clickButton == "MyDenomination"){
            if(myDenomination == ""){
                self.themes.showAlert(title: "Alert ☹️", message: "Please select religion", sender: self)
            }else{
              
                matchPreferanceUpadate(zipcode: "",state: "",city: "",ethnicity: "",religion: "",denomination: myDenomination,education: "",income: "",smoke: "",drink: "",passionate: "",leisure_time: "",occupation: "",partner_age_min: "",partner_age_max: "",partner_match_distance: "",partner_ethnicity: "",partner_denomination: "",partner_education: "",partner_smoke: "",partner_religion: "",partner_drink: "", partner_occupation: "", country: "", matchSearch: "")
            }
        }else if(clickButton == "MatchDenomination"){
            if(multipleSelctedDenominationArray.count == 0){
            }else{
                var religionString = String()
                for i in 0...multipleSelctedDenominationArray.count-1{
                    
                    
                    religionString = religionString + "," + (multipleSelctedDenominationArray[i] as! String)
                    
                }
                religionString.remove(at: religionString.startIndex)
                matchPreferanceUpadate(zipcode: "",state: "",city: "",ethnicity: "",religion: "",denomination: "",education: "",income: "",smoke: "",drink: "",passionate: "",leisure_time: "",occupation: "",partner_age_min: "",partner_age_max: "",partner_match_distance: "",partner_ethnicity: "",partner_denomination: religionString,partner_education: "",partner_smoke: "",partner_religion: "",partner_drink: "", partner_occupation: "", country: "", matchSearch: "")
                // let stringRepresentation = multipleSelctedReligionArray.
            }
            
        }else if(clickButton == "MyEducation"){
            if(myEducation == ""){
            }else{
                matchPreferanceUpadate(zipcode: "",state: "",city: "",ethnicity: "",religion: "",denomination: "",education: myEducation,income: "",smoke: "",drink: "",passionate: "",leisure_time: "",occupation: "",partner_age_min: "",partner_age_max: "",partner_match_distance: "",partner_ethnicity: "",partner_denomination: "",partner_education: "",partner_smoke: "",partner_religion: "",partner_drink: "", partner_occupation: "", country: "", matchSearch: "")
                // let stringRepresentation = multipleSelctedReligionArray.
            }
            
        }else if(clickButton == "MatchEducation"){
            if(multipleSelctedEducationArray.count == 0){
            }else{
                var religionString = String()
                for i in 0...multipleSelctedEducationArray.count-1{
                    
                    
                    religionString = religionString + "," + (multipleSelctedEducationArray[i] as! String)
                    
                }
                religionString.remove(at: religionString.startIndex)
                matchPreferanceUpadate(zipcode: "",state: "",city: "",ethnicity: "",religion: "",denomination: "",education: "",income: "",smoke: "",drink: "",passionate: "",leisure_time: "",occupation: "",partner_age_min: "",partner_age_max: "",partner_match_distance: "",partner_ethnicity: "",partner_denomination: "",partner_education: religionString,partner_smoke: "",partner_religion: "",partner_drink: "", partner_occupation: "", country: "", matchSearch: "")
                // let stringRepresentation = multipleSelctedReligionArray.
            }
            
        }else if(clickButton == "MyEthinicity"){
            if(myEthinicity == ""){
            }else{
                matchPreferanceUpadate(zipcode: "",state: "",city: "",ethnicity: myEthinicity,religion: "",denomination: "",education:"",income: "",smoke: "",drink: "",passionate: "",leisure_time: "",occupation: "",partner_age_min: "",partner_age_max: "",partner_match_distance: "",partner_ethnicity: "",partner_denomination: "",partner_education: "",partner_smoke: "",partner_religion: "",partner_drink: "", partner_occupation: "", country: "", matchSearch: "")
                // let stringRepresentation = multipleSelctedReligionArray.
            }
            
        }else if(clickButton == "MatchEthinicity"){
            if(multipleSelctedEthinicityArray.count == 0){
            }else{
                var religionString = String()
                for i in 0...multipleSelctedEthinicityArray.count-1{
                    
                    
                    religionString = religionString + "," + (multipleSelctedEthinicityArray[i] as! String)
                    
                }
                religionString.remove(at: religionString.startIndex)
                matchPreferanceUpadate(zipcode: "",state: "",city: "",ethnicity: "",religion: "",denomination: "",education: "",income: "",smoke: "",drink: "",passionate: "",leisure_time: "",occupation: "",partner_age_min: "",partner_age_max: "",partner_match_distance: "",partner_ethnicity: religionString,partner_denomination: "",partner_education: "",partner_smoke: "",partner_religion: "",partner_drink: "", partner_occupation: "", country: "", matchSearch: "")
                // let stringRepresentation = multipleSelctedReligionArray.
            }
            
        }else if(clickButton == "MySmoke"){
            if(mySmoke == ""){
            }else{
                matchPreferanceUpadate(zipcode: "",state: "",city: "",ethnicity: "",religion: "",denomination: "",education:"",income: "",smoke: mySmoke,drink: "",passionate: "",leisure_time: "",occupation: "",partner_age_min: "",partner_age_max: "",partner_match_distance: "",partner_ethnicity: "",partner_denomination: "",partner_education: "",partner_smoke: "",partner_religion: "",partner_drink: "", partner_occupation: "", country: "", matchSearch: "")
                // let stringRepresentation = multipleSelctedReligionArray.
            }
            
        }else if(clickButton == "MyDrink"){
            if(myDrink == ""){
            }else{
                matchPreferanceUpadate(zipcode: "",state: "",city: "",ethnicity: "",religion: "",denomination: "",education:"",income: "",smoke: "",drink: myDrink,passionate: "",leisure_time: "",occupation: "",partner_age_min: "",partner_age_max: "",partner_match_distance: "",partner_ethnicity: "",partner_denomination: "",partner_education: "",partner_smoke: "",partner_religion: "",partner_drink: "", partner_occupation: "", country: "", matchSearch: "")
                // let stringRepresentation = multipleSelctedReligionArray.
            }
            
        }else if(clickButton == "MatchSmoke"){
            if(multipleSelctedsmokeArray.count == 0){
            }else{
                var religionString = String()
                for i in 0...multipleSelctedsmokeArray.count-1{
                    
                    
                    religionString = religionString + "," + (multipleSelctedsmokeArray[i] as! String)
                    
                }
                religionString.remove(at: religionString.startIndex)
                matchPreferanceUpadate(zipcode: "",state: "",city: "",ethnicity: "",religion: "",denomination: "",education: "",income: "",smoke: "",drink: "",passionate: "",leisure_time: "",occupation: "",partner_age_min: "",partner_age_max: "",partner_match_distance: "",partner_ethnicity: "",partner_denomination: "",partner_education: "",partner_smoke: religionString,partner_religion: "",partner_drink: "", partner_occupation: "", country: "", matchSearch: "")
                // let stringRepresentation = multipleSelctedReligionArray.
            }
            
        }else if(clickButton == "MatchDrink"){
            if(multipleSelctedDrinkArray.count == 0){
            }else{
                var religionString = String()
                for i in 0...multipleSelctedDrinkArray.count-1{
                    
                    
                    religionString = religionString + "," + (multipleSelctedDrinkArray[i] as! String)
                    
                }
                religionString.remove(at: religionString.startIndex)
                matchPreferanceUpadate(zipcode: "",state: "",city: "",ethnicity: "",religion: "",denomination: "",education: "",income: "",smoke: "",drink: "",passionate: "",leisure_time: "",occupation: "",partner_age_min: "",partner_age_max: "",partner_match_distance: "",partner_ethnicity: "",partner_denomination: "",partner_education: "",partner_smoke: "",partner_religion: "",partner_drink: religionString, partner_occupation: "", country: "", matchSearch: "")
                // let stringRepresentation = multipleSelctedReligionArray.
            }
        } else if(clickButton == "MatchDistance"){
            if(matchDist == ""){
            }else{
                
                matchPreferanceUpadate(zipcode: "",state: "",city: "",ethnicity: "",religion: "",denomination: "",education: "",income: "",smoke: "",drink: "",passionate: "",leisure_time: "",occupation: "",partner_age_min: "",partner_age_max: "",partner_match_distance: matchDist,partner_ethnicity: "",partner_denomination: "",partner_education: "",partner_smoke: "",partner_religion: "",partner_drink: "", partner_occupation: "", country: "", matchSearch: "")
                // let stringRepresentation = multipleSelctedReligionArray.
            }
        }else if(clickButton == "MatchDistanceImp"){
            if(matchImpDist == ""){
            }else{
                
                matchPreferanceUpadate(zipcode: "",state: "",city: "",ethnicity: "",religion: "",denomination: "",education: "",income: "",smoke: "",drink: "",passionate: "",leisure_time: "",occupation: "",partner_age_min: "",partner_age_max: "",partner_match_distance: "",partner_ethnicity: "",partner_denomination: "",partner_education: "",partner_smoke: "",partner_religion: "",partner_drink: "", partner_occupation: "", country: "", matchSearch: matchImpDist)
                // let stringRepresentation = multipleSelctedReligionArray.
            }
          
        }else if(clickButton == "MyOcupation"){
            if(myOccupation == ""){
            }else{
                
                
                matchPreferanceUpadate(zipcode: "",state: "",city: "",ethnicity: "",religion: "",denomination: "",education: "",income: "",smoke: "",drink: "",passionate: "",leisure_time: "",occupation: myOccupation,partner_age_min: "",partner_age_max: "",partner_match_distance: "",partner_ethnicity: "",partner_denomination: "",partner_education: "",partner_smoke: "",partner_religion: "",partner_drink: "", partner_occupation: "", country: "", matchSearch: "")
                // let stringRepresentation = multipleSelctedReligionArray.
            }
            
            
            
        }else if(clickButton == "MatchOcupation"){
            if(occupationSelection.count == 0){
            }else{
                var distanceString = String()
                for i in 0..<occupationSelection.count{
                    
                    
                    distanceString = distanceString + "," + (occupationSelection[i] as! String)
                    
                }
                distanceString.remove(at: distanceString.startIndex)
                matchPreferanceUpadate(zipcode: "",state: "",city: "",ethnicity: "",religion: "",denomination: "",education: "",income: "",smoke: "",drink: "",passionate: "",leisure_time: "",occupation: "",partner_age_min: "",partner_age_max: "",partner_match_distance: "",partner_ethnicity: "",partner_denomination: "",partner_education: "",partner_smoke: "",partner_religion: "",partner_drink: "", partner_occupation: distanceString, country: "", matchSearch: "")
                // let stringRepresentation = multipleSelctedReligionArray.
            }
            
            
            
        }

    }
    
    
    func getReligions(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            let parameters = [:] as [String : Any]
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:getReligion, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    var data1 = response["data"] as! [AnyObject]
                    self.matchReligionArray = data1
                    for i in 0..<data1.count{
                        let obj = data1[i]
                        let religion_name = obj["religion_name"] as! String
                        if(religion_name == "All"){
                            data1.remove(at: i)
                            break
                        }
                    }
                    
                    self.religionArr = data1
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                   
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
//    func getDenomination(id: String){
//        let networkRechability = urlService.connectedToNetwork()
//        if(networkRechability){
//            //            themes.showActivityIndicator(uiView: self.view)
//            var dict = [String : AnyObject]()
//            dict.updateValue(id as AnyObject, forKey: "religion_id")
//            print("log in parameters is \(dict)")
//            urlService.serviceCallPostMethodWithParams(url:getCaste, params: dict as Dictionary<String, Any>) { response in
//                print(response)
//                self.themes.hideActivityIndicator(uiView: self.view)
//                let success = response["status"] as! String
//                if(success == "1"){
//                    self.denominationArr = response["data"] as! [AnyObject]
//                    self.firstPopView.isHidden = true
//                    self.hideView.isHidden = true
//                } else {
//                    let result = response["result"] as! String
//                    self.themes.showAlert(title: "Success", message: result, sender: self)
//                }
//            }
//        } else {
//            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
//        }
//    }
    
    func getMultipleDenomination(id: String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            var dict = [String : AnyObject]()
            dict.updateValue(id as AnyObject, forKey: "religion_id")
            print("log in parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:getMultipleCastes, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    self.denominationArr = response["data"] as! [AnyObject]
                    self.firstPopView.isHidden = true
                    self.hideView.isHidden = true
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
  
    func matchPreferanceUpadate(zipcode: String,state: String,city: String,ethnicity: String,religion: String,denomination: String,education: String,income: String,smoke: String,drink: String,passionate: String,leisure_time: String,occupation: String,partner_age_min: String,partner_age_max: String,partner_match_distance: String,partner_ethnicity: String,partner_denomination: String,partner_education: String,partner_smoke: String,partner_religion: String,partner_drink: String,partner_occupation: String,country:String,matchSearch:String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            var dict = [String : AnyObject]()
            dict.updateValue(country as AnyObject, forKey: "country")
            dict.updateValue(themes.getUserId() as AnyObject, forKey: "user_id")
            dict.updateValue(zipcode as AnyObject, forKey: "zipcode")
            dict.updateValue(state as AnyObject, forKey: "state")
            dict.updateValue(selectedDate as AnyObject, forKey: "dob")
            dict.updateValue(city as AnyObject, forKey: "city")
            dict.updateValue(ethnicity as AnyObject, forKey: "ethnicity")
            dict.updateValue(religion as AnyObject, forKey: "religion")
            dict.updateValue(denomination as AnyObject, forKey: "mtongue_name")
            dict.updateValue(education as AnyObject, forKey: "education")
            dict.updateValue(income as AnyObject, forKey: "income")
            dict.updateValue(smoke as AnyObject, forKey: "smoke")
            dict.updateValue(drink as AnyObject, forKey: "drink")
            dict.updateValue("" as AnyObject, forKey: "height_ft")
            dict.updateValue("" as AnyObject, forKey: "height_cms")
            dict.updateValue("" as AnyObject, forKey: "height_inc")
            dict.updateValue(passionate as AnyObject, forKey: "passionate")
            dict.updateValue(leisure_time as AnyObject, forKey: "leisure_time")
            dict.updateValue("" as AnyObject, forKey: "thankful_1")
            dict.updateValue("" as AnyObject, forKey: "thankful_2")
            dict.updateValue("" as AnyObject, forKey: "thankful_3")
            dict.updateValue(occupation as AnyObject, forKey: "occupation")
            dict.updateValue(partner_age_min as AnyObject, forKey: "partner_age_min")
            dict.updateValue(partner_age_max as AnyObject, forKey: "partner_age_max")
            dict.updateValue(partner_match_distance as AnyObject, forKey: "partner_match_distance")
            dict.updateValue(partner_ethnicity as AnyObject, forKey: "partner_ethnicity")
            dict.updateValue(partner_religion as AnyObject, forKey: "partner_religion")
            dict.updateValue(partner_denomination as AnyObject, forKey: "partner_mtongue_name")
            dict.updateValue(partner_education as AnyObject, forKey: "partner_education")
            dict.updateValue(partner_occupation as AnyObject, forKey: "partner_occupation")
            dict.updateValue(partner_smoke as AnyObject, forKey: "partner_smoke")
            dict.updateValue(partner_drink as AnyObject, forKey: "partner_drink")
            dict.updateValue(matchSearch as AnyObject, forKey: "partner_match_search")
            print("log in parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:updatematchPreference, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    self.firstPopView.isHidden = true
                    self.hideView.isHidden = true
//                    self.themes.showAlert(title: "😁", message: "Updated successfully", sender: self)
                     self.themes.showToast(message: "Updated successfully", sender: self)
                    
                    let userInfo = response["user_info"]as! [String : AnyObject]

                    let id = self.themes.checkNullValue(userInfo["id"])
                    
                    let date_id = self.themes.checkNullValue(userInfo["date_id"])
                    
                    let gender = self.themes.checkNullValue(userInfo["gender"])
                    
                    let lookingfor = self.themes.checkNullValue(userInfo["looking_for"])
                    
                    let firstName = self.themes.checkNullValue(userInfo["first_name"])
                    
                    let email = self.themes.checkNullValue(userInfo["email"])
                    
                    let zipcode = self.themes.checkNullValue(userInfo["zipcode"])
                    
                    let state = self.themes.checkNullValue(userInfo["state"])
                    
                    let referal_from = self.themes.checkNullValue(userInfo["referal_from"])
                    
                    let children = self.themes.checkNullValue(userInfo["children"])
                    
                    let city = self.themes.checkNullValue(userInfo["city"])
                    
                      let country = self.themes.checkNullValue(userInfo["country"])
                    
                    let dob = self.themes.checkNullValue(userInfo["dob"])
                    
                    let ethnicity = self.themes.checkNullValue(userInfo["ethnicity"])
                    
                    let religion = self.themes.checkNullValue(userInfo["religion"])
                    
                    let denomination = self.themes.checkNullValue(userInfo["denomination"])
                    
                    let education = self.themes.checkNullValue(userInfo["education"])
                    
                    let occupation = self.themes.checkNullValue(userInfo["occupation"])
                    
                    let income = self.themes.checkNullValue(userInfo["income"])
                    
                    let smoke = self.themes.checkNullValue(userInfo["smoke"])
                    
                    let drink = self.themes.checkNullValue(userInfo["drink"])
                    
                    let height = self.themes.checkNullValue(userInfo["height"])
                    
                    let passionate = self.themes.checkNullValue(userInfo["passionate"])
                    
                    let leisure_time = self.themes.checkNullValue(userInfo["leisure_time"])
                    
                    let thankful_1 = self.themes.checkNullValue(userInfo["thankful_1"])
                    
                    let thankful_2 = self.themes.checkNullValue(userInfo["thankful_2"])
                    
                    let thankful_3 = self.themes.checkNullValue(userInfo["thankful_3"])
                
                    let partner_age_min = self.themes.checkNullValue(userInfo["partner_age_min"])
                    
                    let partner_age_max = self.themes.checkNullValue(userInfo["partner_age_max"])
                    let partner_match_distance = self.themes.checkNullValue(userInfo["partner_match_distance"])
                    
                    let partner_match_search = self.themes.checkNullValue(userInfo["partner_match_search"])
                    
                    let partner_ethnicity = self.themes.checkNullValue(userInfo["partner_ethnicity"])
                    
                    let partner_religion = self.themes.checkNullValue(userInfo["partner_religion"])
                    
                    let partner_education = self.themes.checkNullValue(userInfo["partner_education"])
                    
                    let partner_occupation = self.themes.checkNullValue(userInfo["partner_occupation"])
                    
                    let partner_smoke = self.themes.checkNullValue(userInfo["partner_smoke"])
                    
                    let partner_denomination = self.themes.checkNullValue(userInfo["partner_denomination"])
                    
                    
                    let mtongue_name = self.themes.checkNullValue(userInfo["mtongue_name"])
                    let partner_mtongue_name = self.themes.checkNullValue(userInfo["partner_mtongue_name"])
                    
                    
                    
                    
                    let partner_drink = self.themes.checkNullValue(userInfo["partner_drink"])
                    
                    let photo1 = self.themes.checkNullValue(userInfo["photo1"])
                    
                    let photo1_approve = self.themes.checkNullValue(userInfo["photo1_approve"])
                    
                    let photo2 = self.themes.checkNullValue(userInfo["photo2"])
                    
                    let photo2_approve = self.themes.checkNullValue(userInfo["photo2_approve"])
                    
                    let photo3 = self.themes.checkNullValue(userInfo["photo3"])
                    UserDefaults.standard.set(self.themes.checkNullValue(userInfo["state"]), forKey: "selectedState")
                    
                    
                    
                    
                    let photo3_approve = self.themes.checkNullValue(userInfo["photo3_approve"])
                    
                    let photo4 = self.themes.checkNullValue(userInfo["photo4"])
                    
                    let photo4_approve = self.themes.checkNullValue(userInfo["photo4_approve"])
                    
                    
                    
                    let photo5 = self.themes.checkNullValue(userInfo["photo5"])
                    
                    let photo5_approve = self.themes.checkNullValue(userInfo["photo5_approve"])
                    
                    let photo6 = self.themes.checkNullValue(userInfo["photo6"])
                    
                    let photo6_aprove = self.themes.checkNullValue(userInfo["photo6_aprove"])
                    
                    let signup_step = self.themes.checkNullValue(userInfo["signup_step"])
                    
                    let status = self.themes.checkNullValue(userInfo["status"])
                    
                    
     
                    self.themes.saveUserID(id as? String)
                    
                    self.themes.savedate_id(date_id as? String)
                    
                    self.themes.saveGender(gender as? String)
                    self.themes.savelooking_for(lookingfor as? String)
                    self.themes.saveFirstName(firstName as? String)
                    self.themes.saveuserEmail(email as? String)
                    self.themes.saveZipcode(zipcode as? String)
                    self.themes.savepartner_mTongue(partner_mtongue_name as? String)
                    self.themes.savemTongue(mtongue_name as? String)
                    self.themes.savestate(state as? String)
                    
                    self.themes.savereferal_from(referal_from as? String)
                    
                    self.themes.saveChildren(children as? String)
                    
                    self.themes.savecity(city as? String)
                    self.themes.savecountry(country as? String)
                    self.themes.savedob(dob as? String)
                    
                    self.themes.saveEthinicity(ethnicity as? String)
                    
                    self.themes.saveReligion(religion as? String)
                    
                    self.themes.savedenomination(denomination as? String)
                    
                    self.themes.saveEducation(education as? String)
                    
                    self.themes.saveoccupation(occupation as? String)
                    
                    self.themes.saveIncome(income as? String)
                    
                    self.themes.savesmoke(smoke as? String)
                    self.themes.savePartnerDenomination(partner_denomination as? String)
                    self.themes.savedrink(drink as? String)
                    
                    self.themes.saveHeight(height as? String)
                    
                    self.themes.savepassionate(passionate as? String)
                    
                    self.themes.saveleisure_time(leisure_time as? String)
                    
                    self.themes.savethankful_1(thankful_1 as? String)
                    
                    self.themes.savethankful_2(thankful_2 as? String)
                    
                    self.themes.savethankful_3(thankful_3 as? String)
                    
                    self.themes.savepartner_age_max(partner_age_max as? String)
                    
                    self.themes.savepartner_age_min(partner_age_min as? String)
                    
                    self.themes.savepartner_match_distance(partner_match_distance as? String)
                   
                    
                    
                    
                    self.themes.savepartner_match_search(partner_match_search as? String)
                    self.themes.savepartner_ethnicity(partner_ethnicity as? String)
                    self.themes.savepartner_religion(partner_religion as? String)
                    self.themes.savepartner_education(partner_education as? String)
                    self.themes.savepartner_occupation(partner_occupation as? String)
                    
                    self.themes.savepartner_smoke(partner_smoke as? String)
                    
                    self.themes.savepartner_drink(partner_drink as? String)
                    
                    self.themes.savephoto1(photo1 as? String)
                    
                    self.themes.savephoto2(photo2 as? String)
                    
                    self.themes.savephoto3(photo3 as? String)
                    
                    self.themes.savephoto4(photo4 as? String)
                    
                    self.themes.savephoto5(photo5 as? String)
                    
                    self.themes.savephoto6(photo6 as? String)
                    
                    self.themes.savephoto1_approve(photo1_approve as? String)
                    
                    self.themes.savephoto2_approve(photo2_approve as? String)
                    
                    self.themes.savephoto3_approve(photo3_approve as? String)
                    self.themes.savephoto4_approve(photo4_approve as? String)
                    self.themes.savephoto5_approve(photo5_approve as? String)
                    self.themes.savephoto6_aprove(photo6_aprove as? String)
                    self.themes.savesignup_step(signup_step as? String)
                    self.themes.savestatus(status as? String)
                    
                    self.matchTableView.reloadData()

                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    @IBAction func countryBtnClick(_ sender: Any) {
        selctHeadingLabel.text = "Selct Your Country"
        selectedHint = 1
        hideView.isHidden = false
        selectStatePop.isHidden = false
        filter_customersArr = countryArray as! [String]
        states = filterCountryArray as! [String]
        countryTableView.reloadData()
        selectedRow2 = -1
        ThirdPopView(hidden : true)
    }
    
    @IBAction func stateBtnClick(_ sender: Any) {
        selectedRow2 = -1
        if(countryField.text == ""){
            themes.showAlert(title: "Alert", message: "Please select country before selecting state", sender: self)
        }else{
            selectedHint = 2
            hideView.isHidden = false
            selectStatePop.isHidden = false
            filter_customersArr = states1
            states = states1
            countryTableView.reloadData()
            selectedRow = -1
            countrySearchField.text = ""
            ThirdPopView(hidden : true)
        }
    }
    @IBAction func countryCloseAction(_ sender: Any) {
       // hideView.isHidden = true
        selectStatePop.isHidden = true
        ThirdPopView(hidden : false)

    }
    @IBAction func countryDoneAction(_ sender: Any) {
        //hideView.isHidden = true
        selectStatePop.isHidden = true
        ThirdPopView(hidden : false)
        if(selectedHint == 1){
            countryField.text = countrySearchField.text
        }else if(selectedHint == 2){
            stateField.text = countrySearchField.text
        }else{
            cityField.text = countrySearchField.text
        }
        
    }
    
    @IBAction func cityClickAction(_ sender: Any) {
        selectedRow2 = -1
        if(stateField.text == ""){
            themes.showAlert(title: "Alert", message: "Please select State before selecting City", sender: self)
        }else{
            selctHeadingLabel.text = "Selct Your City"
            selectedHint = 3
            hideView.isHidden = false
            selectStatePop.isHidden = false
            filter_customersArr = citiesArray as! [String]
            states = filterCityArray as! [String]
//            countryTableView.reloadData()
            selectedRow = -1
            countrySearchField.text = ""
             ThirdPopView(hidden : true)
            countryTableView.reloadData()
        }
    }
    func getCitiesAPI(state:String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            
            var dict = [String : AnyObject]()
            
            
            dict.updateValue(state as AnyObject, forKey: "state_code")
            
            
            //            let parameters = ["gender":gender, "looking_for":lookingFor,"first_name":firstName, "zipcode":zipCode,"state":state,"email":mailField.text!,"password":passwordField.text!] as [String : String]
            
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:getCities, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    let array = response["data"] as! [AnyObject]
                    for i in 0...array.count-1{
                        let obj = array[i]
                        self.citiesArray.append(obj["city_name"] as AnyObject)
                        self.filterCityArray.append(obj["city_name"] as AnyObject)
                    }
                    self.popTableView.reloadData()
                }else if(success == "2"){
                    self.cityBtn.isHidden = true
                }else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    func getMotherToungeApi(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            urlService.serviceCallGetMethod(url: mothertongue, completionHandler: { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    self.denominationArr = response["data"] as! [AnyObject]
                    
                    
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            })
        }else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    
    func dynamicValuesApi(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            let parameters = [:] as [String : Any]
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:dynamicProfile, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    let data = response["data"] as! [AnyObject]
                    let obj = data[0]
                     let obj1 = data[1]
                    let obj2 = data[2]
                    let obj4 = data[4]
                    let obj5 = data[5]
                    self.ethnicityArr = obj["answer"] as! Array
                    self.educationArr = obj1["answer"] as! Array
                    self.smokeArr = obj4["answer"] as! Array
                     self.drinkArr = obj5["answer"] as! Array
                    self.occupationArray = obj2["answer"] as! Array
                //    self.distanceArr =
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    func adsListServiceApi(ScreenType:String){
        self.urlService.adslistServiceCall(ScreenType: ScreenType) { response in
            let success = response["status"] as! String
            if(success == "1"){
                let data = response["data"] as! [AnyObject]
                self.adsListArr = data
                
                self.matchTableView.reloadData()
            }else{
                
            }
        }
    }
    
}
extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}
            

