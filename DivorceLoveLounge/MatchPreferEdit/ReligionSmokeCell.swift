//
//  ReligionSmokeCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 6/24/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class ReligionSmokeCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {

    
    
  var adsListArr = [AnyObject]()
    
    
    @IBOutlet weak var myProfLbl: UILabel!
    @IBOutlet weak var myMatchLbl: UILabel!
    
    @IBOutlet weak var profileLbl1: UILabel!
    @IBOutlet weak var profileLbl2: UILabel!
    @IBOutlet weak var profileBtn1: UIButton!
    @IBOutlet weak var profileBtn2: UIButton!
    @IBOutlet weak var matchLbl1: UILabel!
    @IBOutlet weak var matchDataLbl1: UILabel! 
    @IBOutlet weak var matchDetailLbl1: UILabel!
    @IBOutlet weak var matchBtn1: UIButton!
    @IBOutlet weak var matchLbl2: UILabel!
    @IBOutlet weak var matchDatalbl2: UILabel!
    @IBOutlet weak var matchDetailLbl2: UILabel!
    @IBOutlet weak var matchBtn2: UIButton!    
    @IBOutlet var match1BtnLbl: UILabel!
    @IBOutlet var match2BtnLbl: UILabel!
    
    @IBOutlet var adsBackView: UIView!
    
    @IBOutlet var adsCollectionView: UICollectionView!
    
    @IBOutlet var adsHeightConstant: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        myProfLbl.layer.cornerRadius = 5
        myMatchLbl.layer.cornerRadius = 5
        myProfLbl.layer.masksToBounds = true
        myMatchLbl.layer.masksToBounds = true
        profileBtn1.layer.cornerRadius = 5
        profileBtn1.layer.masksToBounds = true
        profileBtn2.layer.cornerRadius = 5
        profileBtn2.layer.masksToBounds = true
        matchBtn1.layer.cornerRadius = 5
        matchBtn1.layer.masksToBounds = true
        matchBtn2.layer.cornerRadius = 5
        matchBtn2.layer.masksToBounds = true
    
        self.adsCollectionView.delegate = self
        self.adsCollectionView.dataSource = self
        self.adsCollectionView.register(UINib(nibName: "AdsBannerCollecctionCell", bundle: nil), forCellWithReuseIdentifier: "AdsBannerCollecctionCell")
        
        
        if adsListArr.count > 0 {
            setTimer()
        }
        
        
        
        // Initialization code
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setTimer() {
        let _ = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(MemPlanFirstCell.autoScroll), userInfo: nil, repeats: true)
    }
    var x = 0
    @objc func autoScroll() {
        if self.x < self.adsListArr.count {
            let indexPath = IndexPath(item: x, section: 0)
            self.adsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.x = self.x + 1
        }else{
            self.x = 0
            self.adsCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.adsListArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: adsCollectionView.frame.size.width, height: adsCollectionView.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = adsCollectionView.dequeueReusableCell(withReuseIdentifier: "AdsBannerCollecctionCell", for: indexPath) as! AdsBannerCollecctionCell
        let data = self.adsListArr[indexPath.row] as! [String:AnyObject]
        let type = data["add_type"] as! String
        if type == "Custom" {
            cell.adsWebView.isHidden = true
            cell.adsImageView.isHidden = false
            let url = "\(adsUrl)\(data["add_image"] as! String)"
            cell.adsImageView.af_setImage(withURL: URL(string:url)!)
        }else{
            cell.adsWebView.isHidden = false
            cell.adsImageView.isHidden = true
            
        }
        return cell
    }
    
}
