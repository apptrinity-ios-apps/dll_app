//
//  AgeEthncityTableViewCell.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 6/24/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class AgeEthncityTableViewCell: UITableViewCell {

    @IBOutlet weak var headerProfileLbl: UILabel!
    @IBOutlet weak var headerMatchLbl: UILabel!
    @IBOutlet weak var dataBtn: UIButton!
    @IBOutlet weak var profileLbl: UILabel!
    @IBOutlet weak var profileDataLbl: UILabel!
    @IBOutlet weak var matchLbl: UILabel!
    @IBOutlet weak var matchDataLbl: UILabel!
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var popUpBtn: UIButton!
    @IBOutlet var dobBtn: UIButton!
    @IBOutlet var dobTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        headerProfileLbl.layer.cornerRadius = 5
        headerMatchLbl.layer.cornerRadius = 5
        headerProfileLbl.layer.masksToBounds = true
        headerMatchLbl.layer.masksToBounds = true
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
