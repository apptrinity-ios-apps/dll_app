//
//  LocationVC.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 3/14/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class LocationVC: UIViewController {

    @IBOutlet weak var enableBtn: UIButton!
    
    @IBAction func enableAction(_ sender: Any) {
        
        let tabVc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarVC")as! TabBarVC
        self.navigationController?.pushViewController(tabVc, animated: true)        
    }
    
    @IBAction func skipBtn(_ sender: Any) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.enableBtn.layer.cornerRadius = (enableBtn.frame.size.height)/2
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
