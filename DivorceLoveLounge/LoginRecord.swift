//
//  LoginRecord.swift
//  DivorceLoveLounge
//
//  Created by S s Vali on 5/10/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
class LoginRecord: NSObject {
    var childrenStr = ""
    var cityStr = ""
    var languageStr = ""
    var dobStr = ""
    var userEmail = ""
    var ethnicityStr = ""
    var firstName = ""
    var genderStr = ""
    var heightStr = ""
    var idStr = ""
    var incomeStr = ""
    var religionStr = ""
    var zipcodeStr = ""
    
    var userID = ""
    var userImage = ""
    var phNumberStr = ""
    var categoryStr = ""
    var subCategoryStr = ""
    var countryCode = ""
    var xmppKey = ""    
}
