//
//  BlockedUsersVC.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 08/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class BlockedUsersVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    
     var BlockUserArray = [AnyObject]()
     var blockedArray = [AnyObject]()
    var screenFrom = String()

    
    @IBOutlet var blockedCollectionView: UICollectionView!
    
    @IBOutlet weak var headinglabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        blockedCollectionView.delegate = self
        blockedCollectionView.dataSource = self

        self.blockedCollectionView.register(UINib(nibName: "BlockedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BlockedCollectionViewCell")
        
        
        if(screenFrom == "Contact"){
            headinglabel.text = "Contacted Users"
            contactsListAPI()
        }else if(screenFrom == "Block"){
              headinglabel.text = "Blocked Users"
            blockListAPI()
        } else {
            headinglabel.text = "Online"
            ActiveUserListAPI()
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
 
        return self.BlockUserArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BlockedCollectionViewCell", for: indexPath)as! BlockedCollectionViewCell
        
        if (screenFrom == "Contact"){
            cell.blockBtn.setImage(UIImage(named: "block_default100"), for: .normal)
        } else if (screenFrom == "Block"){
            cell.blockBtn.setImage(UIImage(named: "block_active"), for: .normal)
            cell.blockBtn.addTarget(self, action: #selector(UnblockButnClick(_:)), for: .touchUpInside)
            cell.blockBtn.tag = indexPath.item
        } else {
            cell.blockBtn.isHidden = true
        }
       
        
        let userdata = self.BlockUserArray[indexPath.row]
        
        let imageUrl = self.themes.checkNullValue(userdata["photo1"] as AnyObject?) as! String
        
        let photoApproved = self.themes.checkNullValue(userdata["photo1_approve"] as AnyObject?) as! String
        if photoApproved == "APPROVED" {
            
            let fullimageUrl = ImagebaseUrl + imageUrl
//            cell.profPicImg.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "defaultPic"))
            DispatchQueue.main.async {
                cell.userImg.sd_setShowActivityIndicatorView(true)
                cell.userImg.sd_setIndicatorStyle(.whiteLarge)
                cell.userImg.sd_setImage(with: URL(string: fullimageUrl), placeholderImage:UIImage(named: "defaultPic"), completed: { (image, error, cacheType, url) -> Void in
                    if ((error) != nil) {
                        cell.userImg.image = UIImage(named: "defaultPic")
                        cell.userImg.sd_setShowActivityIndicatorView(true)
                    } else {
                        cell.userImg.image = image
                        cell.userImg.sd_setShowActivityIndicatorView(false)
                    }
                })
            }
            
        }else{
            
            cell.userImg.image = UIImage(named: "defaultPic")
//            cell.profPicImg.image = UIImage(named: "defaultPic")
        }
        
        cell.nameLabel.text = self.themes.checkNullValue(userdata["first_name"] as AnyObject?) as? String
        let DOB = self.themes.checkNullValue(userdata["dob"] as AnyObject?) as? String
        if DOB != "" {
            let age = self.themes.getAgeFromDOF(date: DOB!)
            cell.ageLable.text = "\(age.0)"
        }

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        print("screen width is \(screenWidth)")
        if(screenWidth == 375){
            return CGSize(width: 186, height: 186)
        }else if(screenWidth == 414){
            return CGSize(width: 205.5, height: 206)
        }else if(screenWidth == 320) {
            return CGSize(width: 158.5, height: 159)
        }else if(screenWidth == 768) {
            return CGSize(width: 382.5, height: 382)
        }else if(screenWidth == 1024) {
            return CGSize(width: 255, height: 272)
        } else {
            return CGSize(width: 277, height: 277)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        let obj =  self.BlockUserArray[indexPath.row] as! [String : AnyObject]
        let userDetailsVc = self.storyboard?.instantiateViewController(withIdentifier: "UserDetailsVC") as! UserDetailsVC
       userDetailsVc.userDataDict = self.BlockUserArray[indexPath.row] as! [String : AnyObject]
        
        if(screenFrom == "Block"){
            userDetailsVc.fromScreen = "Block"
            userDetailsVc.blockSeleccted = "yes"
        }else{
            
            
            userDetailsVc.fromScreen = "myfavourite"
        }
        
        
       // userDetailsVc.match_id = obj["id"] as! String
        self.navigationController?.pushViewController(userDetailsVc, animated: true)
        
        }
    
    @objc func UnblockButnClick(_ sender : UIButton){
        
        let cell =   blockedCollectionView.cellForItem(at:IndexPath(row: sender.tag, section: 0)) as! BlockedCollectionViewCell
        
        let obj = self.BlockUserArray[sender.tag]
        
        let ProfileName = obj["first_name"] as! String
        let id = obj["id"] as! String
        let alertController = UIAlertController(title: "Alert", message: "Do You Want UnBlock \(ProfileName) ", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            
            let myId = self.themes.getUserId()
            self.blockUnblockApi(user_id: myId, Id: id)
            self.blockedCollectionView.reloadData()
            
        }
        alertController.addAction(OKAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in

        }
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)

    }
    
    func blockUnblockApi(user_id: String, Id : String){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //            themes.showActivityIndicator(uiView: self.view)
            
            let parameters = [ "user_id":user_id, "id":Id,
                ] as [String : Any]
            
            print("favourite parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:blockUnblock, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    
                    if(self.screenFrom == "Contact"){
                       
                        self.contactsListAPI()
                    }else{
                       
                        self.blockListAPI()
                    }
                    
                } else {
                    let result = response["result"] as! String
                    
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    
    func blockListAPI(){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("block user parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:blockList, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
//                    self.data = (response["data"] as AnyObject) as! [AnyObject]
                    self.blockedArray = response["data"]as! [AnyObject]
                   
                    
                    self.BlockUserArray = self.blockedArray
                    self.blockedCollectionView.reloadData()
                    
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    func contactsListAPI(){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("block user parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:contactList, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    self.blockedArray = response["data"]as! [AnyObject]
                    self.BlockUserArray = self.blockedArray
                    self.blockedCollectionView.reloadData()
                    
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    func ActiveUserListAPI(){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let userid = self.themes.getUserId()
            let dict = ["user_id":userid] as [String:Any]
            print("Active users parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:ActiveList, params: dict as Dictionary<String, Any>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){

                    self.blockedArray = response["data"]as! [AnyObject]
                    self.BlockUserArray = self.blockedArray
                    self.blockedCollectionView.reloadData()
                    if self.blockedArray.count == 0 {
                        self.themes.showToast(message: "There is no one Online", sender: self)
                    }
                    
                } else {
                    let result = response["result"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    
    

}
