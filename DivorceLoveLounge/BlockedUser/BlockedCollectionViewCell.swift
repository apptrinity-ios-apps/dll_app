//
//  BlockedCollectionViewCell.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 12/07/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class BlockedCollectionViewCell: UICollectionViewCell {

    @IBOutlet var userImg: UIImageView!
    
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var ageLable: UILabel!
    
    @IBOutlet var genderImgView: UIImageView!
    
    @IBOutlet var blockBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
