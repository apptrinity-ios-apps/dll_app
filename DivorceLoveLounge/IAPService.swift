//
//  IAPService.swift
//  DivorceLoveLounge
//
//  Created by INDOBYTES on 09/01/20.
//  Copyright © 2020 com.Indobytes. All rights reserved.
//

import Foundation
import StoreKit

protocol skpaymentStatusDelegate {
    func skpaymentStatus(status:String,tranactionId:String)
    
   
}



class IAPService: UIViewController{
    
  var themes = Themes()
    
    var delagate:skpaymentStatusDelegate!
    
    
    
    
    
    var products = [SKProduct]()
    let paymentQueue = SKPaymentQueue.default()
    //static let shared = IAPService()
    func getProducts(){
        let products : Set = [IAPProduct.nonRenewingOneMonthSubcription.rawValue,IAPProduct.nonRenewing3MonthSubcription.rawValue,IAPProduct.nonRenewing12MonthSubcription.rawValue,IAPProduct.nonRenewingTestSubcription.rawValue]
        
        let request = SKProductsRequest(productIdentifiers: products)
        request.delegate = self
        request.start()
        paymentQueue.add(self)
    }
    
    func purchase(product : IAPProduct){
        
        guard let productToPurchase = products.filter({ $0.productIdentifier == product.rawValue}).first else{
            return
        }
        let payment = SKPayment(product: productToPurchase)
        paymentQueue.add(payment)
    }
    
    
    
}
extension IAPService: SKProductsRequestDelegate {

    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {

        self.products = response.products
        print(response.products)
        for product in response.products {
            print(product.localizedTitle)
            print(product.localizedTitle)
            print(product.localizedDescription)
            print(product.price)
            
            

        }
    }


}
extension IAPService: SKPaymentTransactionObserver{
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions{
            
            print(transaction.transactionState)
            print(transaction.transactionState.status(), transaction.payment.productIdentifier,transaction.transactionIdentifier,transaction.transactionDate)
            if(transaction.transactionState.status() == "purchased"){
                delagate.skpaymentStatus(status: "purchased", tranactionId:transaction.transactionIdentifier!)
            }else if (transaction.transactionState.status() == "failed"){
                delagate.skpaymentStatus(status: "failed", tranactionId:transaction.transactionIdentifier!)
                print("failed failed")

            }else if (transaction.transactionState.status() == "restored"){
                delagate.skpaymentStatus(status: "restored", tranactionId:transaction.transactionIdentifier!)
            }
            else if (transaction.transactionState.status() == "deferred"){
                delagate.skpaymentStatus(status: "deferred", tranactionId:transaction.transactionIdentifier!)
            }
        }
    }
    
   
    
    
    
    
}


extension SKPaymentTransactionState{
    
    func status() -> String{

        switch self {
        case .deferred:
            return "deferred"
        case .purchasing:
            return "purchasing"
        case .purchased:
            return "purchased"
        case .failed:
            return "failed"
        case .restored:
            return "restored"
        }

    }
    
    
}


