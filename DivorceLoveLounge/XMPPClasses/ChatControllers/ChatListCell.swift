//
//  ChatListCell.swift
//  DivorceLoveLounge
//
//  Created by nagaraj  kumar on 28/11/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit

class ChatListCell: UITableViewCell {

    @IBOutlet var backview: UIView!
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var msgLbl: UILabel!
    @IBOutlet var onlineBG: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        profileImage.layer.cornerRadius = profileImage.frame.size.height / 2
      //  profileImage.layer.borderColor = UIColor.darkGray.cgColor
       // profileImage.layer.borderWidth = 1
        profileImage.clipsToBounds = true
        
        
        onlineBG.layer.cornerRadius = onlineBG.frame.size.height / 2
        onlineBG.layer.borderColor = UIColor.white.cgColor
        onlineBG.layer.borderWidth = 0.5
        onlineBG.clipsToBounds = true
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
