//
//  MyChatViewController.swift
//  DivorceLoveLounge
//
//  Created by nagaraj  kumar on 11/09/19.
//  Copyright © 2019 com.Indobytes. All rights reserved.
//

import UIKit
import MBProgressHUD
import Chatto
import ChattoAdditions


class MyChatViewController:BaseChatViewController, UIGestureRecognizerDelegate, TitleViewModifiable {
    
    let titleProvider: ChatViewControllerTitleProvider
    let messageSender: ChatViewControllerMessageSender
    let additionalActions: [ChatViewControllerAdditionalAction]
    
    // MARK: titleViewModifiable protocol
    var originalTitleViewText: String?
    func resetTitleViewTextToOriginal() {
        self.navigationItem.titleView = UIView()
        self.navigationItem.title = originalTitleViewText
        
    }
    
    init(titleProvider: ChatViewControllerTitleProvider, chatDataSource: ChatDataSourceProtocol, messageSender: ChatViewControllerMessageSender, additionalActions: [ChatViewControllerAdditionalAction]) {
        self.titleProvider = titleProvider
        self.messageSender = messageSender
        self.additionalActions = additionalActions
        super.init(nibName: nil, bundle: nil)
        
        self.chatDataSource = chatDataSource
        self.titleProvider.delegate = self
        updateTitle()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    override func viewDidLoad() {
        
        
        //                let topview = UIView()
        //                topview.frame = CGRect(x: 0, y: 25, width: self.view.frame.width, height: 50)
        //                topview.backgroundColor = UIColor.black
        //
        //
        //                let backbutton = UIButton()
        //                backbutton.frame = CGRect(x: 0, y: self.view.center.y, width: 40, height: 50)
        //                backbutton.setImage(UIImage(named: "back_icon"), for: .normal)
        //                topview.addSubview(backbutton)
        //                backbutton.addTarget(self, action: #selector(MyChatViewController.), for: .touchUpInside)
        //
        //                 self.view.addSubview(topview)
        //
        super.chatItemsDecorator = ChatItemsDemoDecorator()
        //        let rightBarButtonItems = [UIBarButtonItem(title: "Actions", style: .plain, target: self, action: #selector(additionalActionsBarButtonItemTapped(_:)))]
        //        for barButtonItem in rightBarButtonItems {
        //            barButtonItem.tintColor = .mangostaDarkGreen
        //        }
        //        self.navigationItem.rightBarButtonItems = rightBarButtonItems
        //
        //        // FIXME: not complete solution
        //        self.navigationController?.navigationBar.barTintColor = UIColor.lightGray
        
        // MangostaSettings().setNavigationBarColor()
        super.viewDidLoad()
        
        updateTitle()
        
        
        
    }
    
    
    @objc func backaction(sender: UIButton){
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    var chatInputPresenter: BasicChatInputBarPresenter!
    override func createChatInputView() -> UIView {
        let chatInputView = ChatInputBar.loadNib()
        var appearance = ChatInputBarAppearance()
        appearance.sendButtonAppearance.title = NSLocalizedString("Send", comment: "")
        appearance.textInputAppearance.placeholderText = NSLocalizedString("Type a message", comment: "")
        self.chatInputPresenter = BasicChatInputBarPresenter(chatInputBar: chatInputView, chatInputItems: self.createChatInputItems(), chatInputBarAppearance: appearance)
        chatInputView.maxCharactersCount = 1000
        return chatInputView
    }
    
    override func createPresenterBuilders() -> [ChatItemType: [ChatItemPresenterBuilderProtocol]] {
        
        let textMessagePresenter = TextMessagePresenterBuilder(
            viewModelBuilder: TextMessageViewModelDefaultBuilder(),
            interactionHandler: XMPPTextMessageInteractionHandler(contextViewController: self)
        )
        
        let baseMessageStyle = BaseMessageCollectionViewCellDefaultStyle(colors: BaseMessageCollectionViewCellDefaultStyle.Colors(incoming: .mangostaVeryLightGreen, outgoing: .mangostaDarkGreen))
        textMessagePresenter.baseMessageStyle = baseMessageStyle
        textMessagePresenter.textCellStyle = TextMessageCollectionViewCellDefaultStyle(baseStyle: baseMessageStyle)
        
        let photoMessagePresenter = PhotoMessagePresenterBuilder(
            viewModelBuilder: TransferAwarePhotoMessageViewModelDefaultBuilder(),
            interactionHandler: XMPPPhotoMessageInteractionHandler(contextViewController: self)
         )
        photoMessagePresenter.baseCellStyle = baseMessageStyle
        
        return [
            MessageModel.textItemType: [textMessagePresenter],
            MessageModel.photoItemType: [photoMessagePresenter],
            SendingStatusModel.chatItemType: [SendingStatusPresenterBuilder()],
            TimeSeparatorModel.chatItemType: [TimeSeparatorPresenterBuilder()]
        ]
    }
    func createChatInputItems() -> [ChatInputItemProtocol] {
        var items = [ChatInputItemProtocol]()
        items.append(self.createTextInputItem())
        items.append(self.createPhotoInputItem())
        return items
    }
    
    fileprivate func updateTitle() {
        
        if navigationItem.titleView == nil && navigationItem.title == originalTitleViewText {
            navigationItem.title = titleProvider.chatTitle
          }
        originalTitleViewText = titleProvider.chatTitle
        
        
        
        let topview = UIView()
        let width = UIScreen.main.bounds.width
       // let hight = UIScreen.main.bounds.height
       
             //topview.backgroundColor = UIColor.gray
             
             let myFirstButton = UIButton()
            
             myFirstButton.setImage(UIImage(named: "left-arrow (16)"), for: .normal)
             myFirstButton.addTarget(self, action: #selector(pressed), for: .touchUpInside)
             
             let imageview = UIImageView()
             
             imageview.image = UIImage(named: "top_bg")
             
             let lable = UILabel()
             
             let appdelegate = UIApplication.shared.delegate as! AppDelegate
             lable.text =  appdelegate.chatName
             lable.textColor = UIColor.white
             lable.font = UIFont(name: "Poppins-Regular", size: 20)
             
             
             let onlineLbl = UILabel()
             onlineLbl.text =  appdelegate.onlineStr
             onlineLbl.textColor = UIColor.white
             onlineLbl.font = UIFont(name: "Poppins-Regular", size: 13)
        
        if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
            
            case 1792,1920,2208:
                topview.frame = CGRect(x: 0, y: -20, width: width, height: 140)
             myFirstButton.frame = CGRect(x: 0, y: 55, width: 55, height: 55)
            imageview.frame = CGRect(x: 0, y: -20, width: width, height: 140)
            lable.frame = CGRect(x: 60, y: 65, width: 150, height: 30)
            onlineLbl.frame = CGRect(x: 60, y: 85, width: 150, height: 25)

            default:
                 topview.frame = CGRect(x: 0, y: 0, width: width, height: 80)
             myFirstButton.frame = CGRect(x: 0, y: 22, width: 55, height: 55)
            imageview.frame = CGRect(x: 0, y: 0, width: width, height: 80)
            lable.frame = CGRect(x: 60, y: 32, width: 150, height: 30)
            onlineLbl.frame = CGRect(x: 60, y: 65, width: 150, height: 25)
            }
        }
        
        
        topview.addSubview(imageview)
        topview.addSubview(myFirstButton)
        topview.addSubview(lable)
        topview.addSubview(onlineLbl)
        self.view.addSubview(topview)
        
        self.view.backgroundColor = UIColor.white
        //self.view.addSubview(myFirstButton)
        
    }
    
    
    @objc func pressed(sender: UIButton!) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    private func createTextInputItem() -> TextChatInputItem {
        let item = TextChatInputItem()
        item.textInputHandler = { [weak self] text in
            guard let controller = self else { return }
            controller.messageSender.chatViewController(controller, didRequestToSendMessageWithText: text)
        }
        return item
    }
    
    private func createPhotoInputItem() -> PhotosChatInputItem {
        let item = PhotosChatInputItem(presentingController: self)
        item.photoInputHandler = { [weak self] image in
            guard let controller = self else { return }
            controller.messageSender.chatViewController(controller, didRequestToSendMessageWithImage: image)
        }
        return item
    }
    
    override var canBecomeFirstResponder : Bool {
        return true
    }
    
    @IBAction func additionalActionsBarButtonItemTapped(_ sender: UIBarButtonItem) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for additionalAction in additionalActions {
            actionSheet.addAction(UIAlertAction(title: additionalAction.label, style: .default) { _ in
                additionalAction.perform(inContextOf: self)
            })
        }
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(actionSheet, animated: true, completion: nil)
    }
}

protocol ChatViewControllerTitleProvider: class {
    
    var chatTitle: String { get }
    weak var delegate: ChatViewControllerTitleProviderDelegate? { get set }
}

protocol ChatViewControllerTitleProviderDelegate: class {
    
    func chatViewControllerTitleProviderDidChangeTitle(_ titleProvider: ChatViewControllerTitleProvider)
}

protocol ChatViewControllerMessageSender: class {
    
    func chatViewController(_ viewController: MyChatViewController, didRequestToSendMessageWithText messageText: String)
    func chatViewController(_ viewController: MyChatViewController, didRequestToSendMessageWithImage messageImage: UIImage)
}

protocol ChatViewControllerAdditionalAction {
    
    var label: String { get }
    func perform(inContextOf chatViewController: MyChatViewController)
}

extension ChatViewControllerTitleProvider {
    
    weak var delegate: ChatViewControllerTitleProviderDelegate? {
        get { fatalError() } set {}
    }
}

extension MyChatViewController: ChatViewControllerTitleProviderDelegate {
    
    func chatViewControllerTitleProviderDidChangeTitle(_ titleProvider: ChatViewControllerTitleProvider) {
        updateTitle()
    }
}
