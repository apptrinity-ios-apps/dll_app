//
//  ViewController.swift
//  Mangosta
//
//  Created by Tom Ryan on 3/11/16.
//  Copyright © 2016 Inaka. All rights reserved.
//

import UIKit
import XMPPFramework
import MBProgressHUD

class MainViewController: UIViewController, TitleViewModifiable {
    
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    var chatList = [AnyObject]()
    
    @IBOutlet internal var tableView: UITableView!
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>?
    weak var xmppController: XMPPController!
   
    
    var allprofilesdata = [AnyObject]()
    
    
    let sections = ["", ""]
    
    // MARK: titleViewModifiable protocol
    var originalTitleViewText: String? = "Chats"
    func resetTitleViewTextToOriginal() {
        self.navigationItem.titleView = nil
        self.navigationItem.title = originalTitleViewText
    }
    
    override func viewDidLoad() {
        
        
        
       
        
        
        self.xmppController = XMPPController.sharedInstance
        self.xmppController.roomListDelegate = self
        
        let addButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(selectChat(_:)))
        addButton.tintColor = .mangostaDarkGreen
        self.navigationItem.rightBarButtonItems = [addButton]
        
        let meButton = UIBarButtonItem(image: UIImage(named: "Gear"), style: UIBarButtonItemStyle.done, target: self, action: #selector(pushMeViewControler(_:)))
        meButton.tintColor = .mangostaDarkGreen
        self.navigationItem.leftBarButtonItem = meButton
        
        MangostaSettings().setNavigationBarColor()
        
        self.tableView.backgroundColor = UIColor.white// naveen.mangostaLightGreen
        
        
    
        
        
        if AuthenticationModel.load() == nil {
            presentLogInView()
        } else {
            self.setupDataSources()
        }
        
        
        
        self.tableView.register(UINib(nibName: "ChatListCell", bundle: nil), forCellReuseIdentifier: "ChatListCell")
        
        
        super.viewDidLoad()
    }
    
    
    
    func configureAndStartStream() {
        self.xmppController = XMPPController.sharedInstance
        self.xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
        // TODO: revert to UIActivityIndicatorView.
        //  let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        //  hud?.labelText = "Please wait"
        _ = self.xmppController.connect()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        let chatStatus = self.themes.getChatstatus()
        let chatMessage = self.themes.getChatstatusMessage()

               
               if chatStatus == "0"{
                self.themes.showAlert(title: "Alert", message: chatMessage!, sender: self)
                
                
                  
                
                
                
               }else if chatStatus == "1"{
        
        let usenameStr =  self.themes.checkNullValue(self.themes.getXmppUserName()) as! String
        let xmppPassword = self.themes.checkNullValue(self.themes.getxmppPassword()) as! String

              let servername = UITextField()
                servername.text = "divorcelovelounge.com"
              let password = UITextField()
                password.text = (xmppPassword as! String)
              let username = UITextField()
                username.text = "\(usenameStr)@divorcelovelounge.com"
            if let serverText = servername.text, (servername.text?.characters.count)! > 0 {
                    let auth = AuthenticationModel(jidString: username.text!, serverName: servername.text!, password: password.text!)
                    auth.save()
                } else {
                    let auth = AuthenticationModel(jidString: username.text!, password: password.text!)
                    auth.save()
                }
         self.configureAndStartStream()

         getChatListApi()
        if self.xmppController.xmppStream.isAuthenticated() {
        self.resetTitleViewTextToOriginal()
        }
        else {
            let titleLabel = UILabel()
            titleLabel.text = "Connecting"
            self.navigationItem.titleView = titleLabel
            titleLabel.sizeToFit()
        }
        if let selectedRowIndexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: selectedRowIndexPath, animated: animated)
        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
          self.allprofilesdata = appDelegate.allprofiles
        }
        
        
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let chatStatus = self.themes.getChatstatus()
               if chatStatus == "0"{
                   self.themes.showAlert(title: "Alert", message: "These Feature is Not Available", sender: self)
               }else if chatStatus == "1"{
                          try! self.fetchedResultsController?.performFetch()
                          super.viewDidAppear(animated)
        }
    }
    
    func presentLogInView() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.present(loginController, animated: true, completion: nil)
    }
    
    func switchToPrivateChat(with user: XMPPUser, userInitiated isUserInitiated: Bool) {
        
        
        
        
        if !isUserInitiated, let chatIndex = (fetchedResultsController?.fetchedObjects?.index { $0 === user }) {
            tableView.selectRow(at: IndexPath(row: chatIndex, section: 1), animated: false, scrollPosition: .none)
        }
        
        let chatViewController = MyChatViewController(
        
       
            
            
            
            titleProvider: XMPPOneToOneChatTitleProvider(user: user),
            chatDataSource: XMPPCoreDataChatDataSource(xmppController: xmppController, configuration: .privateChat, jid: user.jid().bare()),
            messageSender: xmppController.xmppOneToOneChat.session(forUserJID: user.jid()?.bare()),
            additionalActions: [XMPPOneToOneChatMessageHistoryFetchAction(xmppController: xmppController, userJid: user.jid().bare())]
        )
        
        
        
        switchToChat(with: chatViewController, animated: isUserInitiated)
    }
    
    func switchToGroupChat(in room: XMPPRoomLight, userInitiated isUserInitiated: Bool) {
        
        
        if !isUserInitiated, let roomIndex = xmppController.roomsLight.index(of: room) {
            tableView.selectRow(at: IndexPath(row: roomIndex, section: 0), animated: false, scrollPosition: .none)
        }
        
        let chatViewController = MyChatViewController(
            titleProvider: XMPPRoomLightChatTitleProvider(room: room),
            chatDataSource: XMPPCoreDataChatDataSource(xmppController: xmppController, configuration: .groupChat, jid: room.roomJID),
            messageSender: room as! ChatViewControllerMessageSender,
            additionalActions: [
                XMPPRoomChatMessageHistoryFetchAction(xmppController: xmppController, roomJid: room.roomJID),
                XMPPRoomMemberInviteAction(room: room),
                XMPPRoomMembersListManageAction(room: room),
                XMPPRoomNameChangeAction(room: room)
            ]
        )
        
        switchToChat(with: chatViewController, animated: isUserInitiated)
        
        // TODO: handle a scenario where the user is removed from the room while the chat screen is presented
    }
    
    func switchToChat(with chatViewController: MyChatViewController, animated: Bool) {
        //  _ = self.navigationController?.popToViewController(self, animated: false)
        
        self.navigationController?.pushViewController(chatViewController, animated: true)
        
        
        
        
        //  navigationController?.pushViewController(chatViewController, animated: animated)
    }
    
    @objc func pushMeViewControler(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Me", bundle: nil)
        let meController = storyboard.instantiateViewController(withIdentifier: "MeViewController") as! Me
        meController.xmppController = self.xmppController
        self.navigationController?.pushViewController(meController, animated: true)
    }
    
    @objc func selectChat(_ sender: UIBarButtonItem) {
        
        
        let alertController = UIAlertController(title: nil, message: "New Chat", preferredStyle: .actionSheet)
        alertController.view.tintColor = .mangostaDarkGreen
        let roomChatAction = UIAlertAction(title: "New Room Chat", style: .default) { (action) in
            let storyboard = UIStoryboard(name: "MUCLight", bundle: nil)
            let roomCreatePresenterViewController = storyboard.instantiateViewController(withIdentifier: "MUCLightCreateRoomPresenterViewController") as! UINavigationController
            let roomCreateViewController = roomCreatePresenterViewController.topViewController as! MUCRoomCreateViewController
            roomCreateViewController.delegate = self
            self.present(roomCreatePresenterViewController, animated: true, completion: nil)
        }
        alertController.addAction(roomChatAction)
        
        let privateChatAction = UIAlertAction(title: "New Private Chat", style: .default) { (action) in
            self.createNewFriendChat(sender)
        }
        alertController.addAction(privateChatAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { (action) in
            
        }
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true) {
            
        }
    }
    
    
    func createNewFriendChat(_ sender: UIBarButtonItem) {
        
        
        let alertController = UIAlertController.textFieldAlertController("New Conversation", message: "Enter the JID of the user or group name") { (jidString) in
            guard let userJIDString = jidString,
                let userJID = XMPPJID.init(string: userJIDString) else {
                    return
                    
            }
            self.xmppController?.xmppRoster.addUser(userJID, withNickname: nil)
        }
        self.present(alertController, animated: true, completion: nil)
        
        
        
        
    }
    
    internal func setupDataSources() {
        
        let rosterContext = self.xmppController.managedObjectContext_roster()
        
        let entity = NSEntityDescription.entity(forEntityName: "XMPPUserCoreDataStorageObject", in: rosterContext)
        let sd1 = NSSortDescriptor(key: "sectionNum", ascending: true)
        let sd2 = NSSortDescriptor(key: "displayName", ascending: true)
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        fetchRequest.entity = entity
        fetchRequest.sortDescriptors = [sd1, sd2]
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: rosterContext, sectionNameKeyPath: "sectionNum", cacheName: nil)
        self.fetchedResultsController?.delegate = self
        
        self.tableView.reloadData()
    }
}

extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard section < self.sections.count else {
            return 0
        }
        switch section {
        case 0:
            return self.xmppController?.roomsLight.count ?? 0
            
        case 1:
            guard let controller = self.fetchedResultsController else {
                return 0
            }
            return controller.sections?.first?.numberOfObjects ?? 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView : UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sections[section]
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as? UITableViewHeaderFooterView
        header?.tintColor = UIColor.white
        header?.textLabel?.textColor = UIColor.black
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 67
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListCell") as! ChatListCell
        
        if indexPath.section == 1 {
            let privateChatsIndexPath = IndexPath(row: indexPath.row, section: 0)
            if let user = self.fetchedResultsController?.object(at: privateChatsIndexPath) as? XMPPUserCoreDataStorageObject {
                if let firstResource = user.resources.first as? XMPPResource {
                    if let pres = firstResource.presence() {
                        if pres.type() == "available" {
                            cell.onlineBG.backgroundColor = UIColor.mangostaDarkGreen
                        } else {
                            cell.onlineBG.backgroundColor = UIColor.darkGray
                        }
                    }
                } else {
                    cell.nameLbl.textColor = UIColor.black
                }
                let word = user.jidStr
                if let index = word!.range(of: "@")?.lowerBound {
                    let substring = word![..<index]
                    let string = String(substring)
                   cell.nameLbl.text = string
                    print(string)
                }
                cell.msgLbl.text = ""
                
                for data in self.allprofilesdata {
                    
                    let name = themes.checkNullValue(data["username"] as AnyObject) as! String
                    
                    if(name == ""){
                        
                    }else{
                        let username = data["username"] as! String
                        if  username.lowercased() == cell.nameLbl.text?.lowercased() {
                                                let profile = data["photo1"] as! String
                                               let PhotoFullUrl = ImagebaseUrl + profile
                                               cell.profileImage.sd_setImage(with: URL(string: PhotoFullUrl), placeholderImage: UIImage(named: "defaultPic"))
                                               return cell
                                           }
                    }
                    
                   
                   
                }
                
            } else {
                cell.nameLbl.text = "nope"
            }
        }
        else if indexPath.section == 0 {
            if self.xmppController.roomsLight.count > 0 {
                let room = self.xmppController.roomsLight[indexPath.row]
                cell.nameLbl.text = room.roomname()
            }
            else {
                cell.nameLbl.text = "No rooms"
            }
        }
        
    
        
//        cell.backgroundColor = UIColor.white
//        cell.textLabel?.textColor = UIColor.black
//        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 15.0)
//        cell.detailTextLabel?.textColor = UIColor.black
        
        return cell
        
        
        
        
        
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as UITableViewCell!
//
//        
//        if indexPath.section == 1 {
//            let privateChatsIndexPath = IndexPath(row: indexPath.row, section: 0)
//            if let user = self.fetchedResultsController?.object(at: privateChatsIndexPath) as? XMPPUserCoreDataStorageObject {
//                if let firstResource = user.resources.first as? XMPPResource {
//                    if let pres = firstResource.presence() {
//                        if pres.type() == "available" {
//                            cell?.textLabel?.textColor = UIColor.blue
//                        } else {
//                            cell?.textLabel?.textColor = UIColor.darkGray
//                        }
//                    }
//                } else {
//                    cell?.textLabel?.textColor = UIColor.darkGray
//                }
//
//                cell?.textLabel?.text = user.jidStr
//            } else {
//                cell?.textLabel?.text = "nope"
//            }
//        }
//        else if indexPath.section == 0 {
//            if self.xmppController.roomsLight.count > 0 {
//                let room = self.xmppController.roomsLight[indexPath.row]
//                cell?.textLabel?.text = room.roomname()
//            }
//            else {
//                cell?.textLabel?.text = "No rooms"
//            }
//        }
//
//        cell?.backgroundColor = UIColor.white
//        cell?.textLabel?.textColor = UIColor.black
//        cell?.textLabel?.font = UIFont.boldSystemFont(ofSize: 15.0)
//        cell?.detailTextLabel?.textColor = UIColor.black
//
//        return cell!
//
//
//
        
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            switchToGroupChat(in: xmppController.roomsLight[indexPath.row], userInitiated: true)
        case 1:
            print(fetchedResultsController!.fetchedObjects![indexPath.row] as! XMPPUser)
            
            let privateChatsIndexPath = IndexPath(row: indexPath.row, section: 0)
            if let user = self.fetchedResultsController?.object(at: privateChatsIndexPath) as? XMPPUserCoreDataStorageObject {
            let appdelagate = UIApplication.shared.delegate as! AppDelegate
            let word = user.jidStr
            if let index = word!.range(of: "@")?.lowerBound {
                let substring = word![..<index]
                let string = String(substring)
                appdelagate.chatName = string
                
                if let firstResource = user.resources.first as? XMPPResource {
                    if let pres = firstResource.presence() {
                        if pres.type() == "available" {
                            appdelagate.onlineStr = "Online"
                        } else {
                            appdelagate.onlineStr = "Offline"
                        }
                    }
                }
                
             }
            
            }
            
            switchToPrivateChat(with: fetchedResultsController!.fetchedObjects![indexPath.row] as! XMPPUser, userInitiated: true)
        default:
            break
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        var leaveArray : [UITableViewRowAction] = []
        if indexPath.section  == 0 {
            
            let leave = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Leave") { (UITableViewRowAction, IndexPath) in
                
                self.xmppController.roomsLight[indexPath.row].leave()
            }
            leave.backgroundColor = UIColor.orange
            
            leaveArray.append(leave)
        }
            
        else if indexPath.section == 1 {
            
            
            let privateChatsIndexPath = IndexPath(row: indexPath.row, section: 0)
            let delete = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Delete") { (UITableViewRowAction, IndexPath) in
                let user = self.fetchedResultsController?.object(at: privateChatsIndexPath) as! XMPPUser
                self.xmppController.xmppRoster.removeUser(user.jid())
            }
            delete.backgroundColor = UIColor.red
            leaveArray.append(delete)
        }
        
        return leaveArray
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // 3.17.42.133
    
    func getChatListApi(){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
             self.themes.showActivityIndicator(uiView: self.view)
            let urserId = themes.getXmppUserName() as! String
            var dict = [String : String]()
            dict.updateValue(urserId , forKey: "username")
            print("registration parameters is \(dict)")
            urlService.serviceCallPostMethodWithParams(url:getChatList, params: dict as Dictionary<String, AnyObject>) { response in
                print(response)
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    self.chatList = response["xmpplist"]as! [AnyObject]
                    DispatchQueue.main.async {
                    for Array in self.chatList{
                        let userdata = Array as! [String:AnyObject]
                        let name = userdata["jid"] as! String
                        let user_JID = name // "\(name)@divorcelovelounge.com"
                        let JID = XMPPJID.init(string: user_JID)
                        self.xmppController?.xmppRoster.addUser(JID, withNickname: name)
                       self.tableView.reloadData()
                    }
                        if self.chatList.count <= 0 {
                        self.themes.showAlert(title: "ALERT", message: "No data is available ", sender: self)
                        }
                    }
                      self.themes.hideActivityIndicator(uiView: self.view)
                } else {
                    let result = response["result"] as! String
                   // self.themes.showAlert(title: "ALERT", message: result, sender: self)
                }
            }
        } else {
            self.themes.hideActivityIndicator(uiView: self.view)
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
}

extension MainViewController: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        DispatchQueue.main.async {
             self.tableView.reloadData()
        }
       
    }
}

extension MainViewController: XMPPControllerRoomListDelegate {
    
    func roomListDidChange(in controller: XMPPController) {
        OperationQueue.main.addOperation {
            self.tableView.reloadData()
        }
    }
}

extension MainViewController: MUCRoomCreateViewControllerDelegate {
    
    func createRoom(_ roomName: String, users: [XMPPJID]?) {
        xmppController.addRoom(withName: roomName, initialOccupantJids: users)
        dismiss(animated: true, completion: nil)
    }
}

private extension XMPPCoreDataChatDataSource {
    
    enum Configuration {
        case privateChat, groupChat
    }
    
    convenience init(xmppController: XMPPController, configuration: Configuration, jid: XMPPJID) {
        let baseMessageModelProvider = XMPPCoreDataChatBaseMessageModelProvider(xmppRetransmission: xmppController.xmppRetransmission)
        let textMessageModelProvider = XMPPCoreDataChatDataSourceTextMessageModelProvider(
            baseProvider: baseMessageModelProvider,
            xmppRoster: xmppController.xmppRoster,
            xmppStream: xmppController.xmppStream,
            correctionRecipientJid: jid
        )
        let photoMessageModelProvider = XMPPCoreDataChatDataSourcePhotoMessageModelProvider(
            baseProvider: baseMessageModelProvider,
            xmppOutOfBandMessaging: xmppController.xmppOutOfBandMessaging,
            xmppOutOfBandMessagingStorage: xmppController.xmppOutOfBandMessagingStorage,
            remotePartyJid: jid
        )
        let chatItemBuilders: [XMPPCoreDataChatDataSourceItemBuilder] = [textMessageModelProvider, photoMessageModelProvider]
        let chatItemEventSources: [XMPPCoreDataChatDataSourceItemEventSource] = [baseMessageModelProvider, photoMessageModelProvider]
        
        switch configuration {
        case .privateChat:
            self.init(
                fetchedResultsController: XMPPMessageArchiving_Message_CoreDataObject.chatDataSourceFetchedResultsController(with: xmppController.xmppMessageArchivingStorage.mainThreadManagedObjectContext, userJid: jid),
                chatItemBuilders: chatItemBuilders, chatItemEventSources: chatItemEventSources
            )
            
        case .groupChat:
            self.init(
                fetchedResultsController: XMPPRoomLightMessageCoreDataStorageObject.chatDataSourceFetchedResultsController(with: xmppController.xmppRoomLightCoreDataStorage.mainThreadManagedObjectContext, roomJid: jid),
                chatItemBuilders: chatItemBuilders, chatItemEventSources: chatItemEventSources
            )
        }
    }
}
